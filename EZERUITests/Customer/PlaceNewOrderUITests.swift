//
//  PlaceNewOrderUITests.swift
//  EZERUITests
//
//  Created by Mac mini on 10/01/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import XCTest

class PlaceNewOrderUITests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        let app = XCUIApplication()
        app.launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        addUIInterruptionMonitor(withDescription: "System Dialog") {
            (alert) -> Bool in
            let notifPermission = "Would Like to Send You Notifications"
            if (alert.label.range(of: notifPermission) != nil) {
                alert.buttons["Allow"].tap()
                app.swipeUp()
                return true
            }
            let locPermission = "access your location?"
            if (alert.label.range(of: locPermission) != nil) {
                alert.buttons.element(boundBy: 1).tap()
                app.swipeUp()
                return true
            }
            let notiPermissionAlert = "Notification Alert"
            if (alert.label.range(of: notiPermissionAlert) != nil) {
                alert.buttons["Cancel"].tap()
                app.swipeUp()
                return true
            }
            
            let okButton = alert.buttons["OK"]
            if okButton.exists {
                okButton.tap()
            }
            app.swipeUp()
            return true
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    func testCustomerLoginUIFlow() {
        let app = XCUIApplication()
        app.buttons["CUSTOMER"].tap()
        sleep(2)
        app.swipeDown()
        testLoginCredentials(app, isDeepTest: false)
        sleep(4)
                
    }
    
    // test new order flow
    func testNowOrderFlow() {
        let app = XCUIApplication()
        testAddAddress(app)
        app/*@START_MENU_TOKEN@*/.buttons["Get Going"]/*[[".scrollViews.buttons[\"Get Going\"]",".buttons[\"Get Going\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        testAddDescription(app)
        app.alerts["Ezer"].buttons["OK"].tap()
        app.buttons["Done"].tap()
        app.scrollViews.otherElements.containing(.button, identifier:"SUBMIT ORDER").children(matching: .other).element(boundBy: 3).tap()
        testReviewOrder(app)
    }
    
    // test schedule order flow
    func testScheduleOrderFlow() {
        let app = XCUIApplication()
        testAddAddress(app)
        app/*@START_MENU_TOKEN@*/.buttons["Later"]/*[[".scrollViews.buttons[\"Later\"]",".buttons[\"Later\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.datePickers.pickerWheels["Today"].press(forDuration: 0.6)
        app.buttons["Done"].tap()
        app/*@START_MENU_TOKEN@*/.buttons["Get Going"]/*[[".scrollViews.buttons[\"Get Going\"]",".buttons[\"Get Going\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        testAddDescription(app)
        testReviewOrder(app)
        testLogoutCustomer(app)
    }
    
    private func testReviewOrder(_ app: XCUIApplication) {
        let elementsQuery = app.scrollViews.otherElements
        let notesTextView = elementsQuery.textViews.containing(.staticText, identifier:"Additional Notes to Driver").element
        notesTextView.tap()
        notesTextView.typeText("Additional notes text demo")
        app.toolbars["Toolbar"].buttons["Done"].tap()
        elementsQuery.buttons["SUBMIT ORDER"].tap()
        sleep(2)
        app.buttons["YES"].tap()
        elementsQuery.buttons["TRACK ORDER"].tap()
        sleep(2)
        app.navigationBars["Order Confirmed"].buttons["ic back"].tap()
    }
    
    private func testAddAddress(_ app: XCUIApplication) {
        let scrollViewsQuery = app.scrollViews
        scrollViewsQuery.otherElements.containing(.textField, identifier:"Pick Up Location").buttons["ic navigation"].tap()
        
        let confirmLocationButton = app.buttons["CONFIRM LOCATION"]
        confirmLocationButton.tap()
        let b1txtFld = scrollViewsQuery.children(matching: .other).element(boundBy: 1).textFields["Business Name"]
        b1txtFld.tap()
        b1txtFld.typeText("Business test")
        let unitTextField = scrollViewsQuery.children(matching: .other).element(boundBy: 2).textFields["Unit #"]
        unitTextField.tap()
        unitTextField.typeText("unit123")
        app.toolbars["Toolbar"].buttons["Done"].tap()
        scrollViewsQuery.otherElements.containing(.textField, identifier:"Drop Off Location").buttons["ic navigation"].tap()
        sleep(2)
        app.swipeUp()
        app.tap()
        sleep(2)
        confirmLocationButton.tap()
        let b2txtFld = scrollViewsQuery.children(matching: .other).element(boundBy: 4).textFields["Business Name"]
        b2txtFld.tap()
        b2txtFld.typeText("Business test2")
        
        let unitTextField2 = scrollViewsQuery.children(matching: .other).element(boundBy: 5).textFields["Unit #"]
        unitTextField2.tap()
        unitTextField2.typeText("unit123")
        app.toolbars["Toolbar"].buttons["Done"].tap()
    }
    
    private func testAddDescription(_ app: XCUIApplication){
        let elementsQuery = app.scrollViews.otherElements
        let descriptionTextField = elementsQuery.textViews.containing(.staticText, identifier:"Enter Description / Instructions").element
        descriptionTextField.tap()
        descriptionTextField.typeText("Description text demo")
        
        let totalEstimatedWeightLbsTextField = elementsQuery.textFields["Total estimated weight (lbs)"]
        totalEstimatedWeightLbsTextField.tap()
        totalEstimatedWeightLbsTextField.typeText("12")
        
        let doneButton = app.toolbars["Toolbar"].buttons["Done"]
        doneButton.tap()
        
        elementsQuery.buttons["ic camera"].tap()
        sleep(2)
        app.sheets["Select Mode"].buttons["Image Gallery"].tap()
        sleep(2)
        if #available(iOS 13, *) {
            app.tables.element(boundBy: 1).cells.element(boundBy: 0).tap()
        } else {
            app.tables.cells.element(boundBy: 0).tap()
        }
        sleep(2)
        let collectionView = app.collectionViews["PhotosGridView"]
        collectionView.cells.element(boundBy: collectionView.cells.count - 1).tap()
        sleep(5)
        elementsQuery.buttons["NEXT"].tap()
    }
    
    func testLoginCredentials(_ app: XCUIApplication, isDeepTest: Bool = true) {
        //given
        let emailTextField = app.textFields["Email"]
        let passwordTextField = app.secureTextFields["password"]
        let loginButton = app.buttons["LOGIN"]
        
        //when
        emailTextField.tap()
        emailTextField.typeText("k1@tr.com")
        if isDeepTest{
            //Negative Test Case
            passwordTextField.tap()
            passwordTextField.typeText("wrongpassword")
            app.toolbars["Toolbar"].buttons["Done"].tap()
            
            //then
            loginButton.tap()
            XCTAssert(app.staticTexts["Invalid Password"].waitForExistence(timeout: 5), "Invalid Password test performed failed")
        }
        //Positive Test Case
        passwordTextField.tap()
        passwordTextField.press(forDuration: 1.2)
        passwordTextField.typeText("123456") //correct password
        app.toolbars["Toolbar"].buttons["Done"].tap()
        
        //then
        loginButton.tap()
        XCTAssert(app.navigationBars["New Order"].waitForExistence(timeout: 5), "Customer login and landed to new order screen failed")
    }
    
    private func testLogoutCustomer(_ app: XCUIApplication) {
        //given
        app.navigationBars["New Order"].buttons["ic menu"].tap()
        //when
        app.tables/*@START_MENU_TOKEN@*/.cells.staticTexts["Logout"]/*[[".cells.staticTexts[\"Logout\"]",".staticTexts[\"Logout\"]"],[[[-1,1],[-1,0]]],[1]]@END_MENU_TOKEN@*/.tap()
        //then
        XCTAssert(app.staticTexts["ON DEMAND\nPICK UPS & DELIVERIES"].waitForExistence(timeout: 5), "Customer logout failed")
    }
    
}
