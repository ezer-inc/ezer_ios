//
//  CustomerSideBarUITests.swift
//  EZERUITests
//
//  Created by Mac mini on 11/02/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import XCTest

class CustomerSideBarUITests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        let app = XCUIApplication()
        app.launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        addUIInterruptionMonitor(withDescription: "System Dialog") {
            (alert) -> Bool in
            let notifPermission = "Would Like to Send You Notifications"
            if (alert.label.range(of: notifPermission) != nil) {
                alert.buttons["Allow"].tap()
                app.swipeUp()
                return true
            }
            let locPermission = "access your location?"
            if (alert.label.range(of: locPermission) != nil) {
                alert.buttons.element(boundBy: 1).tap()
                app.swipeUp()
                return true
            }
            let notiPermissionAlert = "Notification Alert"
            if (alert.label.range(of: notiPermissionAlert) != nil) {
                alert.buttons["Cancel"].tap()
                app.swipeUp()
                return true
            }
            
            let okButton = alert.buttons["OK"]
            if okButton.exists {
                okButton.tap()
            }
            app.swipeUp()
            return true
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    func testCustomerLoginUIFlow() {
        let app = XCUIApplication()
        app.buttons["CUSTOMER"].tap()
        sleep(2)
        app.swipeDown()
        testLoginCredentials(app, isDeepTest: false)
        sleep(4)
    }
    
    // test side bar payment flow
    func testSideBarPaymentFlow() {
        let app = XCUIApplication()
        let newOrderNavigationBar = app.navigationBars["New Order"]
        newOrderNavigationBar.buttons["ic menu"].tap()
        app.tables/*@START_MENU_TOKEN@*/.staticTexts["Payment"]/*[[".cells.staticTexts[\"Payment\"]",".staticTexts[\"Payment\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        sleep(2)
        app.swipeUp()
        app.tables.buttons["+  Add new payment method"].tap()
        XCTAssert(app.staticTexts["Payment Options"].waitForExistence(timeout: 5), "Payment options not found")
        app.buttons["ic close"].tap()
        app.navigationBars["Payment"].buttons["New Order"].tap()
        XCTAssert(app.navigationBars["New Order"].waitForExistence(timeout: 5), "Customer landed to new order screen failed")
    }

    func testSideBarOrderHistoryFlow() {
        let app = XCUIApplication()
        app.navigationBars["New Order"].buttons["ic menu"].tap()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Order History"]/*[[".cells.staticTexts[\"Order History\"]",".staticTexts[\"Order History\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        sleep(1)
        app.swipeUp()
        sleep(1)
        app.buttons["LOAD MORE"].tap()
        sleep(1)
        app.swipeUp()
        sleep(1)
        app.navigationBars["Order History"].buttons["New Order"].tap()
        XCTAssert(app.navigationBars["New Order"].waitForExistence(timeout: 5), "Customer landed to new order screen failed")
        
    }
    
    func testSideBarSettingFlow() {
        let app = XCUIApplication()
        app.navigationBars["New Order"].buttons["ic menu"].tap()
        let tablesQuery = app.tables
        tablesQuery.staticTexts["Settings"].tap()
        let firstNameTextField = app.textFields["First Name"]
        firstNameTextField.tap()
        firstNameTextField.tap()
        firstNameTextField.tap()
        firstNameTextField.typeText("Alexa")
        app.toolbars["Toolbar"].buttons["Done"].tap()
        
        app.buttons["SAVE DETAILS"].tap()
        XCTAssert(app.staticTexts["Customer updated successfully"].waitForExistence(timeout: 5), "Customer updated screen failed to update details")
        sleep(1)
        app.navigationBars["Settings"].buttons["New Order"].tap()
        XCTAssert(app.navigationBars["New Order"].waitForExistence(timeout: 5), "Customer landed to new order screen failed")
    }
        
        
        
        
    func testLoginCredentials(_ app: XCUIApplication, isDeepTest: Bool = true) {
        //given
        let emailTextField = app.textFields["Email"]
        let passwordTextField = app.secureTextFields["password"]
        let loginButton = app.buttons["LOGIN"]
        
        //when
        emailTextField.tap()
        emailTextField.typeText("k1@tr.com")
        if isDeepTest{
            //Negative Test Case
            passwordTextField.tap()
            passwordTextField.typeText("wrongpassword")
            app.toolbars["Toolbar"].buttons["Done"].tap()
            
            //then
            loginButton.tap()
            XCTAssert(app.staticTexts["Invalid Password"].waitForExistence(timeout: 5), "Invalid Password test performed failed")
        }
        //Positive Test Case
        passwordTextField.tap()
        passwordTextField.press(forDuration: 1.2)
        passwordTextField.typeText("123456") //correct password
        app.toolbars["Toolbar"].buttons["Done"].tap()
        
        //then
        loginButton.tap()
        XCTAssert(app.navigationBars["New Order"].waitForExistence(timeout: 5), "Customer login and landed to new order screen failed")
    }
    
    private func testLogoutCustomer(_ app: XCUIApplication) {
        //given
        app.navigationBars["New Order"].buttons["ic menu"].tap()
        //when
        app.tables/*@START_MENU_TOKEN@*/.cells.staticTexts["Logout"]/*[[".cells.staticTexts[\"Logout\"]",".staticTexts[\"Logout\"]"],[[[-1,1],[-1,0]]],[1]]@END_MENU_TOKEN@*/.tap()
        //then
        XCTAssert(app.staticTexts["ON DEMAND\nPICK UPS & DELIVERIES"].waitForExistence(timeout: 5), "Customer logout failed")
    }
    
}
