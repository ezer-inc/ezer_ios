//
//  DriverLoginUITests.swift
//  EZERUITests
//
//  Created by Mac mini on 08/01/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import XCTest

class DriverLoginUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        let app = XCUIApplication()
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        addUIInterruptionMonitor(withDescription: "System Dialog") {
            (alert) -> Bool in
            let notifPermission = "Would Like to Send You Notifications"
            if (alert.label.range(of: notifPermission) != nil) {
                alert.buttons["Allow"].tap()
                app.swipeUp()
                return true
            }
            let locPermission = "access your location?"
            if (alert.label.range(of: locPermission) != nil) {
                alert.buttons.element(boundBy: 1).tap()
                app.swipeUp()
                return true
            }
            let notiPermissionAlert = "Notification Alert"
            if (alert.label.range(of: notiPermissionAlert) != nil) {
                alert.buttons["Cancel"].tap()
                app.swipeUp()
                return true
            }
            
            let okButton = alert.buttons["OK"]
            if okButton.exists {
                okButton.tap()
            }
            app.swipeUp()
            return true
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDriverLoginFlowUI() {
        let app = XCUIApplication()
        app.buttons["DRIVER"].tap()
        sleep(2)
        app.swipeDown()
        testForTemrsOfServices(app)
        testForgetpassword(app)
        testLoginCredentials(app)
//        testLogoutDriver(app)
        sleep(2)
    }
    
    // test term of services page
    private func testForTemrsOfServices(_ app: XCUIApplication) {
        //when
        app.buttons["Terms of Services - Privacy"].tap()
        
        //then
        XCTAssert(app.webViews.otherElements["Terms of Service"].staticTexts["Terms of Service"].waitForExistence(timeout: 15), "Terms of Service found successfully")
        app.navigationBars["Terms And Privacy"].buttons["Back"].tap()
    }
    
    // test forget password
    private func testForgetpassword(_ app: XCUIApplication) {
        //given
        app.buttons["Forgot Password?"].tap()
        let emailTextField = app.textFields["Email"]
        
        //when
        emailTextField.tap()
        emailTextField.typeText("k3@tr.com")
        app.toolbars["Toolbar"].buttons["Done"].tap()
        app.buttons["RESET PASSWORD"].tap()
        
        //then
        let ezerAlert = app.alerts["Ezer"]
        XCTAssert(ezerAlert.staticTexts["Reset password link send on your email address"].waitForExistence(timeout: 5), "Forget Password test performed failed")
        //        ezerAlert.buttons["OK"].tap() Ok handled already by UIInterruptionMonitor
    }
    
    // test cutomer credentials
    private func testLoginCredentials(_ app: XCUIApplication) {
        //given
        let emailTextField = app.textFields["Email"]
        let passwordTextField = app.secureTextFields["password"]
        let loginButton = app.buttons["LOGIN"]
        
        //Negative Test Case
        //when
        emailTextField.tap()
        emailTextField.typeText("k3@tr.com")
        passwordTextField.tap()
        passwordTextField.typeText("wrongpassword")
        app.toolbars["Toolbar"].buttons["Done"].tap()
        
        //then
        loginButton.tap()
        XCTAssert(app.staticTexts["Invalid password"].waitForExistence(timeout: 5), "Invalid Password test performed failed")
        
        //Positive Test Case
        //when
        passwordTextField.tap()
        passwordTextField.press(forDuration: 1.2)
        passwordTextField.typeText("123456") //correct password
        app.toolbars["Toolbar"].buttons["Done"].tap()
        
        //then
        loginButton.tap()
    }
    
    private func testLogoutDriver(_ app: XCUIApplication) {
        //given
        app.navigationBars["New Order"].buttons["ic menu"].tap()
        //when
        app.tables/*@START_MENU_TOKEN@*/.cells.staticTexts["Logout"]/*[[".cells.staticTexts[\"Logout\"]",".staticTexts[\"Logout\"]"],[[[-1,1],[-1,0]]],[1]]@END_MENU_TOKEN@*/.tap()
        //then
        XCTAssert(app.staticTexts["ON DEMAND\nPICK UPS & DELIVERIES"].waitForExistence(timeout: 5), "Customer logout failed")
    }
}

