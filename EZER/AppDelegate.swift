//
//  AppDelegate.swift
//  EZER
//
//  Created by TimerackMac1 on 01/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import UserNotifications
import Braintree
import Alamofire
import Firebase
import Reachability
import ChatSDK
import ChatProvidersSDK
import SwiftyJSON
import SDWebImage
import MapKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var viewForInternetReachebilityMessage = UIView()
    let reachability = try! Reachability()
    
    let ExtraStartGroup = DispatchGroup() // initialize
    let ExtraDestinationGroup = DispatchGroup() // initialize
    let WorkOrderGroup = DispatchGroup() // initialize
    var isCurrentOrderSync = false
    var isInternetGone = false
    var locManager = CLLocationManager()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UINavigationBar.appearance().shadowImage = UIImage()
        Chat.initialize(accountKey: IdentifierName.zendeskAccKey, queue: .main) //  zenddesk chat accountt
        Logger.isEnabled = true
        Logger.defaultLevel = .verbose
        
        //UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        GMSPlacesClient.provideAPIKey(IdentifierName.GooglePlaceApiKey)
        GMSServices.provideAPIKey(IdentifierName.GooglePlaceApiKey)
        IQKeyboardManager.shared.enable = true
        //IQKeyboardManager.shared.canAdjustAdditionalSafeAreaInsets = true
        registerForPushNotifications()
        BTAppSwitch.setReturnURLScheme(IdentifierName.ReverseURL)
        decideViewController()
        DataRequest.addAcceptableImageContentTypes(["image/jpg","image/*"])
        FirebaseApp.configure()
        reachability.whenReachable = { reachability in
            self.reachabilityChanged(obj: reachability)
        }
        reachability.whenUnreachable = { _ in
            self.reachabilityChanged(obj: nil)
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        return true
    }    
    func reachabilityChanged(obj: Reachability?) {
        if let reachability = obj, reachability.connection != .unavailable  {
            refreshHomeControllersWhenInternetReached()
            if isInternetGone == true {
                let userId = HelperFunction.getUserId()
                if !userId.isEmpty
                {
                    SocketManager.sharedInstance.startSocket()
                    isInternetGone = false
                }
            }
           
            if reachability.connection == .wifi{
                print("Reachable via WiFi")
                addViewOnWindowForInternetConnection(isAdd: false)
            } else {
                print("Reachable via Cellular")
                addViewOnWindowForInternetConnection(isAdd: false)
            }
            getOrderStatusIfDriverAlreadyProcessed()
        } else {
            print("Network not reachable")
            addViewOnWindowForInternetConnection(isAdd: true)
            isInternetGone = true
            
        }
    }
    
    func refreshHomeControllersWhenInternetUnReached() {
        if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType){
            if let rootViewController = self.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController {
                SocketManager.sharedInstance.refershUser()
                if (nvc.viewControllers.last is DriverHomeController){
                    (nvc.viewControllers.last as! DriverHomeController).checkCurrentOrderFromDB()
                }
            }
        }
    }
    func refreshHomeControllersWhenInternetReached() {
        if LoginType.customer.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType)
        {
            if let rootViewController = self.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController {
                SocketManager.sharedInstance.refershUser()
                if (nvc.viewControllers.last is CustomerHome){
                    (nvc.viewControllers.last as! CustomerHome).getCurrentOrder()
                }
            }
        }else if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType){
            if let rootViewController = self.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController {
                SocketManager.sharedInstance.refershUser()
                if (nvc.viewControllers.last is DriverHomeController){
                    (nvc.viewControllers.last as! DriverHomeController).viewWillAppear(true)
                }
                CDMyJobsManager.shared.get(completionHandler: { (result) in
                    switch result {
                    case .success(let data):
                        if data.count != 0 {
                            if data.firstIndex(where: {$0.pendingOrderStatus.count != 0}) != nil {
                                nvc.viewControllers.last?.showProgressBarWithText()
                            }
                            else {
                                if (nvc.viewControllers.last is ScheduledJobsController){
                                    (nvc.viewControllers.last as! ScheduledJobsController).refreshData(notification: nil, showLoader: true)
                                }
                            }
                        }
                        else {
                            if (nvc.viewControllers.last is ScheduledJobsController){
                                (nvc.viewControllers.last as! ScheduledJobsController).refreshData(notification: nil, showLoader: true)
                            }
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                })
            }
        }
    }
    
    func getOrderStatusIfDriverAlreadyProcessed() {
        if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType){
            CDMyJobsManager.shared.get(completionHandler: { (result) in
                switch result {
                case .success(let data):
                    var isNotImageLoading = false
                    if data.count != 0 {
                        for currentOrder in data {
                            if currentOrder.pendingOrderStatus.count != 0 {
                                self.isCurrentOrderSync = true
                                if currentOrder.beforeLoading.count != 0 {
                                    isNotImageLoading = true
                                    self.UploadBeforeLoadingImages(currentOrder)
                                }
                            }
                        }
                        
                        if (!isNotImageLoading) {
                            //Update Work Order Status when in onmyWay pickup and Im Here at pickup location
                            self.updateWorkOrderStatusAfterOffline()
                        }
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            })
        }
    }
    
    func UploadImagesToServer(_ cdImages: [CDImage], _ completion:@escaping (JSON?) -> Void) {
        BaseServices.uploadImagesToServer(images: cdImages, imageType: ImageType.order.rawValue, role: Roles.client.rawValue) { (json) in
            if let json = json
            {
                print(json)
                completion(json)
            }
            else {
                completion(nil)
            }
        }
    }
    
    //MARK:- Update Images after uploaded in Server and get url and update in DB after driver come online based on orderStatus
    func UpdateImagesInDBWithStatus(_ currentOrder: DriverAvailableJobs,  orderStatus: OrderStatusType, json: JSON, cdImages: [CDImage]){
        if let index = currentOrder.pendingOrderStatus.firstIndex(where: {$0.orderStatus == orderStatus}) {
            let orderStatus = currentOrder.pendingOrderStatus[index]
            self.uploadImagesHandleResponse(json, cdImages: cdImages, orderStatus: orderStatus)
        }
        else {
            self.uploadImagesHandleResponse(json, cdImages: cdImages)
        }
    }
    
    //MARK:- Update Images after uploaded in Server and get url and update in DB after driver come online based on orderStatus and multiOrderStatus
    func UpdateImagesInDBWithMultStatus(_ currentOrder: DriverAvailableJobs,  orderStatus: OrderStatusType, multiWOStatus: OrderStatusType, stopPoint: Double, json: JSON, cdImages: [CDImage]){
        if let index = currentOrder.pendingOrderStatus.firstIndex(where: {$0.orderStatus == orderStatus && $0.multiWOStatus == multiWOStatus && $0.stopPoint == stopPoint}) {
            let orderStatus = currentOrder.pendingOrderStatus[index]
            self.uploadImagesHandleResponse(json, cdImages: cdImages, orderStatus: orderStatus)
        }
        else {
            self.uploadImagesHandleResponse(json, cdImages: cdImages)
        }
    }
    
    func UploadBeforeLoadingImages(_ currentOrder : DriverAvailableJobs)
    {
        self.UploadImagesToServer(currentOrder.beforeLoading) { (json) in
            if let json = json
            {
                print(json)
                self.UpdateImagesInDBWithStatus(currentOrder, orderStatus: OrderStatusType.startLoading, json: json, cdImages: currentOrder.beforeLoading)
                
                if currentOrder.afterLoading.count != 0 {
                    self.UploadAfterLoadingImages(currentOrder)
                }
                else {
                    self.updateWorkOrderStatusAfterOffline()
                }
            }
        }
    }
    
    func UploadAfterLoadingImages(_ currentOrder : DriverAvailableJobs)
    {
        self.UploadImagesToServer(currentOrder.afterLoading) { (json) in
            if let json = json
            {
                print(json)
                self.UpdateImagesInDBWithStatus(currentOrder, orderStatus: OrderStatusType.loaded, json: json, cdImages: currentOrder.afterLoading)
                
                self.HandleExtraStartLocations(currentOrder)
            }
        }
    }
    
    func HandleExtraStartLocations(_ currentOrder : DriverAvailableJobs) {
        if currentOrder.extraStartLocations.count != 0 {
            for (startLocationIndex, extraStartLocation) in currentOrder.extraStartLocations.enumerated() {
                if extraStartLocation.beforeLoading.count != 0 {
                    self.ExtraStartGroup.enter()
                    self.UploadImagesToServer(extraStartLocation.beforeLoading) { (json) in
                        if let json = json
                        {
                            print(json)
                            self.UpdateImagesInDBWithMultStatus(currentOrder, orderStatus: OrderStatusType.multi_pickup_transit, multiWOStatus: OrderStatusType.startLoading, stopPoint: Double(startLocationIndex), json: json, cdImages: extraStartLocation.beforeLoading)
                        
                            if extraStartLocation.afterLoading.count != 0 {
                                self.UploadImagesToServer(extraStartLocation.afterLoading) { (json) in
                                    if let json = json
                                    {
                                        print(json)
                                        self.ExtraStartGroup.leave()
                                        self.UpdateImagesInDBWithMultStatus(currentOrder, orderStatus: OrderStatusType.multi_pickup_transit, multiWOStatus: OrderStatusType.loaded, stopPoint: Double(startLocationIndex), json: json, cdImages: extraStartLocation.afterLoading)
                                    }
                                }
                            }
                            else{
                                self.ExtraStartGroup.leave()
                            }
                        }
                    }
                }
                else{
                    // All requests completed
                    if currentOrder.beforeUnLoading.count != 0 {
                        self.UploadBeforeUnLoadingImages(currentOrder)
                    }
                    else{
                        self.updateWorkOrderStatusAfterOffline()
                    }
                }
            }
            // Configure a completion callback
            self.ExtraStartGroup.notify(queue: .main) {
                // All requests completed
                if currentOrder.beforeUnLoading.count != 0 {
                    self.UploadBeforeUnLoadingImages(currentOrder)
                }
                else{
                    self.updateWorkOrderStatusAfterOffline()
                }
            }
        }
        else{
            // All requests completed
            if currentOrder.beforeUnLoading.count != 0 {
                self.UploadBeforeUnLoadingImages(currentOrder)
            }
            else{
                self.updateWorkOrderStatusAfterOffline()
            }
        }
    }
    
    func UploadBeforeUnLoadingImages(_ currentOrder : DriverAvailableJobs) {
        self.UploadImagesToServer(currentOrder.beforeUnLoading) { (json) in
            if let json = json
            {
                print(json)
                self.UpdateImagesInDBWithStatus(currentOrder, orderStatus: OrderStatusType.beforeUnload, json: json, cdImages: currentOrder.beforeUnLoading)

                if currentOrder.afterUnLoading.count != 0 {
                    self.UploadAfterUnLoadingImages(currentOrder)
                }
                else {
                    self.updateWorkOrderStatusAfterOffline()
                }
            }
        }
    }
    
    func UploadAfterUnLoadingImages(_ currentOrder : DriverAvailableJobs) {
        self.UploadImagesToServer(currentOrder.afterUnLoading) { (json) in
            if let json = json
            {
                print(json)
                self.ExtraDestinationGroup.enter()
                self.UpdateImagesInDBWithStatus(currentOrder, orderStatus: OrderStatusType.unloaded, json: json, cdImages: currentOrder.afterUnLoading)
                
                if currentOrder.podImages.count != 0 {
                    self.UploadPodImages(currentOrder)
                }
                else {
                    self.ExtraDestinationGroup.leave()
                }
                
                // Configure a completion callback
                self.ExtraDestinationGroup.notify(queue: .main) {
                    // All requests completed
                    self.HandleExtraDropLocations(currentOrder)
                }
            }
        }
    }
    
    func HandleExtraDropLocations(_ currentOrder : DriverAvailableJobs) {
        if currentOrder.extraDestinationLocations.count != 0 {
            for (DropOffLocationindex, extraDestinationLocation) in currentOrder.extraDestinationLocations.enumerated() {
                if extraDestinationLocation.beforeUnLoading.count != 0 {
                    self.WorkOrderGroup.enter()
                    self.UploadImagesToServer(extraDestinationLocation.beforeUnLoading) { (json) in
                        if let json = json
                        {
                            self.UpdateImagesInDBWithMultStatus(currentOrder, orderStatus: OrderStatusType.multi_drop_transit, multiWOStatus: OrderStatusType.beforeUnload, stopPoint: Double(DropOffLocationindex), json: json, cdImages: extraDestinationLocation.beforeUnLoading)
                            
                            if extraDestinationLocation.afterUnLoading.count != 0 {
                                self.UploadImagesToServer(extraDestinationLocation.afterUnLoading) { (json) in
                                    if let json = json
                                    {
                                        self.UpdateImagesInDBWithMultStatus(currentOrder, orderStatus: OrderStatusType.multi_drop_transit, multiWOStatus: OrderStatusType.unloaded, stopPoint: Double(DropOffLocationindex), json: json, cdImages: extraDestinationLocation.afterUnLoading)
                                        
                                        if extraDestinationLocation.podImages.count != 0 {
                                            self.UploadImagesToServer(extraDestinationLocation.podImages) { (json) in
                                                if let json = json
                                                {
                                                    self.WorkOrderGroup.leave()
                                                    self.UpdateImagesInDBWithMultStatus(currentOrder, orderStatus: OrderStatusType.multi_drop_transit, multiWOStatus: OrderStatusType.podRequested, stopPoint: Double(DropOffLocationindex), json: json, cdImages: extraDestinationLocation.podImages)
                                                }
                                            }
                                        }
                                        else{
                                            self.WorkOrderGroup.leave()
                                        }
                                    }
                                }
                            }
                            else{
                                self.WorkOrderGroup.leave()
                            }
                        }
                    }
                }
                else{
                    // All requests completed
                    self.updateWorkOrderStatusAfterOffline()
                }
            }
            
            // Configure a completion callback
            self.WorkOrderGroup.notify(queue: .main) {
                // All requests completed
                self.updateWorkOrderStatusAfterOffline()
            }
        }
        else{
            // All requests completed
            self.updateWorkOrderStatusAfterOffline()
        }
    }
    
    func UploadPodImages(_ currentOrder : DriverAvailableJobs) {
        self.UploadImagesToServer(currentOrder.podImages) { (json) in
            if let json = json
            {
                print(json)
                
                self.UpdateImagesInDBWithStatus(currentOrder, orderStatus: OrderStatusType.podRequested, json: json, cdImages: currentOrder.podImages)
                self.ExtraDestinationGroup.leave()
            }
        }
    }

    func uploadImagesHandleResponse(_ json : JSON, cdImages : [CDImage]) {
        if (json[ServerKeys.data].count > 1) {
            if let urlDatas = json[ServerKeys.data].array {
                for (index, url) in urlDatas.enumerated() {
                    CDImageManager.shared.updateUrl(cdImages[index].id ?? "", url: url.string ?? "") { (error) in
                        guard let error = error else {return}
                        print(error)
                    }
                }
            }
        }
        else {
            if let url = json[ServerKeys.data].string {
                CDImageManager.shared.updateUrl(cdImages[0].id ?? "", url: url) { (error) in
                    guard let error = error else {return}
                    print(error)
                }
            }
        }
    }
    
    func uploadImagesHandleResponse(_ json : JSON, cdImages : [CDImage], orderStatus: OrderStatusCD) {
        if (json[ServerKeys.data].count > 1) {
            if let urlDatas = json[ServerKeys.data].array {
                for (index, url) in urlDatas.enumerated() {
                    CDImageManager.shared.updateUrl(cdImages[index].id ?? "", url: url.string ?? "") { (error) in
                        guard let error = error else {return}
                        print(error)
                    }
                    if let index = orderStatus.images.firstIndex(where: {$0.id == cdImages[index].id}) {
                        CDImageManager.shared.updateUrl(orderStatus.images[index].id ?? "", url: url.string ?? "") { (error) in
                            guard let error = error else {return}
                            print(error)
                        }
                    }
                }
            }
        }
        else {
            if let url = json[ServerKeys.data].string {
                CDImageManager.shared.updateUrl(cdImages[0].id ?? "", url: url) { (error) in
                    guard let error = error else {return}
                    print(error)
                }
                if orderStatus.images.count != 0 {
                    CDImageManager.shared.updateUrl(orderStatus.images[0].id ?? "", url: url) { (error) in
                        guard let error = error else {return}
                        print(error)
                    }
                }
            }
        }
    }
    
    func updateWorkOrderStatusAfterOffline() {
        CDMyJobsManager.shared.get(completionHandler: { (result) in
            switch result {
            case .success(let data):
                if data.count != 0 {
                    for currentOrder in data {
                        if currentOrder.pendingOrderStatus .count != 0 {
                            self.makeWorkOrderStatusRequest(currentOrder)
                        }
                    }
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        })
    }
    
    func makeWorkOrderStatusRequest(_ currentOrder : DriverAvailableJobs) {
        var orderstatuss = [Any]()
        if currentOrder.pendingOrderStatus.count != 0 {
            for orderStatus in currentOrder.pendingOrderStatus {
                let jsonObject = self.convertIntoJsonArray(currentOrder, orderStatus: orderStatus)

                let jsonString = self.convertToJsonIntoString(jsonObject)
                
                if let list = self.convertToDictionary(text: jsonString) {
                    print(list);
                    orderstatuss.append(list)
                }
            }
            self.UpdateWorkOrderStatusToServer(orderstatuss, currentOrder: currentOrder)
        }
    }
    
    func UpdateWorkOrderStatusToServer(_ orderStatusList: [Any], currentOrder: DriverAvailableJobs)  {
        if orderStatusList.count != 0 {
            BaseServices.SendPostJsonArray(serverUrl: ServerUrls.Driver.updateWorkOrderStatusOffline, jsonToPost: orderStatusList) { (json) in

                if let json = json
                {
                    print(json)
                    self.isCurrentOrderSync = false
                    if json[ServerKeys.data].count != 0 {
                        self.HandleUpdateWorkOrderStatusOfflineResponse(json, currentOrder: currentOrder)
                    }
                    else{
                        self.UpdateViewControllers()
                    }
                }
            }
        }
    }
    
    func HandleUpdateWorkOrderStatusOfflineResponse(_ json : JSON, currentOrder: DriverAvailableJobs) {
        if let listOrders = json[ServerKeys.data].array {
            if let index = listOrders.firstIndex(where: {$0["orderId"].string == currentOrder.orderId && $0["workOrderStatus"].string == OrderStatusType.finished.rawValue}) {
                let listDic = listOrders[index].dictionary!
                CDMyJobsManager.shared.deleteWorkOrder(listDic["orderId"]?.string ?? "")
            }
        }
        self.UpdateViewControllers()
    }
    
    func UpdateViewControllers() {
        if let rootViewController = self.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController {
            nvc.viewControllers.last?.hideProgressBarWithText()
            SocketManager.sharedInstance.refershUser()
            if (nvc.viewControllers.last is DriverHomeController){
                (nvc.viewControllers.last as! DriverHomeController).hideProgressBarWithText()
                (nvc.viewControllers.last as! DriverHomeController).viewWillAppear(true)
            }
            nvc.viewControllers.last?.hideProgressBarWithText()
            if (nvc.viewControllers.last is ScheduledJobsController){
                (nvc.viewControllers.last as! ScheduledJobsController).refreshData(notification: nil, showLoader: true)
            }
        }
    }
    
    func convertIntoJsonArray(_ currentOrder : DriverAvailableJobs, orderStatus: OrderStatusCD) -> NSMutableDictionary {

        let jsonObject: NSMutableDictionary = NSMutableDictionary()

        jsonObject.setValue(orderStatus.driverID ?? "", forKey: "driverId")
        jsonObject.setValue(currentOrder.orderId ?? "", forKey: "orderId")
        if orderStatus.orderStatus.rawValue == OrderStatusType.podRequested.rawValue {
            jsonObject.setValue(OrderStatusType.unloaded.rawValue, forKey: "orderStatus")
        }
        else {
            jsonObject.setValue(orderStatus.orderStatus.rawValue, forKey: "orderStatus")
        }
        
        jsonObject.setValue(orderStatus.startTime ?? "", forKey: "startTime")
        jsonObject.setValue(orderStatus.stopPoint ?? "", forKey: "stopPoint")
        
        var multiWOStatus = ""
        if orderStatus.multiWOStatus != nil {
            if orderStatus.multiWOStatus.rawValue == OrderStatusType.podRequested.rawValue {
                multiWOStatus = OrderStatusType.unloaded.rawValue
            }
            else {
                multiWOStatus = orderStatus.multiWOStatus.rawValue
            }
        }
        
        jsonObject.setValue(multiWOStatus , forKey: "multiWOStatus")

        if orderStatus.images.count != 0
        {
            var images = [String]()
            for image in orderStatus.images {
                images.append(image.imageUrl ?? "")
            }
            
            if (orderStatus.orderStatus.rawValue != OrderStatusType.multi_drop_transit.rawValue) {
                if orderStatus.orderStatus.rawValue == OrderStatusType.podRequested.rawValue {
                    jsonObject.setValue([], forKey: "images")
                    jsonObject.setValue(images, forKey: "podRequestedImages")
                }
                else {
                    jsonObject.setValue(images, forKey: "images")
                }
            }
            else {
                if orderStatus.multiWOStatus.rawValue == OrderStatusType.podRequested.rawValue {
                    jsonObject.setValue([], forKey: "images")
                    jsonObject.setValue(images, forKey: "podRequestedImages")
                }
                else {
                    jsonObject.setValue(images, forKey: "images")
                }
            }
        }
        else
        {
            jsonObject.setValue([], forKey: "images")
        }

        return jsonObject
    }
    
    func convertToJsonIntoString(_ jsonObject : NSMutableDictionary) -> String {
        let jsonData: NSData

        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
            jsonString.replacingOccurrences(of: "\\", with: "")
            return jsonString
        } catch _ {
            print ("JSON Failure")
        }
        
        return ""
    }

    func convertToDictionary(text: String) -> Any? {
         if let data = text.data(using: .utf8) {
             do {
                 return try JSONSerialization.jsonObject(with: data, options: []) as? Any
             } catch {
                 print(error.localizedDescription)
             }
         }
         return nil
    }

    func addViewOnWindowForInternetConnection(isAdd:Bool){
        if isAdd{
            viewForInternetReachebilityMessage = UIView(frame: CGRect(x: 0, y: (self.window?.frame.height ?? 0) - 40, width: self.window?.frame.width ?? 0, height: 40))
            viewForInternetReachebilityMessage.backgroundColor = UIColor.red
            let lblMessage = UILabel(frame:  CGRect(x: 0, y: 0, width: self.window?.frame.width ?? 0, height: 40))
            lblMessage.text = "Internet connection lost. Check your internet."
            lblMessage.textColor = UIColor.white
            lblMessage.textAlignment = .center
            lblMessage.numberOfLines = 2
            lblMessage.adjustsFontSizeToFitWidth = true
            viewForInternetReachebilityMessage.addSubview(lblMessage)
            self.window?.addSubview(viewForInternetReachebilityMessage)
        }else{
            viewForInternetReachebilityMessage.removeFromSuperview()
        }
    }
    
    private func decideViewController()
    {
        if !HelperFunction.getUserId().isEmpty
        {
            let storyBoard = UIStoryboard.init(name: StoryboardNames.main, bundle: nil)
            let initialViewController = storyBoard.instantiateViewController(withIdentifier: IdentifierName.Main.decideNavigation)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
    }
    
    private func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            guard settings.authorizationStatus == .authorized else {
                DispatchQueue.main.async {
                    self.showNotificationAlert()
                }
                return
            }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    private func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            UNUserNotificationCenter.current().delegate = self
            guard granted else {
                DispatchQueue.main.async {
                    self.showNotificationAlert()
                }
                return
            }
            self.getNotificationSettings()
        }
    }
    
    private func showNotificationAlert()
    {
        let alertController = UIAlertController(title: "Notification Alert", message: "Please enable notifications", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                })
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        updateToken(token: token)
        //print("Device Token: \(token)")
    }
    
    private func updateToken(token : String)
    {
        let userId = HelperFunction.getUserId()
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.deviceToken, value: token)
        if !userId.isEmpty
        {
            var stringUrl = ""
            var params :[String: Any] = ["deviceToken":token,
                                         "deviceType":"ios"]
            if LoginType.customer.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType)
            {
                stringUrl = ServerUrls.Customer.updateDeviceToken
                params["customerId"] = userId
            }
            else {
                stringUrl = ServerUrls.Driver.updateDeviceToken
                params["driverId"] = userId
            }
            BaseServices.sendPostJson(serverUrl: stringUrl, jsonToPost: params)
        }
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let state = UIApplication.shared.applicationState
        guard let notifiedData = response.notification.request.content.userInfo as? [String: Any] else{ return }
        guard let type = notifiedData["type"] as? String else{ return }
        guard let data = notifiedData["data"] as? [String: Any] else{return}
        if state == .active{
            switch type{
            case "newOrderAvailable":
                NotificationCenter.default.post(name: .scheduleOrderAvailble, object: nil, userInfo: data)
                
            case "customerNotifyToDriver":
                NotificationCenter.default.post(name: .immediateOrderAvailble, object: nil, userInfo: data)
                
            case "driverNotifyToCustomer": // order status change from driver
                NotificationCenter.default.post(name: .orderStatusChange, object: nil, userInfo: data)
                
            case "orderCancelFromCustomer":
                NotificationCenter.default.post(name: .orderCancelFromCustomer, object: nil, userInfo: data)
                
            case "orderCancelFromDriver":
                NotificationCenter.default.post(name: .orderCancelFromDriver, object: nil, userInfo:data)
                
            case "orderTimeOut":
                NotificationCenter.default.post(name: .customerOrderTimeout, object: nil, userInfo:data)
                
            case "adminNotifyToDriver":
                NotificationCenter.default.post(name: .orderStatusChange, object: nil, userInfo: data)
                
            case "scheduledOrderTimingChange":
                let userId = HelperFunction.getUserId()
                if !userId.isEmpty
                {
                    if LoginType.customer.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType)
                    {
                        if let rootViewController = self.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController, let vc = nvc.viewControllers.first as? CustomerHome{
                            vc.pushToTrackOrderController(notification: notifiedData)
                        }
                    }else  if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType)
                    {
                        if let rootViewController = self.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController, let vc = nvc.viewControllers.first as? DriverHomeController{
                            if !(nvc.viewControllers.last is DriverCancelController) && !(nvc.viewControllers.last is DriverOrderDetailController) && !(nvc.viewControllers.last is OrderProcessViewController) && !(nvc.viewControllers.last is DriverPicUploadController) && !(nvc.viewControllers.last is LoadTimerController) && !(nvc.viewControllers.last is DriverLocationController) && !(nvc.viewControllers.last is DriverOrderCompleted) && !(nvc.viewControllers.last is GetUserRatingController) && !(nvc.viewControllers.last is ReviewOrderViewController) && !(nvc.viewControllers.last is PopOverDetailController){
                                vc.pushToReviewOrderViewController(notification: notifiedData)
                            }
                        }
                    }
                }
            default: break
            }
        }else{
            switch type{
            case "adminNotifyToDriver":
                NotificationCenter.default.post(name: .orderStatusChange, object: nil, userInfo: data)
            case "scheduledOrderTimingChange":
                let userId = HelperFunction.getUserId()
                if !userId.isEmpty
                {
                    if LoginType.customer.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType)
                    {
                        HelperFunction.saveValueInUserDefaults(key: UserDefaultkeys.isNotification, value:notifiedData)
                        if let rootViewController = self.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController, let vc = nvc.viewControllers.first as? CustomerHome{
                            vc.pushToTrackOrderController(notification: notifiedData)
                        }
                    }else  if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType)
                    {
                        HelperFunction.saveValueInUserDefaults(key: UserDefaultkeys.isNotification, value:notifiedData)
                        if let rootViewController = self.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController, let vc = nvc.viewControllers.first as? DriverHomeController{
                            vc.pushToReviewOrderViewController(notification: notifiedData)
                        }
                    }
                }
            case "additionalChargesNotificationToCustomer":
                let userId = HelperFunction.getUserId()
                if !userId.isEmpty
                {
                    if LoginType.customer.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType)
                    {
                        HelperFunction.saveValueInUserDefaults(key: UserDefaultkeys.isNotification, value:notifiedData)
                        if let rootViewController = self.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController, let vc = nvc.viewControllers.first as? CustomerHome{
                            vc.pushToTrackOrderController(notification: notifiedData)
                        }
                    }
                }
            default: break
            }
        }
        completionHandler()
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo =  notification.request.content.userInfo
        guard let type = userInfo["type"] as? String else{ return }
        if type == "scheduledOrderTimingChange"{
            let userId = HelperFunction.getUserId()
            if !userId.isEmpty
            {
                completionHandler([.alert,.sound])
            }
        }else  if type == "notifyToUsersByZipcode"{
            completionHandler([.alert,.sound])
        }else  if type == "customInApp"{
            completionHandler([.alert])
        }
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    //MARK: - PAUSE Screen Methods
    func redirectToPauseScreen(id:String) {
        self.window?.viewWithTag(100)?.removeFromSuperview()
        let storyBoard = UIStoryboard.init(name: StoryboardNames.driver, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: IdentifierName.Driver.pauseController) as! PauseController
        vc.view!.tag = 100
        vc.orderId = id
        vc.getOrderDetails()
        self.window?.addSubview(vc.view!)
    }
    
    func removePauseScreen()  {
        self.window?.viewWithTag(100)?.removeFromSuperview()
        
    }
    
    //MARK: - Application life cycle
    func applicationDidEnterBackground(_ application: UIApplication) {
        SocketManager.sharedInstance.exitFromSocketEvent()
        SocketManager.sharedInstance.closeConnection()
        SocketManager.sharedInstance.isBackground = true
        UIApplication.shared.isIdleTimerDisabled = false
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
     
        if reachability.connection != .unavailable  {
            refreshHomeControllersWhenInternetReached()
            getOrderStatusIfDriverAlreadyProcessed()
        }
       
    }
    
    func updateDriverLocation(driverId:String) {
        guard let currentLocation = locManager.location else {
                        return
                    }
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            "longitude":currentLocation.coordinate.longitude,
            "latitude":currentLocation.coordinate.latitude
        ]
        print(param)
     
        BaseServices.sendPostJson(serverUrl:ServerUrls.Driver.updateDriverLocation, jsonToPost: param)

        
    }
    
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.isIdleTimerDisabled = true
        let userId = HelperFunction.getUserId()
        if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType){
            let userId = HelperFunction.getUserId()
            if !userId.isEmpty
            {
                updateDriverLocation(driverId: userId)
            }
        }
        if !userId.isEmpty
        {
            
                SocketManager.sharedInstance.startSocket()
        }
        clearAllNotification()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    private func clearAllNotification()
    {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
            center.removeAllDeliveredNotifications() // To remove all delivered notifications
        } else {
            UIApplication.shared.cancelAllLocalNotifications()
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "EZER")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        if url.scheme?.localizedCaseInsensitiveCompare(IdentifierName.ReverseURL) == .orderedSame {
            return BTAppSwitch.handleOpen(url, options: options)
        }
        return true
    }
}

