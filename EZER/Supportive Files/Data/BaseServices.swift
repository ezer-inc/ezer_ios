//
//  File.swift
//  EZER
//
//  Created by Geeta on 24/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Reachability

public enum AJResult<T,U: Error>{
    case success(T)
    case failure(U)
}
open class BaseServices {
    open class func testAPI(method : HTTPMethod = .post, serverUrl:String, jsonToPost:[String:Any], completion: @escaping (AJResult<JSON?, Error>) -> Void) {
        Alamofire.request(
            URL(string: serverUrl)!,
            method: method,
            parameters:jsonToPost, encoding:JSONEncoding.default)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(_):
                    completion(AJResult.success(JSON(response.result.value!)))
                case .failure(let error):
                    completion(AJResult.failure(error))
                }
        }
    }
    
    open class func SendPostJson(viewController: UIViewController?,method :HTTPMethod = .post,serverUrl:String,jsonToPost:[String:Any],isResponseRequired : Bool = false,showLoader : Bool = true, embedAuthTocken: Bool = true, completion:@escaping (JSON?) -> Void) {
        
        do{
            if try Reachability().connection == .unavailable {
//                viewController?.showAlertController( message: ValidationMessages.noInternet)
                return
            }
        }catch{
            print("could not start reachability notifier")
            viewController?.showAlertController( message: ValidationMessages.noInternet)
            return
        }
        
        //let json = JSON(jsonToPost)
        //print("serverUrl",serverUrl)
        //print(json)
        let Auth_header    =  [ServerKeys.AuthToken : "Bearer \(HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.authToken))"]
        debugPrint("Token is \(Auth_header)")
        
        print("APIType: \(method.rawValue)")
        print("jsonToPost: \(jsonToPost)")
        print("APIurl: \(serverUrl)")
        
        //showing loader
        if showLoader{
            viewController?.showProgressBar()
        }
        Alamofire.request(
            URL(string: serverUrl)!,
            method: method,
            parameters:jsonToPost,encoding:JSONEncoding.default,
            headers:embedAuthTocken ? Auth_header : nil)
            .validate()
            .responseJSON { (response) in
                if showLoader{
                    viewController?.hideProgressBar()
                }
                switch response.result {
                case .success(_):
                    if(response.result.value == nil)
                    {
                        viewController?.showAlertController( message: ValidationMessages.serverFailiure)
                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                            print("Data: \(utf8Text)")
                        }
                        completion(nil)
                    }else{
                        let json = JSON(response.result.value!)
                        print(json)
                        //check if unauthorized user
                        if json[ServerKeys.errorCode].int == 401 || json[ServerKeys.errorCode].int == 402{
                            viewController?.logout(withMessage: json[ServerKeys.message].string ?? "")
                            return
                        }
                        
                        if json[ServerKeys.status].int == 1
                        {
                            completion(json)
                        }else if json[ServerKeys.status].int == 0{
                            if isResponseRequired{
                                completion(json)
                            }else{
                                viewController?.showMessage(json[ServerKeys.message].string ?? "", type: .error,options: [.textNumberOfLines(0)])
                                completion(nil)
                            }
                        }else if json[ServerKeys.status].int == 5{
                            let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                                NotificationCenter.default.post(name: .refreshDriverData, object: nil)
                                viewController?.navigationController?.popToRootViewController(animated: true)
                            })
                            viewController?.showAlertController( message: json[ServerKeys.message].string ?? "", action: action)
                        }
                        else{
                            showAlertDialogAccording(errorCode: json[ServerKeys.status].int ?? 0, viewCon: viewController)
                        }
                    }
                case .failure(let error):
                    /*
                     if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                     print("Data:InOther \(utf8Text)")
                     }*/
                    
                    if error._code == NSURLErrorTimedOut {
                        viewController?.showAlertController( message: "Server TimeOut")
                        completion(nil)
                    }
                    else if error._code == NSURLErrorNetworkConnectionLost{
                        // print("Server Repeat")
                        SendPostJson(viewController: viewController,method: method,serverUrl: serverUrl,jsonToPost: jsonToPost, isResponseRequired: isResponseRequired, completion: completion)
                    }
                    else  {
                        if isResponseRequired{
                            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                                print("Data: \(utf8Text)")
                                let json = JSON(data)
                                print(json)
                                //check if unauthorized user
                                if json[ServerKeys.errorCode].int == 401 || json[ServerKeys.errorCode].int == 402{
                                    viewController?.logout(withMessage: json[ServerKeys.message].string ?? "")
                                    completion(nil)
                                    return
                                }
                            }
                            
                            viewController?.showAlertController( message: "Invalid json Data")
                            print(error.localizedDescription)
                            completion(nil)
                        }else {
                            if let data = response.data {
                                let json = JSON(data)
                                if json[ServerKeys.errorCode].int == 401 || json[ServerKeys.errorCode].int == 402{
                                    viewController?.logout(withMessage: json[ServerKeys.message].string ?? "")
                                    return
                                }
                            }
                        }
                    }
                    
                }
        }
    }
    class func uploadImageToServer(image: UIImage,imageType:String,role: String,mimeType:String = "image/jpeg",quality:CGFloat = 0.7,completion:@escaping (JSON?) -> Void )
    {
        let Auth_header    =  [ServerKeys.AuthToken : "Bearer \(HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.authToken))"]
        debugPrint("Token is \(Auth_header)")
        let params: Parameters = ["role": role,"imageType": imageType]
        var imageData : Data!
        var fileName = ""
        switch mimeType {
        case "image/png":
            imageData = image.pngData()!
            fileName = "filename.png"
        default://case "image/jpeg":
            fileName = "filename.jpeg"
            imageData = image.jpegData(compressionQuality: quality)!
        }
        Alamofire.upload(multipartFormData:
            {
                (multipartFormData) in
                multipartFormData.append(imageData, withName: "imageCode", fileName: fileName, mimeType: mimeType)
                for (key, value) in params
                {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
        }, to:ServerUrls.Main.uploadImageToS3,method: .post,headers: Auth_header)
        { (result) in
            switch result {
            case .success(let upload,_,_ ):
                upload.uploadProgress(closure: { (progress) in
                    // print(progress.fractionCompleted)
                    //self.progressBar.setProgress(Float(progress.fractionCompleted), animated: true)
                })
                upload.responseJSON
                    { response in
                        switch response.result {
                        case .success(_):
                            if(response.result.value == nil)
                            {
                                completion(nil)
                            }else{
                                let json = JSON(response.result.value!)
                                //print(json)
                                
                                if json[ServerKeys.status].int == 1
                                {
                                    completion(json)
                                }else{
                                    completion(nil)
                                }
                            }
                        case.failure(let error):
                            //  print("server failiure",error)
                            if error._code == NSURLErrorTimedOut {
                                //   print("server timeout")
                                completion(nil)
                            }
                            else if error._code == NSURLErrorNetworkConnectionLost{
                                //print("Server Repeat")
                                uploadImageToServer(image: image,imageType:imageType,role: role,mimeType:mimeType,quality:quality,completion:completion)
                            }
                            else  {
                                completion(nil)
                            }
                        }
                }
            case .failure(let error):
                
                print("failiure",error)
            }
        }
    }
    
    class func uploadFileToServer(image: Data,fileType:String,role: String,mimeType:String = "application/pdf",quality:CGFloat = 0.7,completion:@escaping (JSON?) -> Void )
    {
        let Auth_header    =  [ServerKeys.AuthToken : "Bearer \(HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.authToken))"]
        debugPrint("Token is \(Auth_header)")
        let params: Parameters = ["role": role,"imageType": fileType]
        let imageData = image
        var fileName = ""
        fileName = "filename.pdf"
        Alamofire.upload(multipartFormData:
            {
                (multipartFormData) in
                multipartFormData.append(imageData, withName: "imageCode", fileName: fileName, mimeType: mimeType)
                for (key, value) in params
                {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
        }, to:ServerUrls.Main.uploadImageToS3,method: .post,headers: Auth_header)
        { (result) in
            switch result {
            case .success(let upload,_,_ ):
                upload.uploadProgress(closure: { (progress) in
                    // print(progress.fractionCompleted)
                    //self.progressBar.setProgress(Float(progress.fractionCompleted), animated: true)
                })
                upload.responseJSON
                    { response in
                        switch response.result {
                        case .success(_):
                            if(response.result.value == nil)
                            {
                                completion(nil)
                            }else{
                                let json = JSON(response.result.value!)
                                //print(json)
                                if json[ServerKeys.status].int == 1
                                {
                                    completion(json)
                                }else{
                                    completion(nil)
                                }
                            }
                        case.failure(let error):
                            //  print("server failiure",error)
                            if error._code == NSURLErrorTimedOut {
                                //   print("server timeout")
                                completion(nil)
                            }
                            else if error._code == NSURLErrorNetworkConnectionLost{
                                //print("Server Repeat")
                                uploadFileToServer(image: image,fileType:fileType,role: role,mimeType:mimeType,quality:quality,completion:completion)
                            }
                            else  {
                                completion(nil)
                            }
                        }
                }
            case .failure(let error):
                
                print("failiure",error)
            }
        }
    }
    
    class func uploadImagesToServer(images: [CDImage],imageType:String,role: String,mimeType:String = "image/jpeg",quality:CGFloat = 0.7,completion:@escaping (JSON?) -> Void )
    {
        let Auth_header    =  [ServerKeys.AuthToken : "Bearer \(HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.authToken))"]
        debugPrint("Token is \(Auth_header)")
        let params: Parameters = ["role": role,"imageType": imageType]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
                    // import image to request
                    for image in images {
                        var imageData : Data!
                        var fileName = ""
                        switch mimeType {
                            case "image/png":
                                imageData = image.imageData ?? Data()
                                fileName = "filename.png"
                            default:
                                fileName = "filename.jpeg"
                                imageData = image.imageData ?? Data()
//                                image.jpegData(compressionQuality: quality)!
                            }
                        
                        multipartFormData.append(imageData, withName: "imageCode", fileName: fileName, mimeType: mimeType)
                    }
            for (key, value) in params
            {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
                }, to: ServerUrls.Main.uploadImageToS3,method: .post,headers: Auth_header) { (result) in
            switch result {
            case .success(let upload,_,_ ):
                upload.uploadProgress(closure: { (progress) in
                    // print(progress.fractionCompleted)
                    //self.progressBar.setProgress(Float(progress.fractionCompleted), animated: true)
                })
                upload.responseJSON
                    { response in
                        switch response.result {
                        case .success(_):
                            if(response.result.value == nil)
                            {
                                completion(nil)
                            }else{
                                let json = JSON(response.result.value!)
                                //print(json)
                                
                                if json[ServerKeys.status].int == 1
                                {
                                    completion(json)
                                }else{
                                    completion(nil)
                                }
                            }
                        case.failure(let error):
                            //  print("server failiure",error)
                            if error._code == NSURLErrorTimedOut {
                                //   print("server timeout")
                                completion(nil)
                            }
                            else if error._code == NSURLErrorNetworkConnectionLost{
                                //print("Server Repeat")
                                uploadImagesToServer(images: images,imageType:imageType,role: role,mimeType:mimeType,quality:quality,completion:completion)
                            }
                            else  {
                                completion(nil)
                            }
                        }
                }
            case .failure(let error):
                
                print("failiure",error)
            }
        }
    }
    
    open class func sendGetJson(serverUrl:String,embedAuthToken : Bool = true,completion:@escaping (JSON?) -> Void) {
        
        print("APIurl: \(serverUrl)")
        
        let Auth_header    =  [ServerKeys.AuthToken : "Bearer \(HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.authToken))"]
        debugPrint("Token is \(Auth_header)")
        Alamofire.request(
            URL(string: serverUrl)!,
            headers: embedAuthToken ? Auth_header : nil)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(_):
                    
                    if(response.result.value == nil)
                    {
                        /*if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                         print("Data: \(utf8Text)")
                         }*/
                        completion(nil)
                    }else{
                        let json = JSON(response.result.value!)
                        completion(json)
                    }
                case .failure(let error):
                    
                    /* if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                     print("Data: \(utf8Text)")
                     }*/
                    
                    if error._code == NSURLErrorTimedOut {
                        completion(nil)
                    }
                    else if error._code == NSURLErrorNetworkConnectionLost{
                        //  print("Server Repeat")
                    }
                    
                }
        }
    }
    
    // use if you don't wanna anything in response
    open class func sendPostJson(serverUrl:String,jsonToPost:[String:Any]) {
        
        
        print("APIType: Post")
        print("jsonToPost: \(jsonToPost)")
        print("APIurl: \(serverUrl)")
        
        let Auth_header    =  [ServerKeys.AuthToken : "Bearer \(HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.authToken))"]
        debugPrint("Token is \(Auth_header)")
        
        Alamofire.request(
            URL(string: serverUrl)!,
            method: .post,
            parameters:jsonToPost,encoding:JSONEncoding.default,
            headers: Auth_header )
            .validate()
            .responseJSON { (response) in
                
                switch response.result {
                case .success(_):
                    
                    if(response.result.value == nil)
                    {
                        /*if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                         print("Data: \(utf8Text)")
                         }*/
                        
                    }else{
                        let json = JSON(response.result.value!)
                        print(json)
                    }
                case .failure(let error):
                    if error._code == NSURLErrorTimedOut {
                    }
                    else if error._code == NSURLErrorNetworkConnectionLost{
                        //  print("Server Repeat")
                    }
                }
        }
    }
    
    open class func SendPostJsonArray(serverUrl:String,jsonToPost:[Any], completion:@escaping (JSON?) -> Void) {
        print("APIType: Post")
        print("jsonToPost: \(jsonToPost)")
        print("APIurl: \(serverUrl)")
        
        let Auth_header    =  [ServerKeys.AuthToken : "Bearer \(HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.authToken))"]
        debugPrint("Token is \(Auth_header)")
        
        let url = URL(string: serverUrl)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.authToken))", forHTTPHeaderField: ServerKeys.AuthToken)

        request.httpBody = try! JSONSerialization.data(withJSONObject: jsonToPost)

        Alamofire.request(request)
            .responseJSON { response in
                // do whatever you want here
                switch response.result {
                case .success(_):
                    print(response.result.value)
                    if(response.result.value == nil)
                    {
                        completion(nil)
                    }else{
                        let json = JSON(response.result.value!)
                        print(json)
                        completion(json)
                    }
                case .failure(let error):
                    print(response.error?.localizedDescription)
                    if error._code == NSURLErrorTimedOut {
                    }
                    else if error._code == NSURLErrorNetworkConnectionLost{
                        //  print("Server Repeat")
                    }
                    completion(nil)
                }
        }
    }
    
    open class func showAlertDialogAccording(errorCode : Int,viewCon : UIViewController?)
    {
        guard let viewCon = viewCon else { return }
        switch errorCode {
        case 100:
            DataBaseHandler.dataBaseHandler.deleteUnfinishedData()
            let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                NotificationCenter.default.post(name: .refreshDriverData, object: nil)
                viewCon.navigationController?.popToRootViewController(animated: true)
            })
            viewCon.showAlertController( message: ValidationMessages.orderCancelByUser, action: action)
        default:
            print("0 case")
        }
    }
}
