//
//  CoreDataManager.swift
//  EZER
//
//  Created by Mac mini on 10/10/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import Foundation
import UIKit
import CoreData

enum CDError: String, Error{
    case dataModelParsingFailed
    case invalidResponse
    case deletionFailed
    case entityNotFound
    case dataNotFound
}
extension CDError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .dataModelParsingFailed:
            return NSLocalizedString("Data model parsing failed",comment: "")
            
        case .invalidResponse:
            return NSLocalizedString("Invlid response",comment: "")
            
        case .deletionFailed:
            return NSLocalizedString("Deletion Failed",comment: "")
            
        case .entityNotFound:
            return NSLocalizedString("Failed getting Entity",comment: "")
            
        case .dataNotFound:
            return NSLocalizedString("Data not found",comment: "")
        }
    }
}

protocol CoreDataManager {
    typealias ResultData = (_ result: Result<[NSManagedObject], Error>) -> ()
    typealias ErrorData = (_ error: Error?) -> ()
    
    var entityName: String { get set }
    var context: NSManagedObjectContext { get }
    
    func fetchRecords(completionHandler: @escaping ResultData)
    func saveContext(completionHandler: @escaping ErrorData)
    func deleteAllRecords(completionHandler: @escaping ErrorData)
}

extension CoreDataManager {
    // Common method to get context.
    var context: NSManagedObjectContext {
        get {
            return (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        }
    }
    
    // Common method to get entity.
    var entity: NSEntityDescription? {
        get {
            return NSEntityDescription.entity(forEntityName: entityName, in: self.context)
        }
    }
    
    // Common method to fetch records.
    func fetchRecords(completionHandler: @escaping ResultData){
        // Create Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        var result = [NSManagedObject]()
        do {
            // Execute Fetch Request
            let records = try context.fetch(fetchRequest)
            if let records = records as? [NSManagedObject] {
                result = records
            }
            completionHandler(Result.success(result))
        } catch {
            completionHandler(Result.failure(error))
        }
    }
    
    // Common method to save context.
    func saveContext(completionHandler: @escaping ErrorData){
        do {
            try self.context.save()
            completionHandler(nil)
        } catch {
            completionHandler(error)
        }
    }
    
    func deleteIfFound(predicate: NSPredicate) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = predicate
        do {
            if let fetchResults = try context.fetch(fetchRequest) as? [NSManagedObject] {
                for data in fetchResults {
                    context.delete(data)
                    try self.context.save()
                }
            }
        }catch {
            print("Predicate Error: \(error.localizedDescription)")
        }
    }
    
    func fetchRecords(predicate: NSPredicate, completionHandler: @escaping ResultData) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = predicate
        do {
            if let fetchResults = try context.fetch(fetchRequest) as? [NSManagedObject] {
                completionHandler(Result.success(fetchResults))
            }else{
                completionHandler(Result.failure(CDError.dataNotFound))
            }
        }catch {
            completionHandler(Result.failure(error))
        }
    }
    
    
    func isAvaialble(predicate: NSPredicate) -> Bool{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = predicate
        do {
            if let fetchResults = try context.fetch(fetchRequest) as? [NSManagedObject] {
                if fetchResults.count != 0{
                    return true
                }else{
                    return false
                }
            }else{
                return false
            }
        }catch {
            print("Predicate Error: \(error.localizedDescription)")
            return false
        }
    }
    
    // Common method to delete all data in entity.
    func deleteAllRecords(completionHandler: @escaping ErrorData) {
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try context.execute(deleteRequest)
            try context.save()
            completionHandler(nil)
        } catch {
            completionHandler(error)
        }
    }
}
