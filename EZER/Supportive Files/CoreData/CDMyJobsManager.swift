//
//  CDMyJobManager.swift
//  EZER
//
//  Created by Mac mini on 14/10/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import Foundation
import CoreData

class CDMyJobsManager: CoreDataManager {
    var entityName = "CDMyJobs"
    static let shared = CDMyJobsManager()
    typealias ResultData = (_ result: Result<[DriverAvailableJobs], Error>) -> ()
    
    // MARK:- Enity Methods
    func save(_ jobs: [DriverAvailableJobs], completionHandler: @escaping ErrorData) {
        guard self.entity != nil else{
            completionHandler(CDError.entityNotFound)
            return
        }
        self.deleteAllRecords { (error) in
            guard let error = error else {return}
            print(error)
        }
        let myJobsCD = CDMyJobs(context: self.context)
        for model in jobs{
            if let myJob = CDDriverJob.create(in: self.context, entity: "CDDriverJob", from: model) {
                myJobsCD.addToDriverJobs(myJob)
            } else {
                completionHandler(CDError.dataModelParsingFailed)
                return
            }
        }
        self.saveContext(completionHandler: { (error) in
            completionHandler(error)
        })
    }
    
    func get(completionHandler: @escaping ResultData) {
        self.fetchRecords() { (result) in
            switch result{
            case .success(let data):
                if let data = data.first as? CDMyJobs, let dataArry = data.driverJobs {
                    var jobs = [DriverAvailableJobs]()
                    for data in dataArry {
                        if let data = data as? CDDriverJob{
                            jobs.append(DriverAvailableJobs(withCoreData: data))
                        }
                    }
                    completionHandler(Result.success(jobs))
                }else{
                    completionHandler(Result.failure(CDError.dataModelParsingFailed))
                }
                
            case .failure(let error):
                completionHandler(Result.failure(error))
            }
        }
    }
    
    func deleteWorkOrder(_ orderId : String) {
        let fetchRequest: NSFetchRequest<CDDriverJob> = CDDriverJob.fetchRequest()
        fetchRequest.predicate=NSPredicate(format: "orderId = %@", orderId)
        var fetchedItems = [CDDriverJob]()
        do{
            fetchedItems = try context.fetch(fetchRequest)
        }catch{
            print("Could not fetch")
        }
        for item in fetchedItems {
            context.delete(item)
        }
        self.saveContext(completionHandler: { (error) in })
    }
    
    func addWorkOrder(_ job : DriverAvailableJobs, completionHandler: @escaping ErrorData) {
        self.fetchRecords() { (result) in
            switch result{
            case .success(let data):
                if let data = data.first as? CDMyJobs {
                    if let myJob = CDDriverJob.create(in: self.context, entity: "CDDriverJob", from: job) {
                        data.addToDriverJobs(myJob)
                    } else {
                        completionHandler(CDError.dataModelParsingFailed)
                    }
                    self.saveContext(completionHandler: { (error) in
                        completionHandler(error)
                    })
                }else{
                    completionHandler(CDError.dataModelParsingFailed)
                }
                
            case .failure(let _):
                completionHandler(CDError.dataModelParsingFailed)
            }
        }
    }
}
