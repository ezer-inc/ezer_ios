//
//  CDDriverJobManager.swift
//  EZER
//
//  Created by Mac mini on 22/10/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import Foundation
import CoreData
enum ImageUploadType {
    case beforeLoading
    case afterLoading
    case beforeUnloading
    case afterUnloading
    case podImage
}
class CDDriverJobManager: CoreDataManager {
    var entityName = "CDDriverJob"
    static let shared = CDDriverJobManager()
    typealias ResultData = (_ result: Result<DriverAvailableJobs, Error>) -> ()
    typealias ResultImageData = (_ result: Result<CDImage, Error>) -> ()
    typealias ResultOrderStatusData = (_ result: Result<CDOrderStatus, Error>) -> ()
    
    // MARK:- Enity Methods
    func getCurrentOrder(_ orderId: String, completionHandler: @escaping ResultData) {
        self.fetchRecords(predicate: NSPredicate(format: "orderId = %@", orderId)) { (result) in
            switch result{
            case .success(let data):
                if let data = data.first as? CDDriverJob {
                    completionHandler(Result.success(DriverAvailableJobs(withCoreData: data)))
                }else{
                    completionHandler(Result.failure(CDError.dataModelParsingFailed))
                }
                
            case .failure(let error):
                completionHandler(Result.failure(error))
            }
        }
    }
    
    func updateCurrentOrder(_ orderId: String, status: OrderStatusType, multiWOStatus: OrderStatusType, completionHandler: @escaping ErrorData) {
        self.fetchRecords(predicate: NSPredicate(format: "orderId = %@", orderId)) { (result) in
            switch result{
            case .success(let data):
                if let data = data.first as? CDDriverJob {
                    // custom values can be added is needed
                    data.status = status.rawValue
                    data.multiWOStatus = multiWOStatus.rawValue
                    
                    self.saveContext(completionHandler: { (error) in
                        completionHandler(error)
                    })
                }else{
                    completionHandler(CDError.dataNotFound)
                }
                
            case .failure(let error):
                completionHandler(error)
            }
        }
    }
    
    func updateCurrentOrder(_ orderId: String, status: String, multiWOStatus: String, stopPoint: Int, completionHandler: @escaping ErrorData) {
        self.fetchRecords(predicate: NSPredicate(format: "orderId = %@", orderId)) { (result) in
            switch result{
            case .success(let data):
                if let data = data.first as? CDDriverJob {
                    // custom values can be added is needed
                    data.status = status
                    data.multiWOStatus = multiWOStatus
                    data.stopPoint = String(stopPoint)
                    self.saveContext(completionHandler: { (error) in
                        completionHandler(error)
                    })
                }else{
                    completionHandler(CDError.dataNotFound)
                }
                
            case .failure(let error):
                completionHandler(error)
            }
        }
    }
    
    func addImage(_ orderId: String, uploadYype: ImageUploadType, image: Data?, imageUrl: String?, stopPoint: Int, isMultiStops: Bool, completionHandler: @escaping ResultImageData) {
        self.fetchRecords(predicate: NSPredicate(format: "orderId = %@", orderId)) { (result) in
            switch result{
            case .success(let data):
                if let data = data.first as? CDDriverJob {
                    // custom values can be added is needed
                    if let cdImage = CDImage.create(in: self.context, entity: "CDImage", from: image, url: imageUrl) {
                        if isMultiStops {
                            switch uploadYype {
                            case .beforeLoading:
                                (data.extraStartLocations?[stopPoint] as! CDExtraStartLocationDriver).addToBeforeLoading(cdImage)
                            case .afterLoading:
                                (data.extraStartLocations?[stopPoint] as! CDExtraStartLocationDriver).addToAfterLoading(cdImage)
                            case .beforeUnloading:
                                (data.extraDestinationLocations?[stopPoint] as! CDExtraDestinationLocationDriver).addToBeforeUnloading(cdImage)
                            case .afterUnloading:
                                (data.extraDestinationLocations?[stopPoint] as! CDExtraDestinationLocationDriver).addToAfterUnloading(cdImage)
                            case .podImage:
                                (data.extraDestinationLocations?[stopPoint] as! CDExtraDestinationLocationDriver).addToPodImage(cdImage)
                            }
                        } else {
                            switch uploadYype {
                            case .beforeLoading:
                                data.addToBeforeLoading(cdImage)
                            case .afterLoading:
                                data.addToAfterLoading(cdImage)
                            case .beforeUnloading:
                                data.addToBeforeUnLoading(cdImage)
                            case .afterUnloading:
                                data.addToAfterUnLoading(cdImage)
                            case .podImage:
                                data.addToPodImage(cdImage)
                            }
                        }
                        self.saveContext(completionHandler: { (error) in
                            completionHandler(Result.success(cdImage))
                        })
                    }else{
                        completionHandler(Result.failure(CDError.dataModelParsingFailed))
                    }
                }else{
                    completionHandler(Result.failure(CDError.dataModelParsingFailed))
                }
                
            case .failure(let error):
                completionHandler(Result.failure(error))
            }
        }
    }
    
    func addPendingOrderStatus(_ orderId: String, driverID: String, multiWOStatus:String, orderStatus: String,images:[Data]?, stopPoint: Double, completionHandler: @escaping ResultOrderStatusData) {
        self.fetchRecords(predicate: NSPredicate(format: "orderId = %@", orderId)) { (result) in
            switch result{
            case .success(let data):
                if let data = data.first as? CDDriverJob {
                    // custom values can be added is needed
                    if let orderStatus = CDOrderStatus.create(in: self.context, entity: "CDOrderStatus", driverID: driverID, multiWOStatus: multiWOStatus, orderStatus: orderStatus, images: images ?? [Data](), stopPoint: stopPoint){
                        data.addToPendingOrderStatus(orderStatus)
                        
                        self.saveContext(completionHandler: { (error) in
                            completionHandler(Result.success(orderStatus))
                        })
                    }
                    else{
                        completionHandler(Result.failure(CDError.dataModelParsingFailed))
                    }
                }else{
                    completionHandler(Result.failure(CDError.dataModelParsingFailed))
                }
            case .failure(let error):
                completionHandler(Result.failure(error))
            }
        }
    }
    
    func getOrderStatus(_ orderId: String, completionHandler: @escaping ResultOrderStatusData) {
        self.fetchRecords(predicate: NSPredicate(format: "orderId = %@", orderId)) { (result) in
            switch result{
            case .success(let data):
                if let data = data.first as? CDDriverJob, let dataArry = data.pendingOrderStatus {
                    var orderStatus = [CDOrderStatus]()
                    var orderStatusImages = [OrderStatusCD]()
                    for data in dataArry {
                        if let data = data as? CDOrderStatus{
                            orderStatus.append(data)
                            orderStatusImages.append(OrderStatusCD(withCoreData: data))
                        }
                    }
                    
                    print("Succcess")
                    
                    completionHandler(Result.success(orderStatus[orderStatus.endIndex - 1]))
                }
                else{
                    completionHandler(Result.failure(CDError.dataModelParsingFailed))
                }
            case .failure(let error):
                completionHandler(Result.failure(error))
            }
        }
    }
    
    func deleteImageIfExist(_ orderId: String, id: String, uploadYype: ImageUploadType, isMultiStops: Bool) {
        self.fetchRecords(predicate: NSPredicate(format: "orderId = %@", orderId)) { (result) in
            switch result{
            case .success(let data):
                if let data = data.first as? CDDriverJob {
                    let driverJob = DriverAvailableJobs(withCoreData: data)
                    // custom values can be added is needed
                    if isMultiStops {
                        switch uploadYype {
                        case .beforeLoading:
                                if driverJob.extraStartLocations?[driverJob.stopPoint].beforeLoading.count != 0 {
                                    if let index = driverJob.extraStartLocations?[driverJob.stopPoint].beforeLoading.firstIndex(where: {$0.id == id}) {
                                        
                                        let cdImage = driverJob.extraStartLocations?[driverJob.stopPoint].beforeLoading[index]
                                        
                                        if (cdImage != nil) {
                                            (data.extraStartLocations?[driverJob.stopPoint] as! CDExtraStartLocationDriver).removeFromBeforeLoading(cdImage!)
                                        }
                                        
                                        driverJob.extraStartLocations?[driverJob.stopPoint].beforeLoading.remove(at: index)
                                    }
                                }
                            break
                        case .afterLoading:
                                if driverJob.extraStartLocations?[driverJob.stopPoint].afterLoading.count != 0 {
                                    if let index = driverJob.extraStartLocations?[driverJob.stopPoint].afterLoading.firstIndex(where: {$0.id == id}) {
                                        
                                        let cdImage = driverJob.extraStartLocations?[driverJob.stopPoint].afterLoading[index]
                                        
                                        if (cdImage != nil) {
                                            (data.extraStartLocations?[driverJob.stopPoint] as! CDExtraStartLocationDriver).removeFromAfterLoading(cdImage!)
                                        }
                                        
                                        driverJob.extraStartLocations?[driverJob.stopPoint].afterLoading.remove(at: index)
                                    }
                                }
                            break
                        case .beforeUnloading:
                                if driverJob.extraDestinationLocations?[driverJob.stopPoint].beforeUnLoading.count != 0 {
                                    if let index = driverJob.extraDestinationLocations?[driverJob.stopPoint].beforeUnLoading.firstIndex(where: {$0.id == id}) {
                                        
                                        let cdImage = driverJob.extraDestinationLocations?[driverJob.stopPoint].beforeUnLoading[index]
                                        
                                        if (cdImage != nil) {
                                            (data.extraDestinationLocations?[driverJob.stopPoint] as! CDExtraDestinationLocationDriver).removeFromBeforeUnloading(cdImage!)
                                        }
                                        
                                        driverJob.extraDestinationLocations?[driverJob.stopPoint].beforeUnLoading.remove(at: index)
                                    }
                                }
                            break
                        case .afterUnloading:
                                if driverJob.extraDestinationLocations?[driverJob.stopPoint].afterUnLoading.count != 0 {
                                    if let index = driverJob.extraDestinationLocations?[driverJob.stopPoint].afterUnLoading.firstIndex(where: {$0.id == id}) {
                                        
                                        let cdImage = driverJob.extraDestinationLocations?[driverJob.stopPoint].afterUnLoading[index]

                                        if (cdImage != nil) {
                                            (data.extraDestinationLocations?[driverJob.stopPoint] as! CDExtraDestinationLocationDriver).removeFromAfterUnloading(cdImage!)
                                        }
                                        
                                        driverJob.extraDestinationLocations?[driverJob.stopPoint].afterUnLoading.remove(at: index)
                                    }
                                }
                            break
                        case .podImage:
                                if driverJob.extraDestinationLocations?[driverJob.stopPoint].podImages.count != 0 {
                                    if let index = driverJob.extraDestinationLocations?[driverJob.stopPoint].podImages.firstIndex(where: {$0.id == id}) {
                                        let cdImage = driverJob.extraDestinationLocations?[driverJob.stopPoint].podImages[index]

                                        if (cdImage != nil) {
                                            (data.extraDestinationLocations?[driverJob.stopPoint] as! CDExtraDestinationLocationDriver).removeFromPodImage(cdImage!)
                                        }
                                        
                                        driverJob.extraDestinationLocations?[driverJob.stopPoint].podImages.remove(at: index)
                                    }
                                }
                            break
                        }
                    } else {
                        switch uploadYype {
                        case .beforeLoading:
                            if driverJob.beforeLoading.count != 0 {
                                if let index = driverJob.beforeLoading.firstIndex(where: {$0.id == id}) {
                                    data.removeFromBeforeLoading(driverJob.beforeLoading[index])
                                    driverJob.beforeLoading.remove(at: index)
                                }
                            }
                            break
                        case .afterLoading:
                            if driverJob.afterLoading.count != 0 {
                                if let index = driverJob.afterLoading.firstIndex(where: {$0.id == id}) {
                                    data.removeFromAfterLoading(driverJob.afterLoading[index])
                                    driverJob.afterLoading.remove(at: index)
                                }
                            }
                            break
                        case .beforeUnloading:
                            if driverJob.beforeUnLoading.count != 0 {
                                if let index = driverJob.beforeUnLoading.firstIndex(where: {$0.id == id}) {
                                    data.removeFromBeforeUnLoading(driverJob.beforeUnLoading[index])
                                    driverJob.beforeUnLoading.remove(at: index)
                                }
                            }
                            break
                        case .afterUnloading:
                            if driverJob.afterUnLoading.count != 0 {
                                if let index = driverJob.afterUnLoading.firstIndex(where: {$0.id == id}) {
                                    data.removeFromAfterUnLoading(driverJob.afterUnLoading[index])
                                    driverJob.afterUnLoading.remove(at: index)
                                }
                            }
                            break
                        case .podImage:
                            if driverJob.podImages.count != 0 {
                                if let index = driverJob.podImages.firstIndex(where: {$0.id == id}) {
                                    data.removeFromPodImage(driverJob.podImages[index])
                                    driverJob.podImages.remove(at: index)
                                }
                            }
                            break
                        }
                    }
                    self.saveContext(completionHandler: { (error) in })
                }
            case .failure(let error):
                break
            }
        }
    }
}
