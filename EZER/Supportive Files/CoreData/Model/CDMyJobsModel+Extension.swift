//
//  CDMyJobsModel.swift
//  EZER
//
//  Created by Mac mini on 14/10/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import Foundation
import CoreData

extension CDDriverJob{
    class func create(in context: NSManagedObjectContext, entity: String, from model: DriverAvailableJobs) -> CDDriverJob? {
        if let cdModel = NSEntityDescription.insertNewObject(forEntityName: entity, into: context) as? CDDriverJob {
            cdModel.customerId = model.customerId
            cdModel.driverName = model.driverName
            cdModel.orderId = model.orderId
            cdModel.orderNumber = String(model.orderNumber)
            cdModel.orderDate = model.orderDate
            cdModel.startAddress = model.startAddress
            cdModel.pickUpFullDate = model.pickUpFullDate
            cdModel.pickupDate = model.pickupDate
            cdModel.pickupTime = model.pickupTime
            cdModel.destinationAddress = model.destinationAddress
            cdModel.userNotes = model.userNotes
            cdModel.contentDescription = model.contentDescription
            cdModel.status = model.status.rawValue
            cdModel.driverId = model.driverId
            cdModel.isMultiOrder = model.isMultiOrder
            cdModel.immediate = model.immediate
            cdModel.multiWOStatus = model.multiWOStatus.rawValue
            cdModel.stopPoint = String(model.stopPoint)
            cdModel.startAddressUnitNumber = model.startAddressUnitNumber
            cdModel.destinationAddressUnitNumber = model.destinationAddressUnitNumber
            cdModel.isReAssigned = model.isReAssigned
            cdModel.estimatedMinPrice = model.estimatedMinPrice
            cdModel.estimatedMaxPrice = model.estimatedMaxPrice
            cdModel.totalDistance = model.totalDistance
            cdModel.orderPickupInterval = model.orderPickupInterval
            cdModel.podRequired = model.podRequired
            cdModel.driverSpecialRate = model.driverSpecialRate
            cdModel.isSpecialRateApplied = model.isSpecialRateApplied
            
            if let customer = CDCustomerDetail.create(in: context, entity: "CDCustomerDetail", from: model.customerDetail){
                cdModel.customerDetail = customer
            }
            
            if let startLoc = CDLocationModel.create(in: context, entity: "CDLocationModel", from: model.startLocation){
                cdModel.startLocation = startLoc
            }
            if let destinLoc = CDLocationModel.create(in: context, entity: "CDLocationModel", from: model.destinationLocation){
                cdModel.destinationLocation = destinLoc
            }
            for loc in model.extraStartLocations{
                if let cdLoc = CDExtraStartLocationDriver.create(in: context, entity: "CDExtraStartLocationDriver", from: loc){
                    cdModel.addToExtraStartLocations(cdLoc)
                }
            }
            for loc in model.extraDestinationLocations{
                if let cdLoc = CDExtraDestinationLocationDriver.create(in: context, entity: "CDExtraDestinationLocationDriver", from: loc){
                    cdModel.addToExtraDestinationLocations(cdLoc)
                }
            }
            for item in model.shippingItems{
                if let cdLoc = CDShippingItemsDriver.create(in: context, entity: "CDShippingItemsDriver", from: item){
                    cdModel.addToShippingItems(cdLoc)
                }
            }
            
            return cdModel
        }
        return nil
    }
}

extension CDCustomerDetail{
    class func create(in context: NSManagedObjectContext, entity: String, from model: CustomerDetail) -> CDCustomerDetail? {
        if let cdModel = NSEntityDescription.insertNewObject(forEntityName: entity, into: context) as? CDCustomerDetail {
            cdModel.firstName = model.firstName
            cdModel.lastName = model.lastName
            return cdModel
        }
        return nil
    }
}

extension CDShippingItemsDriver{
    class func create(in context: NSManagedObjectContext, entity: String, from model: ShippingItemsDriver) -> CDShippingItemsDriver? {
        if let cdModel = NSEntityDescription.insertNewObject(forEntityName: entity, into: context) as? CDShippingItemsDriver {
            cdModel.unitType = model.unitType
            cdModel.quantity = String(model.quantity ?? 0)
            cdModel.width = String(model.width)
            cdModel.name = model.name
            cdModel.weight = String(model.weight)
            cdModel.images = model.images
            cdModel.height = String(model.height)
            cdModel.length = String(model.length)
            cdModel.po = model.po
            return cdModel
        }
        return nil
    }
}

extension CDExtraDestinationLocationDriver{
    class func create(in context: NSManagedObjectContext, entity: String, from model: ExtraDestinationLocationDriver) -> CDExtraDestinationLocationDriver? {
        if let cdModel = NSEntityDescription.insertNewObject(forEntityName: entity, into: context) as? CDExtraDestinationLocationDriver {
            cdModel.address = model.address
            cdModel.destinationAddressUnitNumber = model.destinationAddressUnitNumber
            cdModel.podRequired = model.podRequired
            if let loc = CDLocationModel.create(in: context, entity: "CDLocationModel", from: model.location){
                cdModel.location = loc
            }
            return cdModel
        }
        return nil
    }
}

extension CDExtraStartLocationDriver{
    class func create(in context: NSManagedObjectContext, entity: String, from model: ExtraStartLocationDriver) -> CDExtraStartLocationDriver? {
        if let cdModel = NSEntityDescription.insertNewObject(forEntityName: entity, into: context) as? CDExtraStartLocationDriver {
            cdModel.address = model.address
            cdModel.startAddressUnitNumber = model.startAddressUnitNumber
            cdModel.instruction = model.instruction
            cdModel.userNotes = model.userNotes
            
            for item in model.shippingItems{
                if let cdLoc = CDShippingItemsDriver.create(in: context, entity: "CDShippingItemsDriver", from: item){
                    cdModel.addToShippingItems(cdLoc)
                }
            }
            
            if let loc = CDLocationModel.create(in: context, entity: "CDLocationModel", from: model.location){
                cdModel.location = loc
            }
            return cdModel
        }
        return nil
    }
}

extension CDLocationModel{
    class func create(in context: NSManagedObjectContext, entity: String, from model: LocationModel) -> CDLocationModel? {
        if let cdModel = NSEntityDescription.insertNewObject(forEntityName: entity, into: context) as? CDLocationModel {
            cdModel.type = model.type
            cdModel.coordinates = model.coordinates
            return cdModel
        }
        return nil
    }
}
extension CDImage{
    class func create(in context: NSManagedObjectContext, entity: String, from data: Data?, url: String?) -> CDImage? {
        if let cdModel = NSEntityDescription.insertNewObject(forEntityName: entity, into: context) as? CDImage {
            cdModel.id = ProcessInfo().globallyUniqueString
            cdModel.imageData = data
            cdModel.imageUrl = url
            return cdModel
        }
        return nil
    }
}

extension CDOrderStatus{
    class func create(in context: NSManagedObjectContext,entity: String, driverID: String, multiWOStatus:String, orderStatus: String?,images: [Data], stopPoint: Double) -> CDOrderStatus? {
        if let cdModel = NSEntityDescription.insertNewObject(forEntityName: entity, into: context) as? CDOrderStatus {
            cdModel.startTime = Date().millisecondsSince1970
            cdModel.driverID = driverID
            cdModel.multiWOStatus = multiWOStatus
            cdModel.orderStatus = orderStatus
            cdModel.stopPoint = stopPoint
            if images.count != 0 {
                for image in images{
                    if let cdImage = CDImage.create(in: context, entity: "CDImage", from: image, url: nil) {
                        cdModel.addToImages(cdImage)
                    }
                }
            }
            return cdModel
        }
        return nil
    }
}
