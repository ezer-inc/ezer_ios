//
//  RoundedLabel.swift
//  DreamRay
//
//  Created by TimerackMac1 on 21/09/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

@IBDesignable class RoundedLabel: UILabel {
    private var _borderColor : UIColor = UIColor.lightGray
    @IBInspectable var borderColor : UIColor{
        set{
        _borderColor = newValue
            setup()
        }
        get{
        return _borderColor
        }
    }
    private var _cornerRadius: CGFloat = 4
    @IBInspectable var cornerRadius: CGFloat{
    set{
        _cornerRadius = newValue
        setup()
    }
    get{
    return _cornerRadius
    }
    }
    private var _borderWidth: CGFloat = 0
    @IBInspectable var borderWidth: CGFloat{
    set{
    _borderWidth = newValue
    setup()
    }
    get{
    return _borderWidth
    }
    }

    
    override func layoutSubviews() {
        setup()
    }
    func setup(){
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        layer.borderColor = borderColor.cgColor
        layer.masksToBounds = true
        frame = self.frame.insetBy(dx: -borderWidth, dy: -borderWidth)
    }

}
