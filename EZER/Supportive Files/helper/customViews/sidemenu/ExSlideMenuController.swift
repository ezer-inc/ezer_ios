//
//  ExSlideMenuController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 11/11/15.
//  Copyright © 2015 Yuji Hato. All rights reserved.
//

import UIKit

class ExSlideMenuController : SlideMenuController {

    override func isTagetViewController() -> Bool {
        if let vc = UIApplication.topViewController() {
            if vc is CustomerHome ||
            vc is DriverHomeController /*||
            vc is JavaViewController ||
            vc is GoViewController*/ {
                return true
            }
        }
        return false
    }
    
    override func track(_ trackAction: TrackAction) {
        switch trackAction {
        case .leftTapOpen:
            break
            //print("TrackAction: left tap open.")
        case .leftTapClose:
            break
           // print("TrackAction: left tap close.")
        case .leftFlickOpen:
            break
            //print("TrackAction: left flick open.")
        case .leftFlickClose:
            break
            //print("TrackAction: left flick close.")
        case .rightTapOpen:
            break
           // print("TrackAction: right tap open.")
        case .rightTapClose:
            break
            //print("TrackAction: right tap close.")
        case .rightFlickOpen:
            break
           // print("TrackAction: right flick open.")
        case .rightFlickClose:
            break
           // print("TrackAction: right flick close.")
        }   
    }
}
