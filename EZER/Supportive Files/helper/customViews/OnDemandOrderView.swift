//
//  OnDemandOrderView.swift
//  EZER
//
//  Created by TimerackMac1 on 26/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
import UIKit
@IBDesignable class OnDemandOrderView: UIView {
    
    @IBOutlet weak var orderAwayTime: UILabel!
    
    @IBOutlet weak var orderAcceptTime: UILabel!
    var contentView : UIView!
  
    @IBOutlet weak var outerView: UIView!
        {
        didSet{
            outerView.layer.cornerRadius = 44
            outerView.layer.borderWidth = 1
            outerView.layer.borderColor = UIColor.driverTheme.cgColor
        }
    }
    
    @IBOutlet weak var acceptButton: UIButton!
        {
        didSet{
            acceptButton.layer.cornerRadius = 35
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpLayout()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpLayout()
    }
    private func setUpLayout()
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        contentView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        contentView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(contentView)
        
    }
}
