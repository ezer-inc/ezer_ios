//
//  OrderStatus.swift
//  EZER
//
//  Created by TimerackMac1 on 21/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

@IBDesignable class OrderStatus: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var contentView : UIView!
    @IBInspectable var orderStatus : Int = 0
        {
        didSet{
            resetView()
            if orderStatus > 0 && contentView != nil{
            for index in 1...orderStatus
            {
                (contentView.viewWithTag(index) as! UIImageView).isHighlighted = true
                let msgLabel = (contentView.viewWithTag(index+4) as! UILabel)
                //msgLabel.isEnabled = false
                msgLabel.isHidden = true
            }
                (contentView.viewWithTag(orderStatus+4) as! UILabel).isEnabled = true
                (contentView.viewWithTag(orderStatus+4) as! UILabel).isHidden = false
            }
            firstLabel.isHidden = true
            secondLabel.isHidden = true
            thirdLabel.isHidden = true
            fourthLabel.isHidden = true
        }
    }
    private func resetView()
    {
        if contentView != nil{ //reset to main
            for index in 1...4
            {
                (contentView.viewWithTag(index) as! UIImageView).isHighlighted = false
                let msgLabel = (contentView.viewWithTag(index+4) as! UILabel)
                msgLabel.isHidden = true
            }
        }
    }
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var fourthLabel: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpLayout()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpLayout()
    }
    private func setUpLayout()
    {
        //let contentView = Bundle.main.loadNibNamed("OrderStatus", owner: self, options:nil)?.first as! UIView
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        contentView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        contentView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(contentView)
    }
    
}
