//
//  EZDropDown.swift
//  EZER
//
//  Created by Mac mini on 22/04/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import UIKit
protocol EZDropDownDelegate: class {
    func dropDownDelegate(view: EZDropDown, didSelectData data: String, withIndex index: Int)
}
class EZDropDown: UIView {
    
    @IBOutlet weak var dropDown: UIPickerView!
    var list: [String] = []
    weak var delegate: EZDropDownDelegate?
    
    // MARK:- keyboard initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeSubviews()
    }
    
    func initializeSubviews() {
        let view = Bundle.main.loadNibNamed("EZDropDown", owner: self, options: nil)![0] as! UIView
        self.addSubview(view)
        view.frame = self.bounds
        dropDown.reloadAllComponents()
    }
}

extension EZDropDown: UIPickerViewDataSource, UIPickerViewDelegate {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return list.count
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name:"Roboto-Medium", size: 20)
            pickerLabel?.textAlignment = .center
        }
        pickerLabel?.text = list[row]
        pickerLabel?.textColor = .black
        return pickerLabel!
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        delegate?.dropDownDelegate(view: self, didSelectData: self.list[row], withIndex: row)
    }
}
