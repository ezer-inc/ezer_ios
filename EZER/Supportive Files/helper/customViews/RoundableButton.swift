//
//  RoundableButton.swift
//  DreamRay
//
//  Created by TimerackMac1 on 22/09/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//


import UIKit
@IBDesignable class RoundableButton: UIButton {
    private var _radius = 7
    private var _round = false
    private var _isDropShadow = false
    private var _borderColor = UIColor.clear
    @IBInspectable var borderColor: UIColor {
        set {
            _borderColor = newValue
            makeRound()
        }
        get {
            return self._borderColor
        }
    }
    @IBInspectable var radius: Int {
        set {
            _radius = newValue
            makeRound()
        }
        get {
            return self._radius
        }
    }
    @IBInspectable var round: Bool {
        set {
            _round = newValue
            makeRound()
        }
        get {
            return self._round
        }
    }
    @IBInspectable var isDropShadow: Bool {
        set {
            _isDropShadow = newValue
            makeRound()
        }
        get {
            return self._isDropShadow
        }
    }
    
    private func makeRound() {
        if self.round == true {
            self.layer.cornerRadius = CGFloat(radius)
            self.layer.borderColor = _borderColor.cgColor
            self.layer.borderWidth = 1
        } else {
            self.layer.cornerRadius = 0
        }
        if self.isDropShadow{
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 1,height:1);
            layer.shadowRadius = 5
            layer.shadowOpacity = 0.5
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        makeRound()
    }
}
extension UIBarButtonItem {

    static func menuButton(_ target: Any?, action: Selector, image: UIImage, size: CGFloat) -> UIBarButtonItem {
        let button = UIButton(type: .system)
        button.setImage(image, for: .normal)
        button.addTarget(target, action: action, for: .touchUpInside)

        let menuBarItem = UIBarButtonItem(customView: button)
        menuBarItem.customView?.translatesAutoresizingMaskIntoConstraints = false
        menuBarItem.customView?.heightAnchor.constraint(equalToConstant: size).isActive = true
        menuBarItem.customView?.widthAnchor.constraint(equalToConstant: size).isActive = true

        return menuBarItem
    }
}
