//
//  DriverStatusView.swift
//  EZER
//
//  Created by TimerackMac1 on 23/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

@IBDesignable class DriverStatusView: UIView {

    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    @IBOutlet weak var statusText: UILabel!
    var contentView : UIView!
    @IBInspectable var status : Bool = false
    {
        didSet{
            statusText.backgroundColor = status ? UIColor.driverTheme : UIColor.white.withAlphaComponent(0.1)
            statusText.text = status ? "ON" : "OFF"
            }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpLayout()
    }
    
    private func setUpLayout()
    {
        //let contentView = Bundle.main.loadNibNamed("OrderStatus", owner: self, options:nil)?.first as! UIView
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        contentView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        contentView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(contentView)
    }
  
    override func layoutSubviews() {
        //super.layoutSubviews()
        self.layer.cornerRadius = self.frame.width/2
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.driverTheme.cgColor
        statusText.layer.cornerRadius = statusText.frame.width/2
    }
    
}
