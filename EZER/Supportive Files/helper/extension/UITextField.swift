//
//  UITextField.swift
//  EZER
//
//  Created by Prabhjot Singh on 19/09/19.
//  Copyright © 2019 TimerackMac1. All rights reserved.
//

import Foundation
import UIKit

//MARK:-
extension UITextField{
    
    func setPlaceholder(color: UIColor = #colorLiteral(red: 0.6196078431, green: 0.6196078431, blue: 0.662745098, alpha: 1)) {
        let attributedString = NSAttributedString(string: self.placeholder!,
                                                  attributes: [NSAttributedString.Key.foregroundColor: color])
        self.attributedPlaceholder = attributedString
    }
}
