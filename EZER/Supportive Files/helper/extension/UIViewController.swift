//
//  UIViewControllerExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/19/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit
import CoreLocation
import AVFoundation
extension UIViewController {

    func appDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func logout(withMessage msg: String = "")  {
        SocketManager.sharedInstance.exitFromSocketEvent()
        SocketManager.sharedInstance.closeConnection()
        SocketManager.sharedInstance.isBackground = true
        HelperFunction.clearUserDefault()
        let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
        let mainViewController = storyboard.instantiateInitialViewController()
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.backgroundColor = UIColor.driverTheme
        delegate.window?.rootViewController = mainViewController!
        delegate.window?.makeKeyAndVisible()
        if !msg.isEmpty{
            mainViewController?.showAlertController(message: msg)
        }
    }
    
    func setNavigationBarItem() {
        self.addLeftBarButtonWithImage(#imageLiteral(resourceName: "ic_menu"))
        // self.addRightBarButtonWithImage(UIImage(named: "ic_notifications_black_24dp")!)
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
    
    func showAlertController(heading:String = AppName,message : String, action : UIAlertAction? = nil, showCancel:Bool = false)
    {
        hidePresentedAlert {
            let alertCon = UIAlertController(title: heading, message: message, preferredStyle: .alert)
            if action == nil{
                alertCon.addAction(UIAlertAction.init(title: "OK", style: .cancel))
            }
            else
            {
                alertCon.addAction(action!)
            }
            if showCancel{
                alertCon.addAction(UIAlertAction.init(title: "Cancel", style: .default))
            }
            self.present(alertCon, animated: true)
        }
        
    }
    
    func hidePresentedAlert(completion: @escaping()-> Void)
    {
        if let viewCon = self.presentedViewController
        {
            viewCon.dismiss(animated: false){
                completion()
            }
        }else{
            completion()
        }
    }
    
    func showCameraAlert()
    {
        let action  = UIAlertAction.init(title: "OK", style: .default) { (action) in
            if let url = URL(string: UIApplication.openSettingsURLString){
                if #available(iOS 10.0, *){
                    UIApplication.shared.open(url, completionHandler: nil)
                } else{
                    UIApplication.shared.openURL(url)
                }
            }
        }
        let message = "Please turn on the camera in settings-> Camera"
        showAlertController(message: message, action: action)
    }
    func showLocationAlert()
    {
        hidePresentedAlert {
            let alertCon = UIAlertController(title: AppName, message: "Please turn on the location services in settings-> location", preferredStyle: .alert)
            alertCon.addAction(UIAlertAction.init(title: "Setting", style: .cancel){
                action in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {return}
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    })
                }
            })
            alertCon.addAction(UIAlertAction.init(title: "OK", style: .default)
            { action in
                
            })
            self.present(alertCon, animated: true)
        }
        
    }
    func showProgressBar()
    {
        JustHUD.shared.showInWindow(window: (UIApplication.shared.delegate as! AppDelegate).window!)
        //JustHUD.shared.showInView(view: self.view)
    }
    func hideProgressBar()
    {
        JustHUD.shared.hide()
    }
    
    func showProgressBarWithText()
    {
        showUniversalLoadingView(true)
    }
    
    func hideProgressBarWithText()
    {
        showUniversalLoadingView(false)
    }
    
    func showUniversalLoadingView(_ show: Bool) {
        let existingView = UIApplication.shared.windows[0].viewWithTag(1200)
        if show {
            if existingView != nil {
                return
            }
            let loadingView = self.makeLoadingView(withFrame: UIScreen.main.bounds, loadingText: ValidationMessages.syncedAlertProgress)
            loadingView?.tag = 1200
            UIApplication.shared.windows[0].addSubview(loadingView!)
        } else {
            existingView?.removeFromSuperview()
        }
    }
    
    func makeLoadingView(withFrame frame: CGRect, loadingText text: String?) -> UIView? {
       let loadingView = UIView(frame: frame)
       loadingView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
       let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
//       activityIndicator.backgroundColor = UIColor(red:0.16, green:0.17, blue:0.21, alpha:1)
       activityIndicator.layer.cornerRadius = 6
       activityIndicator.center = loadingView.center
       activityIndicator.hidesWhenStopped = true
       activityIndicator.style = .white
       activityIndicator.startAnimating()
       activityIndicator.tag = 100 // 100 for example

       loadingView.addSubview(activityIndicator)
       if !text!.isEmpty {
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width / 1.5, height: 50))
           let cpoint = CGPoint(x: activityIndicator.frame.origin.x + activityIndicator.frame.size.width / 2, y: activityIndicator.frame.origin.y + 80)
           lbl.center = cpoint
           lbl.textColor = UIColor.white
           lbl.textAlignment = .center
           lbl.numberOfLines = 2
           lbl.lineBreakMode = .byWordWrapping
           lbl.text = text
           lbl.tag = 1234
           loadingView.addSubview(lbl)
       }
       return loadingView
    }

    func checkCameraPermission(completion:@escaping (_ action : Bool) -> Void )
    {
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        
        switch cameraAuthorizationStatus {
        case .denied, .restricted:
            completion(false)
        case .authorized:
            completion(true)
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                DispatchQueue.main.async {
                    if granted {
                        completion(true)
                    } else {
                        completion(false)
                    }
                }
            }
        }
    }
    func checkAppVersion( showLoader : Bool = false)
    {
        let params :[String:Any] = [:]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Main.getAppOutConfigs, jsonToPost: params, showLoader:showLoader, embedAuthTocken: false) { (json) in
            if let json = json
            {
                let appConfig = AppOutConfig(json: json[ServerKeys.data])
                if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                    guard let localVersion = Float(version) else{return}
                    if appConfig.iosMinAppVersion > localVersion
                    {
                        let latestVersionApp = LatestVersionApp(nibName:"LatestVersionApp",bundle:nil)
                        latestVersionApp.appOutConfig = appConfig
                        self.navigationController?.pushViewController(latestVersionApp, animated: true)
                    }
                    else{
                        if appConfig.iosCurrentAppVersion > localVersion
                        {
                            let openUrlAction = UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
                                HelperFunction.openStringUrl(appConfig.linkiOS)
                            })
                            self.showAlertController( message: ValidationMessages.appDownloadMessage, action:openUrlAction , showCancel: true)
                        }
                    }
                }
            }
        }
    }
}
extension UINavigationItem {
    func setTitle(title:String, subtitle:String) {
        
        let one = UILabel()
        one.textColor = .white
        one.text = title
        one.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        one.sizeToFit()
        
        let two = UILabel()
        two.textColor = .white
        two.text = subtitle
        two.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        two.textAlignment = .center
        two.sizeToFit()
        
        let stackView = UIStackView(arrangedSubviews: [one, two])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        stackView.alignment = .center
        
        let width = max(one.frame.size.width, two.frame.size.width)
        stackView.frame = CGRect(x: 0, y: 0, width: width, height: 35)
        
        one.sizeToFit()
        two.sizeToFit()
        self.titleView = stackView
    }
}
