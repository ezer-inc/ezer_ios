//
//  HelperFunction.swift
//  EZER
//
//  Created by TimerackMac1 on 23/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import CoreLocation
import Reachability
import UserNotifications

open class HelperFunction {
    open class func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "XXX-XX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    /// Descritpion
    ///
    /// - Parameter key: preferences key
    /// - Returns: Returns value for key. if key not found returns empty value
    open class func  getSrtingFromUserDefaults(key : String) -> String
    {
        return UserDefaults.standard.string(forKey: key) ?? ""
    }
    
    open class func  getIntFromUserDefaults(key : String) -> Int
    {
        return UserDefaults.standard.integer(forKey: key)
    }
    open class func  getBoolFromUserDefaults(key : String) -> Bool
    {
        return UserDefaults.standard.bool(forKey: key)
    }
    
    open class func getDictFromUserDefault(key : String) -> [String:Any]?{
        return UserDefaults.standard.dictionary(forKey: key) ?? [String:Any]()
    }
    /// Descritpion
    ///
    /// - Parameters:
    ///   - key: String key for pref
    ///   - value: data to be save in pref
    open class func  saveValueInUserDefaults(key : String, value: Any)
    {
        return UserDefaults.standard.set(value, forKey: key)
    }
    open class func validateEmail(email:String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }
    open class func validateName(name:String) -> Bool {
        let nameRegex = "([a-zA-Z]{2,15}\\s*)+"
        return NSPredicate(format: "SELF MATCHES %@", nameRegex).evaluate(with: name)
    }
    open class func validatePassword(password:String) -> Bool {
       // let passwordRegex = "^[A-Za-z0-9 !\"#$%&'()*+,-./:;<=>?@\\[\\\\\\]^_`{|}~].{8,}$"
        
        let passwordRegex = "[a-zA-Z0-9$!#@%&*+.,?_=-]+"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
    }
    
    open class func isValidZip(testStr:String) -> Bool {
        // print("validate calendar: \(ftestStr)")
        let statusCode = "\\d{5}"
        let code = NSPredicate(format:"SELF MATCHES %@", statusCode)
        return code.evaluate(with: testStr)
    }
    /// get file name from url
    ///
    /// - Parameter stringUrl: url from which file name to extracted
    open class func getFileNameFromUrl(stringUrl : String)-> String
    {
        if let url = URL(string: stringUrl) {
            //let withoutExt = url.deletingPathExtension()
            //let name = withoutExt.lastPathComponent
            let name = url.lastPathComponent
            return name
            //let result = name.substring(from: name.index(name.startIndex, offsetBy: 5))
            
        }
        return ""
    }
    
    open class func saveCustomerInPref(loginModel : CustomerLoginModel)
    {
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.isSubAccount, value: loginModel.isSubAccount)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.isCardExpectedToExpire, value: loginModel.isCardExpectedToExpire)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.isDefaultCardExpired, value: loginModel.isDefaultCardExpired)
        
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.isBusinessUser, value: loginModel.isBusinessUser)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.Id, value: loginModel._id)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.profilePic, value: loginModel.profilePic)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.cellPhone, value: loginModel.cellPhone)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.lastName, value: loginModel.lastName)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.userId, value: loginModel.userId)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.firstName, value: loginModel.firstName)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.installationId, value: loginModel.installationId)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.isEnabled, value: loginModel.isEnabled)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.btCustomerId, value: loginModel.btCustomerId)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.profileType, value: LoginType.customer.rawValue)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.weightRange, value: loginModel.weightRange)
    }
    
    open class func saveDriverInPref(loginModel : DriverLoginModel) {
        HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.driverNumber, value: loginModel.driverNumber)
        HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.id, value: loginModel.id)
        HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.firstName, value: loginModel.firstName)
        HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.lastName, value: loginModel.lastName)
        HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.isVehicleInfoSubmitted, value: loginModel.isVehicleInfoSubmitted)
        HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.isPersonalInfoSubmitted, value: loginModel.isPersonalInfoSubmitted)
        HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.vehicleInfoVerified, value: loginModel.vehicleInfoVerified)
        HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.personalInfoVerified, value: loginModel.personalInfoVerified)
        HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.personalInfoVerified, value: loginModel.personalInfoVerified)
        HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.availibiltyStatus, value: loginModel.status.rawValue)
        HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.profilePic, value: loginModel.profilePicURL)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.profileType, value: LoginType.driver.rawValue)
        HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.enableHereDistance, value: loginModel.enableIAmHereBtnDistance)
    }
    
    open class func convertImageToBase64() -> String
    {
        let image : UIImage = UIImage(named:"ic_user")!
        let imageData:NSData = image.pngData()! as NSData
        var strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        strBase64 = "data:image/jpeg;base64,\(strBase64)"
        return strBase64
    }
    
    
    open class func isValidPhoneNumber(testStr:String) -> Bool {
        if(testStr.length > 14)
        {
            return false
        }
        return true
        //        let statusCode = "^(\\([0-9]{3}\\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$"
        //        let code = NSPredicate(format:"SELF MATCHES %@", statusCode)
        //        return code.evaluate(with: testStr)
    }
    
    open class func getUserName(loginType:LoginType)->String {
        var firstName = HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.firstName)
        var lastName = HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.lastName)
        
        if(loginType == .customer)
        {
            firstName = HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.firstName)
            lastName = HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.lastName)
        }
        
        return "\(firstName) \(lastName)"
        
    }
    
    open class func getWeightRange()-> Int{
        HelperFunction.getIntFromUserDefaults(key: ProfileKeys.weightRange)
    }
    
    open class func getUserId()->String {
        
        var userId :  String! = ""
        if LoginType.customer.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType)
        {
            userId = HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.Id)
        }
        else if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType){
            userId = HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.id)
        }
        return userId
        
    }
    open class func getEnableIMHereButtonDistance()->Float {
        
        return UserDefaults.standard.float(forKey: DriverProfileKeys.enableHereDistance)
    }
    
    open class func getUserInitials(loginType:LoginType)->String {
        var firstName = HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.firstName)
        var lastName = HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.lastName)
        
        if(loginType == .customer)
        {
            firstName = HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.firstName)
            lastName = HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.lastName)
        }
        
        return "\(firstName.firstCharacter())\(lastName.firstCharacter())"
    }
    class func createCustomerOrderParams(createOrderModel : CreateOrderModel) -> [String: Any]
    {
        var params = Dictionary<String,Any>()
        var startLocation = Dictionary<String,Any>()
        startLocation["type"] = "Point"
        startLocation["coordinates"] = [
            createOrderModel.startCoordinate.longitude,
            createOrderModel.startCoordinate.latitude
        ]
        params["startLocation"] = startLocation
        params["status"] = "OPEN"
        
        var destinationLocation = Dictionary<String,Any>()
        destinationLocation["type"] = "Point"
        destinationLocation["coordinates"] = [
            createOrderModel.destinationCoordinate.longitude,
            createOrderModel.destinationCoordinate.latitude
        ]
        params["destinationLocation"] = destinationLocation
        params["startAddress"] = createOrderModel.startAddress
        params["destinationAddress"] = createOrderModel.destinationAddress
        params["immediate"] = createOrderModel.immediate
        params["truckType"] = createOrderModel.truckType
        params["contentDescription"] = createOrderModel.contentDescription
        params["totalDistance"] = createOrderModel.totalDistance
        params["estimatedMinPrice"] = createOrderModel.estimatedMinPrice
        params["estimatedMaxPrice"] = createOrderModel.estimatedMaxPrice
        params["estimatedTime"] = createOrderModel.estimatedTime
        params["customerId"] = HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.Id)
        params["shippingItems"] = [
            "name":createOrderModel.name,
            "weight":createOrderModel.shippingItem.weight!,
            "length":createOrderModel.shippingItem.length!,
            "width":createOrderModel.shippingItem.width!,
            "height":createOrderModel.shippingItem.height!,
            "images":createOrderModel.shippingItem.imagesArray
        ]
        params["isMultiOrder"] = createOrderModel.isMultiOrder
        params["isTestOrder"] = createOrderModel.isTestOrder
        params["extraStartLocations"] = createOrderModel.extraStartLocations
        params["extraDestinationLocations"] = createOrderModel.extraDestinationLocations
        if createOrderModel.coupon != nil
        {
            var coupon = Dictionary<String,Any>()
            coupon["couponId"] = createOrderModel.coupon.couponId
            coupon["estimatedDiscountedMaxPrice"] = createOrderModel.coupon.estimatedDiscountedMaxPrice
            coupon["estimatedDiscountedMinPrice"] = createOrderModel.coupon.estimatedDiscountedMinPrice
            params["coupon"] = coupon
        }
        else{
            params["coupon"] = []
        }
        params["userNotes"] = createOrderModel.userNotes
        params["startAddressUnitNumber"] = createOrderModel.startAddressUnitNumber
        params["destinationAddressUnitNumber"] = createOrderModel.destinationAddressUnitNumber 
        params["dateTime"] = ""
        if !createOrderModel.immediate{
            params["dateTime"] = createOrderModel.dateTime
        }
        
        var connection = "Mobile Network"
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if appDelegate.reachability.connection == .wifi {
            connection = "WiFi"
        }
        let dictionary = Bundle.main.infoDictionary!
        var appVersion = ""
        var appVersionCode = ""
        if let version = dictionary["CFBundleShortVersionString"] as? String{
            appVersion = version
        }
        if let build = dictionary["CFBundleVersion"] as? String{
            appVersionCode = build
        }
        params["deviceDetail"] = [
            "platform"      : UIDevice.current.systemName,
            "model"         : UIDevice.modelName,
            "osVersion"     : UIDevice.current.systemVersion,
            "manufacturer"  : "Apple",
            "network"       : connection,
            "appVersion"    : appVersion,
            "appVersionCode": appVersionCode
        ]
        return params
    }
    class func getVersion() -> String {
        var appVersion = "1.0"
        if let dictionary = Bundle.main.infoDictionary {
            if let version = dictionary["CFBundleShortVersionString"] as? String{
                appVersion = version
            }
        }
        return appVersion
    }
    class func createNearByDriverTerms(createOrderModel : CreateOrderModel)-> [String: Any]
    {
        var params = Dictionary<String,Any>()
        params["truckType"] = createOrderModel.truckType
        params["weight"] = createOrderModel.shippingItem.weight!
        params["startLocation"] = [
            "type" : "Point",
            "coordinates":[createOrderModel.startCoordinate.longitude,
                           createOrderModel.startCoordinate.latitude
            ]
        ]
        return params
    }
    class func createCustomerCouponParams(createOrderModel : CreateOrderModel) -> [String: Any]
    {
        var params = Dictionary<String,Any>()
        params["currentMaxPrice"] = createOrderModel.estimatedMaxPrice
        params["currentMinPrice"] = createOrderModel.estimatedMinPrice
        params["extraData"] = [
            "customerId": HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.Id),
            "destinationLocation" : [
                "lat":createOrderModel.destinationCoordinate.latitude,
                "lng":createOrderModel.destinationCoordinate.longitude
            ],
            "estimatedTime": createOrderModel.estimatedTime,
            "immediate": createOrderModel.immediate,
            "shippingItems" : [
                "name":createOrderModel.name,
                "weight":createOrderModel.shippingItem.weight!,
                "length":createOrderModel.shippingItem.length!,
                "width":createOrderModel.shippingItem.width!,
                "height":createOrderModel.shippingItem.height!,
                "images":createOrderModel.shippingItem.imagesArray
            ],
            "startLocation" : [
                "lat":createOrderModel.startCoordinate.latitude,
                "lng":createOrderModel.startCoordinate.longitude
            ],
            "totalDistance": createOrderModel.totalDistance,
            "truckType": createOrderModel.truckType
        ]
        return params
    }
    class func convertToUtc(date : Date)->String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        // "2014-07-23 11:01:35 -0700" <-- same date, local, but with seconds
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = formatter.string(from:date)
        
        return date
    }
    
    class func getCurrentFullDate(date : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        guard let dt = dateFormatter.date(from: date) else { return date}
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        return dateFormatter.string(from: dt)
    }
    
    class func getCurrentDate(date : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        
        guard let dt = dateFormatter.date(from: date) else { return date}
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "MMM dd, yyyy - EEEE"
        return dateFormatter.string(from: dt)
    }
    class func getCurrentHistoryDate(date : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        
        guard let dt = dateFormatter.date(from: date) else { return date}
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "MMM dd, yyyy\nEEEE"
        return dateFormatter.string(from: dt)
    }
    
    class func isLocationServiceEnabled() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            default:
                print("Something wrong with Location services")
                return false
            }
        } else {
            print("Location services are not enabled")
            return false
        }
    }
    class func getCurrentDateWithoutDayName(date : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        
        guard let dt = dateFormatter.date(from: date) else { return date}
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "MMM dd, yyyy"
        return dateFormatter.string(from: dt)
    }
    class func getTime(date : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        
        guard let dt = dateFormatter.date(from: date) else { return date}
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: dt)
    }
    class func getDateComponent(dateString : String) -> DateModel? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        guard let date = dateFormatter.date(from: dateString) else{
            return nil
        }
        dateFormatter.timeZone = TimeZone.current
        let dateModel = DateModel()
        dateFormatter.dateFormat = "EEE"
        let dayName = dateFormatter.string(from: date).capitalized
        dateModel.dayName = dayName
        dateFormatter.dateFormat = "MMM, yyyy"
        let monthYear = dateFormatter.string(from: date).capitalized
        dateModel.monthAndYear = monthYear
        
        dateFormatter.dateFormat = "MMM dd"
        let monthWithDate = dateFormatter.string(from: date).capitalized
        dateModel.monthWithDate = monthWithDate
        
        dateFormatter.dateFormat = "dd"
        let dayOfMonth = dateFormatter.string(from: date).capitalized
        dateModel.dayOfMonth = dayOfMonth
        dateFormatter.dateFormat = "h:mm"
        let time = dateFormatter.string(from: date).capitalized
        dateModel.time = time
        dateFormatter.dateFormat = "a"
        let amORPM = dateFormatter.string(from: date)
        dateModel.amOrPM = amORPM
        dateFormatter.dateFormat = "yyyy"
        let year = dateFormatter.string(from: date).capitalized
        dateModel.year = year
        
        return dateModel
        
    }
    
    class func getDateComponent(date : Date) -> DateModel {
        let dateModel = DateModel()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE"
        let dayName = dateFormatter.string(from: date).capitalized
        dateModel.dayName = dayName
        dateFormatter.dateFormat = "MMM, yyyy"
        let monthYear = dateFormatter.string(from: date).capitalized
        dateModel.monthAndYear = monthYear
        
        dateFormatter.dateFormat = "MMM dd"
        let monthWithDate = dateFormatter.string(from: date).capitalized
        dateModel.monthWithDate = monthWithDate
        
        dateFormatter.dateFormat = "dd"
        let dayOfMonth = dateFormatter.string(from: date).capitalized
        dateModel.dayOfMonth = dayOfMonth
        dateFormatter.dateFormat = "h:mm"
        let time = dateFormatter.string(from: date).capitalized
        dateModel.time = time
        dateFormatter.dateFormat = "a"
        let amORPM = dateFormatter.string(from: date)
        dateModel.amOrPM = amORPM
        dateFormatter.dateFormat = "yyyy"
        let year = dateFormatter.string(from: date).capitalized
        dateModel.year = year
        
        return dateModel
    }
    class func createAttributedString(_ startText : String, textSize: CGFloat ,textColor: UIColor )-> NSAttributedString
    {
        let attributes :[NSAttributedString.Key:Any] = [NSAttributedString.Key.font: UIFont(name: FontNames.medium, size: textSize) as Any,NSAttributedString.Key.foregroundColor: textColor]
        let mutableAttributedString = NSMutableAttributedString(string:startText,attributes: attributes)
        return mutableAttributedString
    }
    class func openMailApp(email: String)
    {
        if let url = URL(string: "mailto:\(email)") {
            UIApplication.shared.open(url)
        }
    }
    class func distanceInMeters(_ start :CLLocationCoordinate2D ,destination : CLLocationCoordinate2D) -> CLLocationDistance {
        let firstLoc = CLLocation(latitude: start.latitude, longitude: start.longitude)
        print("First Location" , firstLoc.coordinate)
        let secondLoc = CLLocation(latitude: destination.latitude, longitude: destination.longitude)
        print("Second Location" , secondLoc.coordinate)
        let distance = firstLoc.distance(from: secondLoc)
        return distance
    }
    class func callToNumber(number : String )
    {
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    class func openStringUrl(_ link: String)
    {
        if let url = URL(string: link), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    class func isValidNumber(testStr:String) -> Bool {
        let statusCode = "^(\\([0-9]{3}\\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$"
        let code = NSPredicate(format:"SELF MATCHES %@", statusCode)
        return code.evaluate(with: testStr)
    }
    
    class func clearUserDefault()
    {
        let appDomain = Bundle.main.bundleIdentifier!
        let profileToken = HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.deviceToken)
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.deviceToken, value: profileToken)
    }
    class func setOrderStatus(status: OrderStatusType, trackOrderBar: OrderStatus)-> Int
    {
        var statusViewIndex = 0
        switch status {
        case .open:
            statusViewIndex = 0
        case .driverReview:
            statusViewIndex = 1
            trackOrderBar.firstLabel.text = "Assigned"
        case .accepted, .assigned:
            statusViewIndex = 1
            trackOrderBar.firstLabel.text = "Accepted"
        case .onTheWay, .onRouteToOrigin:
            trackOrderBar.secondLabel.text = "Driver On Route"
            statusViewIndex = 2
        case .atSource, .arrievedToOrigin:
            trackOrderBar.secondLabel.text = "Driver Arrived"
            statusViewIndex = 2
        case .loaded, .loading:
            trackOrderBar.thirdLabel.text = "Driver On Route"
            statusViewIndex = 3
        case .inTransit, .onRouteDestination:
            trackOrderBar.thirdLabel.text = "Driver On Route"
            statusViewIndex = 3
        case .multi_pickup_transit:
            trackOrderBar.thirdLabel.text = "Driver On Route"
            statusViewIndex = 3
        case .multi_drop_transit:
            trackOrderBar.fourthLabel.text = "Driver On Route"
            statusViewIndex = 4
        case .atDestination, .arrivedAtDestination:
            trackOrderBar.fourthLabel.text = "Driver At Destination"
            statusViewIndex = 4
        case .unloaded, .unLoaded:
            trackOrderBar.fourthLabel.text = "Delivery Completed"
            statusViewIndex = 4
        case .finished:
            statusViewIndex = 4
        case .done:
            statusViewIndex = 4
        case .cancelled:
            break
        case .timeout:
            statusViewIndex = 0
        default :
            break
        }
        return statusViewIndex
    }
    class func openDriver(driverLoginModel: DriverLoginModel,viewController : UIViewController, isOffline: Bool = false)
    {
        let storyboard = UIStoryboard(name: StoryboardNames.driver, bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: IdentifierName.Driver.driverHome) as! DriverHomeController
        mainViewController.driverLoginModel = driverLoginModel
        let leftViewController = storyboard.instantiateViewController(withIdentifier: IdentifierName.Driver.driverLeftMenu) as! LeftMenuController
        leftViewController.mainViewController = mainViewController
        let homeNavigation = UINavigationController.init(rootViewController: mainViewController)
        homeNavigation.navigationBar.tintColor = UIColor.white
        homeNavigation.navigationBar.barTintColor = UIColor.driverTheme
        homeNavigation.navigationBar.isTranslucent = false
        homeNavigation.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        let slideMenuController = ExSlideMenuController(mainViewController:homeNavigation, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.backgroundColor = UIColor.driverTheme
        delegate.window?.rootViewController = slideMenuController
        delegate.window?.makeKeyAndVisible()
        delegate.addViewOnWindowForInternetConnection(isAdd: isOffline)
    }
    class func openCustomer(viewController : UIViewController, isCalledFromCache: Bool = false, loginModel: CustomerLoginModel?)
    {
        let storyboard = UIStoryboard(name: StoryboardNames.customer, bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: IdentifierName.Customer.customerHome) as! CustomerHome
        mainViewController.isCalledFromCache = isCalledFromCache
        let leftViewController = storyboard.instantiateViewController(withIdentifier: IdentifierName.Customer.customerLeftMenu) as! CustomerLeftMenu
        leftViewController.mainViewController = mainViewController
        let homeNavigation = UINavigationController.init(rootViewController: mainViewController)
        homeNavigation.navigationBar.tintColor = UIColor.white
        homeNavigation.navigationBar.barTintColor = UIColor.customerTheme
        homeNavigation.navigationBar.isTranslucent = false
        homeNavigation.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        let slideMenuController = ExSlideMenuController(mainViewController:homeNavigation, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.backgroundColor = UIColor.customerTheme
        delegate.window?.rootViewController = slideMenuController
        delegate.window?.makeKeyAndVisible()
        if let model = loginModel{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                if model.isDefaultCardExpired{
                    mainViewController.showAlertController(heading: AppName, message: ValidationMessages.isDefaultCardExpired)
                }else if model.isCardExpectedToExpire{
                    mainViewController.showAlertController(heading: AppName, message: ValidationMessages.isCardExpectedToExpire)
                }
            }
            
        }
    }
    
    class func showNotification(withText text: String) {
            //creating the notification content
            let content = UNMutableNotificationContent()
            content.title = "EZER"
            content.body = text
            content.userInfo = ["type": "customInApp"]

            //getting the notification trigger
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)

            //getting the notification request
            let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: content, trigger: trigger)

            //adding the notification to notification center
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}
