//
//  Date.swift
//  EZER
//
//  Created by TimerackMac1 on 21/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
extension Date {
    var millisecondsSince1970:Double {
        return Double((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    var startOfWeek: (startDate :Date?, endDate:Date?) {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return (nil,nil)}
        //print(sunday,"sunday")
        return (gregorian.date(byAdding: .day, value: 1, to: sunday) ,gregorian.date(byAdding: .day, value: 7, to: sunday))
    }
    
    /*var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }*/

        
        func offsetFrom(date: Date) -> Int {
            return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
        }
    
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
}
 
