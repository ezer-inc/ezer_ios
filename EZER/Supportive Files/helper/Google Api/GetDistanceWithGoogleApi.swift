//
//  AdditionalCostInfoCell.swift
//  EZER
//
//  Created by time_rack on 23/08/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
struct GoogleDistance {
    var DurationText:String = ""
    var DurationValue:Double = 0
}

class GetDistanceWithGoogleApi: NSObject {
    
    class func distanceByGoogleApi(_ start : CLLocationCoordinate2D,destination : CLLocationCoordinate2D, completion:@escaping (GoogleDistance?) -> Void){
        
        var polyLineStr = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(destination.latitude)+\(destination.longitude)&destinations=\(start.latitude)+\(start.longitude)"
        
        polyLineStr += "&key=\(IdentifierName.GooglePlaceApiKey)"
        print(polyLineStr)
        guard let encodeurl = polyLineStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {return}
        
        BaseServices.sendGetJson(serverUrl: encodeurl) { (json) in
            guard let resJson = json else{return}
            print(resJson)
            if(resJson[ServerKeys.status].string == "ZERO_RESULTS")
            {}
            else if(resJson[ServerKeys.status].string == "NOT_FOUND")
            {}
            else{
                if let routes = resJson["rows"][0]["elements"].array{
                  var distanceStruct = GoogleDistance()
                    distanceStruct.DurationText = routes[0]["duration"]["text"].string ?? ""
                    distanceStruct.DurationValue =  routes[0]["duration"]["value"].double ?? 0.0
                    
                    DispatchQueue.main.async {
                         completion(distanceStruct)
                    }
                    
                }
            }
        }
    }

}
