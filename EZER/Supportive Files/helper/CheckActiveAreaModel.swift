//
//  CheckActiveAreaModel.swift
//  EZER
//
//  Created by TimerackMac1 on 30/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
import SwiftyJSON
class CheckActiveAreaModel
{
    let status:Bool
    let message:String
    let addressAvailable: Bool
    init(json : JSON)
    {
        status = json["_id"].bool ?? false
        message = json["message"].string ?? ""
        addressAvailable = json["addressAvailable"].bool ?? false
    }
    
}
