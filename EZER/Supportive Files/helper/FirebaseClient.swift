//
//  FirebaseClient.swift
//  EZER
//
//  Created by Mac mini on 07/05/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//
import Foundation
import Firebase
import FirebaseDatabase

enum FirebaseObserveType: String{
    case once
    case realtime
}

class SendLocationData: FireBaseClient{
    static let shared = SendLocationData()
    func updateData(data: [String: Any], orderNo: Int){
        if orderNo == 0{
            return
        }
        self.setData(withInputData: data, atChild: "Development/\(orderNo)/\(getCurrentDate())") { (error) in
            print(error.localizedDescription)
        }
//        self.setData(withInputData: data, atChild: "\(currentServer.rawValue)/\(orderNo)/\(getCurrentDate())") { (error) in
//            print(error.localizedDescription)
//        }
    }
    func getCurrentDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss aa"
        dateFormatter.timeZone = TimeZone.current
        return  dateFormatter.string(from: Date())
    }
}

// FireBase Protocol
protocol FireBaseClient{
    typealias ErrorData = (_ error: Error) -> ()
    typealias ResultData = (_ result: Result<Any, Error>) -> ()
    
    func getData(for type:FirebaseObserveType, fromChild path: String, completionHandler: @escaping ResultData)
    func setData(withInputData data: Any, atChild path: String, completionHandler: @escaping ErrorData)
    func removeData(fromChild path: String, completionHandler: @escaping ErrorData)
}

extension FireBaseClient{
    func getData(for type:FirebaseObserveType, fromChild path: String, completionHandler: @escaping ResultData){
        switch type {
        case .once:
            Database.database().reference().child(path).observeSingleEvent(of: DataEventType.value) { (snapshot) in
                if snapshot.exists(){
                    completionHandler(Result.success(snapshot))
                }
            }
            
        case .realtime:
            Database.database().reference().child(path).observe(DataEventType.value) { (snapshot) in
                if snapshot.exists(){
                    completionHandler(Result.success(snapshot))
                }
            }
        }
    }
    func setData(withInputData data: Any, atChild path: String, completionHandler: @escaping ErrorData){
        Database.database().reference().child(path).setValue(data, withCompletionBlock: { (error:Error?, ref:DatabaseReference) in
            if let error = error {
                completionHandler(error)
            }
        })
    }
    func removeData(fromChild path: String, completionHandler: @escaping ErrorData){
        Database.database().reference().child(path).removeValue { (error:Error?, ref:DatabaseReference) in
            if let error = error {
                completionHandler(error)
            }
        }
    }
}
