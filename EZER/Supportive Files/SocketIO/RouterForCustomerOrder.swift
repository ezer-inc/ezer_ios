//
//  RouterForCustomerOrder.swift
//  EZER
//
//  Created by Prabhjot Singh on 08/03/19.
//  Copyright © 2019 TimerackMac1. All rights reserved.
//

import Foundation
import UIKit

class RouterForCustomerOrder:NSObject{

    //Handle socket order cancel by admin for customer
    class func manageCustomerOrder(dataDict:[String:Any]) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController {
            //check if first vc is home then update
            if (nvc.viewControllers.first is CustomerHome){
                (nvc.viewControllers.first as! CustomerHome).getCurrentOrder()
                //check if OrderHistoryController is open in the nav stack
                if nvc.viewControllers.count > 1{
                    if let orderHistoryVC = (nvc.viewControllers[1] as? OrderHistoryController){
                        if !(nvc.viewControllers.last is OrderHistoryController){
                            orderHistoryVC.getOrderHistory(false)
                        }
                    }
                }
                //check last vc
                if (nvc.viewControllers.last is TrackOrderController){
                    if let orderId =  (nvc.viewControllers.last as! TrackOrderController).orderTrackModel?.orderId{
                        if let dataDictOrderId = dataDict["orderId"] as? String{
                            if orderId == dataDictOrderId{
                                (nvc.viewControllers.last as! TrackOrderController).getOrderDetails()
                            }
                        }
                    }
                }else if (nvc.viewControllers.last is OrderHistoryController){
                    (nvc.viewControllers.last as! OrderHistoryController).getOrderHistory()
                    
                }else if (nvc.viewControllers.last is TrackDriverViewController){
                    //check if same order is open
                    if let orderId =  (nvc.viewControllers.last as! TrackDriverViewController).orderTrackModel?.orderId{
                        if let dataDictOrderId = dataDict["orderId"] as? String{
                            if orderId == dataDictOrderId{
                                //show alert controller
                                  (nvc.viewControllers.last as! TrackDriverViewController).getOrderDetails()
                            }
                        }
                    }
                }
            }
        }
    }
    
    //Handle socket order cancel by admin for customer
    class func manageCustomerAddedOrderByAdmin(dataDict:[String:Any]) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController {
            //check if first vc is home then update
            if (nvc.viewControllers.first is CustomerHome){
                (nvc.viewControllers.first as! CustomerHome).getCurrentOrder()
            }
        }
    }
    
}
