//
//  RouterForSocketManager.swift
//  EZER
//
//  Created by Virender on 03/01/19.
//  Copyright © 2019 TimerackMac1. All rights reserved.
//

import Foundation
import UIKit

import ChatSDK
import ChatProvidersSDK
import MessagingSDK

class RouterForSocketManager:NSObject{
    
    //Handle socket notifyDriverStatusChangedByAdmin
    class func notifyDriverStatusChangedByAdmin(dataDict: [String:Any]) {
        if let isActivated = dataDict["isActivated"] as? Bool, isActivated == false {
            let delegate = UIApplication.shared.delegate as! AppDelegate
            let alertController = UIAlertController(title: "Ezer", message: "Your account is disabled. Please contact Ezer.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default) { (_) -> Void in
                SocketManager.sharedInstance.exitFromSocketEvent()
                SocketManager.sharedInstance.closeConnection()
                SocketManager.sharedInstance.isBackground = true
                HelperFunction.clearUserDefault()
                let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
                let mainViewController = storyboard.instantiateInitialViewController()
                delegate.window?.backgroundColor = UIColor.driverTheme
                delegate.window?.rootViewController = mainViewController!
                delegate.window?.makeKeyAndVisible()
                Chat.chatProvider?.endChat()
            }
            alertController.addAction(okAction)
            delegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    //Handle socket order cancel by admin
    
    //Handle socket order cancel by admin
    
    //Handle socket order cancel by admin
    class func manageOrderCancelByAdmin(message:String,data:[String:Any]){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            if LoginType.customer.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType)
            {
                manageCustomerModule(dataDict: data)
            }else if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType){
                manageDriverModule(dataDict: data)
            }
        }
        alertController.addAction(okAction)
        appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    //Handle socket order cancel by admin for all Drivers
    class func manageOrderCancelByAdminForAllDrivers(){
        if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType){
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController{
                if (nvc.viewControllers.last is DriverHomeController){
                    (nvc.viewControllers.last as! DriverHomeController).getAccountDetails(false)
                }
            }
        }
    }
    
    //Handle socket order cancel by admin for Driver
    class func manageDriverModule(dataDict: [String:Any]) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController, let vc = nvc.viewControllers.last{
            
            if (nvc.viewControllers.last is ScheduledJobsController)  {
                (nvc.viewControllers.last as! ScheduledJobsController).refreshData(notification: nil)
            }else if (nvc.viewControllers.last is DriverHomeController){
                (nvc.viewControllers.last as! DriverHomeController).getAccountDetails(false)
            }else if (nvc.viewControllers.last is DriverOrderDetailController){
                if let orderId =   (nvc.viewControllers.last as! DriverOrderDetailController).driverJob.orderId{
                    if let dataDictOrderId = dataDict["orderId"] as? String{
                        if orderId == dataDictOrderId{
                            vc.navigationController?.popToRootViewController(animated: false)
                        }
                    }
                }
                if (nvc.viewControllers.last is ScheduledJobsController)  {
                    (nvc.viewControllers.last as! ScheduledJobsController).refreshData(notification: nil)
                }
            }else if (nvc.viewControllers.last is PauseController){
                let orderId =   (nvc.viewControllers.last as! PauseController).orderId
                if let dataDictOrderId = dataDict["orderId"] as? String{
                    if orderId == dataDictOrderId{
                        vc.navigationController?.popToRootViewController(animated: false)
                    }
                }
            }else if (nvc.viewControllers.last is TrackOldDriverController){
                let orderId =   (nvc.viewControllers.last as! TrackOldDriverController).driverAvailableJobs.orderId
                if let dataDictOrderId = dataDict["orderId"] as? String{
                    if orderId == dataDictOrderId{
                        vc.navigationController?.popToRootViewController(animated: false)
                    }
                }
            }
            for obj in nvc.viewControllers{
                if obj is OrderProcessViewController{
                    if let orderId =  (obj as! OrderProcessViewController).driverJob?.orderId{
                        if let dataDictOrderId = dataDict["orderId"] as? String{
                            if orderId == dataDictOrderId{
                                vc.navigationController?.popToRootViewController(animated: false)
                            }
                        }
                    }
                }
            }
        }
    }
    
    //Handle socket order cancel by admin for customer
    class func manageCustomerModule(dataDict:[String:Any]) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController, let vc = nvc.viewControllers.last {
            if (nvc.viewControllers.first is CustomerHome){
                (nvc.viewControllers.first as! CustomerHome).getCurrentOrder()
                if (nvc.viewControllers.last is TrackOrderController){
                    if let orderId =  (nvc.viewControllers.last as! TrackOrderController).orderTrackModel?.orderId{
                        if let dataDictOrderId = dataDict["orderId"] as? String{
                            if orderId == dataDictOrderId{
                                vc.navigationController?.popToRootViewController(animated: false)
                            }
                        }
                    }
                }else if (nvc.viewControllers.last is TrackDriverViewController){
                    if let orderId =  (nvc.viewControllers.last as! TrackDriverViewController).orderTrackModel?.orderId{
                        if let dataDictOrderId = dataDict["orderId"] as? String{
                            if orderId == dataDictOrderId{
                                vc.navigationController?.popToRootViewController(animated: false)
                            }
                        }
                    }
                }else if (nvc.viewControllers.last is OrderHistoryController){
                    (nvc.viewControllers.last as! OrderHistoryController).orderHistoryList.removeAll()
                    (nvc.viewControllers.last as! OrderHistoryController).getOrderHistory()
                }
            }
        }
    }
    
    //Handle socket for convert order immediate to schedule
    class func manageSocketForConvertOrderImidateToSchedule(data:[String:Any]){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let alertController = UIAlertController(title: nil, message:data["message"] as? String, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            if LoginType.customer.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType)
            {
                manageCustomerModuleForConvertImmediateTOScheduleOrder(dataDict: data)
            }else if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType){
                manageDriverModuleForConvertImmediateTOScheduleOrder(dataDict: data)
            }
        }
        alertController.addAction(okAction)
        appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    //Handle socket for convert order immediate to schedule for customer module
    class func manageCustomerModuleForConvertImmediateTOScheduleOrder(dataDict:[String:Any]){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController {
            if (nvc.viewControllers.first is CustomerHome){
                (nvc.viewControllers.first as! CustomerHome).getCurrentOrder()
                if (nvc.viewControllers.last is TrackOrderController){
                    if let orderId =  (nvc.viewControllers.last as! TrackOrderController).orderTrackModel?.orderId{
                        if let dataDictOrderId = dataDict["orderId"] as? String{
                            if orderId == dataDictOrderId{
                                (nvc.viewControllers.last as! TrackOrderController).getOrderDetails()
                            }
                        }
                    }
                }
            }
        }
    }
    
    //Handle socket for convert order immediate to schedule for driver module
    class func manageDriverModuleForConvertImmediateTOScheduleOrder(dataDict:[String:Any]){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController, let vc = nvc.viewControllers.last{
            if (nvc.viewControllers.last is ScheduledJobsController)  {
                (nvc.viewControllers.last as! ScheduledJobsController).refreshData(notification: nil)
            }else if (nvc.viewControllers.last is DriverHomeController){
                (nvc.viewControllers.last as! DriverHomeController).getAccountDetails(false)
            }else if (nvc.viewControllers.last is ReviewOrderViewController){
                vc.navigationController?.popViewController(animated: false)
            }else if (nvc.viewControllers.last is DriverOrderDetailController){
                vc.navigationController?.popViewController(animated: false)
                if (nvc.viewControllers.last is ScheduledJobsController)  {
                    (nvc.viewControllers.last as! ScheduledJobsController).refreshData(notification: nil)
                }
            }
        }
    }
    
    
    //Handle socket for additional charges for customer
    class func manageAdditionalChargesForCustomer(dataDict:[String:Any]){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let alertController = UIAlertController(title: nil, message:dataDict["message"] as? String, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController {
                if (nvc.viewControllers.first is CustomerHome){
                    (nvc.viewControllers.first as! CustomerHome).getCurrentOrder()
                    if (nvc.viewControllers.last is TrackOrderController){
                        if let orderId =  (nvc.viewControllers.last as! TrackOrderController).orderTrackModel?.orderId{
                            if let dataDictOrderId = dataDict["orderId"] as? String{
                                if orderId == dataDictOrderId{
                                    (nvc.viewControllers.last as! TrackOrderController).getOrderDetails()
                                }
                            }
                        }
                    }
                }
            }
        }
        alertController.addAction(okAction)
        appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    
    //Handle socket for convert order immediate to schedule for driver module
    class func manageDriverModuleForOrderReAssignmentRefreshNewDriver(dataDict:[String:Any]){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let alertController = UIAlertController(title: nil, message:dataDict["message"] as? String, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController, let vc = nvc.viewControllers.last{
                if (nvc.viewControllers.last is DriverHomeController){
                    (nvc.viewControllers.last as! DriverHomeController).viewWillAppear(false)
                }else{
                    //vc.navigationController?.popToRootViewController(animated:false)
                }
            }
        }
        alertController.addAction(okAction)
        appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    
    //Handle socket for convert order immediate to schedule for driver module
    class func manageDriverModuleForresumeOrderNotification(dataDict:[String:Any]){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let alertController = UIAlertController(title: nil, message:dataDict["message"] as? String, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController, let vc = nvc.viewControllers.last{
                if (nvc.viewControllers.last is DriverHomeController){
                    (nvc.viewControllers.last as! DriverHomeController).viewWillAppear(false)
                }else{
                    vc.navigationController?.popToRootViewController(animated:false)
                }
            }
        }
        alertController.addAction(okAction)
        appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    
    class func manageOrderResetOrderNotification(message:String,data:[String:Any]){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            if LoginType.customer.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType)
            {
                manageCustomerModule(dataDict: data)
            }else if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType){
                manageResetOrderForDriverModule(data: data)
            }
        }
        alertController.addAction(okAction)
        appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    class func manageResetOrderForDriverModule(data:[String:Any]){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController, let vc = nvc.viewControllers.last{
            
            if (nvc.viewControllers.last is ScheduledJobsController)  {
                (nvc.viewControllers.last as! ScheduledJobsController).refreshData(notification: nil)
            }else if (nvc.viewControllers.last is DriverHomeController){
                (nvc.viewControllers.last as! DriverHomeController).getAccountDetails(false)
            }else if (nvc.viewControllers.last is PauseController){
                let orderId =   (nvc.viewControllers.last as! PauseController).orderId
                if let dataDictOrderId = data["orderId"] as? String{
                    if orderId == dataDictOrderId{
                        vc.navigationController?.popToRootViewController(animated: false)
                    }
                }
            }else if (nvc.viewControllers.last is TrackOldDriverController){
                let orderId =   (nvc.viewControllers.last as! TrackOldDriverController).driverAvailableJobs.orderId
                if let dataDictOrderId = data["orderId"] as? String{
                    if orderId == dataDictOrderId{
                        vc.navigationController?.popToRootViewController(animated: false)
                    }
                }
            }else if (nvc.viewControllers.last is DriverOrderDetailController){
                if let orderId =   (nvc.viewControllers.last as! DriverOrderDetailController).driverJob.orderId{
                    if let dataDictOrderId = data["orderId"] as? String{
                        if orderId == dataDictOrderId{
                            vc.navigationController?.popToRootViewController(animated: false)
                        }
                    }
                }
                if (nvc.viewControllers.last is ScheduledJobsController)  {
                    (nvc.viewControllers.last as! ScheduledJobsController).refreshData(notification: nil)
                }
            }
            for obj in nvc.viewControllers{
                if obj is OrderProcessViewController{
                    if let orderId =  (obj as! OrderProcessViewController).driverJob?.orderId{
                        if let dataDictOrderId = data["orderId"] as? String{
                            if orderId == dataDictOrderId{
                                vc.navigationController?.popToRootViewController(animated: false)
                            }
                        }
                    }
                }
            }
        }
    }
    
}
