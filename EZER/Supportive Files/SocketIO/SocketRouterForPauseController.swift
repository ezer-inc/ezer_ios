//
//  SocketRouterForPauseController.swift
//  EZER
//
//  Created by Virender on 20/02/19.
//  Copyright © 2019 TimerackMac1. All rights reserved.
//

import Foundation
import UIKit
class SocketRouterForPauseController:NSObject{
    
    //Handle socket order cancel by admin
    class func managePauseViewController(data:String){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController,let vc = nvc.viewControllers.last{
            if !(nvc.viewControllers.last is DriverOrderDetailController) && !(nvc.viewControllers.last is ScheduledJobsController) && !(nvc.viewControllers.last is DriverHomeController) {
                let orderId = HelperFunction.getSrtingFromUserDefaults(key: UserDefaultkeys.currentOrderID)
                if orderId == data{
                    let pauseController = vc.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.pauseController) as! PauseController
                   pauseController.orderId = data
                   nvc.pushViewController(pauseController, animated: false)
                }
             
            }
        }
    }
    
    class func managePauseViewControllerForResume(message:String = "",data:String = ""){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController,let vc = nvc.viewControllers.last{
            if (nvc.viewControllers.last is PauseController)  {
                vc.navigationController?.popToRootViewController(animated: false)
            }else{
                if !(nvc.viewControllers.last is DriverOrderDetailController) && !(nvc.viewControllers.last is ScheduledJobsController) && !(nvc.viewControllers.last is DriverHomeController) {
                    let orderId = HelperFunction.getSrtingFromUserDefaults(key: UserDefaultkeys.currentOrderID)
                    if orderId == data{
                        if message != ""{
                            
                            let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                            }
                            alertController.addAction(okAction)
                            appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                        }
                    }
                   
                }
                
            }
        }
    }

    class func managePausebyAdminBeforePickup(data:[String:Any]){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController,let vc = nvc.viewControllers.last{
            
            let alertController = UIAlertController(title: nil, message: data["message"] as? String, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                if (nvc.viewControllers.last is DriverOrderDetailController)  {
                    if let orderId =  (nvc.viewControllers.last as! DriverOrderDetailController).driverJob.orderId{
                        if let dataDictOrderId = data["orderId"] as? String{
                            if orderId == dataDictOrderId{
                                vc.navigationController?.popToRootViewController(animated: false)
                            }
                        }
                    }
                    
                }else if (nvc.viewControllers.last is OrderProcessViewController){
                    if let orderId =  (nvc.viewControllers.last as! OrderProcessViewController).driverJob.orderId{
                        if let dataDictOrderId = data["orderId"] as? String{
                            if orderId == dataDictOrderId{
                                vc.navigationController?.popToRootViewController(animated: false)
                            }
                        }
                    }
                }
            }
            alertController.addAction(okAction)
            appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    class func manageDriverWhenAdminReassignNewDriver(data:[String:Any])  {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let rootViewController = appDelegate.window!.rootViewController as? ExSlideMenuController, let nvc = rootViewController.mainViewController as? UINavigationController,let vc = nvc.viewControllers.last{
            
            let alertController = UIAlertController(title: nil, message: data["message"] as? String, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                vc.navigationController?.popToRootViewController(animated: false)
            }
            alertController.addAction(okAction)
            appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }

}
