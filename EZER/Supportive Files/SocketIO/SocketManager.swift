//
//  SocketManager.swift
//  SocketManager
//
//

import UIKit
import Starscream
import SwiftyJSON

extension SocketManager: WebSocketDelegate {
    // MARK: - WebSocketDelegate
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            isConnected = true
            print("websocket is connected: \(headers)")
            if isBackground || isRefresh{
                connectUser()
                isBackground = false
                isRefresh = false
            }
        case .disconnected(let reason, let code):
            isConnected = false
            print("websocket is disconnected: \(reason) with code: \(code)")
        case .text(let string):
            print("Received text: \(string)")
            listenForEvents(dataStr: string)
        case .binary(let data):
            print("Received data: \(data.count)")
        case .ping(_):
            break
        case .pong(_):
            break
        case .viabilityChanged(_):
            break
        case .reconnectSuggested(_):
            break
        case .cancelled:
            isConnected = false
//            if isRefresh{
//                establishConnection()
//            }
        case .error(let error):
            isConnected = false
            handleError(error)
        }
    }
    
    func handleError(_ error: Error?) {
        if let e = error as? WSError {
            print("websocket encountered an error: \(e.message)")
        } else if let e = error {
            print("websocket encountered an error: \(e.localizedDescription)")
        } else {
            print("websocket encountered an error")
        }
    }
    
}

class SocketManager: NSObject {
    static let sharedInstance = SocketManager()
    var socket: WebSocket!
    var isConnected = false
    var isBackground = false
    var isRefresh = false
    
    
    
    override init() {
        super.init()
        socket = WebSocket(request: URLRequest(url: URL(string: socketUrl)!))
        socket.delegate = self
        
        //        listenForOtherMsgs()
    }
    
    func establishConnection() {
        socket.connect()
    }
    
    func closeConnection() {
        if isConnected{
            socket.disconnect(closeCode: CloseCode.goingAway.rawValue)
        }
    }
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
    private func listenForEvents(dataStr: String){
        guard let dictData = convertStringToDictionary(text: dataStr), let type = dictData["notificationType"] as? String, let data = dictData["data"] as? [String:Any] else {
            print("Data is not parseable")
            return
        }
        switch type {
        case "customerNotifyToDriver":
            HelperFunction.showNotification(withText: "Live order is avialable right now")
            NotificationCenter.default.post(name: .immediateOrderAvailble, object: nil, userInfo: data)
           
        case "newOrderAvailable":
            NotificationCenter.default.post(name: .scheduleOrderAvailble, object: 1, userInfo: data)
            HelperFunction.showNotification(withText: "Scheduled order is avialable right now")
        case "orderCancelFromCustomer":
            NotificationCenter.default.post(name: .orderCancelFromCustomer, object: nil, userInfo: data)
            
        case "scheduledOrderCancelled":
            NotificationCenter.default.post(name: .scheduledOrderCancelFromCustomer, object: nil, userInfo: data)
            
        case "refreshDriverData":
            NotificationCenter.default.post(name: .refreshDriverData, object: nil, userInfo: data)
            
        case "driverNotifyToCustomer":
            NotificationCenter.default.post(name: .orderStatusChange, object: nil, userInfo: data)
            
        case "orderCancelFromDriver":
            NotificationCenter.default.post(name: .orderCancelFromDriver, object: nil, userInfo: data)
            
        case "notifyWhenDriverAvailable":
            NotificationCenter.default.post(name: .notifyWhenDriverAvailable, object: nil, userInfo: data)
            
        case "orderTimeOut":
            NotificationCenter.default.post(name: .customerOrderTimeout, object: nil, userInfo: data)
            
        case "adminNotifyToDriver":
            if !HelperFunction.getUserId().isEmpty {
                if data["notificationType"] as! String == "PAUSED"{
                    SocketRouterForPauseController.managePauseViewController(data: data["orderId"] as! String)
                }else {
                    SocketRouterForPauseController.managePauseViewControllerForResume(message:data["message"] as! String,data:data["orderId"] as! String)
                }
            }
            
        case "orderReAssignmentBeforePickUpRefreshDriver":
            if !HelperFunction.getUserId().isEmpty {
                SocketRouterForPauseController.manageDriverWhenAdminReassignNewDriver(data:data)
            }
            
        case "additionalChargesNotificationToCustomer":
            if !HelperFunction.getUserId().isEmpty {
                RouterForSocketManager.manageAdditionalChargesForCustomer(dataDict:data)
            }
            
        case "orderCancelFromAdmin":
            if !HelperFunction.getUserId().isEmpty {
                RouterForSocketManager.manageOrderCancelByAdmin(message: data["message"] as! String, data: data)
            }
            
        case "resetOrderNotification":
            if !HelperFunction.getUserId().isEmpty {
                RouterForSocketManager.manageOrderResetOrderNotification(message: data["message"] as! String, data: data)
            }
            
        case "refreshDriverDataWhenOrderCancelFromAdmin":
            if !HelperFunction.getUserId().isEmpty {
                RouterForSocketManager.manageOrderCancelByAdminForAllDrivers()
            }
            
        case "orderReAssignmentRefreshNewDriver":
            if !HelperFunction.getUserId().isEmpty {
                RouterForSocketManager.manageDriverModuleForOrderReAssignmentRefreshNewDriver(dataDict: data)
            }
            
        case "convertImmediateOrderToScheduleOrder":
            if !HelperFunction.getUserId().isEmpty {
                RouterForSocketManager.manageSocketForConvertOrderImidateToSchedule(data: data)
            }
            
        case "resumeOrderNotification":
            if !HelperFunction.getUserId().isEmpty {
                RouterForSocketManager.manageDriverModuleForresumeOrderNotification(dataDict: data)
            }
            
        case "newOrderForCustomer":
            if !HelperFunction.getUserId().isEmpty {
                RouterForCustomerOrder.manageCustomerOrder(dataDict: data)
            }
            
        case "OrderAdded":
            if !HelperFunction.getUserId().isEmpty {
                RouterForCustomerOrder.manageCustomerAddedOrderByAdmin(dataDict: data)
            }
            
        case "orderAssigned":
            if !HelperFunction.getUserId().isEmpty {
                RouterForCustomerOrder.manageCustomerAddedOrderByAdmin(dataDict: data)
            }
            
        case "redirectDriverToHomeScreen":
            if !HelperFunction.getUserId().isEmpty {
                SocketRouterForPauseController.manageDriverWhenAdminReassignNewDriver(data: data)
            }
            
        case "orderReAssignmentRefreshDriver":
            if !HelperFunction.getUserId().isEmpty {
                NotificationCenter.default.post(name: .orderCanceledByAdmin, object: nil, userInfo: data)
            }
            
        case "notifyDriverStatusChangedByAdmin":
            if !HelperFunction.getUserId().isEmpty {
                RouterForSocketManager.notifyDriverStatusChangedByAdmin(dataDict: data)
            }
            
        default:
            print(data)
            break
        }
    }
    
    func getSocketForRefreshOrderType( completion: @escaping (_ result: JSON?)->()) {
        //        socket.on("refreshOrderType", callback: { (dataArray, socketAck) in
        //            if !dataArray.isEmpty{
        //                if !HelperFunction.getUserId().isEmpty
        //                {
        //                    let dataDict = dataArray[0] as! [String:Any]
        //                    if (dataDict["immediateOrderEnable"] as? Int) != nil {
        //                        HelperFunction.saveValueInUserDefaults(key: UserDefaultsKeys.immediateOrderEnable, value: dataDict["immediateOrderEnable"] as? Int ?? 0)
        //                        let json = JSON(dataDict)
        //                         completion(json)
        //                    }
        //                }
        //            }
        //        })
    }
    
    
    func connectUser() {
        if isConnected {
            var type = "client"
            if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType){
                type = "driver"
            }
            let userData :[String:Any] = ["token": (HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.authToken)), "userType": type]
            let jsonData = try! JSONSerialization.data(withJSONObject: userData, options: [])
            let decoded = String(data: jsonData, encoding: .utf8)!
            socket.write(string: decoded)
        } else{
            print("Websocket is not connected, establish connection again")
            establishConnection()
        }
    }
    func refershUser() {
        isRefresh = true

    }
    func startSocket() {
        if !SocketManager.sharedInstance.isConnected{
            SocketManager.sharedInstance.exitFromSocketEvent()
            SocketManager.sharedInstance.isRefresh = true
            SocketManager.sharedInstance.isBackground = true
            SocketManager.sharedInstance.establishConnection()
        }
    }
    
   
    
    
    func exitFromSocketEvent() {
        if isConnected {
            var type = "client"
            if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType){
                type = "driver"
            }
            let userData :[String:Any] = ["token": (HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.authToken)), "userType": type, "message":"close"]
            let jsonData = try! JSONSerialization.data(withJSONObject: userData, options: [])
            let decoded = String(data: jsonData, encoding: .utf8)!
            socket.write(string: decoded)
        } else{
            print("Websocket is not connected, no need to exit event")
        }
    }
}

