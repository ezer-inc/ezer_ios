//
//  DriverLoginModel.swift
//  EZER
//
//  Created by Geeta on 05/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
import SwiftyJSON
open class DriverLoginModel {
    let id:String
    let firstName:String
    let lastName:String
    let driverNumber :Int
    //let isAvailable:Bool
    let email:String
    let cellPhone:String
    let profilePicURL: String
    var isVehicleInfoSubmitted:Bool
    var isPersonalInfoSubmitted:Bool
    let vehicleInfoVerified:Bool
    let personalInfoVerified:Bool
    var status : DriverStatusTypes
    var backgroundCheck = ""
    var enableIAmHereBtnDistance : Float = 225
    let isTextNotification:Bool
    
    init(){
        id = HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.id)
        firstName = HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.firstName)
        lastName = HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.lastName)
        driverNumber = HelperFunction.getIntFromUserDefaults(key: DriverProfileKeys.driverNumber)
        email = ""
        cellPhone = ""
        profilePicURL = HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.profilePic)
        isVehicleInfoSubmitted = true
        isPersonalInfoSubmitted = true
        vehicleInfoVerified = true
        personalInfoVerified = true
        status = DriverStatusTypes(rawValue: HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.availibiltyStatus)) ?? .available
        isTextNotification = true
    }
    
    init(json : JSON)
    {
        id = json["driverId"].string ?? ""
        driverNumber = json["driverNumber"].int ?? 0
        firstName = json["firstName"].string ?? ""
        lastName = json["lastName"].string ?? ""
        email = json["email"].string ?? ""
        cellPhone = json["phoneNumber"].string ?? ""
        profilePicURL = json["profilePic"].string ?? ""
        let availableStatus = json["status"].string ?? DriverStatusTypes.unavailable.rawValue
        status = DriverStatusTypes(rawValue : availableStatus)!
        isVehicleInfoSubmitted = json["isVehicleInfoSubmitted"].bool ?? false
        isPersonalInfoSubmitted = json["isPersonalInfoSubmitted"].bool ?? false
        vehicleInfoVerified = json["vehicleInfoVerified"].bool ?? false
        personalInfoVerified = json["personalInfoVerified"].bool ?? false
        isTextNotification = json["isTextNotification"].bool ?? false
        backgroundCheck = json["backgroundCheck"].string ?? ""
        if let distance = Float(json["enableIAmHereBtn"].string ?? "700")
            {
            enableIAmHereBtnDistance = distance*0.3048 // converting into meter
        }
    }
    
}
