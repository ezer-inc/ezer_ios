//
//  HistoryMainModel.swift
//  EZER
//
//  Created by Naveen on 21/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
class HistoryMainModel{
    var totalAmount : Double = 0
    var weekInterval :String!
    var historyModelList = [PaymentHistoryModel]()
    func appendModel(historyModel:PaymentHistoryModel)
    {
        historyModelList.append(historyModel)
        totalAmount+=historyModel.amount
    }
}
