//
//  NotesModel.swift
//  EZER
//
//  Created by Virender on 16/02/19.
//  Copyright © 2019 TimerackMac1. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation

class NotesModel{
    var reAssignNote : String = ""
    var phoneNumber :String = ""
    var sourceLocation:NotesLocationModel!
    var isIAmHere:Bool = false
  
    init(json :  JSON)
    {
        reAssignNote = json["reAssignNote"].string ?? ""
        phoneNumber = json["phoneNumber"].string ?? ""
        sourceLocation = NotesLocationModel(json: json["sourceLocation"])
        isIAmHere = json["isIAmHere"].bool ?? false
    }
}
class NotesLocationModel : NSObject{
  
    var locationCordinate : CLLocationCoordinate2D?
    init(json :  JSON){
        if json["lat"].double != nil{
            locationCordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(json["lat"].double ?? 0.0), longitude: CLLocationDegrees(json["lng"].double ?? 0.0))
        }
    }
}
