//
//  HistoryModel.swift
//  EZER
//
//  Created by Akash on 20/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import SwiftyJSON

class PaymentHistoryModel: NSObject {
    var id: String!
    var workOrderId :String!
    var status :String!
    var percentage:Int!
    var createdAt : Double!
    var orderNumber : Int!
    var updatedAt : Double!
    var driverId : String!
    var date : String!
    var amount: Double!
    var type : String!
    var time : String!
    var dateObject : Date!
    
    init(json :  JSON) {
        percentage = json["percentage"].int ?? 0
        status = json["status"].string ?? ""
        workOrderId = json["workOrderId"].string ?? ""
        id = json["id"].string ?? ""
        createdAt = json["createdAt"].double ?? 0.0
        orderNumber = json["orderNumber"].int ?? 0
        updatedAt = json["updatedAt"].double ?? 0.0
        driverId = json["driverId"].string ?? ""
        date = json["date"].string ?? ""
        amount = json["amount"].double ?? 0.0
        type = json["type"].string ?? ""
        time = json["time"].string ?? ""
    }
    
}
