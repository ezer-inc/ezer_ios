//
//  PastJobModel.swift
//  EZER
//
//  Created by Naveen on 17/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
import SwiftyJSON
class OrderMiles {
    var distanceInMiles : String!
    init(json :  JSON) {
        distanceInMiles = json["distanceInMiles"].string ?? "NA"
    }
}
class PastJobModel {
    var totalCost : String!
    var tip : String!
    var cost : String!
    var orderMiles : OrderMiles!
    
    var totalEarningAmount : String!
    var totalDistanceInMiles : String!
    var ratingOfDriver : Float!
    var destinationAddress : String!
    var customerId : String!
    var estimatedMinPrice : Double!
    var startLocation : LocationModel!
    var pickupTime : String!
    var shippingItems: [ShippingItemsDriver]!
    var extraDestinationLocations : [ExtraDestinationLocationDriver]!
    var destinationLocation : LocationModel!
    var createdAt : Int!
    var orderNumber : Int!
    var truckType : String!
    var pickupDate : String!
    var startAddress : String!
    var estimatedMaxPrice : Double!
    var status :OrderStatusType!
    var estimatedTime : Int!
    var contentDescription : String!
    var _id : String!
    var isMultiOrder : Bool!
    var immediate : Bool!
    var totalDistance : Int!
    var ratingOfUser : Float!
    var updatedAt : Int!
    var scheduleTime : String!
    var scheduleDate : String!
    var extraStartLocations : [ExtraStartLocationDriver]!
    var isTestOrder : Bool!
    var destinationAddressUnitNumber:String!
    var startAddressUnitNumber:String!
    init(json : JSON) {
        tip = json["tip"].string ?? "0.0"
        totalCost = json["totalCost"].string ?? "0.0"
        cost = json["cost"].string ?? "0.0"
        orderMiles = OrderMiles(json : json["orderMiles"])
        
        totalEarningAmount = json["totalEarningAmount"].string ?? "0.0"
        totalDistanceInMiles = json["totalDistanceInMiles"].string ?? "0.0"
        ratingOfDriver = json["ratingOfDriver"].float ?? 0.0
        destinationAddress = json["destinationAddress"].string ?? ""
        destinationAddressUnitNumber =  json["destinationAddressUnitNumber"].string ?? ""
        customerId = json["customerId"].string ?? ""
        estimatedMinPrice = json["estimatedMinPrice"].double ?? 0.0
        startLocation = LocationModel(json : json["startLocation"])
        let pickUpFullDate = json["dateTime"].string ?? ""
        let localDate = HelperFunction.getCurrentFullDate(date: pickUpFullDate)
        pickupDate = HelperFunction.getCurrentDateWithoutDayName(date: localDate)
        pickupTime = HelperFunction.getTime(date: localDate)
        
        shippingItems = [ShippingItemsDriver]()
        if let shippingArray = json["shippingItems"].array
        {
            for item in shippingArray{
                shippingItems.append(ShippingItemsDriver(json :item))
            }
        }
        extraDestinationLocations = [ExtraDestinationLocationDriver]()
        if let extraDestinations = json["extraDestinationLocations"].array
        {
            for item in extraDestinations
            {
                let model = ExtraDestinationLocationDriver(json : item)
                extraDestinationLocations.append(model)
            }
        }
        destinationLocation = LocationModel(json : json["destinationLocation"])
        createdAt = json["createdAt"].int ?? 0
        orderNumber = json["orderNumber"].int ?? 0
        truckType = json["truckType"].string ?? ""
        startAddress = json["startAddress"].string ?? ""
        startAddressUnitNumber = json["startAddressUnitNumber"].string ?? ""
        estimatedMaxPrice = json["estimatedMaxPrice"].double ?? 0
        status  = OrderStatusType(rawValue: json["status"].string ?? "TIMEOUT") ?? .timeout
        estimatedTime = json["estimatedTime"].int ?? 0
        contentDescription = json["contentDescription"].string ?? ""
        _id = json["_id"].string ?? ""
        isMultiOrder = json["isMultiOrder"].bool ?? false
        immediate = json["immediate"].bool ?? false
        totalDistance = json["totalDistance"].int ?? 0
        ratingOfUser = json["ratingOfUser"].float ?? 0.0
        updatedAt = json["updatedAt"].int ?? 0
        if immediate{
            scheduleTime = json["scheduleTime"].string ?? pickupDate
            scheduleDate = json["scheduleDate"].string ?? pickupTime
        }
        else{
            let scheduleDateTime = json["scheduleDateTime"].string ?? ""
            let localScheduleDate = HelperFunction.getCurrentFullDate(date: scheduleDateTime)
            scheduleDate = HelperFunction.getCurrentDateWithoutDayName(date: localScheduleDate)
            scheduleTime = HelperFunction.getTime(date: localScheduleDate)
        }
        extraStartLocations = [ExtraStartLocationDriver]()
        if let extraStartLocationes = json["extraStartLocations"].array
        {
            for item in extraStartLocationes
            {
                let model = ExtraStartLocationDriver(json : item)
                extraStartLocations.append(model)
            }
        }
        isTestOrder  = json["isTestOrder"].bool ?? false
    }
}
