//
//  VehicleVerificationModel.swift
//  EZER
//
//  Created by Akash on 12/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class VehicleVerificationModel: NSObject {
    
    var imageType   : String = ""
    var imageURL    : String = ""
    var image       : UIImage?
    
}
