//
//  VehicleTypeModel.swift
//  EZER
//
//  Created by TimerackMac1 on 16/03/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import Foundation
import SwiftyJSON
class VehicleTypeModel
{
    let id : String
    let name : String
    let type : String
    init(json : JSON)
    {
        id = json["id"].string ?? ""
        name = json["name"].string ?? ""
        type = json["type"].string ?? ""
    }
}
