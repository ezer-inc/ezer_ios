//
//  DriverAvailableJobs.swift
//  EZER
//
//  Created by Akash on 15/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

class CustomerDetail {
    var firstName: String!
    var lastName : String!
    
    init(withCoreData dataModel: CDCustomerDetail) {
        firstName = dataModel.firstName ?? ""
        lastName = dataModel.lastName ?? ""
    }
    
    init(json :  JSON) {
        lastName    = json["lastName"].string ?? ""
        firstName   = json["firstName"].string ?? ""
    }
}

class DriverAvailableJobs: NSObject {
    var customerDetail: CustomerDetail!
    var customerId: String!
    var driverName :String!
    var orderId :String!
    var orderNumber:Int!
    var orderDate : String!
    var startAddress : String!
    var pickUpFullDate : String!
    var pickupDate: String!
    var pickupTime : String!
    var destinationAddress :String!
    var userNotes : String!
    var contentDescription : String!
    var status :OrderStatusType!
    var extraStartLocations : [ExtraStartLocationDriver]!
    var extraDestinationLocations : [ExtraDestinationLocationDriver]!
    var shippingItems = [ShippingItemsDriver]()
    var startLocation : LocationModel!
    var destinationLocation : LocationModel!
    var driverId : String!
    var isMultiOrder: Bool!
    var immediate : Bool!
    var multiWOStatus : OrderStatusType!
    var stopPoint:Int!
    var allImages = [String]()
    var descriptionToShow:String=""
    var startAddressUnitNumber:String!
    var destinationAddressUnitNumber:String!
    var isReAssigned:Bool!
    var estimatedMinPrice:Double!
    var estimatedMaxPrice:Double!
    var totalDistance:Double!
    var orderPickupInterval:Double!
    var podRequired: Bool!
    
    var beforeLoading = [CDImage]()
    var afterLoading = [CDImage]()
    var beforeUnLoading = [CDImage]()
    var afterUnLoading = [CDImage]()
    var podImages = [CDImage]()
    var pendingOrderStatus = [OrderStatusCD]()
    var driverSpecialRate: Double!
    var isSpecialRateApplied: Bool!
    
    init(withCoreData dataModel: CDDriverJob) {
        if let customer = dataModel.customerDetail {
            customerDetail = CustomerDetail(withCoreData: customer)
        }
        customerId = dataModel.customerId ?? ""
        driverName = dataModel.driverName ?? ""
        orderId = dataModel.orderId ?? ""
        orderNumber = Int(dataModel.orderNumber ?? "")
        orderDate = dataModel.orderDate ?? ""
        startAddress = dataModel.startAddress ?? ""
        pickUpFullDate = dataModel.pickUpFullDate ?? ""
        pickupDate = dataModel.pickupDate ?? ""
        pickupTime = dataModel.pickupTime ?? ""
        destinationAddress = dataModel.destinationAddress ?? ""
        userNotes = dataModel.userNotes ?? ""
        contentDescription = dataModel.contentDescription ?? ""
        status = OrderStatusType(rawValue: dataModel.status ?? "")
        driverId = dataModel.driverId ?? ""
        isMultiOrder = dataModel.isMultiOrder
        immediate = dataModel.immediate
        multiWOStatus = OrderStatusType(rawValue: dataModel.multiWOStatus ?? "")
        stopPoint = Int(dataModel.stopPoint ?? "0")
        startAddressUnitNumber = dataModel.startAddressUnitNumber ?? ""
        destinationAddressUnitNumber = dataModel.destinationAddressUnitNumber ?? ""
        isReAssigned = dataModel.isReAssigned
        estimatedMinPrice = dataModel.estimatedMinPrice
        estimatedMaxPrice = dataModel.estimatedMaxPrice
        totalDistance = dataModel.totalDistance
        orderPickupInterval = dataModel.orderPickupInterval
        podRequired = dataModel.podRequired
        driverSpecialRate = dataModel.driverSpecialRate
        isSpecialRateApplied = dataModel.isSpecialRateApplied

        if let extraStartLoc = dataModel.extraStartLocations {
            extraStartLocations = [ExtraStartLocationDriver]()
            for loc in extraStartLoc{
                if let data = loc as? CDExtraStartLocationDriver{
                    extraStartLocations.append(ExtraStartLocationDriver(withCoreData: data))
                }
            }
        }
        
        if let extraDestLoc = dataModel.extraDestinationLocations {
            extraDestinationLocations = [ExtraDestinationLocationDriver]()
            for loc in extraDestLoc{
                if let data = loc as? CDExtraDestinationLocationDriver{
                    extraDestinationLocations.append(ExtraDestinationLocationDriver(withCoreData: data))
                }
            }
        }
        
        if let shipItems = dataModel.shippingItems {
            shippingItems = [ShippingItemsDriver]()
            for item in shipItems{
                if let data = item as? CDShippingItemsDriver {
                    shippingItems.append(ShippingItemsDriver(withCoreData: data))
                }
            }
        }
        if let cdBeforeLoading = dataModel.beforeLoading {
            for item in cdBeforeLoading {
                if let data = item as? CDImage {
                    beforeLoading.append(data)
                }
            }
        }
        if let cdAfterLoading = dataModel.afterLoading {
            for item in cdAfterLoading {
                if let data = item as? CDImage {
                    afterLoading.append(data)
                }
            }
        }
        if let cdBeforeUnLoading = dataModel.beforeUnLoading {
            for item in cdBeforeUnLoading {
                if let data = item as? CDImage {
                    beforeUnLoading.append(data)
                }
            }
        }
        if let cdAfterUnLoading = dataModel.afterUnLoading {
            for item in cdAfterUnLoading {
                if let data = item as? CDImage {
                    afterUnLoading.append(data)
                }
            }
        }
        if let cdPodImages = dataModel.podImage {
            for item in cdPodImages {
                if let data = item as? CDImage {
                    podImages.append(data)
                }
            }
        }
        if let startLoc = dataModel.startLocation {
            startLocation = LocationModel(withCoreData: startLoc)
        }
        if let destinLoc = dataModel.destinationLocation {
            destinationLocation = LocationModel(withCoreData: destinLoc)
        }
        
        if let cdOrderStatus = dataModel.pendingOrderStatus {
            for item in cdOrderStatus {
                if let data = item as? CDOrderStatus {
                    let orderStatus = OrderStatusCD(withCoreData: data)
                    pendingOrderStatus.append(orderStatus)
                }
            }
        }
    }
        
    init(json :  JSON) {
        customerDetail = CustomerDetail(json : json["customer"])
        podRequired = json["podRequired"].bool ?? false
        userNotes = json["userNotes"].string ?? ""
        if userNotes.isEmpty{
            userNotes = "No Additional Notes."
        }
        driverName = json["driverName"].string ?? ""
        orderId = json["orderId"].string ?? ""
        orderNumber = json["orderNumber"].int ?? 0
        orderDate = json["orderDate"].string ?? ""
        startAddress = json["startAddress"].string ?? ""
        startAddressUnitNumber = json["startAddressUnitNumber"].string ?? ""
        driverId = json["driverId"].string ?? ""
        pickUpFullDate = json["pickupDateTime"].string ?? ""
        
        let localDate = HelperFunction.getCurrentFullDate(date: pickUpFullDate!)
        pickupDate = HelperFunction.getCurrentDate(date: localDate)
        pickupTime = HelperFunction.getTime(date: localDate)
        
        destinationAddress = json["destinationAddress"].string ?? ""
        destinationAddressUnitNumber = json["destinationAddressUnitNumber"].string ?? ""
        status  = OrderStatusType(rawValue: json["status"].string ?? "TIMEOUT") ?? .timeout
        contentDescription = json["contentDescription"].string ?? ""
        customerId = json["customerId"].string ?? ""
        extraStartLocations = [ExtraStartLocationDriver]()
        isMultiOrder = json["isMultiOrder"].bool ?? false
        immediate = json["immediate"].bool ?? false
        isReAssigned = json["isReAssigned"].bool ?? false
        extraDestinationLocations = [ExtraDestinationLocationDriver]()
        estimatedMinPrice = json["estimatedMinPrice"].double ?? 0
        estimatedMaxPrice = json["estimatedMaxPrice"].double ?? 0
        totalDistance = json["totalDistance"].double ?? 0
        driverSpecialRate = json["driverspecialRate"].double ?? 0
        isSpecialRateApplied = json["isSpecialRateApplied"].bool ?? false
        
        if let extraDestinations = json["extraDestinationLocations"].array
        {
            for item in extraDestinations
            {
                let model = ExtraDestinationLocationDriver(json : item)
                extraDestinationLocations.append(model)
            }
        }
        if let shippingArray = json["shippingItems"].array
        {
            for item in shippingArray{
                let item = ShippingItemsDriver(json :item)
                shippingItems.append(item)
                allImages.append(contentsOf: item.images)
                descriptionToShow += "\(item.description)"
               // descriptionToShow += "\nNote: \(userNotes!)"
            }
        }
        descriptionToShow += "\n\(contentDescription!)"
        if let extraStartLocationes = json["extraStartLocations"].array
        {
            for item in extraStartLocationes
            {
                let model = ExtraStartLocationDriver(json : item)
                if !model.shippingItems.isEmpty
                {
                    let shipItem = model.shippingItems[0]
                    allImages.append(contentsOf: shipItem.images)
                   // descriptionToShow += "\n\(shipItem.description)"
                   // if !shipItem.userNotes.isEmpty
                    //{
                      //  descriptionToShow += "\nNote: \(shipItem.userNotes!)"
                    //}
                }
                
                descriptionToShow += "\n\(model.instruction!)"
                extraStartLocations.append(model)
            }
        }
        startLocation = LocationModel(json : json["startLocation"])
        destinationLocation = LocationModel(json : json["destinationLocation"])
        /** will got in specific api call*/
        multiWOStatus = OrderStatusType(rawValue: json["multiWOStatus"].string ?? "ON_THE_WAY") ?? .onTheWay
        stopPoint = Int(json["stopPoint"].string ?? "0")
        orderPickupInterval = json["orderPickupInterval"].double ?? 0.0
        
        if let pendingOrders = json["pendingOrderStatus"].array
        {
            for item in pendingOrders
            {
                let model = OrderStatusCD(json : item)
                pendingOrderStatus.append(model)
            }
        }
    }
}
class ShippingItemsDriver : NSObject {
    var unitType :String?
    var quantity: Int?
    var width : Int!
    var name : String!
    var weight : Int!
    var images :[String]!
    var height : Int!
    var length : Int!
    var po : String!
    
    init(withCoreData dataModel: CDShippingItemsDriver) {
        unitType = dataModel.unitType ?? ""
        quantity = Int(dataModel.quantity ?? "")
        width = Int(dataModel.width ?? "")
        name = dataModel.name ?? ""
        weight = Int(dataModel.weight ?? "")
        images = dataModel.images ?? [""]
        height = Int(dataModel.height ?? "")
        length = Int(dataModel.length ?? "")
        po = dataModel.po ?? ""
    }
    
    init(json : JSON) {
        unitType = json["unitName"].string
        quantity = json["quantity"].int
        width = json["width"].int ?? 0
        name = json["name"].string ?? ""
        weight = json["weight"].int ?? 0
        po = json["po"].string ?? ""
        images = [String]()
        if let imageArray = json["images"].array{
            for item in imageArray {
                images.append(item.string ?? "")
            }
        }
        height = json["height"].int ?? 0
        length = json["length"].int ?? 0
        
    }
}
class ExtraDestinationLocationDriver {
    var location : LocationModel!
    var address : String!
    var destinationAddressUnitNumber : String!
    var podRequired = false
    var beforeUnLoading = [CDImage]()
    var afterUnLoading = [CDImage]()
    var podImages = [CDImage]()
    
    init(withCoreData dataModel: CDExtraDestinationLocationDriver) {
        if let loc = dataModel.location {
            location = LocationModel(withCoreData: loc)
        }
        address = dataModel.address ?? ""
        destinationAddressUnitNumber = dataModel.destinationAddressUnitNumber ?? ""
        podRequired = dataModel.podRequired
        
        if let cdBeforeUnLoading = dataModel.beforeUnloading {
            for item in cdBeforeUnLoading {
                if let data = item as? CDImage {
                    beforeUnLoading.append(data)
                }
            }
        }
        if let cdAfterUnLoading = dataModel.afterUnloading {
            for item in cdAfterUnLoading {
                if let data = item as? CDImage {
                    afterUnLoading.append(data)
                }
            }
        }
        if let cdPodImages = dataModel.podImage {
            for item in cdPodImages {
                if let data = item as? CDImage {
                    podImages.append(data)
                }
            }
        }
    }
    
    init(json :  JSON) {
        address = json["address"].string ?? ""
        destinationAddressUnitNumber = json["addressUnitNumber"].string ?? ""
        location = LocationModel(json : json["location"])
        podRequired = json["podRequired"].bool ?? false
    }
}
class ExtraStartLocationDriver {
    var address : String!
    var startAddressUnitNumber: String!
    var location : LocationModel!
    var shippingItems = [ShippingItemsDriver]()
    var instruction : String!
    var userNotes : String!
    var beforeLoading = [CDImage]()
    var afterLoading = [CDImage]()
    
    init(withCoreData dataModel: CDExtraStartLocationDriver) {
        address = dataModel.address ?? ""
        startAddressUnitNumber = dataModel.startAddressUnitNumber ?? ""
        instruction = dataModel.instruction ?? ""
        userNotes = dataModel.userNotes ?? ""
        
        if let shipItems = dataModel.shippingItems {
            shippingItems = [ShippingItemsDriver]()
            for item in shipItems{
                if let data = item as? CDShippingItemsDriver {
                    shippingItems.append(ShippingItemsDriver(withCoreData: data))
                }
            }
        }
        if let loc = dataModel.location {
            location = LocationModel(withCoreData: loc)
        }
        if let cdBeforeLoading = dataModel.beforeLoading {
            for item in cdBeforeLoading {
                if let data = item as? CDImage {
                    beforeLoading.append(data)
                }
            }
        }
        if let cdAfterLoading = dataModel.afterLoading {
            for item in cdAfterLoading {
                if let data = item as? CDImage {
                    afterLoading.append(data)
                }
            }
        }
    }
    
    init(json :  JSON)
    {
        address = json["address"].string ?? ""
        startAddressUnitNumber = json["addressUnitNumber"].string ?? ""
        userNotes = json["userNotes"].string ?? ""
        if userNotes.isEmpty{
            userNotes = "No Additional Notes."
        }
        if let shippingArray = json["shippingItems"].array
        {
            for item in shippingArray{
                shippingItems.append(ShippingItemsDriver(json :item))
            }
        }
        instruction = json["instruction"].string ?? ""
        location = LocationModel(json : json["location"])
    }
}

class LocationModel : NSObject{
    var type: String!
    var coordinates : [Double]!
    var locationCordinate : CLLocationCoordinate2D?
    
    init(withCoreData dataModel: CDLocationModel) {
        type = dataModel.type ?? ""
        if let coordinates = dataModel.coordinates{
            self.coordinates = coordinates
            locationCordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(coordinates[1]), longitude: CLLocationDegrees(coordinates[0]))
        }
    }
    
    init(json :  JSON){
        type = json["type"].string ?? ""
        
        coordinates = [Double]()
        
        if let locationArray = json["coordinates"].array{
                for item in locationArray
                {
                    coordinates.append(item.double ?? 0.0)
                }
            locationCordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(coordinates[1]), longitude: CLLocationDegrees(coordinates[0]))
        }
       
    }
}
class OrderStatusCD: NSObject {
    var startTime: Double!
    var driverID :String!
    var multiWOStatus : OrderStatusType!
    var orderStatus :OrderStatusType!
    var stopPoint:Double!
    var images = [CDImage]()

    init(withCoreData dataModel: CDOrderStatus) {
        startTime = dataModel.startTime
        driverID = dataModel.driverID ?? ""
        multiWOStatus = OrderStatusType(rawValue: dataModel.multiWOStatus ?? "")
        orderStatus = OrderStatusType(rawValue: dataModel.orderStatus ?? "")
        stopPoint = dataModel.stopPoint ?? 0.0
        
        if let cdImages = dataModel.images {
            for item in cdImages{
                if let data = item as? CDImage {
                    images.append(data)
                }
            }
        }
    }
        
    init(json :  JSON) {
        startTime = json["startTime"].double ?? 0.0
        driverID = json["driverID"].string ?? ""
        multiWOStatus = OrderStatusType(rawValue: json["multiWOStatus"].string ?? "ON_THE_WAY")
        orderStatus  = OrderStatusType(rawValue: json["orderStatus"].string ?? "TIMEOUT") ?? .timeout
        stopPoint = json["stopPoint"].double ?? 0.0
    }
}
