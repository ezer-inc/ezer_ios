//
//  PaymentDetailCell.swift
//  EZER
//
//  Created by Naveen on 21/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class PaymentDetailCell: UITableViewCell {

    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var paymentType: UILabel!
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var date: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUpCell(historyModel: PaymentHistoryModel)
    {
        date.text = historyModel.date
        time.text = historyModel.time
        orderNumber.text = "# \(historyModel.orderNumber!)"
        paymentType.text = "\(historyModel.type!):"
        amount.text = String.init(format: "$ %.2f", historyModel.amount)
    }
}
