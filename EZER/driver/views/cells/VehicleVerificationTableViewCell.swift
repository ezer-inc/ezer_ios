//
//  VehicleVerificationTableViewCell.swift
//  EZER
//
//  Created by Akash on 12/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class VehicleVerificationTableViewCell: UITableViewCell {

    @IBOutlet weak var imageHeightContraintOutlet: NSLayoutConstraint!
    weak var delegate : DeleteImage!
    
    @IBOutlet weak var vehicleVerificationImageViewOutlet: UIImageView!
    override func awakeFromNib(){
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    @IBAction func deleteButtonClicked(_ sender: Any){
        vehicleVerificationImageViewOutlet.image = nil
        delegate.deleteImageAtIndex(tag: vehicleVerificationImageViewOutlet.tag)
    }
}

protocol DeleteImage:class
{
    func deleteImageAtIndex(tag : Int)
}
