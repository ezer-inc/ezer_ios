//
//  AccpetJobTableViewCell.swift
//  EZER
//
//  Created by Akash on 15/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

protocol AcceptJobTableViewCellDelegate: class {
    func acceptButtonClicked(driverJob :DriverAvailableJobs)
}
class AcceptJobTableViewCell: UITableViewCell {

    @IBOutlet weak var acceptedJobButtonOutlet: RoundableButton!
    var driverJobToAccept :DriverAvailableJobs!
    weak var acceptJobDelegate : AcceptJobTableViewCellDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        acceptedJobButtonOutlet.isExclusiveTouch = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func currentJob(driverJob :DriverAvailableJobs){
        driverJobToAccept = driverJob
    }
    
    
    @IBAction func acceptButtonClicked(_ sender: Any) {
        acceptJobDelegate.acceptButtonClicked(driverJob: driverJobToAccept)
    }
    

}
