//
//  PickUpDescriptionTableViewCell.swift
//  EZER
//
//  Created by Virender on 28/12/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit
protocol PickUpDescriptionTableViewCellDelegate:class {
    func PresentZoomViewController(_ url:String)
}

class PickUpDescriptionTableViewCell: UITableViewCell {
    @IBOutlet weak var imagesCollection: UICollectionView!
    @IBOutlet weak var contentDescriptionLblOutlet: UILabel!
    @IBOutlet weak var descriptionLblOutlet: UILabel!
    
    @IBOutlet weak var lblWeight: UILabel!
    
    var imagesArray : [String]!
    let cellName = "ImageUploadingCell"
    
    @IBOutlet weak var lblPickUpCount: UILabel!
    
    weak  var delegate:PickUpDescriptionTableViewCellDelegate?
    var driverJob : DriverAvailableJobs!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imagesCollection.register(UINib(nibName:cellName ,bundle: nil), forCellWithReuseIdentifier: cellName)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setUpCell(_ indexPath: IndexPath, driverJob : DriverAvailableJobs) {
        if let extralocationCount =  driverJob.extraStartLocations?.count {
            if extralocationCount > 0{
                lblPickUpCount.text = String(format: "Pick Up #%d - Item Images or Documents",indexPath.row+1)
            }else{
                lblPickUpCount.text = String(format: "Item Images or Documents")
            }
        }else{
            lblPickUpCount.text = String(format: "Item Images or Documents")
        }
        
        if indexPath.row == 0{
            if driverJob.shippingItems.count > 0{
                imagesArray = driverJob.shippingItems[0].images
                // imagesArray = driverJob.allImages
                //descriptionLblOutlet.text = "\(driverJob.shippingItems[0].description)\n\(driverJob.contentDescription!)"
                descriptionLblOutlet.text = driverJob.contentDescription
                contentDescriptionLblOutlet.text = driverJob.userNotes
                
                if let quantity = driverJob.shippingItems[0].quantity, let unitType = driverJob.shippingItems[0].unitType {
               
                        lblWeight.text = String(format: "Weight: %d lb\n%d in. (H) x %d in. (L) x %d in. (W)\nQuantity: %d\nUnit Type: %@\nReference No: %@",driverJob.shippingItems[0].weight,driverJob.shippingItems[0].height,driverJob.shippingItems[0].length,driverJob.shippingItems[0].width,quantity,unitType,driverJob.shippingItems[0].po)
                  
                  
                }else {
                  
                        lblWeight.text = String(format: "Weight: %d lb\n%d in. (H) x %d in. (L) x %d in. (W)\nReference No: %@",driverJob.shippingItems[0].weight,driverJob.shippingItems[0].height,driverJob.shippingItems[0].length,driverJob.shippingItems[0].width,driverJob.shippingItems[0].po)
                   
                  
                }
                lblWeight.setAttText()
                
                imagesCollection.reloadData()
            }
            
        }else{
            let row = indexPath.row - 1
            if driverJob.extraStartLocations.indices.contains(row) {
                imagesArray = driverJob.extraStartLocations[row].shippingItems[0].images
                descriptionLblOutlet.text = driverJob.extraStartLocations[row].instruction
                contentDescriptionLblOutlet.text = driverJob.extraStartLocations[row].userNotes
                
                if let quantity = driverJob.extraStartLocations[row].shippingItems[0].quantity, let unitType = driverJob.extraStartLocations[row].shippingItems[0].unitType {
                    
                   
                        lblWeight.text = String(format: "Weight: %d lb\n%d in. (H) x %d in. (L) x %d in. (W)\nQuantity: %d\nUnit Type: %@\nReference No: %@",driverJob.extraStartLocations[row].shippingItems[0].weight,driverJob.extraStartLocations[row].shippingItems[0].height,driverJob.extraStartLocations[row].shippingItems[0].length,driverJob.extraStartLocations[row].shippingItems[0].width, quantity, unitType,driverJob.extraStartLocations[row].shippingItems[0].po)
                  
                    
                 
                } else {
                    
                   
                        lblWeight.text = String(format: "Weight: %d lb\n%d in. (H) x %d in. (L) x %d in. (W)\nReference No: %@",driverJob.extraStartLocations[row].shippingItems[0].weight,driverJob.extraStartLocations[row].shippingItems[0].height,driverJob.extraStartLocations[row].shippingItems[0].length,driverJob.extraStartLocations[row].shippingItems[0].width,driverJob.extraStartLocations[row].shippingItems[0].po)
                   
                   
                }
                lblWeight.setAttText()
                
                imagesCollection.reloadData()
            }
        }
    }
}

extension PickUpDescriptionTableViewCell:UICollectionViewDataSource,UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if imagesArray == nil {
            return 0
        }
        return imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellName, for: indexPath) as! ImageUploadingCell
        cell.deleteImage.isHidden = true
        cell.setUpImage(stringUrl:imagesArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = imagesCollection.cellForItem(at: indexPath) as! ImageUploadingCell
        if cell.uploadedImage.image != nil {
            delegate?.PresentZoomViewController(imagesArray[indexPath.row])
        }
    }
}
