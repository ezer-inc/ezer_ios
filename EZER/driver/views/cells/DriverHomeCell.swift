//
//  HomeTableCell.swift
//  EZER
//
//  Created by TimerackMac1 on 23/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

protocol DriverHomeCellDelegate: class {
    func lblSeleted(isMyjobsSeleted:Bool)
}

class DriverHomeCell: UITableViewCell {

    @IBOutlet weak var fwdButton: UIButton!
    @IBOutlet weak var leftSideLabel_out: UILabel!
    @IBOutlet weak var rightSide_out: UILabel!
    @IBOutlet weak var header: UILabel!
    weak var delegate: DriverHomeCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func addGestures(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(DriverHomeCell.leftTapFunction))
        leftSideLabel_out.isUserInteractionEnabled = true
        leftSideLabel_out.addGestureRecognizer(tap)
        
        let rightTap = UITapGestureRecognizer(target: self, action: #selector(DriverHomeCell.rightTapFunction))
        rightSide_out.isUserInteractionEnabled = true
        rightSide_out.addGestureRecognizer(rightTap)
    }
    
    @objc func leftTapFunction(sender:UITapGestureRecognizer) {
        print("left tap working")
        delegate?.lblSeleted(isMyjobsSeleted: false)
    }
    
    @objc func rightTapFunction(sender:UITapGestureRecognizer) {
        print("Right tap working")
        delegate?.lblSeleted(isMyjobsSeleted: true)
    }
    
    

}
