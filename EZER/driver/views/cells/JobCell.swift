//
//  JobCell.swift
//  EZER
//
//  Created by Mac mini on 15/10/19.
//  Copyright © 2019 TimerackMac1. All rights reserved.
//

import UIKit

class JobCell: UITableViewCell {
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblEarnings: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setupCell(model : PastJobModel){
        lblOrderNumber.text = "#\(model.orderNumber ?? 0)"
        if (model.immediate) {
            lblDate.text = model.pickupDate
        } else {
            lblDate.text = model.scheduleDate
        }
        let totalEarn = Double(model.totalEarningAmount)
        lblEarnings.text = String(format: "$%.2f",totalEarn ?? 0.00)
    }
}
