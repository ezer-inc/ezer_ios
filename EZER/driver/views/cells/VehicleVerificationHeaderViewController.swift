//
//  VehicleVerificationHeaderViewController.swift
//  EZER
//
//  Created by Akash on 12/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class VehicleVerificationHeaderViewController: UITableViewHeaderFooterView {

   
    @IBOutlet weak var headerLblOutlet: UILabel!
    @IBOutlet weak var cameraButtonClicked: UIButton!
    var viewController : VehicleInformationController?
    var vehicleInformationModel : VehicleVerificationModel?
    
    func setupheader(sender : VehicleInformationController, vehicleInformationModel : VehicleVerificationModel, section : Int){
        headerLblOutlet.text = vehicleInformationModel.imageType
        cameraButtonClicked.tag = section
        viewController = sender
        self.vehicleInformationModel = vehicleInformationModel
    }
    
    @IBAction func cameraButtonClicked(_ sender: Any) {
        viewController?.showActionSheet(vehicleInformationModel: vehicleInformationModel!)
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
