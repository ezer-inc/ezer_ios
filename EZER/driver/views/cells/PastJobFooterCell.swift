//
//  PastJobFooterCell.swift
//  EZER
//
//  Created by Mac mini on 15/10/19.
//  Copyright © 2019 TimerackMac1. All rights reserved.
//

import UIKit

class PastJobFooterCell: UITableViewHeaderFooterView {
    @IBOutlet weak var lblMiles: UILabel!
    @IBOutlet weak var lblEarned: UILabel!
    @IBOutlet weak var lblPickUpMiles: UILabel!
    @IBOutlet weak var lblTip: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(model : PastJobModel){
        lblMiles.text = "\(model.totalDistanceInMiles ?? "0.0") miles"
        if model.orderMiles.distanceInMiles == "NA"{
            lblPickUpMiles.text = "NA"
        }else{
            lblPickUpMiles.text = "\(model.orderMiles.distanceInMiles  ?? "0.0") miles"        }
        lblEarned.text = "$\(model.totalCost ?? "0.0")"
        lblTip.text = "$\(model.tip ?? "0.0")"
        lblSubTotal.text = "$\(model.cost ?? "0.0")"
    }
}
