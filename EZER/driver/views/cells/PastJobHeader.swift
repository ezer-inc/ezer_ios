//
//  PastJobHeader.swift
//  EZER
//
//  Created by Naveen on 17/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class PastJobHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var orderId: UILabel!

    @IBOutlet weak var orderDate: UILabel!
    
    @IBOutlet weak var orderTime: UILabel!
    @IBOutlet weak var scheduledDate: UILabel!
    @IBOutlet weak var scheduledTime: UILabel!
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    func setupHeader(model : PastJobModel) {
        if (model.immediate) {
            scheduledDate.text = model.pickupDate
            scheduledTime.text = model.pickupTime
        } else {
            scheduledDate.text = model.scheduleDate
            scheduledTime.text = model.scheduleTime
        }
        orderDate.text = model.pickupDate
        orderTime.text = model.pickupTime
        orderId.text = String.init(format: "Order #%d", model.orderNumber)
    }
}
