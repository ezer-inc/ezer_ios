//
//  ScheduleJobTableViewCell.swift
//  EZER
//
//  Created by Akash on 15/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class ScheduleJobTableViewCell: UITableViewCell {
    

    @IBOutlet weak var startAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpCell(_ indexPath: IndexPath, driverAvailableJob : DriverAvailableJobs)
    {
    
     
        if indexPath.row == 0{
            startAddress.text = String(format: "%@\n%@",driverAvailableJob.startAddress,driverAvailableJob.startAddressUnitNumber)
        }
        else{
            let row = indexPath.row - 1
            if driverAvailableJob.extraStartLocations.indices.contains(row)
            {
                 startAddress.text = String(format: "%@\n%@",driverAvailableJob.extraStartLocations[row].address,driverAvailableJob.extraStartLocations[row].startAddressUnitNumber)
            }
            else{
                startAddress.text = ""
            }

        }
    }
    
}
