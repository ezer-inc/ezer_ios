//
//  PastJobsCell.swift
//  EZER
//
//  Created by TimerackMac1 on 24/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class PastJobsCell: UITableViewCell {
    
    @IBOutlet weak var startAddress: UILabel!
    @IBOutlet weak var destinationAddress: UILabel!
    @IBOutlet weak var addressIndex: UILabel!
    @IBOutlet weak var heartIconView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func setUpCell(_ indexPath: IndexPath, pastJobModel : PastJobModel)
    {
        
        //let count = max(pastJobModel.extraStartLocations.count,pastJobModel.extraDestinationLocations.count)
        //heartIconView.isHidden = (count == 0) ? true : false
        addressIndex.text = "\(indexPath.row+1)"
        if indexPath.row == 0{
            startAddress.text = String(format: "%@\n%@",pastJobModel.startAddress,pastJobModel.startAddressUnitNumber)
            destinationAddress.text = String(format: "%@\n%@",pastJobModel.destinationAddress,pastJobModel.destinationAddressUnitNumber)
        }
        else{
            let row = indexPath.row - 1
            if pastJobModel.extraStartLocations.indices.contains(row)
            {
                 startAddress.text = String(format: "%@\n%@",pastJobModel.extraStartLocations[row].address,pastJobModel.extraStartLocations[row].startAddressUnitNumber)
            }
            else{
                startAddress.text = ""
            }
            if pastJobModel.extraDestinationLocations.indices.contains(row)
            {
                 destinationAddress.text = String(format: "%@\n%@",pastJobModel.extraDestinationLocations[row].address,pastJobModel.extraDestinationLocations[row].destinationAddressUnitNumber)
            }
            else{
                destinationAddress.text = ""
            }
        }
    }
    
}

