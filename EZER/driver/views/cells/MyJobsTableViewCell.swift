//
//  MyJobsTableViewCell.swift
//  EZER
//
//  Created by Akash on 16/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class MyJobsTableViewCell: UITableViewCell {

    @IBOutlet weak var startAddress: UILabel!
    @IBOutlet weak var destinationAddress: UILabel!
    @IBOutlet weak var addressIndex: UILabel!
    @IBOutlet weak var heartIconImageOutlet: UIImageView!
//    @IBOutlet weak var showBottomRadiusViewOutlet: UIView!
//    
//    @IBOutlet weak var lblMiles: UILabel!
//    @IBOutlet weak var lblEstimateEarning: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpCell(_ indexPath: IndexPath, driverAvailableJob : DriverAvailableJobs)
    {
        
        let count = max(driverAvailableJob.extraStartLocations.count,driverAvailableJob.extraDestinationLocations.count)
        heartIconImageOutlet.isHidden = (count == 0) ? true : false
        addressIndex.isHidden = (count == 0) ? true : false
        addressIndex.text = "\(indexPath.row + 1)"
        
        if indexPath.row == 0{
            startAddress.text = String(format: "%@\n%@",driverAvailableJob.startAddress,driverAvailableJob.startAddressUnitNumber)
            destinationAddress.text = String(format: "%@\n%@",driverAvailableJob.destinationAddress,driverAvailableJob.destinationAddressUnitNumber)
        }
        else{
            let row = indexPath.row - 1
            if driverAvailableJob.extraStartLocations.indices.contains(row)
            {
                startAddress.text = String(format: "%@\n%@",driverAvailableJob.extraStartLocations[row].address,driverAvailableJob.extraStartLocations[row].startAddressUnitNumber)
            }
            else{
                startAddress.text = ""
            }
            if driverAvailableJob.extraDestinationLocations.indices.contains(row)
            {
                destinationAddress.text = String(format: "%@\n%@",driverAvailableJob.extraDestinationLocations[row].address,driverAvailableJob.extraDestinationLocations[row].destinationAddressUnitNumber)
            }
            else{
                destinationAddress.text = ""
            }
        }
    }
}
