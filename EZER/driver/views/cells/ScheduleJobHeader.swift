//
//  ScheduleJobHeader.swift
//  EZER
//
//  Created by Akash on 08/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class ScheduleJobHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var destinationLblOutlet: UILabel!
    @IBOutlet weak var orderIDLblOutlet: UILabel!
    @IBOutlet weak var pickupdateLblOutlet: UILabel!
    @IBOutlet weak var pickupTimeLblOutlet: UILabel!
    
    @IBOutlet weak var NotSyncedBtn: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func updateNotSynced(_ driverAvailbaleJobs: DriverAvailableJobs){
        if driverAvailbaleJobs.status == OrderStatusType.finished {
            NotSyncedBtn.isHidden = false
        }
        else{
            NotSyncedBtn.isHidden = true
        }
    }

    func updateData( driverAvailbaleJobs : DriverAvailableJobs){
        orderIDLblOutlet.text = "Order # \(driverAvailbaleJobs.orderNumber!)"
        pickupdateLblOutlet.text = driverAvailbaleJobs.pickupDate
        pickupTimeLblOutlet.text = driverAvailbaleJobs.pickupTime
    }
    
    func updateDataOrderDetailsScreen( driverAvailbaleJobs : DriverAvailableJobs){
        orderIDLblOutlet.text = "Review Order # \(driverAvailbaleJobs.orderNumber!)"
        pickupdateLblOutlet.text = driverAvailbaleJobs.pickupDate
        pickupTimeLblOutlet.text = driverAvailbaleJobs.pickupTime
    }
    
}
