//
//  JobDetailVC.swift
//  EZER
//
//  Created by Mac mini on 15/10/19.
//  Copyright © 2019 TimerackMac1. All rights reserved.
//

import UIKit

class JobDetailVC: UIViewController {
    @IBOutlet weak var tblHeightCons: NSLayoutConstraint!
    struct CellNames {
        static let headerName = "PastJobHeader"
        static let cellName = "PastJobsCell"
        static let footerName = "PastJobFooterCell"
    }

    var pastJob: PastJobModel?
    var isPayment = false
    @IBOutlet weak var pastJobsTableView: UITableView!
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tblHeightCons?.constant = self.pastJobsTableView.contentSize.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isPayment{
            self.title = "Payment History"
        }else{
            self.title = "Job History"
        }
        // Do any additional setup after loading the view.
        pastJobsTableView.reloadData()
        pastJobsTableView.register(UINib(nibName: CellNames.headerName, bundle: nil), forHeaderFooterViewReuseIdentifier: CellNames.headerName)
        pastJobsTableView.register(UINib(nibName: CellNames.footerName, bundle: nil), forHeaderFooterViewReuseIdentifier: CellNames.footerName)
        pastJobsTableView.register(UINib(nibName: CellNames.cellName, bundle: Bundle.main), forCellReuseIdentifier: CellNames.cellName)
        pastJobsTableView.estimatedRowHeight = 80
    }
    
}
extension JobDetailVC: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        if pastJob != nil{
            return 1
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let model = pastJob!
        let count = max(model.extraStartLocations.count, model.extraDestinationLocations.count)
        return count+1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier:  CellNames.cellName) as! PastJobsCell
        let model = pastJob!
        cell.setUpCell(indexPath, pastJobModel: model)
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: CellNames.headerName) as! PastJobHeader
        header.setupHeader(model: pastJob!)
        return header
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: CellNames.footerName) as! PastJobFooterCell
        footer.setupCell(model: pastJob!)
        return footer
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
}
