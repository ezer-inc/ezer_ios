//
//  LeftMenuController.swift
//  EZER
//
//  Created by TimerackMac1 on 23/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
enum DriverMenu: Int {
    case payment
    case orderHistory
    case settings
    case changePassword
    case help
    case logout
}
protocol DriverLeftMenuProtocol : class {
    func changeViewController(_ menu: DriverMenu)
}
class LeftMenuController: UIViewController,DriverLeftMenuProtocol {
    
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var profilePicView: RoundableUIImageView!
    var mainViewController : DriverHomeController!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var firstNameInitials: UILabel!
    @IBOutlet weak var userName: UILabel!
    var menuArray: [(imgName: String, heading:String)] = [ ("ic_payment", "Payment History"),
                                                           ("ic_orderlist", "Job History"),
                                                           ("ic_setting", "Settings"),
                                                           ("ic_changePassword", "Change Password"),
                                                           ("ic_help", "Help"),
                                                           ("ic_logout", "Logout")
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        //set appversion
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        lblVersion.text = String.init(format: "Version: %@", appVersionString)
        menuTableView.rowHeight = 50
        menuTableView.separatorStyle = .none
        menuTableView.register(UINib(nibName: "LeftMenuCell", bundle: nil), forCellReuseIdentifier: CellsIdentifiers.leftmenu)
        self.menuTableView.contentInset = UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
        
        menuTableView.dataSource = self
        menuTableView.delegate = self
        firstNameInitials.layer.cornerRadius = firstNameInitials.frame.width/2
        firstNameInitials.clipsToBounds = true
        
        userName.text = HelperFunction.getUserName(loginType:.driver)
        lblNumber.text = "Driver ID - \(HelperFunction.getIntFromUserDefaults(key: DriverProfileKeys.driverNumber))"
        firstNameInitials.text = HelperFunction.getUserInitials(loginType:.driver)
        menuTableView.bounces = false
        // Do any additional setup after loading the view.
    }
    
    @IBAction func privacyLinkAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: StoryboardNames.main, bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: IdentifierName.Main.termsAndPrivacy) as! TermsPrivacyController
        self.mainViewController.navigationController?.pushViewController(controller, animated: true)
        self.slideMenuController()?.closeLeft()
    }
    func changeViewController(_ menu: DriverMenu) {
        
        switch menu
        {
        case .payment:
            let pastJobs = PastJobsController(nibName: "PastJobsController", bundle: nil)
            pastJobs.isPayment = true
            self.mainViewController.navigationController?.pushViewController(pastJobs, animated: false)
//            let paymentHistory = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.paymentHistory) as! PaymentHistoryController
//            self.mainViewController.navigationController?.pushViewController(paymentHistory, animated: false)
        case .orderHistory:
            let pastJobs = PastJobsController(nibName: "PastJobsController", bundle: nil)
            self.mainViewController.navigationController?.pushViewController(pastJobs, animated: false)
        case .settings:
            let settingCon = DriverSettingController.init(nibName: "DriverSettingController", bundle: nil)
            self.mainViewController.navigationController?.pushViewController(settingCon, animated: false)
        case .changePassword:
            let storyBoard = UIStoryboard.init(name: StoryboardNames.main, bundle: nil)
            let changePwd = storyBoard.instantiateViewController(withIdentifier: IdentifierName.Main.changePassword) as! ChangePwdController
            self.mainViewController.navigationController?.pushViewController(changePwd, animated: false)
        case .help:
            let helpMenu = HelpViewController(nibName:"HelpViewController",bundle: nil)
            helpMenu.modalPresentationStyle = .overCurrentContext
            helpMenu.modalTransitionStyle = .crossDissolve
            self.mainViewController.present(helpMenu, animated: true, completion: {})
        case .logout:
            self.mainViewController.logoutUser()
        }
        self.slideMenuController()?.closeLeft()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let picUrl = URL(string:HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.profilePic.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? ""))
        {
            profilePicView.af_setImage(withURL: picUrl)
        }
        userName.text = HelperFunction.getUserName(loginType:.driver)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension LeftMenuController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = DriverMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
}

extension LeftMenuController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellsIdentifiers.leftmenu) as! LeftMenuCell
        let menu = menuArray[indexPath.row]
        cell.menuName.text = menu.heading
        cell.imgLogo.image = UIImage.init(named: menu.imgName)
        return cell
    }
    
    
}

