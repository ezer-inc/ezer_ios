//
//  TrackOldDriverController.swift
//  EZER
//
//  Created by Virender on 15/02/19.
//  Copyright © 2019 TimerackMac1. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
class TrackOldDriverController: UIViewController {
    @IBOutlet weak var lblOrderNumber:AELinkedClickableUILabel!
    
    @IBOutlet weak var lblNotesTitle: UILabel!
    @IBOutlet weak var lblNotes: UILabel!
    @IBOutlet weak var btnIMHere: RoundableButton!
    @IBOutlet weak var locationMap: GMSMapView!
    var destinationCordinate : CLLocationCoordinate2D?
    var destinationMarker : GMSMarker!
    var driverAvailableJobs : DriverAvailableJobs!
    var currentCoordinate : CLLocationCoordinate2D!
    var gmsPolyline : GMSPolyline!
    var currentMarker : GMSMarker!
    var locationManager : LocationManager!
    var noteData:NotesModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        self.navigationItem.hidesBackButton = true
        locationMap.delegate = self
        btnIMHere.isEnabled = false
        lblNotes.isHidden = true
        getNotesAndSourceCoordinates()
        self.title = String(format: "#Order %d", self.driverAvailableJobs.orderNumber)
        //   self.lblOrderNumber.text =
    }
    
    override func viewWillAppear(_ animated: Bool) {
        locationManager = LocationManager(delegate:self)
        locationManager.startUpdatingLocation()
    }
    
    private func setDestinationCordinate(_ locationModel : NotesLocationModel)
    {
        destinationCordinate = locationModel.locationCordinate
        destinationMarker = GMSMarker.init(position: destinationCordinate! )
        destinationMarker.icon = #imageLiteral(resourceName: "ic_destinationLocation")
        //destinationMarker.title = addressHeader.text
        //destinationMarker.snippet = addressLabel.text
        destinationMarker.map = locationMap
    }
    
    func getNotesAndSourceCoordinates(){
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            "orderId":driverAvailableJobs.orderId
        ]
        BaseServices.SendPostJson(viewController: self,serverUrl: ServerUrls.Driver.getNotesOfReAssignedOrder, jsonToPost: param, isResponseRequired: true,showLoader : true ) { (json) in
            if let json = json{
                if json[ServerKeys.status].int == 1{
                    let data = NotesModel(json: json[ServerKeys.data])
                    self.noteData = data
                    self.manageLabelForCallDriver(message: json[ServerKeys.message].string ?? "")
                    self.lblNotesTitle.isHidden = data.reAssignNote != "" ? false : true
                    self.setDestinationCordinate(data.sourceLocation)
                    self.locationManager.stopUpdatingLocation()
                    self.locationManager.startUpdatingLocation()
                    if data.isIAmHere{
                        self.btnIMHere.setTitle("Resume Order", for: .normal)
                        self.disableSendButton(isDisabled: false)
                    }else{
                        self.btnIMHere.setTitle("I'M  HERE", for: .normal)
                        self.handleImhere()
                    }
                    
                }
            }
        }
    }
    
    func manageLabelForCallDriver(message:String) {
        let yourAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]
        let yourOtherAttributes = [NSAttributedString.Key.foregroundColor: UIColor.orange, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]
        
        let partOne = NSMutableAttributedString(string: String(format: "%@ ",message) , attributes: yourAttributes)
        let partTwo = NSMutableAttributedString(string: noteData.phoneNumber, attributes: yourOtherAttributes)
        
        let combination = NSMutableAttributedString()
        
        combination.append(partOne)
        combination.append(partTwo)
        self.lblOrderNumber.attributedText = combination
        self.lblOrderNumber.setLinkedTextWithHandler(text: String(format: "%@ %@",message,noteData.phoneNumber), link: noteData.phoneNumber, handler: {
            if let url = URL(string: "tel://\(self.noteData.phoneNumber)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        })
    }
    
    @IBAction func btnShowOnMap(_ sender: Any) {
        var strUrl = "http://maps.google.com/maps?saddr=\(currentCoordinate.latitude),\(currentCoordinate.longitude)&daddr=\(destinationCordinate!.latitude),\(destinationCordinate!.longitude)"
        if currentCoordinate != nil{
            if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)
            {
                strUrl = "comgooglemaps://?saddr=\(currentCoordinate.latitude),\(currentCoordinate.longitude)&daddr=\(destinationCordinate!.latitude),\(destinationCordinate!.longitude)&zoom=14&views=traffic"
            }
            
            if let url = URL(string: strUrl){
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    @IBAction func btnIMHereAction(_ sender: Any) {
        if btnIMHere.titleLabel?.text == "Resume Order"{
            getCurrentOrderForDriver(true)
        }else{
            let param: [String:Any] = [
                "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
                "orderId":driverAvailableJobs.orderId,
                "orderNumber":driverAvailableJobs.orderNumber
            ]
            BaseServices.SendPostJson(viewController: self,serverUrl: ServerUrls.Driver.iAmHereNotificationToAdmin, jsonToPost: param, isResponseRequired: true,showLoader : true ) { (json) in
                if let json = json{
                    if json[ServerKeys.status].int == 1{
                        self.btnIMHere.setTitle("Resume Order", for: .normal)
                        self.btnIMHere.alpha = 1
                        
                        self.disableSendButton(isDisabled: false)
                    }
                    print("get notes",json)
                }
            }
        }
        
    }
    
    private func disableSendButton(isDisabled: Bool = true)
    {
        if isDisabled
        {
            btnIMHere.isEnabled = false
            btnIMHere.alpha = 0.6
        }else{
            btnIMHere.isEnabled = true
            btnIMHere.alpha = 1
        }
    }
    
    private func handleImhere()
    {
        guard (currentCoordinate) != nil, let destinationCordinate = self.destinationCordinate else{
            self.view.makeToast("Not able to fetch location")
            
            return
        }
        if HelperFunction.distanceInMeters(currentCoordinate, destination: destinationCordinate) < Double(HelperFunction.getEnableIMHereButtonDistance())
        {
            
            disableSendButton(isDisabled: false)
            
        }else{
            disableSendButton(isDisabled: true)
            
        }
    }
    
    func getCurrentOrderForDriver(_ showLoader : Bool = false)
    {
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getCurrentOrderForDriver, jsonToPost: param,isResponseRequired :true ,showLoader : showLoader) { (json) in
            if let json = json{
                print(json)
                if json[ServerKeys.status].int == 1{
                    self.hidePresentedAlert(completion: {
                        let driverJob = DriverAvailableJobs.init(json: json[ServerKeys.data])
                        if driverJob.isReAssigned{
                            self.showAlertController(message: "Please contact admin for resume order.")
                        }else{
                            self.navigationController?.popViewController(animated: false)
                        }
                    })
                }
            }else{
                // print("Api Failiure")
            }
        }
    }
    
    
    
}
extension TrackOldDriverController : LocationManagerDelegate
{
    func locationDenied() {
        
    }
    
    func didChangeinLocation(cordinate: CLLocationCoordinate2D) {
        // let camera = GMSCameraPosition.camera(withLatitude: cordinate.latitude,                                              longitude: cordinate.longitude,zoom: zoomLevel)
        /*if locationMap.isHidden {
         locationMap.isHidden = false
         locationMap.camera = camera
         } else {
         locationMap.animate(to: camera)
         }*/
        if currentCoordinate == nil
        {
            guard let destinationCordinate = destinationCordinate else{ return}
            drawMap(cordinate,endCoordinate: destinationCordinate)
        }
        currentCoordinate = cordinate
        addMarkerOnMap(cordinate)
        if !noteData.isIAmHere{
            self.handleImhere()
        }
        
        // handleImhere() // enable or disable Send button for location
        //locationManager.stopUpdatingLocation()
    }
    
    private func addMarkerOnMap(_ coordinate : CLLocationCoordinate2D)
    {
        //locationMap.clear()
        if currentMarker == nil{
            currentMarker = GMSMarker.init(position: coordinate )
            currentMarker.icon = #imageLiteral(resourceName: "ic_ezerMarker")
            currentMarker.map = locationMap
        }
        else{
            currentMarker.position = coordinate
        }
    }
    
    // draw the path between driver current location and source (where driver has reach)
    func drawMap (_ startCoordinate : CLLocationCoordinate2D, endCoordinate:CLLocationCoordinate2D)
    {
        
        let str = String(format:"https://maps.googleapis.com/maps/api/directions/json?origin=\(startCoordinate.latitude),\(startCoordinate.longitude)&destination=\(endCoordinate.latitude),\(endCoordinate.longitude)&key=%@",IdentifierName.GooglePlaceApiKey)
        
        
        BaseServices.sendGetJson( serverUrl: str) { (json) in
            
            guard let resJson = json else{return}
            
            if(resJson["status"].string == "ZERO_RESULTS")
            {
                
            }
            else if(resJson["status"].string == "NOT_FOUND")
            {
                
            }
            else{
                if let routes = resJson["routes"].array
                {
                    if routes.count > 0
                    {
                        let route = routes[0].dictionary!
                        let overview_polyline = route["overview_polyline"]
                        let points = overview_polyline!["points"].string!
                        //print(points,"point")
                        self.showPath(polyStr: points)
                        if let legs = route["legs"]?.array
                        {
                            if !legs.isEmpty{
                                self.destinationMarker.map = nil
                                let leg = legs[0]
                                if let start_location = leg["start_location"].dictionary{
                                    let startLat = start_location["lat"]!.double!
                                    let startLong = start_location["lng"]!.double!
                                    let startPosition = CLLocationCoordinate2D(latitude: CLLocationDegrees(startLat), longitude: CLLocationDegrees(startLong))
                                    
                                    let end_location = leg["end_location"].dictionary!
                                    let endLat = end_location["lat"]!.double!
                                    let endLong = end_location["lng"]!.double!
                                    let endPosition = CLLocationCoordinate2D(latitude: CLLocationDegrees(endLat), longitude: CLLocationDegrees(endLong))
                                    self.addPathHeader(startPosition, address: leg["start_address"].string!, title: "Start Address",icon: #imageLiteral(resourceName: "ic_startLocation"))
                                    self.addPathHeader(endPosition, address: leg["end_address"].string!, title: "Destination Address",icon: #imageLiteral(resourceName: "ic_destinationLocation"))
                                    let bounds = GMSCoordinateBounds(coordinate: startPosition, coordinate: endPosition)
                                    let update = GMSCameraUpdate.fit(bounds, withPadding: 30)
                                    self.locationMap.animate(with: update)
                                    //self.locationMap?.moveCamera(update)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    //add map path header
    private func addPathHeader(_ coordinate : CLLocationCoordinate2D,address: String,title: String,icon: UIImage)
    {
        let headeMarker = GMSMarker.init(position: coordinate )
        headeMarker.icon = icon
        headeMarker.title = title
        headeMarker.snippet = address
        headeMarker.map = locationMap
    }
    // draw poly line on path
    private func showPath(polyStr : String)
    {
        let newPath = GMSPath.init(fromEncodedPath: polyStr)
        self.gmsPolyline = GMSPolyline(path: newPath)
        self.gmsPolyline.strokeWidth = 4
        self.gmsPolyline.strokeColor = UIColor.gray
        self.gmsPolyline.map = self.locationMap
        
    }
    func didErrorinLocation(error: Error) {
        locationManager.stopUpdatingLocation()
    }
    
    func locationNotAvailable() {
        DispatchQueue.main.async {
            if let _ = self.presentedViewController { return }
            self.showLocationAlert()
        }
    }
    func locationFaliedToUpdate(status: CLAuthorizationStatus) {
        DispatchQueue.main.async {
            self.showLocationAlert()
        }
    }
    
}
extension TrackOldDriverController : GMSMapViewDelegate{
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        return true
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        //addMrkerOnMap(coordinate:coordinate)
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        // reverseGeocodeCoordinate(coordinate: position.target)
    }
}
