//
//  DriverRegistration.swift
//  EZER
//
//  Created by Geeta on 05/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
import UIKit

class DriverRegistration: UIViewController ,UITextFieldDelegate,UINavigationControllerDelegate{
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstName: TJTextField!
    @IBOutlet weak var userProfilePic: RoundableUIImageView!
    @IBOutlet weak var email: TJTextField!
    @IBOutlet weak var confirmPassword: TJTextField!
    @IBOutlet weak var password: TJTextField!
    @IBOutlet weak var lastName: TJTextField!
    
    @IBOutlet weak var aboutInfoView: UIView!
    @IBOutlet weak var hearByLabel: UILabel!
    @IBOutlet weak var parentView: UIView!
    
    
    var profilePic:String!
    var imagePicker = UIImagePickerController()
    var hearAboutEzerValue: String!
    var hearIndex:Int = -1
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        aboutInfoView.layer.cornerRadius = 8
        aboutInfoView.layer.borderWidth = 1
        aboutInfoView.layer.borderColor = UIColor.gray.cgColor
        aboutInfoView.clipsToBounds = true
        
        firstName.delegate = self
        lastName.delegate = self
        email.delegate = self
        password.delegate = self
        confirmPassword.delegate = self
        imagePicker.delegate = self
     
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
        // navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        for index in 1...5
        {
            if let textField = parentView.viewWithTag(index) as? TJTextField{
                textField.addTarget(self, action: #selector(enterPressed), for: .editingDidEndOnExit)
                textField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(enterPressed))
            }
        }
        userProfilePic.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showActionSheet)))
        // Do any additional setup after loading the view.
    }
    @objc func enterPressed(textField : TJTextField)
    {
//        if textField.tag == 5
//        {
//            registerDriver()
//        }else{
//            parentView.viewWithTag(textField.tag + 1)?.becomeFirstResponder()
//        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Sign Up"
        self.navigationController?.navigationBar.barTintColor = UIColor.driverTheme
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func openHearAboutEzerController(_ sender: Any) {
        let paramsData : [String: Any] = [:]
        BaseServices.SendPostJson(viewController: self,method: .post ,serverUrl: ServerUrls.Customer.ezerQsnAsn, jsonToPost: paramsData) { (result) in
            if let result = result{
                if(result[ServerKeys.status].int == ServerStatusCode.Success)
                {
                    let hearAboutEzerCon = UIStoryboard(name: StoryboardNames.customer, bundle: nil).instantiateViewController(withIdentifier: IdentifierName.Customer.howDidYouHear) as! HowDidYouHear
                    hearAboutEzerCon.informationArray = result[ServerKeys.data].arrayObject
                    hearAboutEzerCon.delegate = self
                    hearAboutEzerCon.indexSelected = self.hearIndex
                    hearAboutEzerCon.userHearChoice = self.hearAboutEzerValue
                    hearAboutEzerCon.module = .driver
                    self.navigationController?.pushViewController(hearAboutEzerCon, animated: true)
                }
            }
        }
    }

    func validatePassword() -> Bool {
        if(password.text!.length < 6)
        {
            password .errorEntry = true
            self.view.makeToast(ValidationMessages.passwordLenghthError, duration: 3.0, position: .bottom)
            return true
        }else if(password.text != confirmPassword.text)
        {
            confirmPassword .errorEntry = true
            self.view.makeToast(ValidationMessages.passwordNotMatch, duration: 3.0, position: .bottom)
            return true
        }
        
        return false
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == firstName ||  textField == lastName{
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= IdentifierName.nameLength
        }
        else{
            return true
        }
    }
    func checkValidation()->Bool
    {
        var error = false
        
        firstName.text = firstName?.text?.trimWhiteSpace()
        lastName.text = lastName?.text?.trimWhiteSpace()
        email.text = email?.text?.trimWhiteSpace()
        password.text = password?.text?.trimWhiteSpace()
        confirmPassword.text = confirmPassword?.text?.trimWhiteSpace()
        
        
        if(!HelperFunction.validateName(name: firstName.text!))
        {
            firstName.errorEntry = true
            error = true
        }else
        
        if(!HelperFunction.validateName(name: lastName.text!))
        {
            lastName.errorEntry = true
            error = true
        }else
        
        if( !HelperFunction.validateEmail(email:email.text!))
        {
            email.errorEntry = true
            error = true
             self.view.makeToast(ValidationMessages.invalidEmail , duration: 3.0, position: .bottom)
        }
        else if (!HelperFunction.validatePassword(password:password.text!)){
            password.errorEntry = true
            error = true
             self.view.makeToast(ValidationMessages.invalidPassword , duration: 3.0, position: .bottom)
        }
        else if(!password.hasText)
        {
            password.errorEntry = true
            error = true
        }else
        if(!confirmPassword.hasText)
        {
            confirmPassword.errorEntry = true
            error = true
        }
        
        return error
    }
    
    func validateAboutText() ->Bool {
        if(hearAboutEzerValue == nil ||  hearAboutEzerValue.isEmpty)
        {
            aboutInfoView.layer.borderColor = UIColor.red.cgColor
            return true
        }
        return false
    }
    
    
    @objc func showActionSheet()
    {
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Select Mode", message: nil, preferredStyle:UIAlertController.Style.actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Close", style: .cancel) { _ in
            
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            
            self.checkCameraPermission(completion: { (action) in
                if action {
                    if UIImagePickerController.isSourceTypeAvailable(.camera){
                        self.imagePicker.allowsEditing = false
                        self.imagePicker.sourceType = .camera
                        self.imagePicker.cameraDevice = .front
                        self.imagePicker.cameraCaptureMode = .photo
                        self.imagePicker.modalPresentationStyle = .fullScreen
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                }
                else{
                    self.showCameraAlert()
                }
            })
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
           // self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.imagePicker.modalPresentationStyle = .popover
            self.imagePicker.popoverPresentationController?.sourceView = self.view
            self.imagePicker.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            self.imagePicker.popoverPresentationController?.permittedArrowDirections = []
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.view
        actionSheetControllerIOS8.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        actionSheetControllerIOS8.popoverPresentationController?.permittedArrowDirections = []
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    
    
    @IBAction func buttonsCallBack(_ sender: UIButton) {
        switch sender.tag {
        case 1://open camera options
            showActionSheet()
            break
        case 2:
            break
        case 3:
            showActionSheet()
            break
        case 5: //resgister user
            self.view.endEditing(true)
            registerDriver()
            break
        default:
            break
        }
    }
    
    
    
    func registerDriver() {
        if checkValidation() {
            if(!HelperFunction.validateName(name: firstName.text!)){
                self.view.makeToast(ValidationMessages.invalidFirstName, duration: 3.0, position: .bottom)
            }else if(!HelperFunction.validateName(name: lastName.text!)){
                self.view.makeToast(ValidationMessages.invalidLastName, duration: 3.0, position: .bottom)
            }
            return
        }
        
        if(validatePassword()) {return}
        
        if(validateAboutText()) { return }
        
//        if(!HelperFunction.validateEmail(email:email.text!))
//        {
//            email.errorEntry = true
//            self.view.makeToast(ValidationMessages.invalidEmail , duration: 3.0, position: .bottom)
//            return
//        }
        
        if(profilePic == nil || profilePic.isEmpty)
        {
            self.showAlertController(message: ValidationMessages.profilePicError)
            return
        }
        
        let paramneter :[String:Any] = [
            "email":email.text!,
            "password":password.text!,
            "firstName":firstName.text!,
            "lastName":lastName.text!,
            "profilePic":profilePic,
            "installationId":HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.deviceToken),
            "allowPush": true,
            "allowEmail":true,
            "deviceToken": HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.deviceToken),
            "ezerQA":hearAboutEzerValue
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.register, jsonToPost: paramneter, completion: { (result) in
            
            if (result != nil)
            {
                self.view.showMessage("\(String(describing: result!["message"]))", type: GSMessageType.success)
                self.loginDriver()
            }
            
            
        })
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let tjTextField = textField as! TJTextField
        tjTextField.errorEntry = false
    }
    
    private func loginDriver()
    {
        let params :[String:Any] = [
            "email":email.text!,
            "password":password.text!,
            "deviceType": "ios",
            "model": UIDevice.modelName,
            "appVersion": HelperFunction.getVersion(),
            "deviceToken": HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.deviceToken)
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.login, jsonToPost: params) { (json) in
            if let json = json
            {
                print(json)
                HelperFunction.saveValueInUserDefaults(key: ProfileKeys.authToken, value: json[ServerKeys.data][ProfileKeys.authToken].string ?? "")
                HelperFunction.saveValueInUserDefaults(key: ProfileKeys.email, value: self.email.text!)
                HelperFunction.saveValueInUserDefaults(key: ProfileKeys.password, value: self.password.text!)
                let model = DriverLoginModel(json: json[ServerKeys.data])
                HelperFunction.saveDriverInPref(loginModel: model)
                self.openDriverInformation(driverLoginModel : model)
            }
        }
    }
    
    private func openDriverInformation(driverLoginModel : DriverLoginModel)
    {
        
        let driverInfoController = self.storyboard?.instantiateViewController(withIdentifier:IdentifierName.Driver.driverInformation) as! DriverInformationController
        driverInfoController.driverLoginModel = driverLoginModel
        
        self.navigationController?.pushViewController(driverInfoController, animated: true)
    }
    private func uploadImageToServer(image : UIImage)
    {
        self.showProgressBar()
        BaseServices.uploadImageToServer(image: image, imageType: ImageType.user.rawValue, role: Roles.driver.rawValue,mimeType: "image/png") { (json) in
            self.hideProgressBar()
            if let json = json
            {
                if let url = json[ServerKeys.data].string
                {
                    self.profilePic = url
                }
            }else{
                self.showMessage(ValidationMessages.tryAgain, type: .error)
            }
        }
    }
}

extension DriverRegistration : UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        picker.dismiss(animated: true, completion: nil)
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage{
            let circleCropController = KACircleCropViewController(withImage: image)
            circleCropController.delegate = self
            present(circleCropController, animated: false, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        self.showMessage("Picture selection cancelled", type: .error)
    }
}

extension DriverRegistration: KACircleCropViewControllerDelegate {
    
    func circleCropDidCancel() {
        //Basic dismiss
        dismiss(animated: false, completion: nil)
    }
    
    func circleCropDidCropImage(_ image: UIImage) {
        //Same as dismiss but we also return the image
        let newImage = image.resizeImage(newWidth: 200)
        userProfilePic.image = newImage
        //var data = image.convertImageToBase64(quality: 0.7, imageFormat: .png)
        //data = "data:image/png;base64,\(data)"
        dismiss(animated: false, completion: nil)
        uploadImageToServer(image: newImage)
    }
    
}


//MARK:- Hear About delegate
extension DriverRegistration:ClassHearAboutDelegate{
    
    func getHearAboutEzer(_ value: String?,_ index: Int!) {
        hearAboutEzerValue = value
        hearByLabel.text = hearAboutEzerValue
        aboutInfoView.layer.borderColor = UIColor.gray.cgColor
        hearIndex = index
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
