//
//  DriverHomeExtension.swift
//  EZER
//
//  Created by TimerackMac1 on 01/02/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit
import Foundation
extension DriverHomeController {
    
    public func getCurrentOrderForDriver(_ showLoader : Bool = false) {
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getCurrentOrderForDriver, jsonToPost: param,isResponseRequired :true ,showLoader : showLoader) { (json) in
            if let json = json{
                print(json)
                if json[ServerKeys.status].int == 1 {
                    self.hidePresentedAlert(completion: {
                        let driverJob = DriverAvailableJobs.init(json: json[ServerKeys.data])
                        CDDriverJobManager.shared.updateCurrentOrder(driverJob.orderId, status: driverJob.status.rawValue, multiWOStatus: driverJob.multiWOStatus.rawValue, stopPoint: driverJob.stopPoint) { (error) in
                            guard let error = error else {return}
                            print(error)
                        }
                        if driverJob.isReAssigned {
                            HelperFunction.saveValueInUserDefaults(key: UserDefaultkeys.currentOrderID, value: driverJob.orderId ?? "")
                            let trackOldDriverController = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.trackOldDriverController) as! TrackOldDriverController
                            trackOldDriverController.driverAvailableJobs = driverJob
                            self.navigationController?.pushViewController(trackOldDriverController, animated: true)
                        }else{
                            if driverJob.status == .pause{
                                SocketRouterForPauseController.managePauseViewController(data:driverJob.orderId)
                            }else{
                                if driverJob.status != .cancelled {
                                    HelperFunction.saveValueInUserDefaults(key: UserDefaultkeys.currentOrderID, value: driverJob.orderId ?? "")
                                    self.checkCurrentOrderFromDB()
                                }
                            }
                        }
                    })
                }else {
                    HelperFunction.saveValueInUserDefaults(key: UserDefaultkeys.currentOrderID, value: "")
                    
                }
            }else{
                // print("Api Failiure")
            }
        }
    }
    
    func checkCurrentOrderFromDB() {
        let orderId = HelperFunction.getSrtingFromUserDefaults(key: UserDefaultkeys.currentOrderID)
        if !orderId.isEmpty {
            CDDriverJobManager.shared.getCurrentOrder(orderId) { (result) in
                switch result {
                case .success(let driverJob):
                    if (driverJob.status != OrderStatusType.finished){
                        let orderProcess = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.orderProcessView) as! OrderProcessViewController
                        orderProcess.driverJob = driverJob
                        self.navigationController?.pushViewController(orderProcess, animated: true)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func updateMyOrders(_ showLoader: Bool = true){
        // check current order is in progress
        let orderId = HelperFunction.getSrtingFromUserDefaults(key: UserDefaultkeys.currentOrderID)
        if !orderId.isEmpty {
            self.getCurrentOrderForDriver(true)
            return
        }
        // else refresh all orders again
        
        let params :[String:Any] = [
            "driverId":HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id)
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getOrderForDriver, jsonToPost: params,isResponseRequired:true,showLoader :showLoader) { (json) in
            if let json = json  {
                if let scheduleJobsData = json[ServerKeys.data].array {
                    var myJobs = [DriverAvailableJobs]()
                    for job in scheduleJobsData{
                        let model = DriverAvailableJobs(json : job)
                        myJobs.append(model)
                    }
                    CDMyJobsManager.shared.save(myJobs) { (error) in
                        guard let error = error else {return}
                        print(error)
                    }
                    // check current order if any
                    self.getCurrentOrderForDriver(true)
                }
            }
        }
    }
    func updateDriverLocation() {
        //        let unfinishedOrder = DataBaseHandler.dataBaseHandler.getUnfinishedOrders()
        //        //print(unfinishedOrder,"unfinished Order")
        //        if let unfinishedOrder = unfinishedOrder{
        //            currentOrderId = unfinishedOrder.orderId
        //        }
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            "longitude":currentDriverLocation.longitude,
            "latitude":currentDriverLocation.latitude
            //            ,
            //            "orderId":currentOrderId
        ]
        print(param)
        BaseServices.SendPostJson(viewController: self,serverUrl: ServerUrls.Driver.updateDriverLocation, jsonToPost: param, isResponseRequired: true,showLoader : false ) { (json) in
            if let json = json{
                print("Location Updated",json)
            }
        }
    }
}
