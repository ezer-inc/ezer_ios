//
//  ScheduledJobsController.swift
//  EZER
//
//  Created by TimerackMac1 on 24/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
class ScheduledJobsController: UIViewController, UITableViewDataSource, UITableViewDelegate, AcceptJobTableViewCellDelegate {
    
    enum CurrentSelected:Int{
        case available
        case myjobs
    }
    
    
    @IBOutlet weak var availableJobsCountLblOutlet: UILabel!
    
    @IBOutlet weak var myJobsCoultLblOutlet: UILabel!
    @IBOutlet weak var myjobsViewOutlet: CardView!
    @IBOutlet weak var availableJobsViewOutlet: CardView!
    
    @IBOutlet weak var btnLoadMoreOutlet: RoundableButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    var availableMyJobs : AvailableMyJobs!
    var driverAvailableJobs = [DriverAvailableJobs]()
    var acceptedJobs = [DriverAvailableJobs]()
    var currentSelected : CurrentSelected = CurrentSelected.available
    var shadowOffsetWidth: Int = 0
    var shadowOffsetHeight: Int = 0
    var shadowOpacity: Float = 0.0
    var offsetCount:Int = 0
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var isGetSchedulejobsBusy = false
    var isMyJobs = false
    
    var rev = 123456
    
    var unfinishedOrderId = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = HelperFunction.getUserName(loginType: .driver)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        
        tableView.estimatedRowHeight = 80;
        self.myJobsCoultLblOutlet.text = "\(self.availableMyJobs.myJobs!)"
        self.availableJobsCountLblOutlet.text = "\(self.availableMyJobs.availableJobs!)"
        // Do any additional setup after loading the view.
        if appDelegate().reachability.connection == .unavailable {
            self.availableJobsCountLblOutlet.text = "-"
        } else {
            getScheduledJobs()
            resetScheduleOrderArray()
            getOrders(false)
        }        
        CDMyJobsManager.shared.get { (result) in
            switch result{
            case .success(let data):
                self.acceptedJobs = data
                self.myJobsCoultLblOutlet.text = "\(self.acceptedJobs.count)"
                self.tableView.reloadData()
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        if isMyJobs{
            getMyJobs()
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !self.isMovingToParent{
            refreshData(notification: nil)
        }
        addObserver()
    }
    @objc func refreshData(notification: Notification?, showLoader: Bool = false){
        if appDelegate().reachability.connection != .unavailable {
            getScheduledJobs(showLoader)
            resetScheduleOrderArray()
            getOrders(showLoader)
        }
        tableView.reloadData()
    }
    @objc func reloadScheduleOrder(notification: Notification?){
        
        getScheduledJobs(false)
        resetScheduleOrderArray()
    }
    private func addObserver()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadScheduleOrder(notification:)), name: .refreshDriverData, object: nil) //
        NotificationCenter.default.addObserver(self, selector: #selector(reloadScheduleOrder(notification:)), name: .scheduleOrderAvailble, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(orderCancelFromCustomer(notification:)), name: .orderCancelFromCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(orderCancelFromCustomer(notification:)), name: .scheduledOrderCancelFromCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData(notification:showLoader:)), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    private func removeObserver()
    {
        NotificationCenter.default.removeObserver(self)
    }
    deinit {
        removeObserver()
    }
    @objc func orderCancelFromCustomer(notification : Notification)
    {
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            if let orderId = notificationObject["orderId"] as? String
            {
                filterTableView(orderId: orderId)
            }
        }
    }
    func filterTableView(orderId:String)  {
        DispatchQueue.global(qos: .background).async {
            
            self.driverAvailableJobs = self.driverAvailableJobs.filter({ (job) -> Bool in
                return job.orderId != orderId
            })
            self.acceptedJobs = self.acceptedJobs.filter({ (job) -> Bool in
                return job.orderId != orderId
            })
            DispatchQueue.main.async {
                self.availableJobsCountLblOutlet.text = "\(self.driverAvailableJobs.count)"
                self.myJobsCoultLblOutlet.text = "\(self.acceptedJobs.count)"
                self.tableView.reloadData()
            }
        }
        
    }
    
    func filterAvailableJobs(orderId:String)  {
        DispatchQueue.global(qos: .background).async {
            
            self.driverAvailableJobs = self.driverAvailableJobs.filter({ (job) -> Bool in
                return job.orderId != orderId
            })
            DispatchQueue.main.async {
                self.availableJobsCountLblOutlet.text = "\(self.driverAvailableJobs.count)"
                self.myJobsCoultLblOutlet.text = "\(self.acceptedJobs.count)"
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserver()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getScheduledJobs(_ showLoader: Bool = true){
        if isGetSchedulejobsBusy == false{
            isGetSchedulejobsBusy = true
            guard let currentLocation = locManager.location else {
                            return
                        }
           
            let params :[String:Any] = [
                "driverId":HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
                "offset":String(offsetCount),
                "longitude":currentLocation.coordinate.longitude,
                "latitude":currentLocation.coordinate.latitude
            ]
            
            BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getScheduledJob, jsonToPost: params, isResponseRequired : true,showLoader :showLoader) { (json) in
                if let json = json {
                    //self.driverAvailableJobs.removeAll()
                    self.isGetSchedulejobsBusy = false
                    if let scheduleJobsData = json[ServerKeys.data].array{
                        for job in scheduleJobsData{
                            self.driverAvailableJobs.append(DriverAvailableJobs(json : job))
                        }
                    }
                    self.availableJobsCountLblOutlet.text = String(json["recordCount"].int ?? 0)
                    if  self.driverAvailableJobs.count == json["recordCount"].int ?? 0{
                        self.btnLoadMoreOutlet.isEnabled = false;
                    }
                        
                    self.tableView.reloadData()
                }
                //            self.getOrders()
            }
        }
       
    }
    
    
    
    func getOrders(_ showLoader: Bool = true){
        let params :[String:Any] = [
            "driverId":HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id)
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getOrderForDriver, jsonToPost: params,isResponseRequired:true,showLoader :showLoader) { (json) in
            if let json = json  {
                self.acceptedJobs.removeAll()
                if let scheduleJobsData = json[ServerKeys.data].array{
                    for job in scheduleJobsData{
                        let model = DriverAvailableJobs(json : job)
                        if (model.status != .driverReview && model.status != .pause) || model.immediate {
                            self.unfinishedOrderId = model.orderId
                        }
                        self.acceptedJobs.append(model)
                    }
                }
                CDMyJobsManager.shared.save(self.acceptedJobs) { (error) in
                    guard let error = error else {return}
                    print(error)
                }
            }
            if self.currentSelected == .myjobs {
                self.tableView.reloadData()
            }
            self.myJobsCoultLblOutlet.text = "\(self.acceptedJobs.count)"
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    // MARK: Available jobs clicked...
    @IBAction func availableJobsClicked(_ sender: Any) {
        btnLoadMoreOutlet.isHidden = false
        currentSelected = CurrentSelected.available
        showCurrentSelected()
        if (appDelegate().reachability.connection != .unavailable) {
            getScheduledJobs()
            resetScheduleOrderArray()
        }
        tableView.reloadData()
    }
    
    @IBAction func btnLoadMore(_ sender: Any) {
        offsetCount = offsetCount + 1;
        getScheduledJobs(true)
    }
    
    func resetScheduleOrderArray(){
        self.driverAvailableJobs.removeAll()
        offsetCount = 0
    }
    
    
    // MARK: My jobs clicked...
    @IBAction func myJobsClicked(_ sender: Any) {
        getMyJobs()
    }
    
    func getMyJobs() {
        btnLoadMoreOutlet.isHidden = true
        currentSelected = CurrentSelected.myjobs
        showCurrentSelected()
        if (appDelegate().reachability.connection != .unavailable) {
            self.getOrders()
        }
        else {
            CDMyJobsManager.shared.get(completionHandler: { (result) in
                switch result {
                case .success(let data):
                    self.acceptedJobs = data
                    self.myJobsCoultLblOutlet.text = "\(self.acceptedJobs.count)"
                case .failure(let error):
                    print(error.localizedDescription)
                }
            })
        }
        tableView.reloadData()
    }
    
    
    func showCurrentSelected(){
        if currentSelected == CurrentSelected.available{
            myjobsViewOutlet.layer.shadowOpacity = 0.0
            myjobsViewOutlet.layer.borderWidth = 0.0
            
            availableJobsViewOutlet.layer.shadowOpacity = 0.5
            availableJobsViewOutlet.layer.borderWidth = 0.5
        }else{
            myjobsViewOutlet.layer.shadowOpacity = 0.5
            myjobsViewOutlet.layer.borderWidth = 0.5
            myjobsViewOutlet.layer.borderColor = UIColor.white.cgColor
            let shadowPath = UIBezierPath(roundedRect: myjobsViewOutlet.bounds, cornerRadius: 2)
            myjobsViewOutlet.layer.shadowOffset = CGSize(width: 0, height: 3);
            myjobsViewOutlet.layer.shadowPath = shadowPath.cgPath
            myjobsViewOutlet.layer.masksToBounds = false
            
            availableJobsViewOutlet.layer.shadowOpacity = 0.0
            availableJobsViewOutlet.layer.borderWidth = 0.0
        }
           // tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if  currentSelected == CurrentSelected.available {
            return   driverAvailableJobs.count
        }else{
            return   acceptedJobs.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if  currentSelected == CurrentSelected.available {
            let headerView = Bundle.main.loadNibNamed("ScheduleJobHeader", owner: self, options: nil)![0] as! ScheduleJobHeader
            headerView.destinationLblOutlet.isHidden = false
            headerView.NotSyncedBtn.isHidden = false
          
            if self.driverAvailableJobs[section].immediate{
                headerView.NotSyncedBtn.backgroundColor = UIColor(hex: "FF7F41")
                headerView.NotSyncedBtn.setTitle("Now", for: .normal)
            }else{
                headerView.NotSyncedBtn.backgroundColor = UIColor(hex: "45D2A6")
                headerView.NotSyncedBtn.setTitle("Schedule", for: .normal)
            }
            headerView.updateData(driverAvailbaleJobs: self.driverAvailableJobs[section])
            return headerView
        }else{
            let headerView = Bundle.main.loadNibNamed("ScheduleJobHeader", owner: self, options: nil)![0] as! ScheduleJobHeader
            headerView.destinationLblOutlet.isHidden = false
            headerView.updateNotSynced(self.acceptedJobs[section])
            headerView.updateData(driverAvailbaleJobs: self.acceptedJobs[section])
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let driverAvailableJob : DriverAvailableJobs!
        
        if  currentSelected == CurrentSelected.available {
            driverAvailableJob = driverAvailableJobs[section]
            let count = max(driverAvailableJob.extraStartLocations.count, driverAvailableJob.extraDestinationLocations.count)
            return count + 2
            
        }else{
            driverAvailableJob = acceptedJobs[section]
            let count = max(driverAvailableJob.extraStartLocations.count, driverAvailableJob.extraDestinationLocations.count)
            return count + 2
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if  currentSelected == CurrentSelected.available {
            let driverAvailableJob = driverAvailableJobs[indexPath.section]
            //            let count = driverAvailableJob.extraStartLoications.count
            let count = max(driverAvailableJob.extraStartLocations.count, driverAvailableJob.extraDestinationLocations.count)
            
            if indexPath.row == count + 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier:  "acceptCell") as! AcceptJobTableViewCell
                cell.acceptJobDelegate = self
                if  driverAvailableJobs[indexPath.section].immediate{
                    cell.acceptedJobButtonOutlet.setTitle("View Order Details", for: .normal)
                }else{
                    cell.acceptedJobButtonOutlet.setTitle("Move to My Jobs", for: .normal)
                }
              
                cell.currentJob(driverJob: driverAvailableJobs[indexPath.section])
                return cell;
                
            }else {
                let  cell = tableView.dequeueReusableCell(withIdentifier:  "myJobsCell") as! MyJobsTableViewCell
                cell.setUpCell(indexPath, driverAvailableJob: driverAvailableJobs[indexPath.section])
                //                let  cell = tableView.dequeueReusableCell(withIdentifier:  "sourceDestinationCell") as! ScheduleJobTableViewCell
                //                cell.setUpCell(indexPath, driverAvailableJob: driverAvailableJobs[indexPath.section])
                return cell
            }
        }
        else{
            let driverAvailableJob = acceptedJobs[indexPath.section]
            let count = max(driverAvailableJob.extraStartLocations.count, driverAvailableJob.extraDestinationLocations.count)
            
            if indexPath.row == count + 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier:  "acceptCell") as! AcceptJobTableViewCell
                cell.acceptJobDelegate = self
                cell.acceptedJobButtonOutlet.setTitle("View Order Detail", for: .normal)
                cell.currentJob(driverJob: acceptedJobs[indexPath.section])
                return cell
                
            }else {
                let  cell = tableView.dequeueReusableCell(withIdentifier:  "myJobsCell") as! MyJobsTableViewCell
                cell.setUpCell(indexPath, driverAvailableJob: acceptedJobs[indexPath.section])
                return cell;
            }
            
        }
        
    }
    
    
    func acceptButtonClicked(driverJob: DriverAvailableJobs) {
        var isTwoOrdersInOffline = 0
        if currentSelected == CurrentSelected.available {
//            if driverJob.immediate == true{
//                let driverDetailCntr = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.driverOrderDetail) as! DriverOrderDetailController
//                driverDetailCntr.driverJob = driverJob
//                self.navigationController?.pushViewController(driverDetailCntr, animated: true)
//            }else{
                jobAccepted(driverJob: driverJob)
           // }
            
        }else {
            CDMyJobsManager.shared.get(completionHandler: { (result) in
                switch result {
                case .success(let data):
                    if data.count != 0 {
                        for (_, job) in data.enumerated() {
                            if job.pendingOrderStatus.count != 0 {
                                isTwoOrdersInOffline = isTwoOrdersInOffline + 1
                                if isTwoOrdersInOffline == 2 {
                                    break
                                }
                            }
                        }
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            })
            
            if (driverJob.status != OrderStatusType.finished) {
                if appDelegate().reachability.connection == .unavailable && isTwoOrdersInOffline == 2 {
                    self.showAlertController(message: ValidationMessages.notMoreThanTwoOrderProcess)
                }
                else{
                    let driverDetailCntr = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.driverOrderDetail) as! DriverOrderDetailController
                    driverDetailCntr.driverJob = driverJob
                    driverDetailCntr.unfinishedOrderId = unfinishedOrderId
                    self.navigationController?.pushViewController(driverDetailCntr, animated: true)
                }
            }
            else{
                self.showAlertController(message: ValidationMessages.completedOrderOffline)
            }
        }
    }
    
    func jobAccepted(driverJob: DriverAvailableJobs){
        var pickUpLocation = Dictionary<String,Any>()
        pickUpLocation["type"] = driverJob.startLocation.type
        pickUpLocation["coordinates"] = driverJob.startLocation.coordinates
        
        var dropLocation = Dictionary<String,Any>()
        dropLocation["type"] = driverJob.destinationLocation.type
        dropLocation["coordinates"] = driverJob.destinationLocation.coordinates
        
        var params :[String:Any] = [
            "customerId" : driverJob.customerId,
            "driverId" : HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            "orderId" : driverJob.orderId,
            "pickupLocation" : pickUpLocation,
            "dropLocation" : dropLocation
        ]
        
        
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.driverOrderStart, jsonToPost: params, isResponseRequired: true,  embedAuthTocken: true) {  (json) in
            if let json = json {
                if json[ServerKeys.status].int  == ServerStatusCode.Failure
                {
                    let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                                        self.navigationController?.popViewController(animated: true)
                                    })
                    self.showAlertController(heading: AppName, message: json[ServerKeys.message].string!, action: action)
                    return
                }else{
                    if driverJob.immediate{
                        let driverDetailCntr = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.driverOrderDetail) as! DriverOrderDetailController
                                       driverDetailCntr.driverJob = driverJob
                        driverDetailCntr.unfinishedOrderId = self.unfinishedOrderId
                       self.navigationController?.pushViewController(driverDetailCntr, animated: true)
                    }else{
                        driverJob.status = .driverReview
                        self.acceptedJobs.append(driverJob)
                        self.filterAvailableJobs(orderId: driverJob.orderId)
                    }
                    
                   
    //                CDMyJobsManager.shared.addWorkOrder(driverJob, completionHandler: { (error) in
    //                    guard let error = error else {return}
    //                    print(error)
    //                })
                }
              
            }
        }
    }
    /*private func  emitToSocket(status : String,driverJob: DriverAvailableJobs)
     {
     var customerData: [String:Any] =  ["status" : status]
     customerData["orderId"] = driverJob.orderId
     if (IdentifierName.driverCurrentLocation != nil){
     customerData["sourceLocation"] = ["lat":IdentifierName.driverCurrentLocation.latitude,
     "lng":IdentifierName.driverCurrentLocation.longitude]
     }
     SocketIOManager.sharedInstance.sendChangeOrderStatus(toCustomerId: driverJob.customerId, customerData: customerData)
     }*/
}
