//
//  PaymentDetailController.swift
//  EZER
//
//  Created by Naveen on 21/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class PaymentDetailController: UIViewController {
    private let cellName = "paymentDetailCell"
    @IBOutlet weak var paymentDetailTable: UITableView!
    var historyMainModel: HistoryMainModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = historyMainModel.weekInterval
        paymentDetailTable.register(UINib.init(nibName: "PaymentDetailCell", bundle: nil), forCellReuseIdentifier: cellName)
         paymentDetailTable.dataSource = self
        paymentDetailTable.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PaymentDetailController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellName) as! PaymentDetailCell
        cell.setUpCell(historyModel: historyMainModel.historyModelList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyMainModel.historyModelList.count
    }
}
