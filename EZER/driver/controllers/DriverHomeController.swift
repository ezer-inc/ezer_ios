//
//  DriverHomeController.swift
//  EZER
//
//  Created by TimerackMac1 on 23/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import CoreLocation
import AudioToolbox
class DriverHomeController: UIViewController, DriverHomeCellDelegate{
   
    
    enum HomeOption:Int{
        case scheduledJobs = 0
        case pastJobs
        case accountDetails
        case paymentHistory
    }
    
    var headers = [
        ["Scheduled Jobs","Available","My Jobs","0","0"],
        ["See Past Jobs","This month","Total","0","0"],
        ["Account Details","Client Rating","Cancel Rate","0%","0%"],
        ["Payment History","This Week","This Month","$0","$0"]
    ]
    
    @IBOutlet weak var onDemandOrderView: OnDemandOrderView!
    var myJobs : Int = 0
    var availableJobs : Int = 0
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var driverStatusView_Out: DriverStatusView!
    var locationManager : LocationManager!
    var driverLoginModel : DriverLoginModel!
    let homeCellName = "DriverHomeCell"
    var cancelOrderSec = IdentifierName.cancelOrderTimeout
    var cancelOrderTimer:  Timer!
    var locManager = CLLocationManager()
    var currentDriverLocation : CLLocationCoordinate2D!
    {
        didSet{
            IdentifierName.driverCurrentLocation = currentDriverLocation
        }
    }
    
    var currentOrderId : String = "222"
    var isClickedOnStatus = false
    
    var strLabel = UILabel()
       let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        let loadingTextLabel = UILabel()
    
    var indicator: UIActivityIndicatorView!
    
    var alertSyncProgress = UIAlertController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        //self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "nav_back")
        //self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "nav_back")
        if let dict = HelperFunction.getDictFromUserDefault(key: UserDefaultkeys.isNotification) {
            if dict.count > 0 {
                self.pushToReviewOrderViewController(notification: dict)
            }
        }
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        driverStatusView_Out.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDriverStatusViewTap)))
        self.navigationController?.navigationBar.titleTextAttributes =  [NSAttributedString.Key.foregroundColor:UIColor.white]
        homeTableView.dataSource = self
        homeTableView.delegate = self
        
        locationManager = LocationManager(delegate: self)
        if driverLoginModel.status == .available || driverLoginModel.status == .busy
        {
            driverStatusView_Out.status = true
            locationManager.startUpdatingLocation()
        }
        onDemandOrderView.isHidden = true
        onDemandOrderView.acceptButton.addTarget(self, action: #selector(acceptOnDemandOrder), for: .touchUpInside)
       
        addObserver()
       // SocketManager.sharedInstance.startSocket()
    }
    
    private func playSound(_ soundType:SoundType)
    {
        var soundURL: NSURL?
        var soundID: SystemSoundID = 0
        let filePath = Bundle.main.path(forResource: soundType.rawValue, ofType: "mp3")
        soundURL = NSURL(fileURLWithPath: filePath!)
        if let url = soundURL {
            AudioServicesCreateSystemSoundID(url, &soundID)
            //AudioServicesPlaySystemSound(soundID)
            AudioServicesPlayAlertSound(soundID)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = HelperFunction.getUserName(loginType:.driver)
        if appDelegate().reachability.connection == .unavailable {
            CDMyJobsManager.shared.get { (result) in
                switch result {
                case .success(let data):
                    self.myJobs = data.count
                    self.headers = [
                        ["Scheduled Jobs","Available","My Jobs","-", "\(self.myJobs)"],
                        ["See Past Jobs","This month","Total","-","-"],
                        ["Account Details","Client Rating","Cancel Rate","-","-"],
                        ["Payment History","This Week","This Month","-","-"]
                    ]
                    
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
            checkCurrentOrderFromDB()
        } else{
            getAccountDetails(false)
            checkLocationService()
            if !appDelegate().isCurrentOrderSync {
                updateMyOrders()
            }
            else {
                self.showProgressBarWithText()
            }
        }
    }

    private func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData(notification:)), name: .scheduleOrderAvailble, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleNewOrder(notification:)), name: .immediateOrderAvailble, object: nil)//
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData(notification:)), name: .refreshDriverData, object: nil)// when new scheduled job available
        NotificationCenter.default.addObserver(self, selector: #selector(handleOrderCancelFromCustomer(notification:)), name: .scheduledOrderCancelFromCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleOrderCancelFromCustomer(notification:)), name: .orderCancelFromCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground(notification:)), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func handleOrderCancelFromCustomer(notification:Notification)
    {
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            if let orderId = notificationObject["orderId"] as? String
            {
                if orderId == currentOrderId
                {
                    resetTimer()
                    onDemandOrderView.isHidden = true
                    currentOrderId = ""
                }
                else{
                    getAccountDetails(false)
                }
            }
        }
    }
    
    private func removeObserver(){
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func refreshData(notification: Notification)
    {
        //AudioServicesPlaySystemSound(1054)
        if let _ = notification.object as? Int{
            self.playSound(SoundType.scheduled)
        }
        getAccountDetails(false)
    }
    
    @objc func willEnterForeground(notification: Notification)
    {
        if HelperFunction.isLocationServiceEnabled()
        {
            if self.onDemandOrderView.isHidden{
                getOnDemandOrders()
            }
            getAccountDetails(false)
            getDriverStatus(false)
        }else{
            removeCacheData()
        }
        
    }
    
    @objc func handleNewOrder(notification: Notification)
    {
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            //print(notificationObject)
            if let isImmediate = notificationObject["isImmediate"] as? Bool
            {
                self.playSound(SoundType.immediate)
                if isImmediate{
                    onDemandOrderView.isHidden = false
                    cancelOrderSec = IdentifierName.cancelOrderTimeout
                    resetTimer()
                    cancelOrderTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimerForCurrentOrder), userInfo: nil, repeats: true)
                    currentOrderId = notificationObject["orderId"] as! String
                    self.onDemandOrderView.orderAwayTime.text = ""
                    if let sourceLocation = notificationObject["sourceLocation"] as? [String:Any]
                    {
                        let orderlat = sourceLocation["lat"] as! Double
                        let orderlong = sourceLocation["lng"] as! Double
                        getEstimateDistanceAndTime(lattitude: orderlat,longitude: orderlong)
                    }
                }
            }
            
        }
    }
    
    private func resetTimer()
    {
        onDemandOrderView.orderAcceptTime.text = "\(cancelOrderSec) sec left"
        if cancelOrderTimer != nil && cancelOrderTimer.isValid
        {
            cancelOrderTimer.invalidate()
        }
    }
    
    @objc func acceptOnDemandOrder()
    {
        let scheduledJobs = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.reviewOrderViewController) as! ReviewOrderViewController
        scheduledJobs.orderID = currentOrderId
        onDemandOrderView.isHidden = true
        scheduledJobs.delegate = self
        currentOrderId = ""
        if cancelOrderTimer.isValid{
            cancelOrderTimer.invalidate()
        }
        //            scheduledJobs.driverJob = driverJob
        self.navigationController?.pushViewController(scheduledJobs, animated: true)
    }
    
    func pushToReviewOrderViewController(notification : [String:Any]){
        DispatchQueue.main.async {
            guard let data = notification["data"] as? [String:Any] else{ return}
            guard let orderId = data["orderId"] as? String else{ return}
            let scheduledJobs = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.reviewOrderViewController) as! ReviewOrderViewController
            scheduledJobs.orderID = orderId
            self.onDemandOrderView.isHidden = true
            scheduledJobs.delegate = self
            self.currentOrderId = ""
            self.navigationController?.pushViewController(scheduledJobs, animated: true)
        }
    }
    
    private func getEstimateDistanceAndTime(lattitude: Double,longitude: Double)
    {
        if currentDriverLocation != nil{
            let params :[String:Any] = [
                "startlat":currentDriverLocation.latitude,
                "endlat":lattitude,
                "startlong":currentDriverLocation.longitude,
                "endlong":longitude
            ]
            BaseServices.SendPostJson(viewController: self ,serverUrl: ServerUrls.Customer.getTotalDistanceAndTime, jsonToPost: params, isResponseRequired : true,showLoader: false) { (json) in
                if let json = json
                {
                    let data = json[ServerKeys.data]
                    if let timeInHours = data["timeInHours"].string
                    {
                        let miles = data["distanceInMiles"].double ?? 0.0
                        if miles == 1{
                            self.onDemandOrderView.orderAwayTime.text = String(format: "%@ away\n%.2f mile",timeInHours,miles)
                        }else{
                            self.onDemandOrderView.orderAwayTime.text = String(format: "%@ away\n%.2f miles",timeInHours,miles)
                        }
                    }
                }
            }
        }
    }
    
    @objc func updateTimerForCurrentOrder()
    {
        cancelOrderSec -= 1
        if cancelOrderSec < 0
        {
            cancelOrderTimer.invalidate()
            onDemandOrderView.isHidden = true
            cancelOrder() // cancel order from driver side if order not accepted
        }
        else{
            onDemandOrderView.orderAcceptTime.text = "\(cancelOrderSec) sec left"
        }
        
    }
    
    func cancelOrder() {
        //self.currentOrderId = ""
        if currentOrderId.isEmpty
        {return}
        let cancelParams :[String:Any] = [
            "orderId":currentOrderId,
            "driverId" : HelperFunction.getUserId()
            
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.orderTimeout, jsonToPost: cancelParams) { (json) in
            if let _ = json
            {
                self.currentOrderId = ""
                // print("order cancelled")
            }
        }
    }
    
    @objc func handleDriverStatusViewTap()
    {
        isClickedOnStatus = true
        if driverLoginModel.status == .available
        {
            updateDriverAvailabilityStatus(showLoader: true,statusToSend: .unavailable)
            locationManager.stopUpdatingLocation()
        }
        else if driverLoginModel.status == .busy
        {
            updateDriverAvailabilityStatus(showLoader: true,statusToSend: .unavailable)
        }
        else{
            showAlertForAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    private func showAlertForAlwaysAuthorization()
    {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            let alertDialog = UIAlertController.init(title: "Alert", message: ValidationMessages.driverLocationAlert, preferredStyle: .alert)
            alertDialog.addAction(UIAlertAction(title: "OK", style: .default))
            alertDialog.addAction(UIAlertAction(title: "Go To Setting", style: .default)
            {
                action in
                let url = URL(string: UIApplication.openSettingsURLString)!
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            })
            self.present(alertDialog, animated: true)
        }
    }
    
    func lblSeleted(isMyjobsSeleted: Bool) {
        let availableMyJobs = AvailableMyJobs()
        availableMyJobs.availableJobs   = self.availableJobs
        availableMyJobs.myJobs          = self.myJobs
        let scheduledJobs = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.scheduledJobs) as! ScheduledJobsController
        scheduledJobs.availableMyJobs = availableMyJobs
        scheduledJobs.isMyJobs = isMyjobsSeleted
        self.navigationController?.pushViewController(scheduledJobs, animated: true)
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    private func updateDriverAvailabilityStatus(showLoader : Bool = false, statusToSend: DriverStatusTypes? = nil)
    {
        var  status = driverLoginModel.status.rawValue
        
        if let driverStaus = statusToSend
        {
            status = driverStaus.rawValue
        }
        
        if currentDriverLocation == nil
        {
            currentDriverLocation = CLLocationCoordinate2D(latitude: CLLocationDegrees.init(1.0), longitude: CLLocationDegrees.init(1.0))
        }
        
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            "status": status,
            "longitude":currentDriverLocation.longitude,
            "latitude":currentDriverLocation.latitude
        ]
        
        BaseServices.SendPostJson(viewController: self,serverUrl: ServerUrls.Driver.updateDriverAvailabilityStatus, jsonToPost: param, isResponseRequired: true ,showLoader :showLoader) { (json) in
            if let json = json{
                //                self.driverStatusView_Out.status = !self.driverStatusView_Out.status
                guard let serverStatus = json["availabiltyStatus"].string else{return }
                if let stausFromServer = DriverStatusTypes(rawValue: serverStatus){
                    self.driverLoginModel.status = stausFromServer
                   
                }
                else{
                    self.driverLoginModel.status = .unavailable
                }
                if self.driverLoginModel.status == .available{
                    self.getAccountDetails(false)
                }
                if self.driverLoginModel.status == .available || self.driverLoginModel.status == .busy{
                    self.driverStatusView_Out.status =  true
                }
                else{
                    self.driverStatusView_Out.status =  false
                    self.locationManager.stopUpdatingLocation()
                }
                
                if json[ServerKeys.status].int == 1 && showLoader
                {
                    if self.driverLoginModel.status == .busy{
                        self.showMessage(ValidationMessages.statusBusy, type: .error)
                    }
                    else if self.driverLoginModel.status == .unavailable && statusToSend == .available{
                        self.showMessage(ValidationMessages.ezerNotActive, type: .error)
                    }
                }
                HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.availibiltyStatus, value: self.driverLoginModel.status.rawValue)
            }
        }
    }
    
    func getAccountDetails(_ showLoader : Bool = true)
    {
        guard let currentLocation = locManager.location else {
                        return
                    }
        
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            "longitude":currentLocation.coordinate.longitude,
            "latitude":currentLocation.coordinate.latitude
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getDriverAccountDetail, jsonToPost: param,showLoader : showLoader) { (json) in
            if let json = json{
                
                let data = json[ServerKeys.data].dictionary
                let currentMonthJobCount = data!["currentMonthJobCount"]?.int
                let totalJobCount = data!["totalJobCount"]?.int
                self.availableJobs = data!["availableJobs"]?.int ?? 0
                self.myJobs = data!["myJobs"]?.int ?? 0
                let monthlyPayment = data!["monthlyPayment"]?.int
                let weeklyPayment = data!["weeklyPayment"]?.int
                let clientRating = data!["clientRating"]?.int
                let cancelRate = data!["cancelRate"]?.int
                
                self.headers = [
                    
                    ["Scheduled Jobs","Available","My Jobs",String(self.availableJobs),String(self.myJobs)],
                    ["See Past Jobs","This month","Total",String(currentMonthJobCount!),String(totalJobCount!)],
                    ["Account Details","Client Rating","Cancel Rate",String(format: "%d%%",clientRating!),String(format: "%d%%",cancelRate!)],
                    ["Payment History","This Week","This Month",String(format: "$%d",weeklyPayment!),String(format: "$%d",monthlyPayment!)]
                ]
                
                self.homeTableView.reloadData()
                
            }
            
        }
    }
    func getDriverStatus(_ showLoader : Bool = true) {
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getdriverStatus, jsonToPost: param,showLoader : showLoader) { (json) in
            if let json = json, let status =  json["availabiltyStatus"].string {
                self.driverLoginModel.status = DriverStatusTypes(rawValue: status) ?? .unavailable
                if self.driverLoginModel.status == .available || self.driverLoginModel.status == .busy{
                    self.driverStatusView_Out.status =  true
                }
                else{
                    self.driverStatusView_Out.status =  false
                    self.locationManager.stopUpdatingLocation()
                }
                HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.availibiltyStatus, value: self.driverLoginModel.status.rawValue)
            }
            
        }
    }
    
    private func getOnDemandOrders(_ showLoader : Bool = false)
    {
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id)
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getOnDemandOrder, jsonToPost: param,isResponseRequired :true ,showLoader : showLoader) { (json) in
            if let json = json{
                if json[ServerKeys.status].int == 1 {
                    self.playSound(SoundType.immediate)
                    let availableJobs = DriverAvailableJobs(json: json[ServerKeys.data])
                    self.cancelOrderSec = IdentifierName.cancelOrderTimeout
                    self.resetTimer()
                    self.onDemandOrderView.isHidden = false
                    self.cancelOrderTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimerForCurrentOrder), userInfo: nil, repeats: true)
                    self.currentOrderId = availableJobs.orderId
                    self.onDemandOrderView.orderAwayTime.text = ""
                    if let sourceLocation = availableJobs.startLocation
                    {
                        let orderlat = sourceLocation.coordinates[1]
                        let orderlong = sourceLocation.coordinates[0]
                        self.getEstimateDistanceAndTime(lattitude: orderlat,longitude: orderlong)
                    }
                }else{
                    self.resetTimer()
                    self.onDemandOrderView.isHidden = true
                }
            }else{
                // print("Api Failiure")
            }
            
        }
    }
    
    private func createAttributedString(_ startText : String, endText :String)-> NSAttributedString
    {
        let mutableAttributedString = NSMutableAttributedString(string:startText,attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize:36)] )
        mutableAttributedString.append(NSAttributedString(string:"\n\(endText)",attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)] ))
        return mutableAttributedString
    }
    
    // MARK:- logout user
    func logoutUser()
    {
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            "deviceToken": HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.deviceToken)
            
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.logout, jsonToPost: param) { (json) in
            if (json != nil){
                self.removeCacheData()
            }
            
        }
    }
    
    private func removeCacheData()
    {
        self.navigationController?.dismiss(animated: false)
        removeObserver()
        SocketManager.sharedInstance.exitFromSocketEvent()
        SocketManager.sharedInstance.closeConnection()
        SocketManager.sharedInstance.isBackground = true
        HelperFunction.clearUserDefault()
        
        let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
        let mainViewController = storyboard.instantiateInitialViewController()
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.backgroundColor = UIColor.driverTheme
        delegate.window?.rootViewController = mainViewController!
        delegate.window?.makeKeyAndVisible()
    }
}


extension DriverHomeController: UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: homeCellName) as! DriverHomeCell
        cell.header.text = headers[indexPath.row][0]
        cell.leftSideLabel_out.attributedText = createAttributedString(headers[indexPath.row][3], endText: headers[indexPath.row][1])
        cell.rightSide_out.attributedText = createAttributedString(headers[indexPath.row][4], endText: headers[indexPath.row][2])
        cell.delegate = self
        switch HomeOption(rawValue: indexPath.row)! {
        case .scheduledJobs, .pastJobs, .paymentHistory:
            cell.fwdButton.isHidden = false
        case .accountDetails:
            cell.fwdButton.isHidden = true
        }
        
        switch HomeOption(rawValue: indexPath.row)! {
        case .scheduledJobs:
            cell.addGestures()
          
        case .pastJobs: break
            
        case .accountDetails:break
            
        case .paymentHistory:break
            
        }
        
        return cell
    }
    
}


extension DriverHomeController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch HomeOption(rawValue: indexPath.row)! {
        case .scheduledJobs:
            //            getScheduledJobsDetail()
            
            
            //            let orderProcess = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.orderProcessView) as! OrderProcessViewController
            //            self.navigationController?.pushViewController(orderProcess, animated: true)
            break
        case .pastJobs:
            openJOBHistory()
        //            getPastJobs()
        case .paymentHistory:
            openJOBHistory(isPayement: true)
            //            let paymentHistory = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.paymentHistory) as! PaymentHistoryController
        //            self.navigationController?.pushViewController(paymentHistory, animated: true)
        default:
            break
        }
    }
    private func openJOBHistory( isPayement: Bool = false){
        let pastJobs = PastJobsController(nibName: "PastJobsController", bundle: nil)
        pastJobs.isPayment = isPayement
        self.navigationController?.pushViewController(pastJobs, animated: true)
    }
}
extension DriverHomeController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        //print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        // print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        //print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        //print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        //print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        //print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        //print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        //print("SlideMenuControllerDelegate: rightDidClose")
    }
}

extension DriverHomeController : LocationManagerDelegate {
    func didChangeinLocation(cordinate: CLLocationCoordinate2D) {
        DispatchQueue.main.async {
            if self.currentDriverLocation == nil{ // first time update driver location when app opened
                self.currentDriverLocation = cordinate
                self.updateDriverLocation()
                self.getOnDemandOrders()
            }
            //self.locationManager.stopUpdatingLocation()
            if self.isClickedOnStatus { // if tapped on toggle button show loader
                self.currentDriverLocation = cordinate
                self.updateDriverAvailabilityStatus(showLoader:self.isClickedOnStatus,statusToSend: .available)
                self.isClickedOnStatus = false
            }
            else{ // update driver location if he is moving
                if HelperFunction.distanceInMeters(self.currentDriverLocation,destination: cordinate) > 50{
                    self.currentDriverLocation = cordinate
                    self.updateDriverLocation()
                }
            }
        }
    }
    
    func didErrorinLocation(error: Error) {
        DispatchQueue.main.async {
            
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    func locationNotAvailable() {
        DispatchQueue.main.async {
            if let _ = self.presentedViewController { return }
            self.showLocationAlert()
        }
    }
    
    func locationDenied()
    {
        
    }
    
    func locationFaliedToUpdate(status:CLAuthorizationStatus)
    {
        DispatchQueue.main.async {
            self.showLocationAlert()
        }
    }
    public func checkLocationService()
    {
        if !HelperFunction.isLocationServiceEnabled()
        {
//            removeCacheData()
        }
    }
}

extension DriverHomeController :ReviewOrderViewControllerDelegate{
    func removeOrderView(id:String) {
        if  currentOrderId == id {
            resetTimer()
            onDemandOrderView.isHidden = true
            currentOrderId = ""
        }
        
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
