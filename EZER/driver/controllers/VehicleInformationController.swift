//
//  VehicleInformationController.swift
//  EZER
//
//  Created by Geeta on 06/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
import UIKit

class VehicleInformationController: UIViewController ,UITextFieldDelegate, UINavigationControllerDelegate{
    
    var imagePicker = UIImagePickerController()
    let vehicleDropDown = EZDropDown(frame: CGRect(x: 0, y: 0, width: 0, height: 200))
    let cellName = "vehicleVerificationCell"
    @IBOutlet weak var model: TJTextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var make: TJTextField!
    @IBOutlet weak var year: TJTextField!
    @IBOutlet weak var vehcleType: TJTextField!
    @IBOutlet weak var licensePlate: TJTextField!
    var vehicleVerificationData :[VehicleVerificationModel] = []
    var selectedVehicleInformationModel : VehicleVerificationModel!
    var vehicleTypelist = [VehicleTypeModel]()
    var driverLoginModel : DriverLoginModel!
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraintOutlet: NSLayoutConstraint!
    var typeDataSource  = [String]()
    var typeId :String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Sign Up"
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        }

        getVehicleTypes()
        
        let backBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "ic_back"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonClicked))
        backBarButtonItem.imageInsets = UIEdgeInsets(top: 0, left: -10.0, bottom: 0, right: 10.0)
        self.navigationItem.setLeftBarButton(backBarButtonItem, animated: true)
        
        model.delegate = self
        make.delegate = self
        year.delegate = self
        licensePlate.delegate = self
        
        vehicleDropDown.delegate = self
        vehcleType.inputView = vehicleDropDown
        vehcleType.enableLongPressActions = false
        
        addData(dataType: "Photo of Insurance")
        addData(dataType: "Photo of Registration")
        addData(dataType: "Vehicle Front")
        addData(dataType: "Vehicle Right-Side")
        addData(dataType: "Vehicle Left-Side")
        addData(dataType: "Vehicle back")
        
        imagePicker.delegate = self
    }
    
    @objc func backButtonClicked(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is DriverInformationController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func addData(dataType : String){
        let vehicleData = VehicleVerificationModel()
        vehicleData.imageType = dataType
        vehicleVerificationData.append(vehicleData)
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tableViewHeightConstraintOutlet?.constant = self.tableView.contentSize.height
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if  textField == year{
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let tjTextField = textField as! TJTextField
        tjTextField.errorEntry = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func checkValidation()->Bool
    {
        var error = false
        make.text = make?.text?.trimWhiteSpace()
        year.text = year?.text?.trimWhiteSpace()
        model.text = model?.text?.trimWhiteSpace()
        self.vehcleType.text =  self.vehcleType?.text?.trimWhiteSpace()
        licensePlate.text = licensePlate?.text?.trimWhiteSpace()
        if(!make.hasText)
        {
            make.errorEntry = true
            error = true
        }
        if(!year.hasText)
        {
            year.errorEntry = true
            error = true
        }
        
        if(!model.hasText)
        {
            model.errorEntry = true
            error = true
        }
        
        if(typeId.isEmpty)
        {
            self.vehcleType.errorEntry = true
            error = true
        }
        
        if(!licensePlate.hasText)
        {
            licensePlate.errorEntry = true
            error = true
        }
        let dateModel = HelperFunction.getDateComponent(date: Date())
        if let userYear = Int(year.text!){
            if (userYear > Int(dateModel.year)!){
            year.errorEntry = true
            error = true
        }
    }
        
        if vehicleVerificationData[0].imageURL == "" && error == false{
            error = true
            self.showMessage(ValidationMessages.photoOfInsurance, type: .error,options: [.textNumberOfLines(0)])
        }
        
        if vehicleVerificationData[1].imageURL == "" && error == false {
            error = true
            self.showMessage(ValidationMessages.photoOfRegistration, type: .error,options: [.textNumberOfLines(0)])
        }
        
        if vehicleVerificationData[2].imageURL == "" && error == false{
            error = true
            self.showMessage(ValidationMessages.photoOfVehicleFrontRequired, type: .error,options: [.textNumberOfLines(0)])
        }
        
        if vehicleVerificationData[3].imageURL == ""  && error == false{
            error = true
            self.showMessage(ValidationMessages.photoOfVehicleRightRequired, type: .error,options: [.textNumberOfLines(0)])
        }
        
        if vehicleVerificationData[4].imageURL == ""  && error == false{
            error = true
            self.showMessage(ValidationMessages.photoOfVehicleLeftRequired, type: .error,options: [.textNumberOfLines(0)])
        }
        
        if vehicleVerificationData[5].imageURL == ""  && error == false{
            error = true
            self.showMessage(ValidationMessages.photoOfVehicleBackRequired, type: .error,options: [.textNumberOfLines(0)])
        }
        
        return error
    }
    
    func addVehicleInformation()  {
        let paramneter:[String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.id),
            "make": make.text!,
            "licensePlate" : licensePlate.text!,
            "insuranceImage" : vehicleVerificationData[0].imageURL,
            "registrationImage": vehicleVerificationData[1].imageURL,
            "frontImage": vehicleVerificationData[2].imageURL,
            "rightImage": vehicleVerificationData[3].imageURL,
            "leftImage":vehicleVerificationData[4].imageURL,
            "backImage": vehicleVerificationData[5].imageURL,
            "bedImage": vehicleVerificationData[5].imageURL,
            "vehicleTypeId": typeId,
            "model": model.text!,
            "year": year.text!
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.addVehicleInfo, jsonToPost: paramneter, completion: { (result) in
            if (result != nil)
            {
                self.view.showMessage("\(String(describing: result!["message"]))", type: GSMessageType.success,options: [.textNumberOfLines(0)])
                let driverInfoController = self.storyboard?.instantiateViewController(withIdentifier:IdentifierName.Driver.driverInformation) as! DriverInformationController
                 self.driverLoginModel.isVehicleInfoSubmitted = true
                driverInfoController.driverLoginModel = self.driverLoginModel
                self.navigationController?.pushViewController(driverInfoController, animated: true)
            }
            
        })
    }
    private func getVehicleTypes()
    {
        let paramneter:[String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.id),
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getVehicleTypes, jsonToPost: paramneter, completion: { (result) in
            if (result != nil)
            {
                if let jsonData = result![ServerKeys.data].array
                {   self.vehicleTypelist.removeAll()
                    for item in jsonData
                    {
                       let model = VehicleTypeModel(json: item)
                        self.typeDataSource.append(model.name)
                        self.vehicleTypelist.append(model)
                    }
                    self.vehicleDropDown.list = self.typeDataSource
                }
            }
            
        })
    }
    
    @IBAction func menuCallBack(_ sender: UIButton) {
        if(checkValidation()) {return}
        
        addVehicleInformation()
    }
    
    func showActionSheet(vehicleInformationModel : VehicleVerificationModel)
    {
        selectedVehicleInformationModel = vehicleInformationModel
        
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Select Mode", message: nil, preferredStyle:UIAlertController.Style.actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Close", style: .cancel) { _ in
            
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            self.checkCameraPermission(completion: { (action) in
                if action {
                    if UIImagePickerController.isSourceTypeAvailable(.camera){
                        self.imagePicker.allowsEditing = false
                        self.imagePicker.sourceType = .camera
                        self.imagePicker.cameraCaptureMode = .photo
                        self.imagePicker.modalPresentationStyle = .fullScreen
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                }
                else{
                    self.showCameraAlert()
                }
            })
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            //self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.imagePicker.modalPresentationStyle = .popover
            self.imagePicker.popoverPresentationController?.sourceView = self.view
            self.imagePicker.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            self.imagePicker.popoverPresentationController?.permittedArrowDirections = []
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.view
        actionSheetControllerIOS8.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        actionSheetControllerIOS8.popoverPresentationController?.permittedArrowDirections = []
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    
    
    
    private func uploadImageToServer(image : UIImage)
    {
        self.showProgressBar()
        BaseServices.uploadImageToServer(image: image, imageType: ImageType.user.rawValue, role: Roles.driver.rawValue) { (json) in
            self.hideProgressBar()
            if let json = json
            {
                if let url = json[ServerKeys.data].string
                {
                    self.selectedVehicleInformationModel.imageURL = url
                    self.tableView.reloadData()
                }
            }
        }
    }
    
}

extension VehicleInformationController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return vehicleVerificationData.count;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("VehicleVerificationheaderView", owner: self, options: nil)![0] as! VehicleVerificationHeaderViewController
        headerView.setupheader(sender: self, vehicleInformationModel: vehicleVerificationData[section], section: section)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if vehicleVerificationData[section].imageURL != "" {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellName) as! VehicleVerificationTableViewCell
        cell.delegate = self;
        cell.vehicleVerificationImageViewOutlet.tag = indexPath.section
        if let url = URL(string:vehicleVerificationData[indexPath.section].imageURL.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? "")
        {
            cell.vehicleVerificationImageViewOutlet.af_setImage(withURL: url)
        }
        return cell
    }
}

extension VehicleInformationController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        //        let trackOrder = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.trackOrder) as! TrackOrderController
        //        trackOrder.orderId = orderHistoryList[indexPath.row].orderId
        //        self.navigationController?.pushViewController(trackOrder, animated: true)
    }
}

extension VehicleInformationController : UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        picker.dismiss(animated: true, completion: nil)
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage{
            let comImage = image.resizeImage(newWidth: 500)
            uploadImageToServer(image: comImage)
        }
        //        self.selectedVehicleInformationModel.image = image
        //        self.tableView.reloadData();
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        self.showMessage("Picture selection cancelled", type: .error)
    }
}

extension VehicleInformationController: DeleteImage
{
    func deleteImageAtIndex(tag: Int) {
        let filename = HelperFunction.getFileNameFromUrl(stringUrl: vehicleVerificationData[tag].imageURL)
        //print(filename)
        let params :[String:Any] = [
            "role":Roles.driver.rawValue,
            "image": filename,
            "imageType":ImageType.user.rawValue
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Main.deleteImageFromS3, jsonToPost: params) { (json) in
            if let _ = json
            {
                self.vehicleVerificationData[tag].imageURL = ""
                self.tableView.reloadData()
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}

extension VehicleInformationController: EZDropDownDelegate{
    func dropDownDelegate(view: EZDropDown, didSelectData data: String, withIndex index: Int) {
        self.vehcleType.text = data
        self.vehcleType.errorEntry = false
        self.typeId = self.vehicleTypelist[index].id
    }
}
