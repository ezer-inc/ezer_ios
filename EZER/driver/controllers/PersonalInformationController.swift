//
//  PersonalInformationController.swift
//  EZER
//
//  Created by Geeta on 06/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
import UIKit

extension PersonalInformationController: EZDropDownDelegate{
    func dropDownDelegate(view: EZDropDown, didSelectData data: String, withIndex index: Int) {
        if view == stateDropDown{
            self.stateName.text = data
            self.stateName.errorEntry = false
        }else if view == stateInsuranceDropDown{
            self.stateInsuranceName.text = data
            self.stateInsuranceName.errorEntry = false
        }
    }
}

extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
}

class PersonalInformationController: UIViewController , UITextFieldDelegate {
    
    let stateDropDown = EZDropDown(frame: CGRect(x: 0, y: 0, width: 0, height: 200))
    let stateInsuranceDropDown = EZDropDown(frame: CGRect(x: 0, y: 0, width: 0, height: 200))
    
    var driverLoginModel : DriverLoginModel!
    
    
    @IBOutlet weak var dateField: TJTextField!
    @IBOutlet weak var socialSecurity: TJTextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var zipCode: TJTextField!
    @IBOutlet weak var stateInsuranceName: TJTextField!
    @IBOutlet weak var stateName: TJTextField!

    @IBOutlet weak var phoneNumber: TJPhoneNumberTextField!
    @IBOutlet weak var driverLicense: TJTextField!
    
    @IBOutlet weak var city: TJTextField!
    @IBOutlet weak var address: TJTextField!
    
    @IBOutlet weak var datePickerOutlet: UIDatePicker!
    //    let datePickerView:UIDatePicker = UIDatePicker()
    var acknowledge:Bool = false
    
    let stateDataSource : [String] = {
        var statesArray = [String]()
        for state in statesList
        {
            statesArray.append(state.name)
        }
        return statesArray
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        }
        self.title = "Sign Up"
        address.delegate = self
        city.delegate = self
        zipCode.delegate = self
        driverLicense.delegate = self
        socialSecurity.delegate = self
        phoneNumber.delegate = self
        dateField.inputView = datePickerOutlet
        datePickerOutlet.addTarget(self, action: #selector(datePickerValueChanged), for: UIControl.Event.valueChanged)
        datePickerOutlet.maximumDate = Date().yesterday
        
        stateName.inputView = stateDropDown
        stateInsuranceName.inputView = stateInsuranceDropDown
        stateDropDown.list = stateDataSource
        stateInsuranceDropDown.list = stateDataSource
        stateDropDown.delegate = self
        stateInsuranceDropDown.delegate = self
        stateName.enableLongPressActions = false
        stateInsuranceName.enableLongPressActions = false
        
        phoneNumber.maxDigits = 10
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    func showViewErrors(view:UIView , error:Bool){
        if(error)
        {
            view.layer.borderColor = UIColor.red.cgColor
        }else
        {
            view.layer.borderColor = UIColor.gray.cgColor
        }
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        dateField.errorEntry = false
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateField.text = dateFormatter.string(from: sender.date)
        
    }
    @IBOutlet weak var addButtonClicked: UIButton!
    @IBAction func addButtonClicked(_ sender: Any) {
        dateField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == zipCode{
            let maxLength = 5
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == socialSecurity{
            let maxLength = 11
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length <= maxLength{
                guard let text = textField.text else { return false }
                let newString = (text as NSString).replacingCharacters(in: range, with: string)
                textField.text = HelperFunction.formattedNumber(number: newString)
                return false
            }
            return false
        }else if textField == phoneNumber{
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= IdentifierName.phoneNumber
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if  let tjTextField = textField as? TJTextField
        {
            tjTextField.errorEntry = false
        }else
        {
            let tjTextField = textField as! TJPhoneNumberTextField
            tjTextField.errorEntry = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func checkValidation()->Bool {
        var error = false
        
        address.text = address?.text?.trimWhiteSpace()
        city.text = city?.text?.trimWhiteSpace()
        zipCode.text = zipCode?.text?.trimWhiteSpace()
        driverLicense.text = driverLicense?.text?.trimWhiteSpace()
        socialSecurity.text = socialSecurity?.text?.trimWhiteSpace()
        dateField.text = dateField?.text?.trimWhiteSpace()
        phoneNumber.text = phoneNumber?.text?.trimWhiteSpace()
        stateInsuranceName.text = stateInsuranceName?.text?.trimWhiteSpace()
        stateName.text = stateName?.text?.trimWhiteSpace()
        
        if(!address.hasText)
        {
            address.errorEntry = true
            error = true
        }
        
        if(!city.hasText)
        {
            city.errorEntry = true
            error = true
        }
        
        if(!self.stateName.hasText)
        {
            self.stateName.errorEntry = true
            error = true
        }
        
        if(!zipCode.hasText)
        {
            zipCode.errorEntry = true
            error = true
        }
        
        if(!HelperFunction.isValidZip(testStr: zipCode.text!))
        {
            zipCode.errorEntry = true
            error = true
        }
        
        if(!driverLicense.hasText)
        {
            driverLicense.errorEntry = true
            error = true
        }
        if(!self.stateInsuranceName.hasText)
        {
            self.stateInsuranceName.errorEntry = true
            error = true
        }
        
        if(!socialSecurity.hasText) {
            socialSecurity.errorEntry = true
            error = true
        } else if(socialSecurity.text?.count != 11){
            socialSecurity.errorEntry = true
            error = true
        }
        
        
        if(!dateField.hasText) {
            dateField.errorEntry = true
            error = true
        }
        
        //        if(phoneNumber.errorEntry)
        //        {
        //            phoneNumber.errorEntry = true
        //            error = true
        //        }
        
        if(!phoneNumber.hasText)
        {
            phoneNumber.errorEntry = true
            error = true
        }else if(!isValidNumber(testStr: phoneNumber.text!)){
            phoneNumber.errorEntry = true
            error = true
        }
        
        return error
    }
    
    func isValidNumber(testStr:String) -> Bool {
        let statusCode = "^(\\([0-9]{3}\\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$"
        let code = NSPredicate(format:"SELF MATCHES %@", statusCode)
        return code.evaluate(with: testStr)
    }
    
    @IBAction func menuCallBack(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            sender.isSelected = !sender.isSelected
            acknowledge = sender.isSelected
            break
        case 2:
            if(checkValidation()) { return }
            
            AddPersonalinformation()
            break
        default:
            break
        }
    }
    
    func AddPersonalinformation()  {
        let paramneter:[String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.id),
            "address": address.text!,
            "city": city.text!,
            "state": stateName.text!,
            "zipCode": zipCode.text!,
            "driversLicense": driverLicense.text!,
            "stateOfIssuance": stateInsuranceName.text!,
            "socialSecurity": socialSecurity.text!,
            "phoneNumber": phoneNumber.text!,
            "dob": dateField.text!
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.addPersonalInfo, jsonToPost: paramneter, completion: { (result) in
            if (result != nil)
            {
                self.view.showMessage("\(String(describing: result!["message"]))", type: GSMessageType.success,options: [.textNumberOfLines(0)])
                if !self.driverLoginModel.isVehicleInfoSubmitted{
                    let vehicleInfoController = self.storyboard?.instantiateViewController(withIdentifier:IdentifierName.Driver.vehicleInformation) as! VehicleInformationController
                    self.driverLoginModel.isPersonalInfoSubmitted = true
                    vehicleInfoController.driverLoginModel = self.driverLoginModel
                    self.navigationController?.pushViewController(vehicleInfoController, animated: true)
                }else{
                    self.driverLoginModel.isPersonalInfoSubmitted = true
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
        })
    }
    
    
    
}

