//
//  PastJobsController.swift
//  EZER
//
//  Created by TimerackMac1 on 23/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class PastJobsController: UIViewController {
    
    var isPayment = false
    var pastJobs = [PastJobModel]()
    @IBOutlet weak var pastJobsTableView: UITableView!
    let cellName = "JobCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        if isPayment{
            self.title = "Payment History"
        }else{
            self.title = "Job History"
        }
        pastJobsTableView.delegate = self
        pastJobsTableView.dataSource = self
        pastJobsTableView.register(UINib(nibName: cellName, bundle: Bundle.main), forCellReuseIdentifier: cellName)
        getPastJobs()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loadMoreItem(_ sender: RoundableButton) {
        getPastJobs()
    }
    
    func getPastJobs() {
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),//"WZASt3pidFj39Jn3B"
            "offset":pastJobs.count
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getJobHistory, jsonToPost: param) { (json) in
            if let json = json{
                if let jsonData = json[ServerKeys.data].array {
                    for item in jsonData {
                        self.pastJobs.append(PastJobModel(json: item))
                    }
                    self.pastJobsTableView.reloadData()
                }
            }
        }
    }
}
extension PastJobsController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pastJobs.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: cellName) as! JobCell
        cell.setupCell(model: pastJobs[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension PastJobsController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let jobDetailVC = JobDetailVC(nibName: "JobDetailVC", bundle: nil)
        jobDetailVC.isPayment = isPayment
        jobDetailVC.pastJob = pastJobs[indexPath.row]
        self.navigationController?.pushViewController(jobDetailVC, animated: true)
    }
}
