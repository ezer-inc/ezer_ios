//
//  DriverSettingController.swift
//  EZER
//
//  Created by TimerackMac1 on 23/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class DriverSettingController: UIViewController,UITextFieldDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var firstLastNameLblOutlet: UILabel!
    @IBOutlet weak var firstName: TJTextField!
    @IBOutlet weak var lastName: TJTextField!
    @IBOutlet weak var email: TJTextField!
    @IBOutlet weak var cellPhone: TJPhoneNumberTextField!
    @IBOutlet weak var profilePic: RoundableUIImageView!
    @IBOutlet weak var switchNotification: UISwitch!
    
    
    var imagePicker = UIImagePickerController()
    var profilePicString:String = ""
    override func viewDidLoad(){
        super.viewDidLoad()
        self.title = "Settings"
        
        firstName.delegate = self
        lastName.delegate = self
        email.delegate = self
        cellPhone.delegate = self
        cellPhone.maxDigits = 10
        
        getDriverDetails()
        
        imagePicker.delegate = self
        profilePic.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showActionSheet)))
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    private func getDriverDetails(){
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.id)
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getDriverDetails, jsonToPost: param) { (json) in
            if let json = json{
                let driverLoginModel = DriverLoginModel(json: json[ServerKeys.data])
                self.firstName.text =  driverLoginModel.firstName
                self.lastName.text = driverLoginModel.lastName
                self.email.text = driverLoginModel.email
                self.cellPhone.text = driverLoginModel.cellPhone
                self.firstLastNameLblOutlet.text = "\(driverLoginModel.firstName + " " + driverLoginModel.lastName)"
                self.switchNotification.isOn = driverLoginModel.isTextNotification
                
                if let url = URL(string:driverLoginModel.profilePicURL.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? "")
                {
                    self.profilePic.af_setImage(withURL: url)
                }
            }
        }
    }
    
    
    func checkValidation()->Bool
    {
        var error = false
        firstName.text = firstName.text
        lastName.text = lastName.text?.trimWhiteSpace()
        email.text = email.text?.trimWhiteSpace()
        cellPhone.text = cellPhone.text?.trimWhiteSpace()
        
        if(!HelperFunction.validateName(name: firstName.text!))
        {
            firstName.errorEntry = true
            error = true
        }else
            
            if(!HelperFunction.validateName(name: lastName.text!))
            {
                lastName.errorEntry = true
                error = true
            }else
                
                if(!email.hasText)
                {
                    email.errorEntry = true
                    error = true
                }else
                    
                    
                    if(!cellPhone.hasText)
                    {
                        cellPhone.errorEntry = true
                        error = true
                        //        }else if(!HelperFunction.isValidNumber(testStr: cellPhone.text!)){
                    }else if cellPhone.text?.length != 14 {
                        let phoneNumber = cellPhone.text?.removeWhitespace()
                        if phoneNumber?.length != 14 {
                            cellPhone.errorEntry = true
                            self.view.makeToast(ValidationMessages.invalidPhoneNumber,duration: 3.0, position: .bottom)
                            error = true
                        }
                    }
        
        return error
    }
    
   
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if  let tjTextField = textField as? TJTextField
        {
            tjTextField.errorEntry = false
        }else
        {
            let tjTextField = textField as! TJPhoneNumberTextField
            tjTextField.errorEntry = false
        }
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
            }
        
        if textField == lastName || textField == firstName{
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= IdentifierName.nameLength
        }
        else if textField == cellPhone{
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
           // let trimmedString = newString.trimmingCharacters(in: .whitespaces)

            return newString.length <= IdentifierName.phoneNumber
        }
        return true
    }
    
    @IBAction func switchAction(_ sender: Any) {
        
        
    }
    
    
    @IBAction func saveCustomerDetails(_ sender: Any) {
        self.view.endEditing(true) // remove keyboard when user click on save button
        if(checkValidation()) {
            if(!HelperFunction.validateName(name: firstName.text!)){
                self.view.makeToast(ValidationMessages.invalidFirstName, duration: 3.0, position: .bottom)
            }else if(!HelperFunction.validateName(name: lastName.text!)){
                self.view.makeToast(ValidationMessages.invalidLastName,duration: 3.0, position: .bottom)
            }
            return
        }
        
        if(!HelperFunction.validateEmail(email: email.text!))
        {
            email.errorEntry = true;
            self.showMessage(ValidationMessages.invalidEmail, type: GSMessageType.error)
            return
        }
        
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            "email": email.text!,
            "firstName": firstName.text!,
            "lastName": lastName.text!,
            "profilePic": profilePicString,
            "phoneNumber": cellPhone.text!,
            "isTextNotification": switchNotification.isOn
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.updateDriverDetail, jsonToPost: param) { (json) in
            if let json = json{
                self.showMessage("\(json[ServerKeys.message])", type: GSMessageType.success)
                HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.cellPhone, value: self.cellPhone.text!)
                HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.lastName, value: self.lastName.text!)
                HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.firstName, value: self.firstName.text!)
                self.firstLastNameLblOutlet.text = "\(self.firstName.text!) \(self.lastName.text!)"
                HelperFunction.saveValueInUserDefaults(key: ProfileKeys.email, value: self.email.text!)
                if !self.profilePicString.isEmpty{
                    HelperFunction.saveValueInUserDefaults(key: DriverProfileKeys.profilePic, value: json["profilePic"].string ?? "")
                }
            }
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func cameraButtonClicked(_ sender: Any) {
        showActionSheet()
    }
    
    @objc func showActionSheet()
    {
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Select Mode", message: nil, preferredStyle:UIAlertController.Style.actionSheet)
        let cancelActionButton = UIAlertAction(title: "Close", style: .cancel) { _ in
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            self.checkCameraPermission(completion: { (action) in
                if action {
                    if UIImagePickerController.isSourceTypeAvailable(.camera){
                        self.imagePicker.allowsEditing = false
                        self.imagePicker.sourceType = .camera
                        self.imagePicker.cameraDevice = .front
                        self.imagePicker.cameraCaptureMode = .photo
                        self.imagePicker.modalPresentationStyle = .fullScreen
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                }
                else{
                    self.showCameraAlert()
                }
            })
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let galleryActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            //   self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.imagePicker.modalPresentationStyle = .popover
            self.imagePicker.popoverPresentationController?.sourceView = self.view
            self.imagePicker.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            self.imagePicker.popoverPresentationController?.permittedArrowDirections = []
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        actionSheetControllerIOS8.addAction(galleryActionButton)
        actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.profilePic
        actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.profilePic.bounds
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    private func uploadImageToServer(image : UIImage)
    {
        self.showProgressBar()
        BaseServices.uploadImageToServer(image: image, imageType: ImageType.user.rawValue, role: Roles.driver.rawValue,mimeType: "image/png") { (json) in
            self.hideProgressBar()
            if let json = json
            {
                if let url = json[ServerKeys.data].string
                {
                    self.profilePicString = url
                }
            }else{
                self.showMessage(ValidationMessages.tryAgain, type: .error)
            }
        }
    }
}

extension DriverSettingController : UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        picker.dismiss(animated: true, completion: nil)
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage{
            let circleCropController = KACircleCropViewController(withImage: image)
            circleCropController.delegate = self
            present(circleCropController, animated: false, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        self.showMessage("Picture selection cancelled", type: .error)
    }
}

extension DriverSettingController: KACircleCropViewControllerDelegate {
    
    func circleCropDidCancel(){
        //Basic dismiss
        dismiss(animated: false, completion: nil)
    }
    
    func circleCropDidCropImage(_ image: UIImage) {
        //Same as dismiss but we also return the image
        
        profilePic.image = image
        //var data = image.convertImageToBase64(quality: 0.7, imageFormat: .png)
        //data = "data:image/png;base64,\(data)"
        dismiss(animated: false, completion: nil)
        uploadImageToServer(image: image)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
extension String {
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }

    mutating func removeWhitespace() -> String {
        var strWithOutSpace:String = self.replace(string: " ", replacement: "")
           strWithOutSpace.insert(contentsOf: " ", at:strWithOutSpace.index(strWithOutSpace.startIndex, offsetBy: 5))
        return strWithOutSpace
    }
  }
