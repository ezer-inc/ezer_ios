//
//  AlertDialogController.swift
//  EZER
//
//  Created by Upkesh Thakur on 09/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
class DriverCancelController: UIViewController {
    var message: String!
    
    @IBOutlet weak var noButton: RoundableButton!
    @IBOutlet weak var yesButton: RoundableButton!
    @IBOutlet weak var messageLabel: UILabel!
    var actionConfirmed:((_ action: Bool)->Void)?
    var actionCanceled:((_ action: Bool)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messageLabel.text = message
        // Do any additional setup after loading the view.
        
    }
    
    
    @IBAction func handleAction(_ sender: RoundableButton) {
        switch(sender.tag)
        {
        case 1:
            self.dismiss(animated: false, completion: {
                self.actionConfirmed?(true)
            })
        
        //case 2:
        default:
            self.dismiss(animated: false, completion: {
                self.actionCanceled?(false)
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
