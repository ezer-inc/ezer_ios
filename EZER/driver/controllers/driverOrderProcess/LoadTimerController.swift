//
//  TimerController.swift
//  EZER
//
//  Created by TimerackMac1 on 25/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class LoadTimerController: UIViewController {
    
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var cancelOrderButton: RoundableButton!
    @IBOutlet weak var doneLoadingButton: RoundableButton!
    @IBOutlet weak var timerLabel: UILabel!
    weak var orderProcessCon :  OrderProcessViewController?
    var statusType : OrderStatusType!
    var timer : Timer!
    var currentTimer = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        timerLabel.layer.borderColor = UIColor(hex:"#979797").cgColor
        timerLabel.layer.borderWidth = 1
        timerLabel.layer.cornerRadius = 7
        // Do any additional setup after loading the view.
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(handleTimer), userInfo: nil, repeats: true)
        adjustViewsAccordningToStatus()
        
    }
    func adjustViewsAccordningToStatus()
    {
//        if statusType == OrderStatusType.loadTimer
//        {
//            loadingLabel.text
//        }else
        if statusType == OrderStatusType.unloadTimer{
             loadingLabel.text = "Unloading Time"
            doneLoadingButton.setTitle("DONE UNLOADING", for: .normal)
            cancelOrderButton.isHidden = true
        }
        if orderProcessCon!.currentMuliWorkStatus.isEmpty
        {
//           DataBaseHandler.dataBaseHandler.updateUnfishedOrderStatus(orderId: orderProcessCon!.driverJob.orderId, status: statusType.rawValue)
        }else{
//            DataBaseHandler.dataBaseHandler.updateUnfishedOrderStatus(orderId: orderProcessCon!.driverJob.orderId, status: orderProcessCon!.currentMuliWorkStatus ,multiWOStatus:statusType.rawValue)
//             cancelOrderButton.isHidden = true
        }
    }
    @objc func handleTimer ()
     {
        currentTimer += 1
       timerLabel.text = secondsToHoursMinutesSeconds(currentTimer)
    }
    private func secondsToHoursMinutesSeconds (_ seconds : Int) -> String {
        
        //return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        return String.init(format: "%02d:%02d:%02d", seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doneLoadingAction(_ sender: RoundableButton) {
        if timer.isValid
        {
            timer.invalidate()
        }
        if statusType == OrderStatusType.loadTimer
        {
            orderProcessCon?.changePageViewController(status: .loaded)
        }
        else if statusType == OrderStatusType.unloadTimer{
            orderProcessCon?.changePageViewController(status: .unloaded)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if timer.isValid
        {
            timer.invalidate()
        }
    }
    @IBAction func cancelOrderAction(_ sender: RoundableButton) {
        self.orderProcessCon?.cancelOrder()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
