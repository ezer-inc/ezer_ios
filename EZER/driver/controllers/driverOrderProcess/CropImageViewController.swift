//
//  CropImageViewController.swift
//  EZER
//
//  Created by Virender on 28/12/21.
//  Copyright © 2021 TimerackMac1. All rights reserved.
//

import UIKit
import CropPickerView

protocol CropImageViewControllerDelegate:class{
    func dismissPresentViewController(image:UIImage)
}
class CropImageViewController: UIViewController {
    @IBOutlet private weak var cropContainerView: UIView!
    @IBOutlet private weak var resultImageView: UIImageView!
    var cropImage:UIImage = UIImage()
    //MARK:- Custom variables
    weak var delegate:CropImageViewControllerDelegate?

    private let cropPickerView: CropPickerView = {
        let cropPickerView = CropPickerView()
        cropPickerView.translatesAutoresizingMaskIntoConstraints = false
        cropPickerView.backgroundColor = .black
        return cropPickerView
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.cropContainerView.addSubview(self.cropPickerView)
        
        self.cropContainerView.addConstraints([
            NSLayoutConstraint(item: self.cropContainerView!, attribute: .top, relatedBy: .equal, toItem: self.cropPickerView, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.cropContainerView!, attribute: .bottom, relatedBy: .equal, toItem: self.cropPickerView, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.cropContainerView!, attribute: .leading, relatedBy: .equal, toItem: self.cropPickerView, attribute: .leading, multiplier: 1, constant:-10),
            NSLayoutConstraint(item: self.cropContainerView!, attribute: .trailing, relatedBy: .equal, toItem: self.cropPickerView, attribute: .trailing, multiplier: 1, constant: 10)
        ])
        
        self.cropPickerView.delegate = self
        DispatchQueue.main.async {
            self.cropPickerView.image(self.cropImage)
        }
        // Do any additional setup after loading the view.
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

      
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(self.cropTap(_:)))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action:  #selector(self.cropTap(_:)))
    }
    
    @objc func cropTap(_ sender: UIBarButtonItem) {
        self.cropPickerView.crop { (crop) in
            self.delegate?.dismissPresentViewController(image: crop.image ?? UIImage())
            self.navigationController?.popViewController(animated: false)
        }
    }

}

// MARK: CropPickerViewDelegate
extension CropImageViewController: CropPickerViewDelegate {
    func cropPickerView(_ cropPickerView: CropPickerView, result: CropResult) {

    }

    func cropPickerView(_ cropPickerView: CropPickerView, didChange frame: CGRect) {
       
    }
}
