//
//  ReviewOrderViewController.swift
//  EZER
//
//  Created by Akash on 26/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

protocol ReviewOrderViewControllerDelegate:class {
    func removeOrderView(id:String)
}

class ReviewOrderViewController: UIViewController {
    
    @IBOutlet weak var podView: UIView!
    @IBOutlet weak var lblPickDate: UILabel!
    @IBOutlet weak var lblPickTime: UILabel!
    @IBOutlet weak var lblEarnings: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblCustomer: UILabel!
    
    @IBOutlet weak var declineButtonClicked: RoundableButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraintOutlet: NSLayoutConstraint!
    @IBOutlet weak var goButtonOutlet: RoundableButton!
    @IBOutlet weak var itemDescritionTableView: UITableView!
    @IBOutlet weak var itemDescritionTableViewHeightConstraintOutlet: NSLayoutConstraint!
    
    @IBOutlet weak var lblEarningsTitle: UILabel!
    
    var isGoDone = false
    var driverJob : DriverAvailableJobs!
    var imagesArray : [String]!
    var orderID : String!
    weak var delegate:ReviewOrderViewControllerDelegate?
    
    let cellName = "ImageUploadingCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 80
        
        self.navigationItem.setTitle(title: "Order", subtitle: "Open")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain,target: nil, action: nil)
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.layer.zPosition = -1
        getOrderDetails()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefaults.standard.set(nil, forKey: UserDefaultkeys.isNotification)
        addObserver()
    }
   
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleOrderCancelFromCustomer(notification:)), name: .orderCancelFromCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleOrderCancelFromCustomer(notification:)), name: .scheduledOrderCancelFromCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getOrderDetails), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func handleOrderCancelFromCustomer(notification:Notification)
    {
        
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            if let orderId = notificationObject["orderId"] as? String
            {
                if driverJob != nil && orderId == driverJob.orderId
                {
                    handleCancelOrder()
                }
            }
        }
    }
    private func removeObserver()
    {
        NotificationCenter.default.removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tableViewHeightConstraintOutlet?.constant = self.tableView.contentSize.height
        self.itemDescritionTableViewHeightConstraintOutlet?.constant = self.itemDescritionTableView.contentSize.height
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserver()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc func getOrderDetails(){
        let params :[String:Any] = [
            "orderId": orderID
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getOrderDetail, jsonToPost: params, isResponseRequired: true,  embedAuthTocken: true) {  (json) in
            if let json = json
            {
                if json[ServerKeys.status].int  == ServerStatusCode.Failure
                {
                    
                    let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                                       self.navigationController?.popViewController(animated: true)
                                   })
                    self.showAlertController(heading: AppName, message: json[ServerKeys.message].string!, action: action)
                    return
                }

                self.driverJob = DriverAvailableJobs(json: json[ServerKeys.data])
                if self.driverJob.status == .cancelled {
                    self.handleCancelOrder()
                }else {
                    self.lblCustomer.text = "\(self.driverJob.customerDetail.firstName ?? "") \(self.driverJob.customerDetail.lastName ?? "")"

                    self.lblPickDate.text = self.driverJob.pickupDate
                    self.lblPickTime.text = self.driverJob.pickupTime
                    self.lblDistance.text = String(format: "%.02f Miles", self.driverJob.totalDistance!)
                    
                    if  self.driverJob.isSpecialRateApplied ?? false &&  self.driverJob.driverSpecialRate != 0{
                        self.lblEarningsTitle.text = "Earnings"
                        self.lblEarnings.text = String(format: "$%.02f", self.driverJob.driverSpecialRate)
                    }else{
                        self.lblEarningsTitle.text = "Estimated Earnings"
                        self.lblEarnings.text = String(format: "$%.02f - $%.02f",  self.driverJob.estimatedMinPrice!, self.driverJob.estimatedMaxPrice)
                    }
                  //  self.lblEarnings.text = String(format: "$%.02f - $%.02f", self.driverJob.estimatedMinPrice!, self.driverJob.estimatedMaxPrice)

                    self.navigationItem.setTitle(title: "Order \(self.driverJob.orderNumber!)", subtitle: "Open")
                    self.tableView.reloadData()
                    self.itemDescritionTableView.reloadData()
                    self.updateView()
                    if  self.driverJob.status == .driverReview {
                        NotificationCenter.default.post(name: .refreshDriverData, object: nil)
                        self.isGoDone = true
                        self.driverJob.status = .driverReview
                        self.goButtonOutlet.setTitle("CONTINUE", for: .normal)
                        self.navigationItem.hidesBackButton = false
                    }
                    self.podView.isHidden = true
                    if self.driverJob.podRequired {
                        self.podView.isHidden = false
                        return
                    }
                    for stop in self.driverJob.extraDestinationLocations {
                        if stop.podRequired {
                            self.podView.isHidden = false
                            return
                        }
                    }
                }
            }
        }
    }
    private func updateView(){
        self.view.layoutIfNeeded()
        super.updateViewConstraints()
        self.tableViewHeightConstraintOutlet?.constant = self.tableView.contentSize.height
        self.itemDescritionTableViewHeightConstraintOutlet?.constant = self.itemDescritionTableView.contentSize.height
    }
    
    private func handleCancelOrder()
    {
        let action = UIAlertAction.init(title: "OK", style: .cancel){
            action in
            self.navigationController?.popToRootViewController(animated: true)
        }
        showAlertController(message: ValidationMessages.orderCancelByUser,action: action)
    }
    
    @IBAction func declineButtonClicked(_ sender: Any) {
        let cancelJobController = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.cancelViewController) as! CancelJobViewController
        cancelJobController.actionConfirmed = {
            action in
            self.cancelOrder(reason: action)
        }
        cancelJobController.actionCanceled = { (action)-> () in}
        cancelJobController.modalPresentationStyle = .overCurrentContext
        self.present(cancelJobController, animated: false, completion: nil)
    }
    
    func jobAccepted(driverJob: DriverAvailableJobs){
        var pickUpLocation = Dictionary<String,Any>()
        pickUpLocation["type"] = driverJob.startLocation.type
        pickUpLocation["coordinates"] = driverJob.startLocation.coordinates
        
        var dropLocation = Dictionary<String,Any>()
        dropLocation["type"] = driverJob.destinationLocation.type
        dropLocation["coordinates"] = driverJob.destinationLocation.coordinates
        
        var params :[String:Any] = [
            "customerId" : driverJob.customerId,
            "driverId" : HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            "orderId" : driverJob.orderId,
            "pickupLocation" : pickUpLocation,
            "dropLocation" : dropLocation
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.driverOrderStart, jsonToPost: params, isResponseRequired: true,  embedAuthTocken:true) {  (json) in
            if let json = json {
                if json[ServerKeys.status].int  == ServerStatusCode.Failure
                {
                    let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                                        self.navigationController?.popViewController(animated: true)
                                    })
                    self.showAlertController(heading: AppName, message: json[ServerKeys.message].string!, action: action)
                    return
                }else{
                    NotificationCenter.default.post(name: .refreshDriverData, object: nil)
                    self.isGoDone = true
                    self.driverJob.status = .driverReview
                    self.goButtonOutlet.setTitle("CONTINUE", for: .normal)
                    self.navigationItem.hidesBackButton = true
                    self.navigationItem.setTitle(title: "Order \(self.driverJob.orderNumber!)", subtitle: "Assigned")
                    
                    CDMyJobsManager.shared.addWorkOrder(driverJob, completionHandler: { (error) in
                        guard let error = error else {return}
                        print(error)
                    })
                }
            }
        }
    }
    
    
    @IBAction func goButtonClicked(_ sender: Any){
        if isGoDone {
            
            if  !driverJob.immediate{
                let localDate = HelperFunction.getCurrentFullDate(date: driverJob.pickUpFullDate!)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                
                let dt = dateFormatter.date(from: localDate)
                let now = Date()
        
                if dt! >= now  {
                    let hours = dt!.offsetFrom(date: now)
                    if hours < 2{
                        updateWorkOrderStatus()
                    }else{
                        self.showAlertController(message: ValidationMessages.timeIsDelayed)
                    }
                }
            }else{
               updateWorkOrderStatus()
            }
           
        }else{
            
            jobAccepted(driverJob: driverJob)
        }
    }
    private func updateWorkOrderStatus(){
        let params :[String:Any] = [
            "orderId": driverJob.orderId,
            "driverId" : HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            "orderStatus" : OrderStatusType.accepted.rawValue,
            "loadingImages" : [],
            "unloadingImages" : [],
            "multiWOStatus":"",
            "stopPoint":""
        ]
        
        if appDelegate().reachability.connection == .unavailable {
            CDDriverJobManager.shared.addPendingOrderStatus(driverJob.orderId, driverID: HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id), multiWOStatus: "", orderStatus: OrderStatusType.accepted.rawValue,images: nil, stopPoint: 0) { (result) in
                switch result {
                case .success(let data):
                    print("Succcess")
                    CDDriverJobManager.shared.updateCurrentOrder(self.driverJob.orderId, status: OrderStatusType.accepted.rawValue , multiWOStatus: "", stopPoint: self.driverJob.stopPoint){ (error) in
                        guard let error = error else {return}
                        print(error)
                    }
                    
                    HelperFunction.saveValueInUserDefaults(key: UserDefaultkeys.currentOrderID, value: self.driverJob.orderId ?? "")
                    
                    self.driverJob.status = .accepted
                    let scheduledJobs = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.orderProcessView) as! OrderProcessViewController
                    scheduledJobs.driverJob = self.driverJob
                    self.navigationController?.pushViewController(scheduledJobs, animated: true)
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
        else {
            BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.updateWorkOrderStatus, jsonToPost: params) { (json) in
                if let _ = json {
                    CDDriverJobManager.shared.updateCurrentOrder(self.driverJob.orderId, status: OrderStatusType.accepted.rawValue , multiWOStatus: "", stopPoint: self.driverJob.stopPoint){ (error) in
                        guard let error = error else {return}
                        print(error)
                    }
                    self.driverJob.status = .accepted
                    let scheduledJobs = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.orderProcessView) as! OrderProcessViewController
                    scheduledJobs.driverJob = self.driverJob
                    self.navigationController?.pushViewController(scheduledJobs, animated: true)
                }
            }
        }
    }
   
    /*private func  emitToScoket(status : String)
    {
        var customerData: [String:Any] =  ["status" : status]
        customerData["orderId"] = self.driverJob.orderId
        if (IdentifierName.driverCurrentLocation != nil){
            customerData["sourceLocation"] = ["lat":IdentifierName.driverCurrentLocation.latitude,
                                              "lng":IdentifierName.driverCurrentLocation.longitude]
        }
        SocketIOManager.sharedInstance.sendChangeOrderStatus(toCustomerId: self.driverJob.customerId, customerData: customerData)
    }*/
    @IBAction func assignTwillioNumber(){
        let params :[String:Any] = [
            "orderId": orderID
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Main.assignTwilioNumber, jsonToPost: params) { (json) in
            if let json = json {
                if let twilioNumber = json["twilioNumber"].string {
                    HelperFunction.callToNumber(number: twilioNumber)
                }
            }
        }
    }
    
    func cancelOrder(reason: String){
        var stringURL = ServerUrls.Driver.declineOrder
        if isGoDone {
            stringURL = ServerUrls.Driver.cancelOrder
        }
        let cancelParams:[String:Any] = [
            "orderId":driverJob.orderId,
            "cancelReason": reason,
            "driverId" : HelperFunction.getUserId()
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: stringURL, jsonToPost: cancelParams, isResponseRequired: true,  embedAuthTocken: true) {  (json) in
            if let json = json
            {
                if json[ServerKeys.status].int  == ServerStatusCode.Failure
                {
                    let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                                        self.navigationController?.popViewController(animated: true)
                                    })
                    self.showAlertController(heading: AppName, message: json[ServerKeys.message].string!, action: action)
                    return
                }
                
                self.delegate?.removeOrderView(id:self.driverJob.orderId)
                
                self.navigationController?.popViewController(animated: true)
                if self.isGoDone
                {
                    //SocketIOManager.sharedInstance.sendCancelOrderFromDriver(fromDriverId: HelperFunction.getUserId(), toCustomerId: self.driverJob.customerId, orderId: self.driverJob.orderId)
                    NotificationCenter.default.post(name: .refreshDriverData, object: nil)
                    
                }
            }
        }
    }
}

extension ReviewOrderViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (driverJob == nil){
            return 0
        }
        if tableView != itemDescritionTableView {
            let count = max(driverJob.extraStartLocations.count, driverJob.extraDestinationLocations.count)
            return count + 1// ONe is default row must have Source Or Destination
        }else{
            let count = driverJob.extraStartLocations.count
            return count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView != itemDescritionTableView {
            let  cell = tableView.dequeueReusableCell(withIdentifier:  "myJobsCell") as! MyJobsTableViewCell
            cell.setUpCell(indexPath, driverAvailableJob: driverJob)
            return cell;
        }else{
            let  cell = tableView.dequeueReusableCell(withIdentifier:  "PickUpDescriptionTableViewCell") as! PickUpDescriptionTableViewCell
            cell.setUpCell(indexPath, driverJob: driverJob)
            cell.delegate = self
            return cell;
        }
    }
}

extension ReviewOrderViewController:PickUpDescriptionTableViewCellDelegate,zoomImageViewControllerDelegate {
    func PresentZoomViewController(_ url: String) {
        let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
        let driverInfoController = storyboard.instantiateViewController(withIdentifier:IdentifierName.Driver.zoomImageViewController) as! ZoomImageViewController
        driverInfoController.urlString = url
        driverInfoController.delegate = self
        self.navigationController?.present(driverInfoController, animated: false, completion: nil)
    }
    
    func dismissPresentViewController() {
        self.navigationController?.dismiss(animated: false, completion: nil)
    }
}
