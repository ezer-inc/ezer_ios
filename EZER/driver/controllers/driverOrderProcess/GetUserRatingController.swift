//
//  GetUserRatingController.swift
//  EZER
//
//  Created by TimerackMac1 on 24/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class GetUserRatingController: UIViewController {
    weak var orderProcessCon :  OrderProcessViewController?
    var finalAmount : Double!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var finalAmoutLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        finshOrder()
        //
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func submitUserRating(_ sender: RoundableButton) {
        if ratingView.rating > 0 {
            rateUser()
        }
        else{
            self.view.makeToast(ValidationMessages.ratingRequiredUser)
        }
    }
    
    func finshOrder(){
        let params :[String:Any] = [
            "orderId": orderProcessCon!.driverJob.orderId,
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.finishOrder, jsonToPost: params) { (json) in
            if let json = json {
                NotificationCenter.default.post(name: .refreshDriverData, object: nil)
                self.finalAmoutLabel.text = String(format: "$ %.2f", json["finalPrice"].double ?? 0.0)
            }
        }
    }
    
    func rateUser(){
        let params :[String:Any] = [
            "orderId": orderProcessCon!.driverJob.orderId,
            "rating" : Int(ratingView.rating)
        ]

        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.addDriverRatingToUser, jsonToPost: params) { (json) in
            if json != nil{
                self.orderProcessCon?.popToRootController()
            }
        }
    }
    
    @IBAction func reportIssue(_ sender: RoundableButton) {
        HelperFunction.openMailApp(email: ServerUrls.driverSupportEmail)
    }
}
