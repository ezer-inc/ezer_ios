//
//  DriverOrderCompleted.swift
//  EZER
//
//  Created by TimerackMac1 on 24/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class DriverOrderCompleted: UIViewController {
weak var orderProcessCon :  OrderProcessViewController?
    var statusType : OrderStatusType!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func finishOrder(_ sender: RoundableButton) {
        
            let params :[String:Any] = [
                "orderId": orderProcessCon!.driverJob.orderId,
            ]
            
            BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.finishOrder, jsonToPost: params) { (json) in
                if let json = json
                {
                    NotificationCenter.default.post(name: .refreshDriverData, object: nil)
                    //self.sendChangeStatusToCustomer(.done)
                    self.orderProcessCon?.changePageViewController(status: .done,finalPrice: json["finalPrice"].double ?? 0.0)
                }
            }
    }
    /*private func sendChangeStatusToCustomer(_ status : OrderStatusType)
    {
        var customerData =  ["status" :status.rawValue ]
        customerData["orderId"] = orderProcessCon!.driverJob.orderId
        SocketIOManager.sharedInstance.sendChangeOrderStatus(toCustomerId: orderProcessCon!.driverJob.customerId, customerData: customerData)
    }*/
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
