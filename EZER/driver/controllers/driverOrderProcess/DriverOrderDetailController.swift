//
//  DriverOrderDetailController.swift
//  EZER
//
//  Created by Akash on 16/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class DriverOrderDetailController: UIViewController {
    
    
    @IBOutlet weak var podView: UIView!
    @IBOutlet weak var lblPickDate: UILabel!
    @IBOutlet weak var lblPickTime: UILabel!
    @IBOutlet weak var lblEarnings: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblCustomer: UILabel!
    
    @IBOutlet weak var lblOrderType: UILabel!
    
    @IBOutlet weak var declineButtonClicked: RoundableButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var itemDescritionTableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraintOutlet: NSLayoutConstraint!
    @IBOutlet weak var itemDescritionTableViewHeightConstraintOutlet: NSLayoutConstraint!
    @IBOutlet weak var lblEarningTitle: UILabel!
    
    var driverJob : DriverAvailableJobs!
    
    let cellName = "ImageUploadingCell"
    var unfinishedOrderId:String = ""
    
    @IBOutlet weak var btnGoOutlet: RoundableButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if appDelegate().reachability.connection != .unavailable {
            getOrderDetails()
        }else{
            customizeUI()
        }
        self.navigationItem.setTitle(title: "Order \(driverJob.orderNumber!)", subtitle: "Assigned")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain,
                                                                target: nil, action: nil)
        self.navigationController?.navigationBar.layer.zPosition = -1
        // Do any additional setup after loading the view.
        
        self.tableView.estimatedRowHeight = 380.0
        self.tableView.rowHeight = UITableView.automaticDimension
        
        switch driverJob.status {
        case .loaded?, .inTransit?, .multi_pickup_transit?, .multi_drop_transit?, .atDestination?, .unloaded?, .finished?, .done?, .cancelled?, .timeout?:
            
            declineButtonClicked.isHidden = true
            
            break
        default: //.open, .driverReview, .accepted, .onTheWay, .atSource:
            break
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
    }
    
    
    func customizeUI()  {
        lblPickDate.text = driverJob.pickupDate
        lblPickTime.text = driverJob.pickupTime
        lblDistance.text = String(format: "%.02f Miles", driverJob.totalDistance!)
        
        if driverJob.isSpecialRateApplied ?? false && driverJob.driverSpecialRate != 0{
            lblEarningTitle.text = "Earnings"
            lblEarnings.text = String(format: "$%.02f",driverJob.driverSpecialRate)
        }else{
            lblEarningTitle.text = "Estimated Earnings"
            lblEarnings.text = String(format: "$%.02f - $%.02f", driverJob.estimatedMinPrice!, driverJob.estimatedMaxPrice)
        }
    
        if driverJob.immediate == true{
            lblOrderType.backgroundColor = UIColor(hex: "FF7F41")
            lblOrderType.text = "Now"
        }else{
            lblOrderType.backgroundColor = UIColor(hex: "45D2A6")
            lblOrderType.text = "Schedule"
        }
       
        
        
        
        if driverJob.status.rawValue == OrderStatusType.driverReview.rawValue{
            let localDate = HelperFunction.getCurrentFullDate(date: driverJob.pickUpFullDate!)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
            let dt = dateFormatter.date(from: localDate)
            let now = Date()
            if dt! >= now  {
                let hours = dt!.offsetFrom(date: now)
                if Double(hours) < driverJob.orderPickupInterval{}else{
                    btnGoOutlet.backgroundColor = #colorLiteral(red: 0.3803921569, green: 0.4784313725, blue: 0.5294117647, alpha: 1)
                }
            }
        }
        
        
        if driverJob.status == .pause {
            handlePauseOrder()
        }
        
        if driverJob.status == .cancelled {
            self.handleCancelOrder()
        }
        self.lblCustomer.text = "\(driverJob.customerDetail.firstName ?? "") \(driverJob.customerDetail.lastName ?? "")"
        self.podView.isHidden = true
        if driverJob.podRequired {
            self.podView.isHidden = false
            return
        }
        for stop in driverJob.extraDestinationLocations {
            if stop.podRequired {
                self.podView.isHidden = false
                return
            }
        }
    }
    
    private func addObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(handleOrderCancelFromCustomer(notification:)), name: .orderCancelFromCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleOrderCancelFromCustomer(notification:)), name: .scheduledOrderCancelFromCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getOrderDetails), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func getOrderDetails() {
        let params :[String:Any] = [
            "orderId": driverJob.orderId
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getOrderDetail, jsonToPost: params, isResponseRequired: true,  embedAuthTocken: true) {  (json) in
            if let json = json
            {
                if json[ServerKeys.status].int  == ServerStatusCode.Failure
                {
                    let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                                       self.navigationController?.popViewController(animated: true)
                                   })
                    self.showAlertController(heading: AppName, message: json[ServerKeys.message].string!, action: action)
                }
                if json[ServerKeys.status].int  == ServerStatusCode.Success
                {
                    self.driverJob = DriverAvailableJobs(json: json[ServerKeys.data])
                    if self.unfinishedOrderId != ""{
                        if self.unfinishedOrderId == self.driverJob.orderId{
                            self.customizeUI()
                        }else{
                            let action = UIAlertAction.init(title: "OK", style: .cancel){
                                action in
                                self.navigationController?.popViewController(animated: true)
                            }
                            self.showAlertController(message: ValidationMessages.finishPreviousOrder,action: action)
                        }
                    }else{
                        self.customizeUI()
                    }
                    
                }
            }
          
        }
        
    }
    
    private func handleCancelOrder(){
        HelperFunction.saveValueInUserDefaults(key: UserDefaultkeys.currentOrderID, value: "")
        let action = UIAlertAction.init(title: "OK", style: .cancel){
            action in
            self.navigationController?.popToRootViewController(animated: true)
        }
        showAlertController(message: ValidationMessages.orderCancelByUser,action: action)
    }
    
    private func handlePauseOrder(){
        HelperFunction.saveValueInUserDefaults(key: UserDefaultkeys.currentOrderID, value: "")
        let action = UIAlertAction.init(title: "OK", style: .cancel){
            action in
            self.navigationController?.popViewController(animated: true)
        }
        showAlertController(message: ValidationMessages.orderPauseMessage,action: action)
    }
    
    
    
    private func removeObserver(){
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func handleOrderCancelFromCustomer(notification:Notification) {
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            if let orderId = notificationObject["orderId"] as? String
            {
                if orderId == driverJob.orderId {
                    self.handleCancelOrder()
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserver()
        //else new vew pushed
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    override func viewWillLayoutSubviews() {
        self.view.layoutIfNeeded()
        super.updateViewConstraints()
        self.tableViewHeightConstraintOutlet?.constant = self.tableView.contentSize.height
        self.itemDescritionTableViewHeightConstraintOutlet?.constant = self.itemDescritionTableView.contentSize.height
    }
    
    @IBAction func declineButtonClicked(_ sender: Any) {
        if appDelegate().reachability.connection == .unavailable {
            self.showAlertController(message: ValidationMessages.declinedOrderOffline)
        }
        else{
            let cancelJobController = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.cancelViewController) as! CancelJobViewController
            cancelJobController.actionConfirmed = {
                action in
                self.cancelOrder(reason: action)
            }
            cancelJobController.actionCanceled = { (action)-> () in}
            
            cancelJobController.modalPresentationStyle = .overCurrentContext
            self.present(cancelJobController, animated: false, completion: nil)
        }
    }
    deinit {
        removeObserver()
    }
    
    @IBAction func goButtonClicked(_ sender: Any){
        if appDelegate().reachability.connection != .unavailable {
            getOrderStatus()
        }
    }
    
    func continueButtonFuntionality(){
        if driverJob.status != .pause{
            if driverJob.immediate && driverJob.status.rawValue != OrderStatusType.driverReview.rawValue{
                
                if (driverJob.status.rawValue == OrderStatusType.finished.rawValue) {
                    return;
                }
                self.openOrderProcessViewController()
            }else{
                if driverJob.status.rawValue == OrderStatusType.driverReview.rawValue{
                    let localDate = HelperFunction.getCurrentFullDate(date: driverJob.pickUpFullDate!)
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                    
                    let dt = dateFormatter.date(from: localDate)
                    let now = Date()
                    
                    
                    if dt! >= now  {
                        let hours = dt!.offsetFrom(date: now)
                        if Double(hours) < driverJob.orderPickupInterval{
                            btnGoOutlet.backgroundColor = #colorLiteral(red: 0.2705882353, green: 0.8235294118, blue: 0.6509803922, alpha: 1)
                            processGOCall()
                        }else{
                            if driverJob.orderPickupInterval > 1{
                                self.showAlertController(message: String(format: "Unable to “Go” %.2f hours from the pick-up time!", driverJob.orderPickupInterval))
                            }else{
                                self.showAlertController(message: String(format: "Unable to “Go” %.2f hour from the pick-up time!", driverJob.orderPickupInterval))
                            }
                        }
                    }
                    else{
                        processGOCall()
                        //self.showAlertController(message: ValidationMessages.OrderDatePasted)
                    }
                }
                else{
                    self.openOrderProcessViewController()
                }
            }
        }
    }
    
    
    func getOrderStatus() {
       let params :[String:Any] = [
           "orderId": driverJob.orderId
       ]
       BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getOrderDetail, jsonToPost: params, isResponseRequired: true,  embedAuthTocken: true) {  (json) in
           if let json = json
           {
               if json[ServerKeys.status].int  == ServerStatusCode.Failure
               {
                   let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                                      self.navigationController?.popViewController(animated: true)
                                  })
                   self.showAlertController(heading: AppName, message: json[ServerKeys.message].string!, action: action)
               }
               if json[ServerKeys.status].int  == ServerStatusCode.Success
               {
                   self.driverJob = DriverAvailableJobs(json: json[ServerKeys.data])
                   self.customizeUI()
                   self.continueButtonFuntionality()
               }
           }
         
       }
       
   }
    
    
    
    
    private func processGOCall(){
        if driverJob.status.rawValue == OrderStatusType.driverReview.rawValue{
            let status = HelperFunction.getSrtingFromUserDefaults(key: DriverProfileKeys.availibiltyStatus)
            if DriverStatusTypes.available.rawValue == status || DriverStatusTypes.busy.rawValue == status {
                let params :[String:Any] = [
                    "orderId": driverJob.orderId,
                    "orderStatus" : OrderStatusType.accepted.rawValue,
                    "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
                    "loadingImages" : [],
                    "unloadingImages" : [],
                    "multiWOStatus":"",
                    "stopPoint":""
                ]
                if appDelegate().reachability.connection == .unavailable {
                    
                    CDDriverJobManager.shared.addPendingOrderStatus(driverJob.orderId, driverID: HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id), multiWOStatus: "", orderStatus: OrderStatusType.accepted.rawValue,images: nil, stopPoint: 0.0) { [self] (result) in
                        switch result {
                        case .success(let data):
                            print("Succcess")
                            
                            CDDriverJobManager.shared.updateCurrentOrder(self.driverJob.orderId, status: OrderStatusType.accepted.rawValue , multiWOStatus: "", stopPoint: self.driverJob.stopPoint){ (error) in
                                guard let error = error else {return}
                                print(error)
                            }
                            
                            self.driverJob.status = .accepted
                            self.openOrderProcessViewController()
                        case .failure(let error):
                            print(error.localizedDescription)
                        }
                    }
                } else {
                    BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.updateWorkOrderStatus, jsonToPost: params) { (json) in
                        if let _ = json {
                            CDDriverJobManager.shared.updateCurrentOrder(self.driverJob.orderId, status: OrderStatusType.accepted.rawValue , multiWOStatus: "", stopPoint: self.driverJob.stopPoint){ (error) in
                                guard let error = error else {return}
                                print(error)
                            }
                            
                            self.driverJob.status = .accepted
                            self.openOrderProcessViewController()
                        }
                    }
                }
            }else{
                self.showAlertController(message: ValidationMessages.changeAvailableStatus)
            }
        }
    }
    
    private func openOrderProcessViewController(){
        let orderController = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.orderProcessView) as! OrderProcessViewController
        orderController.driverJob = self.driverJob
        HelperFunction.saveValueInUserDefaults(key: UserDefaultkeys.currentOrderID, value: driverJob.orderId ?? "")
        self.navigationController?.pushViewController(orderController, animated: true)
    }
    
    /*private func  emitToSocket(status : String)
     {
     /*var customerData: [String:Any] =  ["status" : status]
     customerData["orderId"] = self.driverJob.orderId
     if (IdentifierName.driverCurrentLocation != nil){
     customerData["sourceLocation"] = ["lat":IdentifierName.driverCurrentLocation.latitude,
     "lng":IdentifierName.driverCurrentLocation.longitude]
     }
     SocketIOManager.sharedInstance.sendChangeOrderStatus(toCustomerId: self.driverJob.customerId, customerData: customerData)*/
     saveToDatabase()
     }*/
    //saving state to Database
    func cancelOrder(reason: String){
        let cancelParams :[String:Any] = [
            "orderId":driverJob.orderId,
            "cancelReason": reason
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.cancelOrder, jsonToPost: cancelParams) { (json) in
            if let _ = json
            {
                self.handleCanceOrderEmit()
//                CDMyJobsManager.shared.deleteWorkOrder(self.driverJob.orderId ?? "")
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        
    }
    
    private func handleCanceOrderEmit()
    {
        NotificationCenter.default.post(name: .refreshDriverData, object: nil)
        /*
         if self.driverJob.immediate{
         
         }
         else{
         SocketIOManager.sharedInstance.refreshDriverScreens(customerData: ["orderId" : self.driverJob.orderId])
         }
         NotificationCenter.default.post(name: .refreshDriverData, object: nil)
         SocketIOManager.sharedInstance.sendCancelOrderFromDriver(fromDriverId: HelperFunction.getUserId(), toCustomerId: self.driverJob.customerId, orderId: self.driverJob.orderId)*/
    }
    
    @IBAction func callAction(_ sender: Any) {
        let params :[String:Any] = [
            "orderId": driverJob.orderId
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Main.assignTwilioNumber, jsonToPost: params) { (json) in
            if let json = json
            {
                if let twilioNumber = json["twilioNumber"].string
                {
                    HelperFunction.callToNumber(number: twilioNumber)
                }
            }
        }
    }
    
}

extension DriverOrderDetailController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return   1
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView != itemDescritionTableView {
            let count = max(driverJob.extraStartLocations.count, driverJob.extraDestinationLocations.count)
            return count + 1// ONe is default row must have Source Or Destination
        }else{
            let count = driverJob.extraStartLocations.count
            return count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView != itemDescritionTableView {
            let  cell = tableView.dequeueReusableCell(withIdentifier:  "myJobsCell") as! MyJobsTableViewCell
            cell.setUpCell(indexPath, driverAvailableJob: driverJob)
            return cell;
        }else{
            let  cell = tableView.dequeueReusableCell(withIdentifier:  "PickUpDescriptionTableViewCell") as! PickUpDescriptionTableViewCell
            cell.setUpCell(indexPath, driverJob: driverJob)
            cell.delegate = self
            return cell;
        }
        
    }
    
}

extension DriverOrderDetailController:PickUpDescriptionTableViewCellDelegate,zoomImageViewControllerDelegate
{
    func PresentZoomViewController(_ url: String) {
        let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
        let driverInfoController = storyboard.instantiateViewController(withIdentifier:IdentifierName.Driver.zoomImageViewController) as! ZoomImageViewController
        driverInfoController.urlString = url
        driverInfoController.delegate = self
        self.navigationController?.present(driverInfoController, animated: false, completion: nil)
    }
    
    func dismissPresentViewController() {
        self.navigationController?.dismiss(animated: false, completion: nil)
    }
    
}
