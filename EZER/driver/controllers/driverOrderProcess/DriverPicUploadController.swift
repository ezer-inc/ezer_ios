//
//  DriverPicUploadController.swift
//  EZER
//
//  Created by TimerackMac1 on 25/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import CoreData

class DriverPicUploadController: UIViewController ,UINavigationControllerDelegate{
    @IBOutlet weak var ic_addMore: UIImageView!
    let cellName = "ImageUploadingCell"
    @IBOutlet weak var cameraViewVerticalConst: NSLayoutConstraint!
    @IBOutlet weak var loadingStatus: UILabel!
    @IBOutlet weak var cancelOrderButton: RoundableButton!
    @IBOutlet weak var nextButton: RoundableButton!
    @IBOutlet weak var picCollectionView: UICollectionView!
    @IBOutlet weak var cameraOrGallery_out: UIButton!
    weak var orderProcessCon :  OrderProcessViewController?
    var statusType : OrderStatusType!
    var imagePicker = UIImagePickerController()
    var imagesArray = [CDImage]()
    var imageUploadType = ImageUploadType.beforeLoading
    var maxImageLimit = IdentifierName.maximumImageUploads
    var maxImageUploadError = ValidationMessages.maxImageUploadError
    var stopPoint = 0
    var isMultiStop = false
    var imagesDataArray = [Data]()
    override func viewDidLoad() {
        super.viewDidLoad()
        stopPoint = self.orderProcessCon!.driverJob.stopPoint
        isMultiStop = (self.orderProcessCon!.currentMuliWorkStatus == OrderStatusType.multi_pickup_transit.rawValue) || (self.orderProcessCon!.currentMuliWorkStatus == OrderStatusType.multi_drop_transit.rawValue)
        cameraOrGallery_out.layer.cornerRadius = cameraOrGallery_out.bounds.height/2
        cameraOrGallery_out.layer.borderWidth = 1
        cameraOrGallery_out.layer.borderColor = UIColor.driverTheme.cgColor
        adjustViewAccordingToStatus()
        cameraViewVerticalConst.isActive = true
        picCollectionView.register(UINib(nibName:cellName ,bundle: nil), forCellWithReuseIdentifier: cellName)
        picCollectionView.dataSource = self
        picCollectionView.delegate = self
        imagePicker.delegate = self
        let numberOfCell = 4
        let cellSpecing = 2
        let screenSize = UIScreen.main.bounds.width - 52
        if let layout = picCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let width = (screenSize - CGFloat(cellSpecing * (numberOfCell + 1))) / CGFloat(numberOfCell)
            layout.itemSize = CGSize(width: width, height: width)
            layout.invalidateLayout()
        }
        if !imagesArray.isEmpty{
            self.ic_addMore.isHidden = false
            self.cameraViewVerticalConst.priority = UILayoutPriority.init(249)
        }
        // Do any additional setup after loading the view.
    }
    
    private func adjustViewAccordingToStatus()  {
        if statusType == OrderStatusType.startLoading {
            loadingStatus.text = "Before Loading"
            imageUploadType = .beforeLoading
            if isMultiStop {
                imagesArray = orderProcessCon?.driverJob.extraStartLocations[stopPoint].beforeLoading ?? [CDImage]()
            } else {
                imagesArray = orderProcessCon?.driverJob.beforeLoading ?? [CDImage]()
                for image in imagesArray {
                    imagesDataArray.append(image.imageData ?? Data())
                }
            }
        }
        else if statusType == OrderStatusType.loaded {
            loadingStatus.text = "After Loading"
            cancelOrderButton.isHidden = true
            imageUploadType = .afterLoading
            if isMultiStop {
                imagesArray = orderProcessCon?.driverJob.extraStartLocations[stopPoint].afterLoading ?? [CDImage]()
            } else {
                imagesArray = orderProcessCon?.driverJob.afterLoading ?? [CDImage]()
                for image in imagesArray {
                    imagesDataArray.append(image.imageData ?? Data())
                }
            }
        }
        else if statusType == OrderStatusType.beforeUnload {
            cancelOrderButton.isHidden = true
            loadingStatus.text = "Before Unloading"
            imageUploadType = .beforeUnloading
            if isMultiStop {
                imagesArray = orderProcessCon?.driverJob.extraDestinationLocations[stopPoint].beforeUnLoading ?? [CDImage]()
            } else {
                imagesArray = orderProcessCon?.driverJob.beforeUnLoading ?? [CDImage]()
                for image in imagesArray {
                    imagesDataArray.append(image.imageData ?? Data())
                }
            }
        }
        else if statusType == OrderStatusType.unloaded {
            cancelOrderButton.isHidden = true
            loadingStatus.text = "After Unloading"
            imageUploadType = .afterUnloading
            if isMultiStop {
                imagesArray = orderProcessCon?.driverJob.extraDestinationLocations[stopPoint].afterUnLoading ?? [CDImage]()
            } else {
                imagesArray = orderProcessCon?.driverJob.afterUnLoading ?? [CDImage]()
                for image in imagesArray {
                    imagesDataArray.append(image.imageData ?? Data())
                }
            }
        }
        else if statusType == OrderStatusType.podRequested {
            HelperFunction.saveValueInUserDefaults(key: PodStausUpload.podStaus, value: "0")
            cancelOrderButton.isHidden = true
            maxImageLimit = 1
            maxImageUploadError = "You can upload only one image"
            loadingStatus.text = "Signature Requested"
            imageUploadType = .podImage
            if isMultiStop {
                imagesArray = orderProcessCon?.driverJob.extraDestinationLocations[stopPoint].podImages ?? [CDImage]()
            } else {
                imagesArray = orderProcessCon?.driverJob.podImages ?? [CDImage]()
                for image in imagesArray {
                    imagesDataArray.append(image.imageData ?? Data())
                }
            }
        }
    }
    @IBAction func OpenCameraOrGallery(_ sender: UIButton) {
        if (imagesArray.count >= maxImageLimit) { // check size of image array
            self.showMessage(maxImageUploadError, type: .error,options: [.textNumberOfLines(0)])
            return
        }
        let optionAlert = UIAlertController(title: "Select Mode", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            //self.assetView.isHidden=false
            self.checkCameraPermission(completion: { (action) in
                if action {
                    if UIImagePickerController.isSourceTypeAvailable(.camera){
    
                        self.imagePicker.allowsEditing = false
                        self.imagePicker.sourceType = .camera
                        self.imagePicker.cameraCaptureMode = .photo
                        self.imagePicker.modalPresentationStyle = .fullScreen
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                }else{
                    self.showCameraAlert()
                }
            })
        }
        optionAlert.addAction(cameraAction)
        let galleryAction = UIAlertAction(title: "Image Gallery", style: .default) { (action) in
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
           self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            // self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.imagePicker.modalPresentationStyle = .popover
            self.imagePicker.popoverPresentationController?.sourceView = sender
            self.present(self.imagePicker, animated: true, completion: nil)
            //self.assetView.isHidden=false
        }
        optionAlert.addAction(galleryAction)
        optionAlert.addAction(UIAlertAction(title: "Close", style: .cancel))
        optionAlert.popoverPresentationController?.sourceView = sender
        optionAlert.popoverPresentationController?.sourceRect = sender.bounds
        self.present(optionAlert, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nextButtonAction(_ sender: RoundableButton) {
        if imagesArray.isEmpty {
            self.view.showMessage(ValidationMessages.uploadImageError, type: .error)
            return
        }
        if statusType == OrderStatusType.startLoading {
            self.nextButton.isEnabled = false

            self.orderProcessCon!.dataToSend = [
                "beforeImages": imagesArray.map {$0.imageUrl},
                "afterImages": [],
                "startTime":Date().timeIntervalSince1970
            ]

            sendStatusToServer()
        }
        else if statusType == OrderStatusType.loaded {
            
            self.orderProcessCon!.dataToSend = [
                "beforeImages": [],
                "afterImages": imagesArray.map {$0.imageUrl},
                "startTime":Date().timeIntervalSince1970
            ]
            sendStatusToServer()
        }
        else if statusType == OrderStatusType.beforeUnload {
            self.nextButton.isEnabled = false

            self.orderProcessCon!.dataToSend = [
                "beforeImages": imagesArray.map {$0.imageUrl},
                "afterImages" : [],
                "startTime":Date().timeIntervalSince1970
            ]

            sendStatusToServer()
        }
        else if statusType == OrderStatusType.unloaded
        {
            self.orderProcessCon!.dataToSend = [
                "beforeImages": [],
                "afterImages" : imagesArray.map {$0.imageUrl},
                "startTime":Date().timeIntervalSince1970
            ]
            
            if (orderProcessCon?.driverJob.isMultiOrder ?? false && orderProcessCon!.currentIndex >= 0){
                if orderProcessCon!.currentIndex < orderProcessCon!.driverJob.extraDestinationLocations.count {
                    if (orderProcessCon!.driverJob.extraDestinationLocations[orderProcessCon!.currentIndex].podRequired) {
                        self.nextButton.isEnabled = false
                        sendStatusToServer()
                    }
                    else
                    {
                        sendStatusToServer()
                    }
                }
                else
                {
                    sendStatusToServer()
                }
            }
            else
            {
                if (orderProcessCon?.driverJob.podRequired ?? false)
                {
                    self.nextButton.isEnabled = false
                    sendStatusToServerWithoutHandleStatus()
                }
                else
                {
                    sendStatusToServer()
                }
            }
        }
        else if statusType == OrderStatusType.podRequested
        {
            sendStatusToServer()
        }
        if !orderProcessCon!.currentMuliWorkStatus.isEmpty
        {
            cancelOrderButton.isHidden = true
        }
    }
    private func handleStatusChange()
    {
        if statusType == OrderStatusType.startLoading{
            orderProcessCon?.changePageViewController(status: .loaded)
        }
        else if statusType == OrderStatusType.beforeUnload{
            orderProcessCon?.changePageViewController(status: .unloaded)
        }
        else  if statusType == OrderStatusType.loaded
        {
            if orderProcessCon!.driverJob.isMultiOrder
            {
                orderProcessCon!.currentIndex += 1
                if orderProcessCon!.currentIndex < orderProcessCon!.driverJob.extraStartLocations.count
                {
                    orderProcessCon?.currentMuliWorkStatus = OrderStatusType.multi_pickup_transit.rawValue
                    orderProcessCon?.changePageViewController(status: .onTheWay)
                }
                else{
                    orderProcessCon?.resetStatus()
                    orderProcessCon?.changePageViewController(status: .inTransit)
                }
            }else{
                orderProcessCon?.changePageViewController(status: .inTransit)
            }
        }
        else if statusType == OrderStatusType.unloaded
        {
            if orderProcessCon!.driverJob.isMultiOrder
            {
                orderProcessCon!.currentIndex += 1
                if orderProcessCon!.currentIndex < orderProcessCon!.driverJob.extraDestinationLocations.count {
                    
                    let dropPosition = orderProcessCon!.currentIndex - 1
                    
                    if (dropPosition != -1 && orderProcessCon!.driverJob.extraDestinationLocations[dropPosition].podRequired) {
                        orderProcessCon!.currentIndex = orderProcessCon!.currentIndex - 1
                        orderProcessCon?.currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                        orderProcessCon?.changePageViewController(status: .podRequested)
                    }
                    else
                    {
                       self.orderProcessCon!.driverJob.stopPoint = orderProcessCon!.currentIndex
                       orderProcessCon?.currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                       orderProcessCon?.changePageViewController(status: .atDestination)
                    }
                }
                else
                {
                    let dropPosition = orderProcessCon!.currentIndex - 1
                    
                    if (dropPosition != -1 && orderProcessCon!.driverJob.extraDestinationLocations[dropPosition].podRequired) {
                        orderProcessCon!.currentIndex = orderProcessCon!.currentIndex - 1
                        orderProcessCon?.currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                        orderProcessCon?.changePageViewController(status: .podRequested)
                    }
                    else
                    {
                        orderProcessCon?.resetStatus()
                        orderProcessCon?.changePageViewController(status: .finished)
                    }
                }
            }
            else
            {
                orderProcessCon?.changePageViewController(status: .finished)
            }
        }
        else if statusType == OrderStatusType.podRequested
        {
            if orderProcessCon!.driverJob.isMultiOrder
            {
                orderProcessCon!.currentIndex += 1
                if orderProcessCon!.currentIndex < orderProcessCon!.driverJob.extraDestinationLocations.count
                {
                    self.orderProcessCon!.driverJob.stopPoint = orderProcessCon!.currentIndex
                    orderProcessCon?.currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                    orderProcessCon?.changePageViewController(status: .atDestination)
                }
                else{
                    orderProcessCon?.resetStatus()
                    orderProcessCon?.changePageViewController(status: .finished)
                }
            }
            else
            {
                orderProcessCon?.changePageViewController(status: .finished)
            }
        }
    }
    private func sendStatusToServer() {
        var params :[String:Any] = [
            "orderId": orderProcessCon!.driverJob.orderId ?? ""
        ]
        var currentStatus = ""
        var multiWOStatus = ""
        var currentIndex = 0
        if orderProcessCon!.currentMuliWorkStatus.isEmpty{
            currentStatus = statusType.rawValue
            if statusType == OrderStatusType.podRequested{
                currentStatus = OrderStatusType.unloaded.rawValue
            }
        }else {
            currentStatus = orderProcessCon!.currentMuliWorkStatus
            multiWOStatus = statusType.rawValue
            if statusType == OrderStatusType.podRequested{
                multiWOStatus = OrderStatusType.unloaded.rawValue
            }
            currentIndex = orderProcessCon!.currentIndex
        }
        params["multiWOStatus"] = multiWOStatus
        if statusType == OrderStatusType.startLoading
        {
            params["loadingImages"] = self.orderProcessCon!.dataToSend
            params["unloadingImages"] = []
        }
        else  if statusType == OrderStatusType.loaded
        {
            params["loadingImages"] = self.orderProcessCon!.dataToSend
            params["unloadingImages"] = []
        }
        else  if statusType == OrderStatusType.beforeUnload
        {
            params["loadingImages"] = []
            params["unloadingImages"] = self.orderProcessCon!.dataToSend
        }
        else if statusType == OrderStatusType.unloaded{
            params["loadingImages"] = []
            params["unloadingImages"] = self.orderProcessCon!.dataToSend
        }
        else if statusType == OrderStatusType.podRequested{
            params["loadingImages"] = []
            params["unloadingImages"] = self.orderProcessCon!.dataToSend
            params["podRequestedImages"] = imagesArray.map {$0.imageUrl}
            params["multiWOStatus"] = OrderStatusType.unloaded.rawValue
        }
        params["orderStatus"] = currentStatus
        params["stopPoint"] = currentIndex
        params["driverId"] = HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id)
        
        if appDelegate().reachability.connection == .unavailable {
            var status = ""
            var multiOrderStatus = ""
            if (orderProcessCon!.currentMuliWorkStatus.isEmpty) {
                if statusType == OrderStatusType.podRequested {
                    status = OrderStatusType.podRequested.rawValue
                }
                else {
                    status = currentStatus
                }
            }
            else {
                status = currentStatus
                if statusType == OrderStatusType.podRequested {
                    multiOrderStatus = OrderStatusType.podRequested.rawValue
                }
                else {
                    multiOrderStatus = multiWOStatus
                }
            }
            
            CDDriverJobManager.shared.addPendingOrderStatus(orderProcessCon?.driverJob.orderId ?? "0", driverID: HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id), multiWOStatus: multiOrderStatus, orderStatus: status,images: imagesDataArray,stopPoint: Double(currentIndex)) { (result) in
                switch result {
                case .success(let data):
                    print("Succcess")
                    
                    CDDriverJobManager.shared.updateCurrentOrder(self.orderProcessCon?.driverJob.orderId ?? "0", status: status , multiWOStatus: multiWOStatus, stopPoint: currentIndex){ (error) in
                        guard let error = error else {return}
                        print(error)
                    }
                    
                    if (self.statusType == OrderStatusType.podRequested) {
                        HelperFunction.saveValueInUserDefaults(key: PodStausUpload.podStaus, value: "1")
                    }
                    
                    self.handleStatusChange()
                    
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
            
        }else{
            BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.updateWorkOrderStatus, jsonToPost: params) { (json) in
                if json != nil {
                    CDDriverJobManager.shared.updateCurrentOrder(self.orderProcessCon?.driverJob.orderId ?? "0", status: currentStatus , multiWOStatus: multiWOStatus, stopPoint: currentIndex){ (error) in
                        guard let error = error else {return}
                        print(error)
                    }
                    
                    if (self.statusType == OrderStatusType.podRequested) {
                        HelperFunction.saveValueInUserDefaults(key: PodStausUpload.podStaus, value: "1")
                    }
                    
                    self.handleStatusChange()
                }
            }
        }
    }
    
    private func sendStatusToServerWithoutHandleStatus() {
        var params :[String:Any] = [
            "orderId": orderProcessCon!.driverJob.orderId ?? ""
        ]
        var currentStatus = ""
        var multiWOStatus = ""
        var currentIndex = 0
        if orderProcessCon!.currentMuliWorkStatus.isEmpty{
            currentStatus = statusType.rawValue
            if statusType == OrderStatusType.podRequested{
                currentStatus = OrderStatusType.unloaded.rawValue
            }
        }else {
            currentStatus = orderProcessCon!.currentMuliWorkStatus
            multiWOStatus = statusType.rawValue
            currentIndex = orderProcessCon!.currentIndex
        }
        params["multiWOStatus"] = multiWOStatus
        if statusType == OrderStatusType.startLoading
        {
            params["loadingImages"] = self.orderProcessCon!.dataToSend
            params["unloadingImages"] = []
        }
        else  if statusType == OrderStatusType.loaded
        {
            params["loadingImages"] = self.orderProcessCon!.dataToSend
            params["unloadingImages"] = []
        }
        else  if statusType == OrderStatusType.beforeUnload
        {
            params["loadingImages"] = []
            params["unloadingImages"] = self.orderProcessCon!.dataToSend
        }
        else if statusType == OrderStatusType.unloaded{
            params["loadingImages"] = []
            params["unloadingImages"] = self.orderProcessCon!.dataToSend
        }
        else if statusType == OrderStatusType.podRequested{
            params["loadingImages"] = []
            params["unloadingImages"] = self.orderProcessCon!.dataToSend
            params["podRequestedImages"] = imagesArray.map {$0.imageUrl}
            params["multiWOStatus"] = OrderStatusType.unloaded.rawValue
        }
        params["orderStatus"] = currentStatus
        params["stopPoint"] = currentIndex
        params["driverId"] = HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id)
        
        if appDelegate().reachability.connection == .unavailable {
            var status = ""
            if statusType == OrderStatusType.podRequested {
                status = OrderStatusType.podRequested.rawValue
            }
            else {
                status = currentStatus
            }
            
            CDDriverJobManager.shared.addPendingOrderStatus(orderProcessCon?.driverJob.orderId ?? "0", driverID: HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id), multiWOStatus: multiWOStatus, orderStatus: status,images: imagesDataArray,stopPoint: Double(currentIndex)) { (result) in
                switch result {
                case .success(let data):
                    print("Succcess")
                    
                    CDDriverJobManager.shared.updateCurrentOrder(self.orderProcessCon?.driverJob.orderId ?? "0", status: status , multiWOStatus: multiWOStatus, stopPoint: currentIndex){ (error) in
                        guard let error = error else {return}
                        print(error)
                    }
                    self.orderProcessCon?.changePageViewController(status: .podRequested)
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
            
        }else{
            BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.updateWorkOrderStatus, jsonToPost: params) { (json) in
                if json != nil {
                    CDDriverJobManager.shared.updateCurrentOrder(self.orderProcessCon?.driverJob.orderId ?? "0", status: currentStatus , multiWOStatus: multiWOStatus, stopPoint: currentIndex){ (error) in
                        guard let error = error else {return}
                        print(error)
                    }
                    self.orderProcessCon?.changePageViewController(status: .podRequested)
                }
            }
        }
    }
    
    @IBAction func cancelOrderAction(_ sender: RoundableButton) {
        if (appDelegate().reachability.connection == .unavailable) {
            self.showAlertController(message: ValidationMessages.declinedOrderOffline)
        }
        else {
            self.orderProcessCon?.cancelOrder()
        }
    }
    
    private func uploadImageToServer(image : UIImage) {
        
        if appDelegate().reachability.connection == .unavailable {
            CDDriverJobManager.shared.addImage(self.orderProcessCon!.driverJob.orderId, uploadYype: self.imageUploadType, image: image.jpegData(compressionQuality: 0.7)!, imageUrl: nil, stopPoint: stopPoint, isMultiStops: isMultiStop) { (result) in
                switch result {
                case .success(let data):
                    self.imagesArray.append(data)
                    self.picCollectionView.reloadData()
                    self.ic_addMore.isHidden = false
                    self.cameraViewVerticalConst.priority = UILayoutPriority.init(249)
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        } else{
            self.showProgressBar()
            BaseServices.uploadImageToServer(image: image, imageType: ImageType.order.rawValue, role: Roles.driver.rawValue) { (json) in
                self.hideProgressBar()
                if let json = json {
                    if let url = json[ServerKeys.data].string {
                        CDDriverJobManager.shared.addImage(self.orderProcessCon!.driverJob.orderId, uploadYype: self.imageUploadType, image: image.jpegData(compressionQuality: 0.7)!, imageUrl: url, stopPoint: self.stopPoint, isMultiStops: self.isMultiStop) { (result) in
                            switch result {
                            case .success(let data):
                                self.imagesArray.append(data)
                                self.picCollectionView.reloadData()
                                self.ic_addMore.isHidden = false
                                self.cameraViewVerticalConst.priority = UILayoutPriority.init(249)
                                
                            case .failure(let error):
                                print(error.localizedDescription)
                            }
                        }
                        
                    }
                }else{
                    self.showMessage(ValidationMessages.tryAgain, type: .error)
                }
            }
        }
    }
    
    private func uploadFileToServer(data : Data)
    {
        self.showProgressBar()
        BaseServices.uploadFileToServer(image: data, fileType: ImageType.order.rawValue,  role: Roles.driver.rawValue) { (json) in
            self.hideProgressBar()
            if let json = json
            {
                if let url = json[ServerKeys.data].string
                {
                    CDDriverJobManager.shared.addImage(self.orderProcessCon!.driverJob.orderId, uploadYype: self.imageUploadType, image:data, imageUrl: url, stopPoint: self.stopPoint, isMultiStops: self.isMultiStop) { (result) in
                        switch result {
                        case .success(let data):
                            self.imagesArray.append(data)
                            self.picCollectionView.reloadData()
                            self.ic_addMore.isHidden = false
                            self.cameraViewVerticalConst.priority = UILayoutPriority.init(249)
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                        }
                    }
                }
            }else{
                self.showMessage(ValidationMessages.tryAgain, type: .error,options: [.textNumberOfLines(0)])
            }
        }
    }
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension DriverPicUploadController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellName, for: indexPath) as! ImageUploadingCell
        cell.deleteImage.tag = indexPath.row
        cell.delegate = self
        if imageUploadType == .podImage{
            cell.setUpImage(stringUrl: imagesArray[indexPath.row].imageUrl ?? "")
           // cell.setUpImage(stringUrl:)
        }else{
            if let imagedata = imagesArray[indexPath.row].imageData {
                cell.uploadedImage.image = UIImage(data: imagedata)
                cell.indicator.stopAnimating()
            }
        }
      
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
       presentZoomViewController(imagesArray[indexPath.row].imageUrl ?? "")
    }
}

extension DriverPicUploadController: DeleteImageDelegate {
    func deleteImageAtIndex(tag: Int) {
        let filename = HelperFunction.getFileNameFromUrl(stringUrl: imagesArray[tag].imageUrl ?? "")
        if filename.isEmpty || appDelegate().reachability.connection == .unavailable {
            if let id = self.imagesArray[tag].id {
//                CDImageManager.shared.deleteIfFound(predicate: NSPredicate(format: "id = %@", id))
                CDDriverJobManager.shared.deleteImageIfExist(self.orderProcessCon!.driverJob.orderId, id: id, uploadYype: self.imageUploadType, isMultiStops: self.isMultiStop)
            }
            
            if self.imagesArray.count != 0 {
                self.imagesArray.remove(at: tag)
            }
            
            if self.imagesDataArray.count != 0 {
                self.imagesDataArray.remove(at: tag)
            }
        
            self.picCollectionView.reloadData()
            if self.imagesArray.isEmpty {
                self.ic_addMore.isHidden = true
                self.cameraViewVerticalConst.priority = UILayoutPriority.init(251)
            }
            return
        }
        print(filename)
        let params :[String:Any] = [
            "role":Roles.driver.rawValue,
            "image": filename,
            "imageType":ImageType.order.rawValue
            
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Main.deleteImageFromS3, jsonToPost: params) { (json) in
            if let _ = json {
                if let id = self.imagesArray[tag].id {
//                    CDImageManager.shared.deleteIfFound(predicate: NSPredicate(format: "id = %@", id))
                    CDDriverJobManager.shared.deleteImageIfExist(self.orderProcessCon!.driverJob.orderId, id: id, uploadYype: self.imageUploadType, isMultiStops: self.isMultiStop)
                }
                self.imagesArray.remove(at: tag)
                self.picCollectionView.reloadData()
                if self.imagesArray.isEmpty {
                    self.ic_addMore.isHidden = true
                    self.cameraViewVerticalConst.priority = UILayoutPriority.init(251)
                }
            }
        }
    }
}
extension DriverPicUploadController : UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        picker.dismiss(animated: false, completion: nil)
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage{
            let imgData = NSData(data: image.jpegData(compressionQuality: 1)!)
            let imageSize: Int = imgData.count
            print("actual size of image in KB: %f ", Double(imageSize) / 1000.0)
            let uploadImage = image.resizeImage(newWidth: 500)
            let imgData1 = NSData(data: uploadImage.jpegData(compressionQuality: 1)!)
            let imageSize1: Int = imgData1.count
            print("compress size of image in KB: %f ", Double(imageSize1) / 1000.0)
            
            if self.imageUploadType == .podImage{
                let storyboard = UIStoryboard(name: StoryboardNames.driver, bundle: nil)
                let driverInfoController = storyboard.instantiateViewController(withIdentifier:IdentifierName.Driver.cropImageViewController) as! CropImageViewController
                driverInfoController.cropImage = uploadImage
                driverInfoController.delegate = self

                self.navigationController?.pushViewController(driverInfoController, animated: false)
//              var pdf =  createPDFDataFromImage(image: uploadImage)
//                uploadFileToServer(data:pdf as Data)
//            imagesDataArray.append(pdf as Data)
            }else{
                uploadImageToServer(image: uploadImage)
                imagesDataArray.append(image.jpegData(compressionQuality: 0.3)!)
            }
            
           
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        self.showMessage("Picture selection cancelled", type: .error)
    }
    
    func createPDFDataFromImage(image: UIImage) -> NSMutableData {
        let pdfData = NSMutableData()
        let imgView = UIImageView.init(image: image)
        let imageRect = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        UIGraphicsBeginPDFContextToData(pdfData, imageRect, nil)
        UIGraphicsBeginPDFPage()
        let context = UIGraphicsGetCurrentContext()
        imgView.layer.render(in: context!)
        UIGraphicsEndPDFContext()

        //try saving in doc dir to confirm:
        let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        let path = dir?.appendingPathComponent("file.pdf")

        do {
                try pdfData.write(to: path!, options: NSData.WritingOptions.atomic)
        } catch {
            print("error catched")
        }

        return pdfData
    }
    
}


extension DriverPicUploadController: zoomImageViewControllerDelegate {
    func presentZoomViewController(_ url: String) {
        let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
        let driverInfoController = storyboard.instantiateViewController(withIdentifier:IdentifierName.Driver.zoomImageViewController) as! ZoomImageViewController
//        driverInfoController.image = image.imageData
//        driverInfoController.urlString = image.imageUrl ?? ""
//        driverInfoController.mineType = "image/jpeg"
        driverInfoController.urlString = url
        driverInfoController.delegate = self
        self.navigationController?.present(driverInfoController, animated: false, completion: nil)
    }
    
    func dismissPresentViewController() {
        self.navigationController?.dismiss(animated: false, completion: nil)
    }
}

extension DriverPicUploadController:CropImageViewControllerDelegate{
    func dismissPresentViewController(image: UIImage) {
          var pdf =  createPDFDataFromImage(image: image)
            uploadFileToServer(data:pdf as Data)
        imagesDataArray.append(pdf as Data)
    }
    
    
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
