//
//  DriverHomeExtension.swift
//  EZER
//
//  Created by TimerackMac1 on 01/02/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import Foundation
import UIKit
extension OrderProcessViewController {
    func handleViewControllerAccordingToStatus() {
          switch driverJob.status {
          case .driverReview, .accepted?, .assigned?:
              currentStatus = .onTheWay
          case .onTheWay?, .onRouteToOrigin?:
              currentStatus = .atSource
          case .atSource?, .arrievedToOrigin?:
              currentStatus = .startLoading
          case .startLoading?:
              currentStatus = .loaded
          case .loaded?, .loading?:
              if driverJob.isMultiOrder{
                  if driverJob.extraStartLocations.isEmpty{
                      currentStatus = .inTransit
                  }else{
                      currentIndex = 0
                      currentMuliWorkStatus = OrderStatusType.multi_pickup_transit.rawValue//"MULTI_ORDER_PICKUP_IN_TRANSIST"
                      currentStatus = .onTheWay
                  }
              }else{
                  currentStatus = .inTransit
              }
          case .inTransit?, .onRouteDestination?:
              currentStatus = .atDestination
          case .multi_pickup_transit?:
              currentIndex = driverJob.stopPoint
              switch driverJob.multiWOStatus {
              case .onTheWay?, .onRouteToOrigin?:
                  currentMuliWorkStatus = OrderStatusType.multi_pickup_transit.rawValue
                  currentStatus = .atSource
              case .atSource?, .arrievedToOrigin?:
                  currentMuliWorkStatus = OrderStatusType.multi_pickup_transit.rawValue
                  currentStatus = .startLoading
              case .startLoading?:
                  currentMuliWorkStatus = OrderStatusType.multi_pickup_transit.rawValue
                  currentStatus = .loaded
              case .loaded?, .loading?:
                  currentIndex+=1
                  if currentIndex < driverJob.extraStartLocations.count
                  {
                      currentMuliWorkStatus = OrderStatusType.multi_pickup_transit.rawValue
                      currentStatus = .startLoading
                  }
                  else{
                      resetStatus()
                      currentStatus = .inTransit
                  }
              default:
                  currentMuliWorkStatus = OrderStatusType.multi_pickup_transit.rawValue
                  currentStatus = .atSource
              }
          case .atDestination?, .arrivedAtDestination?:
              currentStatus = .beforeUnload
          case .beforeUnload?:
              currentStatus = .unloaded
          case .unloaded?, .unLoaded?:
            if driverJob.isMultiOrder {
                if driverJob.extraDestinationLocations.isEmpty {
                    if (driverJob.podRequired ?? false) {
                        singleMultipleOrderWithPod()
                    }
                    else {
                        currentStatus = .finished
                    }
                }
                else
                {
                    if (driverJob.podRequired ?? false) {
                        multiOrderWithExtraDestinationWithPod()
                    }
                    else {
                        currentIndex = 0
                        currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                        currentStatus = .atDestination
                    }
                }
            }
            else
            {
                if (driverJob.podRequired ?? false) {
                    currentStatus = .podRequested
                }
                else{
                    currentStatus = .finished
                }
            }
            break
          case .podRequested:
              currentStatus = .finished
          case .multi_drop_transit?:
              currentIndex = driverJob.stopPoint
              switch driverJob.multiWOStatus {
              case .inTransit?, .onRouteDestination?:
                  currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                  currentStatus = .atDestination
              case .atDestination?, .arrivedAtDestination?:
                  currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                  currentStatus = .beforeUnload
              case .beforeUnload?:
                  currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                  currentStatus = .unloaded
              case .unloaded?, .unLoaded?:
                currentIndex += 1
                if currentIndex < driverJob.extraDestinationLocations.count
                {
                    let dropPosition = currentIndex - 1
                    if (dropPosition != -1 &&  driverJob.extraDestinationLocations[dropPosition].podRequired) {
                        currentIndex = currentIndex - 1
                        multiOrderDropInTransit()
                    }
                    else
                    {
                        currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                        currentStatus = .atDestination
                    }
                }
                else
                {
                    currentIndex = currentIndex - 1
                    multiOrderDropInTransitPodRequest()
                }
              case .podRequested:
                    currentIndex+=1
                    if currentIndex < driverJob.extraDestinationLocations.count
                    {
                        currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                        currentStatus = .atDestination
                    }
                    else{
                        resetStatus()
                        currentStatus = .finished
                    }
              default:
                  currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                  currentStatus = .atDestination
              }
          default:
              break
          }
      }

    func singleMultipleOrderWithPod() {
        let podStatus = HelperFunction.getSrtingFromUserDefaults(key: PodStausUpload.podStaus)
        if (podStatus == "0" || podStatus == "")
        {
            if (driverJob.podImages.isEmpty) {
                currentStatus = .podRequested
            }
            else {
                if driverJob.podImages.count > 0 {
                    currentStatus = .podRequested
                }
                else {
                    currentStatus = .finished
                }
            }
        }
    }
    
    func multiOrderWithExtraDestinationWithPod() {
        let podStatus = HelperFunction.getSrtingFromUserDefaults(key: PodStausUpload.podStaus)
        if (podStatus == "0" || podStatus == "")
        {
            if (driverJob.podImages.isEmpty) {
                currentStatus = .podRequested
            }
            else {
                if driverJob.podImages.count > 0 {
                    currentStatus = .podRequested
                }
                else {
                    currentIndex = 0
                    currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                    currentStatus = .atDestination
                }
            }
        }
        else
        {
            currentIndex = 0
            currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
            currentStatus = .atDestination
        }
    }
    
    func multiOrderDropInTransit() {
        let podStatus = HelperFunction.getSrtingFromUserDefaults(key: PodStausUpload.podStaus)
        if (podStatus == "0" || podStatus == "")
        {
            if (driverJob.extraDestinationLocations[currentIndex].podImages.isEmpty)
            {
                currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                currentStatus = .podRequested
            }
            else
            {
                if driverJob.extraDestinationLocations[currentIndex].podImages.count > 0
                {
                    currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                    currentStatus = .podRequested
                }
                else
                {
                    currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                    currentStatus = .atDestination
                }
            }
        }
        else
        {
            currentIndex = currentIndex + 1
            currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
            currentStatus = .atDestination
        }
    }
    
    func multiOrderDropInTransitPodRequest() {
        let podStatus = HelperFunction.getSrtingFromUserDefaults(key: PodStausUpload.podStaus)
        if (podStatus == "0" || podStatus == "")
        {
            if (driverJob.extraDestinationLocations[currentIndex].podImages.isEmpty)
            {
                currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                currentStatus = .podRequested
            }
            else
            {
                if driverJob.extraDestinationLocations[currentIndex].podImages.count > 0
                {
                    currentMuliWorkStatus = OrderStatusType.multi_drop_transit.rawValue
                    currentStatus = .podRequested
                }
                else
                {
                    resetStatus()
                    currentStatus = .finished
                }
            }
        }
        else
        {
            resetStatus()
            currentStatus = .finished
        }
    }
    
    private func setDataToDataSend(unfinishedOrder : UnfinishedOrderModel)
    {
        self.dataToSend = [
            "beforeImages": unfinishedOrder.beforeImages,
            "startTime":unfinishedOrder.startTime,
            "afterImages":unfinishedOrder.afterImages
        ]
    }
    
    func handleViewController(unfinishedOrder : UnfinishedOrderModel)
    {
        let status = OrderStatusType(rawValue: unfinishedOrder.status)!
        switch status {
        case .accepted, .assigned:
            currentStatus = .onTheWay
        case .onTheWay, .onRouteToOrigin:
            currentStatus = .onTheWay
        case .atSource, .arrievedToOrigin:
            currentStatus = .atSource
        case .startLoading:
            currentStatus = .startLoading
            setDataToDataSend(unfinishedOrder: unfinishedOrder)
        case .loadTimer:
            currentStatus = .loadTimer
            setDataToDataSend(unfinishedOrder: unfinishedOrder)
        case .loaded, .loading :
            currentStatus = .loaded
            setDataToDataSend(unfinishedOrder: unfinishedOrder)
            
        case .inTransit, .onRouteDestination:
            currentStatus = .inTransit
        case .atDestination, .arrivedAtDestination:
            currentStatus = .atDestination
        case .beforeUnload:
            currentStatus = .beforeUnload
            setDataToDataSend(unfinishedOrder: unfinishedOrder)
        case .unloadTimer:
            currentStatus = .unloadTimer
            setDataToDataSend(unfinishedOrder: unfinishedOrder)
        case .unloaded, .unLoaded:
            currentStatus = .unloaded
            setDataToDataSend(unfinishedOrder: unfinishedOrder)
        case .finished:
            currentStatus = .finished
        case .multi_pickup_transit:
            self.currentMuliWorkStatus = unfinishedOrder.status
            self.currentIndex = Int(unfinishedOrder.stop)
            guard let multiStatus = OrderStatusType(rawValue: unfinishedOrder.multiWOStatus) else{
                return
            }
            switch multiStatus
            {
            case .onTheWay, .onRouteToOrigin:
                currentStatus = .onTheWay
            case .atSource, .arrievedToOrigin:
                currentStatus = .atSource
            case .startLoading:
                currentStatus = .startLoading
                setDataToDataSend(unfinishedOrder: unfinishedOrder)
            case .loadTimer:
                currentStatus = .loadTimer
                setDataToDataSend(unfinishedOrder: unfinishedOrder)
            case .loaded, .loading :
                currentStatus = .loaded
                setDataToDataSend(unfinishedOrder: unfinishedOrder)
                
            default: currentStatus = .timeout
            }
        case .multi_drop_transit:
            self.currentMuliWorkStatus = unfinishedOrder.status
            self.currentIndex = Int(unfinishedOrder.stop)
            guard let multiStatus = OrderStatusType(rawValue: unfinishedOrder.multiWOStatus) else{
                return
            }
            switch multiStatus
            {
            case .inTransit, .onRouteDestination:
                currentStatus = .inTransit
            case .atDestination, .arrivedAtDestination:
                currentStatus = .atDestination
            case .beforeUnload:
                currentStatus = .beforeUnload
                setDataToDataSend(unfinishedOrder: unfinishedOrder)
            case .unloadTimer:
                currentStatus = .unloadTimer
                setDataToDataSend(unfinishedOrder: unfinishedOrder)
            case .unloaded, .unLoaded:
                currentStatus = .unloaded
                setDataToDataSend(unfinishedOrder: unfinishedOrder)
            default:
                currentStatus = .timeout
            }
        default:
            break
        }
    }
}
