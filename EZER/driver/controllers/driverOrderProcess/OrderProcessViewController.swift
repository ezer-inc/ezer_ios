//
//  OrderProcessViewController.swift
//  EZER
//
//  Created by TimerackMac1 on 15/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class OrderProcessViewController: UIViewController {
    var viewCon: UIViewController?
    var pageViewController : UIPageViewController!
    var currentStatus : OrderStatusType = .accepted
    var driverJob : DriverAvailableJobs!
    var currentIndex : Int = -1
    var currentMuliWorkStatus = ""
    var dataToSend:[String:Any] = [:] // changing from controllers

    lazy var callCustomButton :UIButton = {
        let button = UIButton.init(type: .custom)
        button.backgroundColor = UIColor.customerTheme
        button.setImage(#imageLiteral(resourceName: "ic_callButton"), for: .normal)
        let height = self.navigationController!.navigationBar.frame.height-25 + UIApplication.shared.statusBarFrame.height
        button.frame = CGRect.init(x: UIScreen.main.bounds.width - 70, y: height, width: 50, height: 50)
        button.layer.cornerRadius = 25
        button.addTarget(self, action: #selector(assignTwillioNumber), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageViewController = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.OrderProcessPageView) as! UIPageViewController
        
        handleViewControllerAccordingToStatus()
        
        print(driverJob.orderId)
        viewCon = self.viewControllerAtStatus(status: currentStatus)
        self.pageViewController.setViewControllers([viewCon!], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
        // Do any additional setup after loading the view.
        self.pageViewController.view.frame = self.view.bounds
        self.addChild(pageViewController)
        self.view.addSubview(pageViewController.view)
        self.pageViewController.willMove(toParent: self)
        self.pageViewController.didMove(toParent: self)
        
        /*let backBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonClicked))
         self.navigationItem.setLeftBarButton(backBarButtonItem, animated: true)
         backBarButtonItem.isEnabled = false*/
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.menuButton(self, action: #selector(openOrderDetail), image: #imageLiteral(resourceName: "ic_viewDetails"), size: 30)
        self.navigationItem.hidesBackButton = true
        if appDelegate().reachability.connection != .unavailable {
            getOrderDetails()
        }
    }
    
    @objc func openOrderDetail(sender : UIButton)
    {
        let popOver = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.popOverDetail) as! PopOverDetailController
        popOver.driverJob = driverJob
        popOver.modalPresentationStyle = .popover
        popOver.popoverPresentationController?.delegate = self
        popOver.popoverPresentationController?.barButtonItem = UIBarButtonItem(customView: sender)
        popOver.popoverPresentationController?.backgroundColor = UIColor.init(hex: "233C49")
        self.present(popOver, animated: true)
    }
    
    @objc func backButtonClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
    }
    
    private func addObserver()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(handleOrderCancelFromCustomer(notification:)), name: .scheduledOrderCancelFromCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleOrderCancelFromCustomer(notification:)), name: .orderCancelFromCustomer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleOrderCancelFromAdmin(notification:)), name: .orderCanceledByAdmin, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getOrderDetails), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func getOrderDetails(){
        let params :[String:Any] = [
            "orderId": driverJob.orderId
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getOrderDetail, jsonToPost: params) { (json) in
            if let json = json {
                let driverJob = DriverAvailableJobs(json: json[ServerKeys.data])
                if let controller = self.viewCon as? DriverLocationController {
                    controller.nameLabel.text = "\(driverJob.customerDetail.firstName ?? "") \(driverJob.customerDetail.lastName ?? "")"
                }
                if driverJob.driverId == HelperFunction.getUserId() {
                    if driverJob.status == .cancelled
                    {
                        self.handleCancelOrder()
                    }else if driverJob.status == .pause{
                        // let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        //  appDelegate.redirectToPauseScreen( id:driverJob.orderId)
                        SocketRouterForPauseController.managePauseViewController(data: driverJob.orderId)
                    }
                    else {
                        SocketRouterForPauseController.managePauseViewControllerForResume()
                    }
                }else{
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    
    private func handleCancelOrder() {
        let action = UIAlertAction.init(title: "OK", style: .cancel){
            action in
            self.popToRootController()
        }
        showAlertController(message: ValidationMessages.orderCancelByUser,action: action)
    }
    
    private func removeObserver() {
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit {
        removeObserver()
    }
    
    @objc func handleOrderCancelFromCustomer(notification:Notification) {
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            if let orderId = notificationObject["orderId"] as? String
            {
                if orderId == driverJob.orderId
                {
                    handleCancelOrder()
                }
            }
        }
    }
    @objc func handleOrderCancelFromAdmin(notification:Notification) {
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            if let orderId = notificationObject["orderId"] as? String
            {
                if orderId == driverJob.orderId
                {
                    DispatchQueue.main.async {
                        let action = UIAlertAction.init(title: "OK", style: .cancel){
                            action in
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                        self.showAlertController(message: ValidationMessages.orderCancelByAdmin,action: action)
                    }
                }
            }
        }
    }
    private func showCallButton(isShow: Bool = true){
        if isShow{
            if callCustomButton.superview == nil {
                self.navigationController!.view.addSubview(callCustomButton)
            }
        }else {
            if callCustomButton.superview != nil {
                callCustomButton.removeFromSuperview()
            }
        }
    }
    func viewControllerAtStatus(status : OrderStatusType) -> UIViewController? {
        self.navigationItem.setTitle(title: "Order \(driverJob.orderNumber!)", subtitle: status.rawValue)
        switch status {
        case .onTheWay,.atSource:
            if status == .onTheWay {
                self.navigationItem.setTitle(title: "Order \(driverJob.orderNumber!)", subtitle:"Assigned")
            }else{
                self.navigationItem.setTitle(title: "Order \(driverJob.orderNumber!)", subtitle:"On Route")
            }
            showCallButton(isShow: false)
            let driverLocationController = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.driverLocationStatus) as! DriverLocationController
            driverLocationController.statusType =  status
            driverLocationController.orderProcessCon = self
            return driverLocationController
        case .startLoading: // start loading items. truck is empty
            showCallButton()
            self.navigationItem.setTitle(title: "Order \(driverJob.orderNumber!)", subtitle:"Arrived")
            let driverPicUpload = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.driverPicUpload) as! DriverPicUploadController
            driverPicUpload.statusType = .startLoading
            driverPicUpload.orderProcessCon = self
//            if let unfinishOrder = unfinishOrder{
//                driverPicUpload.imagesArray = unfinishOrder.beforeImages
//            }
            return driverPicUpload
        case .loadTimer:
        showCallButton()
        self.navigationItem.setTitle(title: "Order \(driverJob.orderNumber!)", subtitle:"Arrived")
            let loadTimer = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.loadTimerView) as! LoadTimerController
            loadTimer.statusType = .loadTimer
            loadTimer.orderProcessCon = self
//            if let unfinishOrder = unfinishOrder{
//                loadTimer.currentTimer = Int(unfinishOrder.loadingTime)
//            }
            return loadTimer
        case .loaded:
        showCallButton()
        self.navigationItem.setTitle(title: "Order \(driverJob.orderNumber!)", subtitle:"Arrived")
            let driverPicUpload = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.driverPicUpload) as! DriverPicUploadController
            driverPicUpload.statusType = .loaded
            driverPicUpload.orderProcessCon = self
//            if let unfinishOrder = unfinishOrder{
//                driverPicUpload.imagesArray = unfinishOrder.afterImages
//            }
            return driverPicUpload
        case .inTransit: // on my way : source to destination
        showCallButton(isShow: false)
        self.navigationItem.setTitle(title: "Order \(driverJob.orderNumber!)", subtitle:"On Route")
            let driverLocationController = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.driverLocationStatus) as! DriverLocationController
            driverLocationController.statusType = .inTransit
            driverLocationController.orderProcessCon = self
            return driverLocationController
        case .atDestination:
        showCallButton(isShow: false)
        self.navigationItem.setTitle(title: "Order \(driverJob.orderNumber!)", subtitle:"On Route")
            let driverLocationController = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.driverLocationStatus) as! DriverLocationController
            driverLocationController.statusType = .atDestination
            driverLocationController.orderProcessCon = self
            return driverLocationController
        case .beforeUnload: // upload pics
            showCallButton()
            self.navigationItem.setTitle(title: "Order \(driverJob.orderNumber!)", subtitle:"Arrived")
            let driverPicUpload = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.driverPicUpload) as! DriverPicUploadController
            driverPicUpload.statusType = .beforeUnload
            driverPicUpload.orderProcessCon = self
//            if let unfinishOrder = unfinishOrder{
//                driverPicUpload.imagesArray = unfinishOrder.beforeImages
//            }
            return driverPicUpload
        case .unloadTimer:// unloading here
            showCallButton()
            self.navigationItem.setTitle(title: "Order \(driverJob.orderNumber!)", subtitle:"Arrived")
            let loadTimer = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.loadTimerView) as! LoadTimerController
            loadTimer.statusType = .unloadTimer
            loadTimer.orderProcessCon = self
//            if let unfinishOrder = unfinishOrder{
//                loadTimer.currentTimer = Int(unfinishOrder.loadingTime)
//            }
            return loadTimer
        case .unloaded:// send finish here
        showCallButton()
        self.navigationItem.setTitle(title: "Order \(driverJob.orderNumber!)", subtitle:"Arrived")
            let driverPicUpload = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.driverPicUpload) as! DriverPicUploadController
            driverPicUpload.statusType = .unloaded
            driverPicUpload.orderProcessCon = self
//            if let unfinishOrder = unfinishOrder{
//                driverPicUpload.imagesArray = unfinishOrder.afterImages
//            }
            return driverPicUpload
        case .podRequested:// send pod here
        showCallButton()
        self.navigationItem.setTitle(title: "Order \(driverJob.orderNumber!)", subtitle:"Arrived")
            let driverPicUpload = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.driverPicUpload) as! DriverPicUploadController
            driverPicUpload.statusType = .podRequested
            driverPicUpload.orderProcessCon = self
//            if let unfinishOrder = unfinishOrder{
//                driverPicUpload.imagesArray = unfinishOrder.afterImages
//            }
            return driverPicUpload
        case .finished, .done:
            //            let driverOrderCompleted = DriverOrderCompleted(nibName:"DriverOrderCompleted", bundle: nil)
            //            driverOrderCompleted.orderProcessCon = self
            //            driverOrderCompleted.statusType = .finished
            //            return driverOrderCompleted
            //        case .done:÷
            if (appDelegate().reachability.connection == .unavailable) {
                let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                    CDDriverJobManager.shared.addPendingOrderStatus(self.driverJob.orderId ?? "0", driverID: HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id), multiWOStatus: "", orderStatus: OrderStatusType.finished.rawValue,images: nil, stopPoint: 0) { (result) in
                        switch result {
                        case .success(let data):
                            print("Succcess")
                            CDDriverJobManager.shared.updateCurrentOrder(self.driverJob.orderId ?? "0", status:  OrderStatusType.finished.rawValue , multiWOStatus: "", stopPoint: self.driverJob.stopPoint) { (error) in
                                guard let error = error else {return}
                                print(error)
                            }
                        case .failure(let error):
                            print(error.localizedDescription)
                        }
                    }
                    self.popToRootController()
                })
                self.showAlertController(message: ValidationMessages.completedOrderOffline,action: action)
                
                return UIViewController()
            }
            else
            {
                showCallButton()
                self.navigationItem.setTitle(title: "Order \(driverJob.orderNumber!)", subtitle:"Completed")
                let getUserRating = GetUserRatingController(nibName:"GetUserRatingController", bundle: nil)
                getUserRating.orderProcessCon = self
                return getUserRating
            }
        //return self.navigationController?.popViewController(animated: true)
        default:
            return UIViewController()
        }
    }
    
    func changePageViewController(status : OrderStatusType, finalPrice : Double? = nil) {
        self.currentStatus = status
        if (self.currentStatus == OrderStatusType.finished && appDelegate().reachability.connection == .unavailable) {
            let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                HelperFunction.saveValueInUserDefaults(key: PodStausUpload.podStaus, value: "0")
                CDDriverJobManager.shared.addPendingOrderStatus(self.driverJob.orderId ?? "0", driverID: HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id), multiWOStatus: "", orderStatus: OrderStatusType.finished.rawValue,images: nil,stopPoint: 0) { (result) in
                    switch result {
                    case .success(let data):
                        print("Succcess")
                        CDDriverJobManager.shared.updateCurrentOrder(self.driverJob.orderId ?? "0", status:  OrderStatusType.finished.rawValue , multiWOStatus: "", stopPoint: self.driverJob.stopPoint) { (error) in
                            guard let error = error else {return}
                            print(error)
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
                self.popToRootController()
            })
            self.showAlertController(message: ValidationMessages.completedOrderOffline,action: action)
        }
        else {
            viewCon = self.viewControllerAtStatus(status: currentStatus)
            if let finalPrice = finalPrice
            {
                HelperFunction.saveValueInUserDefaults(key: PodStausUpload.podStaus, value: "0")
                if let controller = viewCon as? GetUserRatingController
                {
                    controller.finalAmount = finalPrice
                }
            }
            self.pageViewController.setViewControllers([viewCon!], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
        }
    }
    
    func resetStatus() {
        currentMuliWorkStatus = ""
        self.currentIndex = -1
    }
    
    func changeStatus(_ status : OrderStatusType) {
        self.currentStatus = status
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserver()
        showCallButton(isShow: false)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func cancelOrder() {
//        let cancelJobController = DriverCancelController(nibName:"DriverCancelController" , bundle: nil)
//        cancelJobController.message = ValidationMessages.cancelOrder
//        cancelJobController.actionConfirmed = {
//            action in
//            let cancelParams :[String:Any] = [
//                "orderId":self.driverJob.orderId,
//                "cancelReason": ""
//            ]
//            BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.cancelOrder, jsonToPost: cancelParams) { (json) in
//                if let _ = json
//                {
//                    self.handleCancelEmit()
////                    CDMyJobsManager.shared.deleteWorkOrder(self.driverJob.orderId ?? "")
//                    self.popToRootController()
//                }
//            }
//        }
//        cancelJobController.actionCanceled = { (action)-> () in}
//        cancelJobController.modalPresentationStyle = .overCurrentContext
//        self.present(cancelJobController, animated: false, completion: nil)
        
        
        if appDelegate().reachability.connection == .unavailable {
            self.showAlertController(message: ValidationMessages.declinedOrderOffline)
        }
        else{
            let cancelJobController = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.cancelViewController) as! CancelJobViewController
            cancelJobController.actionConfirmed = {
                action in
                self.declineOrder(reason: action)
            }
            cancelJobController.actionCanceled = { (action)-> () in}
            
            cancelJobController.modalPresentationStyle = .overCurrentContext
            self.present(cancelJobController, animated: false, completion: nil)
        }
    }
    
    func declineOrder(reason:String) {
        let cancelParams :[String:Any] = [
            "orderId":self.driverJob.orderId,
            "cancelReason": reason
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.cancelOrder, jsonToPost: cancelParams) { (json) in
            if let _ = json
            {
                self.handleCancelEmit()
//                    CDMyJobsManager.shared.deleteWorkOrder(self.driverJob.orderId ?? "")
                self.popToRootController()
            }
        }
    }
    
    
    private func handleCancelEmit()
    {
        if self.driverJob.immediate{
            NotificationCenter.default.post(name: .refreshDriverData, object: nil)
        }/*else{
         SocketIOManager.sharedInstance.refreshDriverScreens(customerData: ["orderId" : self.driverJob.orderId])
         }
         SocketIOManager.sharedInstance.sendCancelOrderFromDriver(fromDriverId: HelperFunction.getUserId(), toCustomerId: self.driverJob.customerId, orderId: self.driverJob.orderId)*/
    }
    
    func popToRootController()
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func assignTwillioNumber()
    {
        let params :[String:Any] = [
            "orderId": driverJob.orderId
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Main.assignTwilioNumber, jsonToPost: params) { (json) in
            if let json = json
            {
                if let twilioNumber = json["twilioNumber"].string
                {
                    HelperFunction.callToNumber(number: twilioNumber)
                }
            }
        }
    }
}

extension OrderProcessViewController:UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}
