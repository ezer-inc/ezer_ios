 //
 //  DriverLocationController.swift
 //  EZER
 //
 //  Created by TimerackMac1 on 24/11/17.
 //  Copyright © 2017 TimerackMac1. All rights reserved.
 //
 
 import UIKit
 import GoogleMaps
 import GooglePlaces
 class DriverLocationController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressHeader: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0
    @IBOutlet weak var locationMap: GMSMapView!
    var selectedPlace: GMSPlace?
    var locationManager : LocationManager!
    @IBOutlet weak var sendStatusButton: RoundableButton!
    weak var orderProcessCon :  OrderProcessViewController?
    var statusType : OrderStatusType!
    var destinationCordinate : CLLocationCoordinate2D?
    var gmsPolyline : GMSPolyline!
    @IBOutlet weak var cancelOrderButton: RoundableButton!
    var currentMarker : GMSMarker!
    var currentCoordinate : CLLocationCoordinate2D!
    var destinationMarker : GMSMarker!
    var currentOrderId : String = ""
    var isCheckInitial = true
    override func viewDidLoad() {
        super.viewDidLoad()
        placesClient = GMSPlacesClient.shared()
        locationMap.delegate = self
        locationMap.isHidden = true
        //locationMap.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //mapView.isMyLocationEnabled = true
        setViewAccordningTOStatus()
        if let customerDetail = self.orderProcessCon?.driverJob.customerDetail {
            nameLabel.text = "\(customerDetail.firstName ?? "") \(customerDetail.lastName ?? "")"
        }
    }
    

    private func setViewAccordningTOStatus() {
        var locationModel :LocationModel?
        switch statusType{
        case .inTransit?,.atDestination?:
            if statusType == .atDestination{
                sendStatusButton.setTitle("I'M  HERE", for: .normal)
                handleImhere() // disable for i am here
            }
            addressHeader.text = "Drop Off Point"
            cancelOrderButton.isHidden = true
            if orderProcessCon?.currentIndex == -1
            {
                addressLabel.text = String(format: "%@\n%@",orderProcessCon?.driverJob.destinationAddress ?? "",orderProcessCon?.driverJob.destinationAddressUnitNumber ?? "").trimWhiteSpace()
                locationModel = orderProcessCon?.driverJob.destinationLocation
            }else{
                addressLabel.text = String(format: "%@\n%@",orderProcessCon?.driverJob.extraDestinationLocations[orderProcessCon!.currentIndex].address ?? "",orderProcessCon?.driverJob.extraDestinationLocations[orderProcessCon!.currentIndex].destinationAddressUnitNumber ?? "").trimWhiteSpace()
                locationModel = orderProcessCon!.driverJob.extraDestinationLocations![orderProcessCon!.currentIndex].location
                
            }
        default:
            if statusType == .atSource{
                sendStatusButton.setTitle("I'M  HERE", for: .normal)
                handleImhere() // disable for i am here
            }
            if orderProcessCon?.currentIndex == -1
            {
                addressLabel.text = String(format: "%@\n%@",orderProcessCon?.driverJob.startAddress ?? "",orderProcessCon?.driverJob.startAddressUnitNumber ?? "").trimWhiteSpace()
                locationModel = orderProcessCon?.driverJob.startLocation
            }else{
                addressLabel.text = String(format: "%@\n%@", orderProcessCon?.driverJob.extraStartLocations[orderProcessCon!.currentIndex].address ?? "",orderProcessCon?.driverJob.extraStartLocations[orderProcessCon!.currentIndex].startAddressUnitNumber ?? "").trimWhiteSpace()
                
                locationModel = orderProcessCon!.driverJob.extraStartLocations![orderProcessCon!.currentIndex].location
            }
            cancelOrderButton.isHidden = false
            
        }
        if let locationModel = locationModel
        {
            if let coordinate = locationModel.locationCordinate{
                let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude,longitude: coordinate.longitude,zoom: 12)
                self.locationMap?.camera = camera
            }
            setDestinationCordinate(locationModel)
        }
        if !orderProcessCon!.currentMuliWorkStatus.isEmpty
        {
            cancelOrderButton.isHidden = true
        }
    }
    
    private func disableSendButton(isDisabled: Bool = true)
    {
        if isDisabled
        {
            sendStatusButton.isEnabled = false
            sendStatusButton.alpha = 0.6
//            sendStatusButton.isEnabled = true
//            sendStatusButton.alpha = 1kujh discuss 
        }else{
            sendStatusButton.isEnabled = true
            sendStatusButton.alpha = 1
        }
    } 
    
    private func handleImhere()
    {
        print(statusType)
        /* if statusType == OrderStatusType.atSource || statusType == OrderStatusType.atDestination
         {
         guard (currentCoordinate) != nil, let destinationCordinate = self.destinationCordinate else{
         self.view.makeToast("Not able to fetch location")
         return
         }
         if HelperFunction.distanceInMeters(currentCoordinate, destination: destinationCordinate) > 250
         {
         self.view.makeToast("Please Go Near to Location \( HelperFunction.distanceInMeters(currentCoordinate, destination: destinationCordinate))" )
         return
         }
         }*/ // disable i m here
        
//        if statusType == OrderStatusType.accepted{
//            if isCheckInitial{
//                updateDriverLocation(currentDriverLocation:currentCoordinate )
//            }
//
//        }
//        if statusType == OrderStatusType.onTheWay{
//            if isCheckInitial{
//                updateDriverLocation(currentDriverLocation:currentCoordinate )
//            }
//
//        }
        if statusType == OrderStatusType.atSource || statusType == OrderStatusType.atDestination
        {
            guard (currentCoordinate) != nil, let destinationCordinate = self.destinationCordinate else{
                self.view.makeToast("Not able to fetch location")
                
                return
            }
            
           let myCordinates = Double(HelperFunction.getEnableIMHereButtonDistance())
            
            if HelperFunction.distanceInMeters(currentCoordinate, destination: destinationCordinate) < myCordinates
            {
                // sendStatusButton.isEnabled = true
                disableSendButton(isDisabled: false)
            }else{
                //sendStatusButton.isEnabled = false
                disableSendButton(isDisabled: true)
            }
        }
        
    }
    
    private func setDestinationCordinate(_ locationModel : LocationModel)
    {
        if !locationModel.coordinates.isEmpty
        {
            destinationCordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(locationModel.coordinates[1]), longitude: CLLocationDegrees(locationModel.coordinates[0]))
            destinationMarker = GMSMarker.init(position: destinationCordinate! )
            destinationMarker.icon = #imageLiteral(resourceName: "ic_destinationLocation")
            destinationMarker.title = addressHeader.text
            destinationMarker.snippet = addressLabel.text
            destinationMarker.map = locationMap
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        locationManager = LocationManager(delegate:self)
        locationManager.startUpdatingLocation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //  locationMap = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func addMarkerOnMap(_ coordinate : CLLocationCoordinate2D)
    {
        //locationMap.clear()
        if currentMarker == nil{
            currentMarker = GMSMarker.init(position: coordinate )
            currentMarker.icon = #imageLiteral(resourceName: "ic_ezerMarker")
            currentMarker.map = locationMap
        }
        else{
            currentMarker.position = coordinate
        }
    }
    @IBAction func callCustomer(_ sender: Any) {
        let params :[String:Any] = [
            "orderId": orderProcessCon?.driverJob.orderId ?? "0"
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Main.assignTwilioNumber, jsonToPost: params) { (json) in
            if let json = json
            {
                if let twilioNumber = json["twilioNumber"].string
                {
                    HelperFunction.callToNumber(number: twilioNumber)
                }
            }
        }
    }
    
    @IBAction func sendStatusAction(_ sender: Any) {
        sendStatusToServer()
    }
    
    @IBAction func cancelOrderAction(_ sender: RoundableButton) {
        if (appDelegate().reachability.connection == .unavailable) {
            self.showAlertController(message: ValidationMessages.declinedOrderOffline)
        }
        else{
            self.orderProcessCon?.cancelOrder()
        }
    }
    
    private func sendStatusToServer()
    {
        var currentStatus = ""
        var multiWOStatus = ""
        var currentIndex = 0
        if orderProcessCon!.currentMuliWorkStatus.isEmpty{
            currentStatus = statusType.rawValue
        }
        else
        {
            currentStatus = orderProcessCon!.currentMuliWorkStatus
            multiWOStatus = statusType.rawValue
            currentIndex = orderProcessCon!.currentIndex
        }
        let params :[String:Any] = [
            "orderId": orderProcessCon?.driverJob.orderId ?? "0",
            "driverId":HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            "orderStatus" : currentStatus,
            "loadingImages" : [],
            "unloadingImages" : [],
            "multiWOStatus":multiWOStatus,
            "stopPoint":currentIndex
            
        ]

        if appDelegate().reachability.connection == .unavailable {
            CDDriverJobManager.shared.addPendingOrderStatus(orderProcessCon?.driverJob.orderId ?? "0", driverID: HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id), multiWOStatus: multiWOStatus, orderStatus: currentStatus,images: nil,stopPoint: Double(currentIndex)) { (result) in
                switch result {
                case .success(let data):
                    print("Succcess")
                    
                    CDDriverJobManager.shared.updateCurrentOrder(self.orderProcessCon?.driverJob.orderId ?? "0", status: currentStatus , multiWOStatus: multiWOStatus,stopPoint: currentIndex){ (error) in
                        guard let error = error else {return}
                        print(error)
                    }
                    
                    self.handleStatusChange()
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
            
        }else{
            BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.updateWorkOrderStatus, jsonToPost: params) { (json) in
                if json != nil{
                    //print(self.statusType.rawValue,"current status")
                    //self.sendChangeStatusToCustomer(self.statusType)
                    CDDriverJobManager.shared.updateCurrentOrder(self.orderProcessCon?.driverJob.orderId ?? "0", status: currentStatus , multiWOStatus: multiWOStatus, stopPoint: currentIndex){ (error) in
                        guard let error = error else {return}
                        print(error)
                    }
                    self.handleStatusChange()
                }
            }
        }
    }
    
    /*private func sendChangeStatusToCustomer(_ status : OrderStatusType)
     {
     var customerData =  ["status" :status.rawValue ]
     customerData["orderId"] = orderProcessCon!.driverJob.orderId
     SocketIOManager.sharedInstance.sendChangeOrderStatus(toCustomerId: orderProcessCon!.driverJob.customerId, customerData: customerData)
     }*/
    
    func updateDriverLocation(currentDriverLocation:CLLocationCoordinate2D) {
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            "longitude":currentDriverLocation.longitude,
            "latitude":currentDriverLocation.latitude,
            "orderId": orderProcessCon?.driverJob.orderId ?? "0"
        ]
        print(param)
        BaseServices.SendPostJson(viewController: self,serverUrl: ServerUrls.Driver.updateDriverLocation, jsonToPost: param, isResponseRequired: true,showLoader : false ) { (json) in
            if let json = json{
                self.isCheckInitial = false
                print("Location Updated",json)
            }
        }
        
        let fbParam: [String:Any] = [
            "deviceType": "ios",
            "longitude":currentDriverLocation.longitude,
            "latitude":currentDriverLocation.latitude,
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            "orderId":currentOrderId,
            "orderNumber": orderProcessCon?.driverJob.orderNumber ?? 0,
            "orderStatus": orderProcessCon?.driverJob.status.rawValue ?? "NA"
        ]
        SendLocationData.shared.updateData(data: fbParam, orderNo: orderProcessCon?.driverJob.orderNumber ?? 0)
    }
    
    
    private func handleStatusChange()
    {
        if statusType == OrderStatusType.onTheWay{
            self.orderProcessCon?.navigationItem.setTitle(title: "Order \(self.orderProcessCon?.driverJob.orderNumber ?? 0)", subtitle:"On Route")
            sendStatusButton.setTitle("I'M  HERE", for: .normal)
            self.orderProcessCon?.changeStatus ( .atSource)
            statusType = .atSource
            handleImhere() // disable for  i am here
        }
        else if statusType == OrderStatusType.atSource{
            sendStatusButton.isEnabled = false
            //sendStatusButton.isEnabled = true
            self.orderProcessCon?.changePageViewController(status: .startLoading)
            
        }
        else if statusType == OrderStatusType.inTransit{
            sendStatusButton.setTitle("I'M  HERE", for: .normal)
            self.orderProcessCon?.changeStatus ( .atDestination)
            statusType = .atDestination
            handleImhere() // disable for  i am here
        }
        else if statusType == OrderStatusType.atDestination{
            sendStatusButton.isEnabled = false
            //sendStatusButton.isEnabled = true
            self.orderProcessCon?.changePageViewController(status: .beforeUnload)
        }
    }
    
    @IBAction func seeOnMap(_ sender : UIButton)
    {
        var strUrl = "http://maps.google.com/maps?saddr=\(currentCoordinate.latitude),\(currentCoordinate.longitude)&daddr=\(destinationCordinate!.latitude),\(destinationCordinate!.longitude)"
        if currentCoordinate != nil{
            if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)
            {
                strUrl = "comgooglemaps://?saddr=\(currentCoordinate.latitude),\(currentCoordinate.longitude)&daddr=\(destinationCordinate!.latitude),\(destinationCordinate!.longitude)&zoom=14&views=traffic"
            }
            
            if let url = URL(string: strUrl){
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
 }
 
 extension DriverLocationController : LocationManagerDelegate {
    func locationDenied() {
        
    }
    
    func didChangeinLocation(cordinate: CLLocationCoordinate2D) {
        // let camera = GMSCameraPosition.camera(withLatitude: cordinate.latitude,                                              longitude: cordinate.longitude,zoom: zoomLevel)
        /*if locationMap.isHidden {
         locationMap.isHidden = false
         locationMap.camera = camera
         } else {
         locationMap.animate(to: camera)
         }*/
        locationMap.isHidden = false
        if currentCoordinate == nil
        {
            guard let destinationCordinate = destinationCordinate else{ return}
            drawMap(cordinate,endCoordinate: destinationCordinate)
        }
        currentCoordinate = cordinate
        addMarkerOnMap(cordinate)
        handleImhere() // enable or disable Send button for location
        
        updateDriverLocation(currentDriverLocation:currentCoordinate)
        //locationManager.stopUpdatingLocation()
    }
    
    // draw the path between driver current location and source (where driver has reach)
    func drawMap (_ startCoordinate : CLLocationCoordinate2D, endCoordinate:CLLocationCoordinate2D)
    {
        //Update Map path offline
        if (appDelegate().reachability.connection == .unavailable){
            let path = GMSMutablePath()
            path.add(startCoordinate)
            path.add(endCoordinate)
            self.gmsPolyline = GMSPolyline(path: path)
            self.gmsPolyline.strokeWidth = 4
            self.gmsPolyline.strokeColor = UIColor.gray
            self.gmsPolyline.map = self.locationMap

            let bounds = GMSCoordinateBounds(coordinate: startCoordinate, coordinate: endCoordinate)
            let update = GMSCameraUpdate.fit(bounds, withPadding: 30)
            self.locationMap.animate(with: update)
        }
        else {
            let str = String(format:"https://maps.googleapis.com/maps/api/directions/json?origin=\(startCoordinate.latitude),\(startCoordinate.longitude)&destination=\(endCoordinate.latitude),\(endCoordinate.longitude)&key=%@",IdentifierName.GooglePlaceApiKey)
            
            
            BaseServices.sendGetJson( serverUrl: str) { (json) in
                
                guard let resJson = json else{return}
                
                if(resJson["status"].string == "ZERO_RESULTS")
                {
                    
                }
                else if(resJson["status"].string == "NOT_FOUND")
                {
                    
                }
                else{
                    if let routes = resJson["routes"].array
                    {
                        if routes.count > 0
                        {
                            let route = routes[0].dictionary!
                            let overview_polyline = route["overview_polyline"]
                            let points = overview_polyline!["points"].string!
                            //print(points,"point")
                            self.showPath(polyStr: points)
                            if let legs = route["legs"]?.array
                            {
                                if !legs.isEmpty{
                                    self.destinationMarker.map = nil
                                    let leg = legs[0]
                                    if let start_location = leg["start_location"].dictionary{
                                        let startLat = start_location["lat"]!.double!
                                        let startLong = start_location["lng"]!.double!
                                        let startPosition = CLLocationCoordinate2D(latitude: CLLocationDegrees(startLat), longitude: CLLocationDegrees(startLong))
                                        
                                        let end_location = leg["end_location"].dictionary!
                                        let endLat = end_location["lat"]!.double!
                                        let endLong = end_location["lng"]!.double!
                                        let endPosition = CLLocationCoordinate2D(latitude: CLLocationDegrees(endLat), longitude: CLLocationDegrees(endLong))
                                        self.addPathHeader(startPosition, address: leg["start_address"].string!, title: "Start Address",icon: #imageLiteral(resourceName: "ic_startLocation"))
                                        self.addPathHeader(endPosition, address: leg["end_address"].string!, title: "Destination Address",icon: #imageLiteral(resourceName: "ic_destinationLocation"))
                                        let bounds = GMSCoordinateBounds(coordinate: startPosition, coordinate: endPosition)
                                        let update = GMSCameraUpdate.fit(bounds, withPadding: 30)
                                        self.locationMap.animate(with: update)
                                        //self.locationMap?.moveCamera(update)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    //add map path header
    private func addPathHeader(_ coordinate : CLLocationCoordinate2D,address: String,title: String,icon: UIImage)
    {
        let headeMarker = GMSMarker.init(position: coordinate )
        headeMarker.icon = icon
        headeMarker.title = title
        headeMarker.snippet = address
        headeMarker.map = locationMap
    }
    // draw poly line on path
    private func showPath(polyStr : String)
    {
        let newPath = GMSPath.init(fromEncodedPath: polyStr)
        self.gmsPolyline = GMSPolyline(path: newPath)
        self.gmsPolyline.strokeWidth = 4
        self.gmsPolyline.strokeColor = UIColor.gray
        self.gmsPolyline.map = self.locationMap
        
    }
    func didErrorinLocation(error: Error) {
//        locationManager.stopUpdatingLocation()
    }
    
    func locationNotAvailable() {
        DispatchQueue.main.async {
            if let _ = self.presentedViewController { return }
            self.showLocationAlert()
        }
    }
    func locationFaliedToUpdate(status: CLAuthorizationStatus) {
        DispatchQueue.main.async {
            self.showLocationAlert()
        }
    }
    
 }
 extension DriverLocationController : GMSMapViewDelegate{
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        return true
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        //addMrkerOnMap(coordinate:coordinate)
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        // reverseGeocodeCoordinate(coordinate: position.target)
    }
 }
