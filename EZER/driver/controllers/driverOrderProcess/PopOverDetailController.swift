//
//  PopOverDetailController.swift
//  EZER
//
//  Created by TimerackMac1 on 31/01/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit

class PopOverDetailController: UIViewController {
    @IBOutlet weak var lblPickDate: UILabel!
    @IBOutlet weak var lblPickTime: UILabel!
    @IBOutlet weak var lblEarnings: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    
    @IBOutlet weak var lblEarningsTitle: UILabel!
    
    var driverJob : DriverAvailableJobs!
    var imagesArray : [String]!
    let cellName = "ImageUploadingCell"
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraintOutlet: NSLayoutConstraint!
    @IBOutlet weak var itemDescritionTableView: UITableView!
    @IBOutlet weak var itemDescritionTableViewHeightConstraintOutlet: NSLayoutConstraint!
    var  navigationVC:UINavigationController = UINavigationController()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain,
                                                                target: nil, action: nil)
        lblPickDate.text = driverJob.pickupDate
        lblPickTime.text = driverJob.pickupTime
        lblDistance.text = String(format: "%.02f Miles", driverJob.totalDistance!)
        
        if driverJob.isSpecialRateApplied ?? false && driverJob.driverSpecialRate != 0{
            lblEarningsTitle.text = "Earnings"
            lblEarnings.text = String(format: "$%.02f",driverJob.driverSpecialRate)
        }else{
            lblEarningsTitle.text = "Estimated Earnings"
            lblEarnings.text = String(format: "$%.02f - $%.02f", driverJob.estimatedMinPrice!, driverJob.estimatedMaxPrice)
        }
        self.itemDescritionTableView.estimatedRowHeight = 380.0
        self.itemDescritionTableView.rowHeight = UITableView.automaticDimension
       // lblEarnings.text = String(format: "$%.02f - $%.02f", driverJob.estimatedMinPrice!, driverJob.estimatedMaxPrice)
    }
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    override func viewWillLayoutSubviews() {
        self.view.layoutIfNeeded()
        super.updateViewConstraints()
        self.tableViewHeightConstraintOutlet?.constant = self.tableView.contentSize.height
        
        let count = driverJob.extraStartLocations.count
       // if count > 0{
       //     self.itemDescritionTableViewHeightConstraintOutlet?.constant += self.itemDescritionTableView.contentSize.height
//}else{
            self.itemDescritionTableViewHeightConstraintOutlet?.constant = self.itemDescritionTableView.contentSize.height
       // }
       
    }
}

extension PopOverDetailController : UITableViewDataSource, UITableViewDelegate  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return   1
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        if tableView == itemDescritionTableView {
//            print(self.itemDescritionTableView.contentSize.height)
//        }
//        
//        return UITableView.automaticDimension
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView != itemDescritionTableView {
            let count = max(driverJob.extraStartLocations.count, driverJob.extraDestinationLocations.count)
            return count + 1// ONe is default row must have Source Or Destination
        }else{
            let count = driverJob.extraStartLocations.count
            return count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView != itemDescritionTableView {
            let  cell = tableView.dequeueReusableCell(withIdentifier:  "myJobsCell") as! MyJobsTableViewCell
            cell.setUpCell(indexPath, driverAvailableJob: driverJob)
            return cell;
        }else{
            let  cell = tableView.dequeueReusableCell(withIdentifier:  "PickUpDescriptionTableViewCell") as! PickUpDescriptionTableViewCell
            cell.setUpCell(indexPath, driverJob: driverJob)
            cell.delegate = self
            return cell;
        }
    }
}

extension PopOverDetailController:PickUpDescriptionTableViewCellDelegate,zoomImageViewControllerDelegate {
    func PresentZoomViewController(_ url: String) {
        let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
        let driverInfoController = storyboard.instantiateViewController(withIdentifier:IdentifierName.Driver.zoomImageViewController) as! ZoomImageViewController
        // driverInfoController.itemImage = cell.uploadedImage.image!
        driverInfoController.urlString = url
        driverInfoController.delegate = self
        //  self.navigationController?.present(driverInfoController, animated: false, completion: nil)
        navigationVC = UINavigationController(rootViewController: driverInfoController)
        self.present(navigationVC, animated: false, completion: nil)
    }
    
    func dismissPresentViewController() {
         navigationVC.dismiss(animated: false, completion: nil)
    }
    
}


