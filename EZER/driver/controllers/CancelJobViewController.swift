//
//  CancelJobViewController.swift
//  EZER
//

//  Created by Akash on 19/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class CancelJobViewController: UIViewController {

    @IBOutlet weak var textViewOutlet: UITextField!
    @IBOutlet weak var radionotfitButtonOutlet: UIButton!
    @IBOutlet weak var radionotfitSecondButtonOutlet: UIButton!
    @IBOutlet weak var radionotfitThirdButtonOutlet: UIButton!
    @IBOutlet weak var radionotfitFourthButtonOutlet: UIButton!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var radioOtherButtonOutlet: UIButton!
    var actionConfirmed:((_ action: String)->Void)?
    var actionCanceled:((_ action: Bool)->())?
    var selectedOptionText = "The order won't fit in my vehicle."
    
    var currentOtherSelected = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func ordernotfitButtonClicked(_ sender: Any) {
        textViewHeightConstraint.constant = 0
        currentOtherSelected = 0
        radionotfitButtonOutlet.setImage(#imageLiteral(resourceName: "radio_selected"), for: .normal)
        radioOtherButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radionotfitSecondButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radionotfitThirdButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radionotfitFourthButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        selectedOptionText = "The order won't fit in my vehicle."
    }
    @IBAction func ordernotfitSecondButtonClicked(_ sender: Any) {
        textViewHeightConstraint.constant = 0
        currentOtherSelected = 1
        radionotfitSecondButtonOutlet.setImage(#imageLiteral(resourceName: "radio_selected"), for: .normal)
        radioOtherButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radionotfitButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radionotfitThirdButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radionotfitFourthButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        selectedOptionText = "The order is too heavy for my vehicle."
    }
    @IBAction func ordernotfitThirdButtonClicked(_ sender: Any) {
        textViewHeightConstraint.constant = 0
        currentOtherSelected = 2
        radionotfitThirdButtonOutlet.setImage(#imageLiteral(resourceName: "radio_selected"), for: .normal)
        radioOtherButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radionotfitButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radionotfitSecondButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radionotfitFourthButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        selectedOptionText = "The order is too far away to pick up."
    }
    @IBAction func ordernotfitFourthButtonClicked(_ sender: Any) {
        textViewHeightConstraint.constant = 0
        currentOtherSelected = 3
        radionotfitFourthButtonOutlet.setImage(#imageLiteral(resourceName: "radio_selected"), for: .normal)
        radioOtherButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radionotfitThirdButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radionotfitSecondButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radionotfitButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        selectedOptionText = "I'm not available at this schediled time."
    }
    @IBAction func otherButtonClicked(_ sender: Any) {
        textViewHeightConstraint.constant = 30
        currentOtherSelected = 4
        radionotfitButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radioOtherButtonOutlet.setImage(#imageLiteral(resourceName: "radio_selected"), for: .normal)
        radionotfitThirdButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radionotfitSecondButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
        radionotfitFourthButtonOutlet.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
    }
    
    @IBAction func noButtonClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            self.actionCanceled?(false)
        })
    }
    
    @IBOutlet weak var yesButtonClicked: RoundableButton!
    
    @IBAction func yesButtonClicked(_ sender: Any) {
        if currentOtherSelected == 4{
            if textViewOutlet.text?.length == 0{
                self.view.showMessage(ValidationMessages.invalidReason, type: .error,options: [.textNumberOfLines(0)])
            }else{
                self.dismiss(animated: false, completion: {
                    self.actionConfirmed?(self.textViewOutlet.text!)
                })
            }
        }else{
            self.dismiss(animated: false, completion: {
                self.actionConfirmed?(self.selectedOptionText)
            })
        }
    }
}
