//
//  PaymentHistory.swift
//  EZER
//
//  Created by TimerackMac1 on 23/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class PaymentHistoryController: UIViewController {
    
    var mainModel = [HistoryMainModel]()
    @IBOutlet weak var paymentHistoryTable: UITableView!
    
    @IBOutlet weak var noPaymentLabel: UILabel!
    let cellName = "PaymentHistory"
    var keyArray = [String]()
    var dataDictionary :Dictionary<String,HistoryMainModel> =  [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Payment History"
        noPaymentLabel.isHidden = true
        paymentHistoryTable.delegate = self
        paymentHistoryTable.dataSource = self
        paymentHistoryTable.tableFooterView = UIView()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        getPaymentHistory()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func getPaymentHistory(){
        
        let params :[String:Any] = [
            "driverId":HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id)
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getDriverPaymentsHistory, jsonToPost: params,isResponseRequired : true) { (json) in
            if let json = json
            {
                if json[ServerKeys.status].int == 1{
                    if let scheduleJobsData = json[ServerKeys.data].array{
                        var historyModels = [PaymentHistoryModel]()
                        for job in scheduleJobsData{
                            let historyModelLocal = PaymentHistoryModel(json : job)
                            historyModelLocal.dateObject = self.convertStringToDate(dateString: historyModelLocal.date)
                            historyModels.append(historyModelLocal)
                        }
                        DispatchQueue.global(qos: .background).async {
                            self.parseData(historyModels)
                            DispatchQueue.main.async {
                                self.paymentHistoryTable.reloadData()
                            }
                        }
                    }
                }else{
                    self.paymentHistoryTable.isHidden = true
                    self.noPaymentLabel.isHidden = false
                }
            }
        }
    }
    
    func parseData(_ models : [PaymentHistoryModel])
    {
        var historyModels = models
        while !historyModels.isEmpty{
            let currentDate = historyModels[0].dateObject!
            let startWeek = currentDate.startOfWeek
            let dateString = String(format:"%@ - %@",getStringFromDate(date: startWeek.startDate!), getStringFromDate(date: startWeek.endDate!))
            var dataArray = dataDictionary[dateString]
            if dataArray == nil
            {
                //                dataArray!.appendModel(historyModel: historyModel[0])
                //            }else{
                dataArray = HistoryMainModel()
                keyArray.append(dateString)
            }
            dataArray!.appendModel(historyModel: historyModels[0])
            dataArray!.weekInterval = dateString
            dataDictionary[dateString] = dataArray
            historyModels.remove(at: 0)
        }
        
    }
    
    private func getStringFromDate(date: Date)->String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        return formatter.string(from:date)
    }
    private func convertStringToDate(dateString : String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.date(from: dateString)!
    }
    
}
extension PaymentHistoryController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let paymentDetail = PaymentDetailController(nibName:"PaymentDetailController", bundle: Bundle.main)
        paymentDetail.historyMainModel = dataDictionary[keyArray[indexPath.row]]
        self.navigationController?.pushViewController(paymentDetail, animated: true)
    }
}

extension PaymentHistoryController: UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return keyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellName) as! PaymentHistoryCell
        let model = dataDictionary[keyArray[indexPath.row]]
        cell.textLabel?.text = model!.weekInterval
        cell.detailTextLabel?.text = String.init(format: "Total Amount: $%.2f",model!.totalAmount)
        return cell
    }
}
