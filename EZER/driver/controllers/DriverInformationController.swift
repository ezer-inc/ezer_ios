 //
//  DriverInformationController.swift
//  EZER
//
//  Created by Geeta on 06/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
import UIKit

class DriverInformationController: UIViewController {
    
    @IBOutlet weak var openPersonalInformationButtonOutlet: UIButton!
    @IBOutlet weak var submitPersonalInformationLabelOutlet: UILabel!
    
    @IBOutlet weak var submitLabelOutlet: UILabel!
    @IBOutlet weak var pendingApprovalOutlet: UILabel!
    
    @IBOutlet weak var submitVehicleInformationLableOutlet: UILabel!
    @IBOutlet weak var openVehicleInformationButtonOutlet: UIButton!
    
    @IBOutlet weak var pendingVehicleApprovalLableOutlet: UILabel!
    @IBOutlet weak var submitVehicleLabelOutlet: UILabel!
    @IBOutlet weak var yourProfileLblOutlet: UILabel!
    
    @IBOutlet weak var informationLbl: UILabel!
    
    var driverLoginModel : DriverLoginModel!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.title = "Sign Up"
        navigationItem.hidesBackButton = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColor.driverTheme
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        SocketManager.sharedInstance.refershUser()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
        handleDriverLogin()
    }
    
    private func handleDriverLogin()
    {
        if driverLoginModel.isPersonalInfoSubmitted{
            submitLabelOutlet.isHidden = false
            pendingApprovalOutlet.isHidden = false
            
            openPersonalInformationButtonOutlet.isHidden = true
            submitPersonalInformationLabelOutlet.isHidden = true
        }
        
        if driverLoginModel.isVehicleInfoSubmitted {
            submitVehicleInformationLableOutlet.isHidden = true
            openVehicleInformationButtonOutlet.isHidden = true
            
            pendingVehicleApprovalLableOutlet.isHidden = false
            submitVehicleLabelOutlet.isHidden = false
        }
        
        if driverLoginModel.isPersonalInfoSubmitted && driverLoginModel.isVehicleInfoSubmitted {
            yourProfileLblOutlet.isHidden = false
            informationLbl.isHidden  = true
            switch driverLoginModel.backgroundCheck
            {
            case "FILED","SENT","RESPONDED":
                yourProfileLblOutlet.text = ValidationMessages.backgroundCheck
            case "REJECTED":
                yourProfileLblOutlet.text = ValidationMessages.backgroundCheckRejected
            case "ACCEPT":
                break
            default:
                break
            }
        }
    }
    @objc func loginDriver() {
        let params :[String:Any] = [
            "email":HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.email),
            "password":HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.password),
            "deviceType": "ios",
            "deviceToken": HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.deviceToken)
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.login, jsonToPost: params ,isResponseRequired: true) { (json) in
            if let json = json
            {
                if json[ServerKeys.status].int  == ServerStatusCode.Failure
                {
                    return
                }
                HelperFunction.saveValueInUserDefaults(key: ProfileKeys.authToken, value: json[ServerKeys.data][ProfileKeys.authToken].string ?? "")
                if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType) // check in login type
                {
                    let model = DriverLoginModel(json: json[ServerKeys.data])
                    HelperFunction.saveDriverInPref(loginModel: model)
                    if model.personalInfoVerified && model.vehicleInfoVerified && model.backgroundCheck == "ACCEPT"{
                        HelperFunction.openDriver(driverLoginModel: model, viewController: self)
                    }
                    else{
                        self.driverLoginModel = model
                        self.handleDriverLogin()
                    }
                }
                
            }
        }
        
    }
    @IBAction func menuCallBack(_ sender: UIButton) {
        
        switch sender.tag {
        case 1: //personal Information
            let personalInfoController = self.storyboard?.instantiateViewController(withIdentifier:IdentifierName.Driver.peronalInformation) as! PersonalInformationController
            personalInfoController.driverLoginModel = driverLoginModel
            self.navigationController?.pushViewController(personalInfoController, animated: true)
        case 2: //vehicle information
            let vehicleInfoController = self.storyboard?.instantiateViewController(withIdentifier:IdentifierName.Driver.vehicleInformation) as! VehicleInformationController
            vehicleInfoController.driverLoginModel = driverLoginModel
            self.navigationController?.pushViewController(vehicleInfoController, animated: true)
        case 3: //vehicle information
            //openDriver()
            HelperFunction.openDriver(driverLoginModel: driverLoginModel, viewController: self)
        default:
            break
        }
    }
    
    @IBAction func logoutButtonClicked(_ sender: Any) {
        logoutUser()
    }
    
    func logoutUser()
    {
        let param: [String:Any] = [
            "driverId": HelperFunction.getSrtingFromUserDefaults(key:DriverProfileKeys.id),
            "deviceToken": HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.deviceToken)
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.logout, jsonToPost: param) { (json) in
            if (json != nil){
                SocketManager.sharedInstance.exitFromSocketEvent()
                SocketManager.sharedInstance.closeConnection()
                SocketManager.sharedInstance.isBackground = true
              
                HelperFunction.clearUserDefault()
                
                let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
                let mainViewController = storyboard.instantiateInitialViewController()
                let delegate = UIApplication.shared.delegate as! AppDelegate
                delegate.window?.backgroundColor = UIColor.driverTheme
                delegate.window?.rootViewController = mainViewController!
                delegate.window?.makeKeyAndVisible()
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    private func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(loginDriver), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
}
 
/*
 FILED - your profile has been submitted for background verification
 SENT - your profile has been submitted for background verification
 RESPONDED - your profile has been submitted for background verification
 ACCEPT
 REJECTED
 */
