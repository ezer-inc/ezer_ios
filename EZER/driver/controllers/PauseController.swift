//
//  PauseController.swift
//  EZER
//
//  Created by time_rack on 27/07/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit

class PauseController: UIViewController {
    var orderId:String = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Paused"
         self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        self.navigationItem.hidesBackButton = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func getOrderDetails(){
        
        let params :[String:Any] = [
                "orderId": orderId
            ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Driver.getOrderDetail, jsonToPost: params) { (json) in
            if let json = json
            {
                let driverJob = DriverAvailableJobs(json: json[ServerKeys.data])
                if driverJob.status == .pause{
                    
                } else{
                    SocketRouterForPauseController.managePauseViewControllerForResume()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        let driverJob = DriverAvailableJobs.init(json: json[ServerKeys.data])
                        let scheduledJobs = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.orderProcessView) as! OrderProcessViewController
                        scheduledJobs.driverJob = driverJob
                        self.navigationController?.pushViewController(scheduledJobs, animated: false)
                    })
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       //  NotificationCenter.default.removeObserver(self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
