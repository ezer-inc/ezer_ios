//
//  TrackOrderViewController.swift
//  EZER
//
//  Created by Akash on 06/01/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
class TrackDriverViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var lblETAHeading: UILabel!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var locationMap: GMSMapView!
    @IBOutlet weak var trackOrderBar: OrderStatus!
    @IBOutlet weak var driverNameOutlet: UILabel!
    @IBOutlet weak var notifyLabelOutlet: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblETA: UILabel!
    @IBOutlet weak var lblETAtime: UILabel!
    
    //MARK:- Custom variables
    var currentOrderModel : CurrentOrderModel!
    var orderTrackModel : OrderTrackModel!
    var driverPositionTimer: Timer!
    var driverMarker : GMSMarker!
    var driverId : String = ""
    var orderId : String = ""
    var estimatedTime : String?
    var pickupMarker : GMSMarker!
    var destinationMarker : GMSMarker!
    var extraStartLocation : [ExtraStartLocation]!
    
    //MARK:- override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        lblOrderStatus.isHidden = true
        setData()
        self.title = "Track Driver"
        lblOrderId.text = "Order #\(currentOrderModel.orderNumber)"
        driverId = currentOrderModel.driverId
        
        //34.0419985,-118.346452
        let camera = GMSCameraPosition.camera(withLatitude: 34.0419985,longitude: -118.346452,zoom: 6)
        locationMap.animate(to: camera)
        getOrderDetails()
       
        
        // Do any additional setup after loading the view.
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblETA.isHidden = true
        lblETAtime.isHidden = true
        lblETAHeading.isHidden = true
        addObserver()
        driverPositionTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(callFetchDriverLocation), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        if driverPositionTimer.isValid
        {
            driverPositionTimer.invalidate()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- private methods
    @objc func callFetchDriverLocation() {
        if orderTrackModel != nil{
            if self.orderTrackModel?.status != .open && self.orderTrackModel?.status != .driverReview {
                fetchDriverLatLong()
            }else{
                
            }
        }
    }
    private func addPickUpMarker(coordinate :CLLocationCoordinate2D)
    {
        if pickupMarker == nil
        {
            pickupMarker = GMSMarker(position: coordinate)
            pickupMarker.map = locationMap
            pickupMarker.icon = #imageLiteral(resourceName: "ic_startLocation")
            let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude,longitude: coordinate.longitude,zoom: 6)
            locationMap.animate(to: camera)
        }
    }
    
    private func addDestinationMarker(coordinate :CLLocationCoordinate2D)
    {
        if destinationMarker == nil
        {
            destinationMarker = GMSMarker(position: coordinate)
            destinationMarker.map = locationMap
            destinationMarker.icon = #imageLiteral(resourceName: "ic_destinationLocation")
            let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude,longitude: coordinate.longitude,zoom: 6)
            locationMap.animate(to: camera)
        }
    }
    
    private func addOrUpDateDriverPosition(coordinate :CLLocationCoordinate2D)
    {
        if driverMarker == nil{
            driverMarker = GMSMarker()
        }
        var zoomLevel : Float = 6
        if (locationMap.camera.zoom > zoomLevel)
        {
            zoomLevel = locationMap.camera.zoom
        }
        driverMarker.icon = #imageLiteral(resourceName: "ic_ezerMarker")
       
        driverMarker.map = locationMap
        let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude,longitude: coordinate.longitude,zoom: zoomLevel)
       // locationMap.animate(to: camera)
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.5)
        CATransaction.commit()
        
        // Movement
        CATransaction.begin()
        CATransaction.setAnimationDuration(1)
         driverMarker.position = coordinate
        CATransaction.commit()
        
    }
    
    private func addDestinationMultipleMarker(coordinate :CLLocationCoordinate2D)
    {
        let destinationMarker = GMSMarker()
        destinationMarker.map = locationMap
        destinationMarker.icon = #imageLiteral(resourceName: "ic_destinationLocation")
        destinationMarker.position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        //        state_marker.title = state.name
        //        state_marker.snippet = "Hey, this is \(state.name)"
        destinationMarker.map = locationMap
    }
    
    private func addPickUpMultipleMarker(coordinate :CLLocationCoordinate2D)
    {
        let pickUpMarker = GMSMarker()
        pickUpMarker.map = locationMap
        pickUpMarker.icon = #imageLiteral(resourceName: "ic_startLocation")
        pickUpMarker.position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        pickUpMarker.map = locationMap
    }
    
    @objc private func fetchDriverLatLong()
    {
        if !driverId.isEmpty
        {
           
            if orderTrackModel == nil{
                orderId = currentOrderModel.orderId
            }else{
                orderId = orderTrackModel.orderId
            }
            let orderParams :[String:Any] = [
                "driverId":driverId,
                "orderId":orderId
            ]
            print(orderParams)
            BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.getDriverLocationDetail, jsonToPost: orderParams,isResponseRequired: true,showLoader:false ) { (json) in
                if let json = json{
                    print(json)
                    if json[ServerKeys.status] == 1{
                        let locationModel = LocationModel(json: json["driverLocation"])
                        if let coordinate = locationModel.locationCordinate{
                            self.addOrUpDateDriverPosition(coordinate: coordinate)
                        }
                        self.estimatedTime = String(format: "%@ %@ %@",json["estimatedTimeMsg"].string ?? "",json["strTitleText"].string ?? "",json["time"].string ?? "")
                        if json["strTitleText"].string ?? "" != ""{
                            self.lblETA.isHidden = false
                            self.lblETAtime.isHidden = false
                            self.lblETA.text = self.estimatedTime
                            self.lblETAHeading.isHidden = false
                            
                        }else{
                            self.lblETA.isHidden = true
                            self.lblETAtime.isHidden = true
                            self.lblETAHeading.isHidden = true
                        }
                    }
                }
                else{
                    //print("internet error")
                }
            }
        }
    }
    
    private func updateTimeForReachDestination(_ driverlocation:CLLocationCoordinate2D){
        lblETA.isHidden = false
        lblETAtime.isHidden = false
        lblETAHeading.isHidden = false
        var isPickup:Bool
        var pickUpLoc:CLLocationCoordinate2D?
        if self.orderTrackModel?.status == .accepted || self.orderTrackModel?.status == .onTheWay || self.orderTrackModel?.status == .driverReview {
            let startLocation = self.orderTrackModel!.startLocation!
            guard let startCordinate = startLocation.locationCordinate else {
                return
            }
            pickUpLoc = startCordinate
            isPickup = true
        }else{
            
            let destLocation = self.orderTrackModel!.destinationLocation!
            guard let destCordinate = destLocation.locationCordinate else {
                return
            }
            pickUpLoc = destCordinate
            isPickup = false
        }
        
        GetDistanceWithGoogleApi.distanceByGoogleApi(pickUpLoc!, destination: driverlocation) {(json) in
            print(json ?? "")
            if isPickup{
                self.lblETA.text = "Driver will arrived at the pickUp location in"
                self.lblETAtime.text = String(format: "%@",json?.DurationText ?? "0.0")
            }else{
                self.lblETA.text = "Driver will arrive at the destination in"
                self.lblETAtime.text = String(format: "%@",json?.DurationText ?? "0.0")
            }
        }
    }
    
    private func setData()
    {
        if orderTrackModel == nil{
            trackOrderBar.orderStatus = HelperFunction.setOrderStatus(status: currentOrderModel.status, trackOrderBar: trackOrderBar)
            self.driverId = currentOrderModel.driverId
            setDriverName(driverName: currentOrderModel.driverName)
        }
        else{
            //check if order is cancelled
            if self.orderTrackModel!.status == OrderStatusType.cancelled{
                self.notifyLabelOutlet.isHidden = true
                self.driverNameOutlet.isHidden = true
                self.trackOrderBar.isHidden = true
                self.lblOrderStatus.isHidden = false
            }else{
                self.lblOrderStatus.isHidden = true
                self.trackOrderBar.isHidden = false
                self.trackOrderBar.orderStatus = HelperFunction.setOrderStatus(status: self.orderTrackModel!.status, trackOrderBar: self.trackOrderBar)
                self.driverId = orderTrackModel!.driverId
                setDriverName(driverName: orderTrackModel.driverName)
            }
        }
    }
    
    private func setDriverName(driverName : String)
    {
        if driverName.isEmpty{
            driverNameOutlet.text = ValidationMessages.noDriverAssigned
            notifyLabelOutlet.isHidden = false
        }
        else{
            driverNameOutlet.text = driverName
            notifyLabelOutlet.isHidden = true
        }
    }
    
    private func addObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(handleOrderStatusChange(notification:)), name: .orderStatusChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(orderCancelFromDriver(notification:)), name: .orderCancelFromDriver, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getOrderDetails), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func getOrderDetails(showLoader : Bool = true)
    {
        let orderParams :[String:Any] = [
            "orderId":currentOrderModel.orderId,
            ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.orderDetails, jsonToPost: orderParams,showLoader:showLoader ) { (json) in
            if let json = json
            {
                self.orderTrackModel =  OrderTrackModel(json:json[ServerKeys.data])
                self.driverId = self.orderTrackModel!.driverId
                let startLocation = self.orderTrackModel!.startLocation!
                let destLocation = self.orderTrackModel!.destinationLocation!
                if let coordinate = startLocation.locationCordinate{
                    self.addPickUpMarker(coordinate: coordinate)
                }
                if let coordinate = destLocation.locationCordinate{
                    self.addDestinationMarker(coordinate: coordinate)
                }
                
                if self.orderTrackModel.extraStartLocations.count > 0 {
                    for obj  in self.orderTrackModel.extraStartLocations{
                        if let coordinate = obj.location.locationCordinate{
                            self.addPickUpMultipleMarker(coordinate: coordinate)
                        }
                    }
                }
                
                if self.orderTrackModel.extraDestinationLocations.count > 0 {
                    for obj  in self.orderTrackModel.extraDestinationLocations{
                        if let coordinate = obj.location.locationCordinate{
                            self.addDestinationMultipleMarker(coordinate: coordinate)
                        }
                    }
                }
                
                self.setData()
                if self.orderTrackModel?.status == .done
                {
                    self.navigationController?.popViewController(animated: true)
                }else{
                    self.callFetchDriverLocation()
                }
            }
        }
    }
    
    //MARK:- Public methods...
    @objc func openForDriverRating()
    {
        let completeOrder = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.completeOrder) as! CompleteOrderController
        completeOrder.orderTrackModel = orderTrackModel
        self.navigationController?.pushViewController(completeOrder, animated: true)
    }
    
    @objc func handleOrderStatusChange(notification: Notification)
    {
        guard let notificationObject = notification.userInfo as? [ String: Any] else{ return}
        
        guard let currentOrderId = notificationObject["orderId"] as? String else{ return}
        
        if currentOrderId == currentOrderModel.orderId{
            if let status = notificationObject["status"] as? String
            {
                guard let status = OrderStatusType(rawValue: status) else{ return }
                if status == .done
                {
                    self.navigationController?.popToRootViewController(animated: true)
                }else{
                    let driverName = notificationObject["driverName"] as? String ?? ""
                    let driverId = notificationObject["driverId"] as? String ?? ""
                    let status = status
                    if orderTrackModel == nil{
                        currentOrderModel.driverName = driverName
                        currentOrderModel.driverId = driverId
                        currentOrderModel.status = status
                    }
                    else{
                        orderTrackModel.driverName = driverName
                        orderTrackModel.driverId = driverId
                        orderTrackModel.status = status
                    }
                    setData()
                }
            }
        }
    }
    
    @objc func orderCancelFromDriver(notification: Notification)
    {
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            if let currentOrderId = notificationObject["orderId"] as? String
            {
                if currentOrderId == currentOrderModel.orderId
                {
                    self.navigationController?.popToRootViewController(animated: true)
//                    let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
//                        self.navigationController?.popToRootViewController(animated: true)
//                    })
//                    showAlertController(message: ValidationMessages.orderCancelByDriver,action : action)
                    
                }
            }
        }
    }
    
}
