//
//  CustomerHome.swift
//  EZER
//
//  Created by TimerackMac1 on 16/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import GooglePlaces
import SwiftyJSON
import GoogleMaps
import CoreLocation
import ChatSDK
import ChatProvidersSDK
import MessagingSDK
enum AddressType : Int{
    case startAddress = 1
    case endAddress = 2
}

class CustomerHome: UIViewController , UITextFieldDelegate{
    
    //MARK:- IBOutlets
//    @IBOutlet weak var getMovingMargin: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var myCurrentViewOutlet: UIView!
    @IBOutlet weak var endLocation: UITextField!
    @IBOutlet weak var startLocation: UITextField!
//    @IBOutlet weak var segmented: YSSegmentedControl!
    @IBOutlet weak var startAddressUnitNumberTxtField: UITextField!
    @IBOutlet weak var txtStartBusinessName: UITextField!
    
    
    @IBOutlet weak var destinationAddressUnitNumberTxtField: UITextField!
    
    @IBOutlet weak var txtDestinationBusinessName: UITextField!
    @IBOutlet weak var btnNowOutlet: UIButton!
    @IBOutlet weak var btnLaterOutlet: UIButton!
    
    //DatePickerView
    
    @IBOutlet weak var parentDatePickerView: UIView!
    
    @IBOutlet weak var pickerViewBottomMargin: NSLayoutConstraint!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var lblPickupDateTime: UILabel!
    
    @IBOutlet weak var constraintHeightOfPickupDateTime: NSLayoutConstraint!
    
    
    //MARK:- Custom variables
    let chooseAddressDropDown = DropDown()
    var addressArray = [String]()
    var addressPlaceKeys = [String]()
    var textToFind :String!
    var searchTimer : Timer!
    var startAddress : String!
    var endAddress: String!
    var startCoordinate : CLLocationCoordinate2D!
    var destinationCoordinate : CLLocationCoordinate2D!
    var addressType : AddressType = .startAddress
    var isStartAddressSelected = false
    var isEndAddressSelected = false
    var createOrderModel:CreateOrderModel = CreateOrderModel()
    var currentOrderList = [CurrentOrderModel]()
    let cellName    = "OpenOrderTableViewCell"
    var isCalledFromCache = false
    
    //MARK:- override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
         setPlaceholderColor() //set place holder color
        // set maximim date picker time
        datePicker.setValue(UIColor.customerTheme, forKey: "textColor")
        var fifteenDaysfromNow: Date {
            return (Calendar.current as NSCalendar).date(byAdding: .day, value: 15, to: Date(), options: [])!
        }
        datePicker.maximumDate = fifteenDaysfromNow
        
        self.setNavigationBarItem()
        
        self.title = "New Order"
//        segmented.titles = ["Now", "Later"]
        if let dict = HelperFunction.getDictFromUserDefault(key: UserDefaultkeys.isNotification) {
            self.pushToTrackOrderController(notification: dict)
        }
        //segmented.appearance.se = UIColor.blue
//        segmented.appearance?.unselectedTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.white]
//        segmented.appearance?.selectedTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.white]
//        segmented.action = { control, index in
//            print ("segmented did pressed \(index)")
//            CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: index)!
//        }
        endLocation.addTarget(self, action: #selector(addressChanged(_:)), for: .editingChanged)
        startLocation.addTarget(self, action: #selector(addressChanged(_:)), for: .editingChanged)
        startLocation.addTarget(self, action: #selector(enterPressed), for: .editingDidEndOnExit)
        endLocation.addTarget(self, action: #selector(enterPressed), for: .editingDidEndOnExit)
        // Do any additional setup after loading the view.
        setUpDropDown()
        //self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "nav_back")
        //self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "nav_back")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        SocketManager.sharedInstance.refershUser()
        addObserver()
        tableViewOutlet.register(UINib(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
        self.getCurrentOrder()
        if isCalledFromCache
        {
            checkAppVersion()
        }
        fireEventOnGetSocketForRefreshOrderType()
        checkImmediateOrder()
        callGetScheduleHoursApi()

    }
    
    func setPlaceholderColor()  {
        endLocation.setPlaceholder()
        startLocation.setPlaceholder()
        startAddressUnitNumberTxtField.setPlaceholder()
        txtStartBusinessName.setPlaceholder()
        destinationAddressUnitNumberTxtField.setPlaceholder()
        txtDestinationBusinessName.setPlaceholder()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         createOrderModel.shippingItem = ShippingItem() // for pop from image pick vc
        //check and select now or later option
        self.selectedNowLaterBtn(orderScheduleType: CurrentCustomerOrder.orderScheduleType.rawValue)

//        segmented.selectItem(at: CurrentCustomerOrder.orderScheduleType.rawValue, withAnimation: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        invalidateTimer()
    }
    
    override func viewDidLayoutSubviews() {
        
        self.view.layoutIfNeeded()
        super.updateViewConstraints()
        if tableViewOutlet.contentSize.height < 50
        {
//            getMovingMargin.constant = 20
            myCurrentViewOutlet.isHidden = true
            tableViewHeightConstraint.constant = 25
        }else{
//            getMovingMargin.constant = 7
            tableViewHeightConstraint.constant = tableViewOutlet.contentSize.height
            myCurrentViewOutlet.isHidden = false
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- buttons Action
    
    @IBAction func btnNowLaterAction(_ sender: UIButton) {
        
        if sender == btnNowOutlet{
            btnNowOutlet.setImage(#imageLiteral(resourceName: "ic_radio_button_on"), for: .normal)
            btnLaterOutlet.setImage(#imageLiteral(resourceName: "ic_radio_button_off"), for: .normal)
            CurrentCustomerOrder.orderScheduleType = .now
            constraintHeightOfPickupDateTime.constant = 0
            createOrderModel.dateTime  = nil
            createOrderModel.orderDate = nil
            showHidePickerView(true)
        }else{
             showHidePickerView(false)
             btnLaterOutlet.setImage(#imageLiteral(resourceName: "ic_radio_button_on"), for: .normal)
             btnNowOutlet.setImage(#imageLiteral(resourceName: "ic_radio_button_off"), for: .normal)
             CurrentCustomerOrder.orderScheduleType = .later
        }
    }
    
    func selectedNowLaterBtn(orderScheduleType:Int)  {
        
        if orderScheduleType == 0{
            btnNowOutlet.setImage(#imageLiteral(resourceName: "ic_radio_button_on"), for: .normal)
            btnLaterOutlet.setImage(#imageLiteral(resourceName: "ic_radio_button_off"), for: .normal)
            constraintHeightOfPickupDateTime.constant = 0
            createOrderModel.dateTime  = nil
            createOrderModel.orderDate = nil
        }else{
            //check if user come from review screen and option now selected then reset the date and time
            if btnNowOutlet.imageView?.image == #imageLiteral(resourceName: "ic_radio_button_on"){
                createOrderModel.dateTime  = nil
                createOrderModel.orderDate = nil
            }
            btnLaterOutlet.setImage(#imageLiteral(resourceName: "ic_radio_button_on"), for: .normal)
            btnNowOutlet.setImage(#imageLiteral(resourceName: "ic_radio_button_off"), for: .normal)
        }
    }
    
    @IBAction func cancelDatePicker(_ sender: UIButton) {
        switch sender.tag {
        case 1: //cancel clicked
            showHidePickerView(true)
        default : // 2
            showHidePickerView(true)
            if datePicker.date > Date(){
                let  dateModel = HelperFunction.getDateComponent(date: datePicker.date)
                createOrderModel.dateTime = HelperFunction.convertToUtc(date :datePicker.date)
                createOrderModel.orderDate = datePicker.date
                lblPickupDateTime.text = "\(dateModel.dayName ?? "") \(dateModel.dayOfMonth ?? "") \(dateModel.monthAndYear ?? "") \(dateModel.time ?? "") \(dateModel.amOrPM ?? "")"
                constraintHeightOfPickupDateTime.constant = 50
            }
            else{
//
//                let  dateModel = HelperFunction.getDateComponent(date: Date().adding(minutes: (CurrentCustomerOrder.scheduleHours*60)))
//                let dateTime = "\(dateModel.dayOfMonth ?? "") \(dateModel.monthAndYear ?? "") \(dateModel.time ?? "") \(dateModel.amOrPM ?? "")"
                self.showMessage("\(ValidationMessages.scheduleDateError)", type: .error,options: [.textNumberOfLines(0)])
            }
        }
    }
    
    func showHidePickerView(_ isHide : Bool)
    {
        
        if isHide{
            UIView.animate(withDuration :0.5, delay: 0, usingSpringWithDamping :1, initialSpringVelocity :0.9, options: .curveEaseIn, animations:{
                self.pickerViewBottomMargin.constant = 216
                self.parentDatePickerView.isHidden = true
                self.view.layoutIfNeeded()
            }, completion:  nil)
        }else{
            UIView.animate(withDuration :0.5, delay: 0, usingSpringWithDamping :0.5, initialSpringVelocity :0.9, options: .curveEaseIn, animations:{
                self.pickerViewBottomMargin.constant = 0
                self.datePicker.date = Date().adding(minutes: (CurrentCustomerOrder.scheduleHours*60))
                self.parentDatePickerView.isHidden = false
                self.view.layoutIfNeeded()
            }, completion:  nil)
        }
    }
    
    //MARK:- Custom methods
    func  fireEventOnGetSocketForRefreshOrderType() {
        SocketManager.sharedInstance.getSocketForRefreshOrderType { (result) in
            if result != nil{
                let json = JSON(result!)
                if json[ServerKeys.immediateOrderEnable].int ?? 1 == 0 && (CurrentCustomerOrder.orderScheduleType == OrderScheduleTypes.now ||  CurrentCustomerOrder.orderScheduleType == OrderScheduleTypes.later){
                    if let controller = self.navigationController?.viewControllers.last
                    {
                        if controller is TakePictureView || controller is CustomerHome || controller is PlaceOrderORSchedule || controller is NowOrderDetailCon || controller is LaterScheduleOrderCon
                        {
                            let alert = UIAlertController(title: nil, message: json[ServerKeys.message].string ?? "", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                
                                
                                //                                self.segmented.disableItem(at: 0, isEnabled: HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0 ? false : true)
                                
                                // Disable or enable Now Option
                                self.btnNowOutlet.isEnabled = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0 ? false : true
                                
                                let orderType = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable)
                                CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: orderType == 0 ? 1 : 0)!
                                if HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0{
                                    //check and select now or later option
                                    self.selectedNowLaterBtn(orderScheduleType: CurrentCustomerOrder.orderScheduleType.rawValue)
                                    //                                    self.segmented.selectItem(at: CurrentCustomerOrder.orderScheduleType.rawValue, withAnimation: false)
                                }
                                self.navigationController?.popToRootViewController(animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }else{
                    
                    // Disable or enable Now Option
                    self.btnNowOutlet.isEnabled = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0 ? false : true
                    
                    //                    self.segmented.disableItem(at: 0, isEnabled: HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0 ? false : true)
                    
                    //                    self.segmented.selectItem(at: CurrentCustomerOrder.orderScheduleType.rawValue, withAnimation: false)
                    
                    //check and select now or later option
                    self.selectedNowLaterBtn(orderScheduleType: CurrentCustomerOrder.orderScheduleType.rawValue)
                    
                    let orderType = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable)
                    CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: orderType == 0 ? 1 : 0)!
                    
                }
            }
        }
    }
    
    @objc func enterPressed(_ textField : UITextField){
        //do something with typed text if needed
        textField.resignFirstResponder()
    }
    
    @objc func addressChanged(_ textField: UITextField) {
        chooseAddressDropDown.anchorView = textField.superview
        
        if textField == endLocation {
            destinationCoordinate = nil
            addressType = .endAddress
            isEndAddressSelected = false
        }
        else {
            startCoordinate = nil
            addressType = .startAddress
            isStartAddressSelected = false
        }
        textToFind = textField.text!
        invalidateTimer()
        searchTimer = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: false, block: { (timer) in
            self.placeAutocomplete(queryString: self.textToFind)
        })
    }
    
    func placeAutocomplete(queryString :String) {
        if queryString.isEmpty{
            clearAddressArray()
            chooseAddressDropDown.hide()
            return }
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        filter.country = "US"
        let placeClient = GMSPlacesClient.shared()
        self.showProgressBar()
       
        let token = GMSAutocompleteSessionToken.init()

        placeClient.findAutocompletePredictions(fromQuery: queryString, filter: filter, sessionToken: token) { (results, error) in
            self.hideProgressBar()
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
                self.clearAddressArray()
                for result in results {
                    self.addressArray.append(result.attributedFullText.string.trimCountry())
                    self.addressPlaceKeys.append(result.placeID)
                }
                self.chooseAddressDropDown.dataSource = self.addressArray
                self.showDropDown()
            }
        }
        
        
//        placeClient.autocompleteQuery(queryString, bounds: nil, filter: filter, callback: {(results, error) -> Void in
//            self.hideProgressBar()
//            if let error = error {
//                print("Autocomplete error \(error)")
//                return
//            }
//            if let results = results {
//                self.clearAddressArray()
//                for result in results {
//                    self.addressArray.append(result.attributedFullText.string.trimCountry())
//                    self.addressPlaceKeys.append(result.placeID)
//                }
//                self.chooseAddressDropDown.dataSource = self.addressArray
//                self.showDropDown()
//            }
//        })
    }
    
    
    func logoutUser()
    {
        
        let param: [String:Any] = [
            "customerId": HelperFunction.getSrtingFromUserDefaults(key:ProfileKeys.Id),
            "deviceToken": HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.deviceToken)
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.logout, jsonToPost: param) { (json) in
            if (json != nil){
                self.removeObserver()
                SocketManager.sharedInstance.exitFromSocketEvent()
                SocketManager.sharedInstance.closeConnection()
                SocketManager.sharedInstance.isBackground = true
                HelperFunction.clearUserDefault()
                let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
                let mainViewController = storyboard.instantiateInitialViewController()
                let delegate = UIApplication.shared.delegate as! AppDelegate
                delegate.window?.backgroundColor = UIColor.driverTheme
                delegate.window?.rootViewController = mainViewController!
                delegate.window?.makeKeyAndVisible()
                Chat.chatProvider?.endChat()
            }
        }
    }
    
    func checkImmediateOrder() {
        BaseServices.sendGetJson(serverUrl: ServerUrls.Customer.checkImmediateOrder) { (json) in
            if (json != nil){
                let json = JSON(json!)
                if let status = json[ServerKeys.status].int {
                    HelperFunction.saveValueInUserDefaults(key: UserDefaultsKeys.immediateOrderEnable, value: status)
                 
//                    self.segmented.disableItem(at: 0, isEnabled: HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0 ? false : true)
                    
                     self.btnNowOutlet.isEnabled = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0 ? false : true
                    
                    if HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0{
                        
//                        self.segmented.selectItem(at: CurrentCustomerOrder.orderScheduleType.rawValue, withAnimation: false)
                        //check and select now or later option
                        self.selectedNowLaterBtn(orderScheduleType: CurrentCustomerOrder.orderScheduleType.rawValue)
                        
                        let orderType = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable)
                        CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: orderType == 0 ? 1 : 0)!
                        let alert = UIAlertController(title: nil, message: json[ServerKeys.message].string ?? "", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            //check and select now or later option
            self.selectedNowLaterBtn(orderScheduleType: CurrentCustomerOrder.orderScheduleType.rawValue)
//            self.segmented.selectItem(at: CurrentCustomerOrder.orderScheduleType.rawValue, withAnimation: false)
        }
    }
   
    func getCurrentOrder()
    {
        let params = [
            "customerId":HelperFunction.getUserId()
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.getCurrentOrderDetail, jsonToPost: params,showLoader : false) { (json) in
            if let json = json
            {
                self.currentOrderList.removeAll()
                if let orderArray = json[ServerKeys.data].array
                {
                    for item in orderArray
                    {
                        let currentOder = CurrentOrderModel(json : item)
                        if currentOder.status != OrderStatusType.draft{
                            self.currentOrderList.append(currentOder)
                        }
                    }
                    self.reloadTableView()
                }
            }
        }
    }
    
    func pushToTrackOrderController(notification : [String:Any]){
        DispatchQueue.main.async {
            guard let data = notification["data"] as? [String:Any] else{ return}
            guard let orderId = data["orderId"] as? String else{ return}
            self.navigationController?.popToRootViewController(animated: false)
            let trackOrder = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.trackOrder) as! TrackOrderController
            trackOrder.orderId = orderId
            self.navigationController?.pushViewController(trackOrder, animated: true)
        }
    }
    
    //MARK:- Private methods
    private func removeObserver()
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit {
        removeObserver()
    }
    
    private func setUpDropDown()
    {
        chooseAddressDropDown.dataSource = []
        chooseAddressDropDown.direction = .bottom
        let appearance = DropDown.appearance()
        
        appearance.cellHeight = 40
        appearance.backgroundColor = UIColor(red:0.38, green:0.48, blue:0.53, alpha:1)
        appearance.selectionBackgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1)
        appearance.cornerRadius = 4
        appearance.textFont = UIFont(name: FontNames.light, size: 14)!
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.6
        appearance.shadowRadius = 5
        appearance.animationduration = 0.25
        appearance.textColor = .white
        
        chooseAddressDropDown.selectionAction = { [unowned self] (index, item) in
            guard let textfield = self.chooseAddressDropDown.anchorView?.plainView.viewWithTag(1) as? UITextField else{ return }
            textfield.text = item
            textfield.resignFirstResponder()
            self.getPlaceInformation(placeID: self.addressPlaceKeys[index])
        }
        chooseAddressDropDown.cancelAction = { [unowned self] in
            if !self.addressArray.isEmpty
            {
                if self.addressType == .startAddress
                {
                    self.startLocation.text = self.addressArray[0]
                }
                else{
                    self.endLocation.text = self.addressArray[0]
                }
                self.getPlaceInformation(placeID: self.addressPlaceKeys[0])
            }
            guard let textfield = self.chooseAddressDropDown.anchorView?.plainView.viewWithTag(1) as? UITextField else{ return }
            textfield.resignFirstResponder()
            //   print("Drop down dismissed")
        }
    }
    
    private func openMapWithAddress(selectedPlace : GMSPlace? = nil, selectedAddressType :AddressType,currentLatLong:CLLocationCoordinate2D? = nil)
    {
        let confirmLocation = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.confirmMapLocation) as! ConfirmMapLocation
        confirmLocation.currentPlace = selectedPlace
        confirmLocation.addressType = selectedAddressType
        confirmLocation.confirmDelegate = self
        confirmLocation.currentCoordinate = currentLatLong
        self.view.endEditing(true)
        self.navigationController?.pushViewController(confirmLocation, animated: true)
    }
    
    private func invalidateTimer()
    {
        if searchTimer != nil && searchTimer.isValid {
            searchTimer.invalidate()
        }
    }
    
    private func clearAddressArray()
    {
        self.addressArray.removeAll()
        self.addressPlaceKeys.removeAll()
    }
    
    private func getEstimateDistanceAndTime()
    {
        let params :[String:Any] = [
            "startlat":startCoordinate.latitude,
            "endlat":destinationCoordinate.latitude,
            "startlong":startCoordinate.longitude,
            "endlong":destinationCoordinate.longitude
        ]
        BaseServices.SendPostJson(viewController: self ,serverUrl: ServerUrls.Customer.getTotalDistanceAndTime, jsonToPost: params, isResponseRequired : true) { (json) in
            if let json = json
            {
                let takePicture = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.takePicture) as! TakePictureView
//                self.createOrderModel = CreateOrderModel()
                self.createOrderModel.startAddress = self.startAddress
                self.createOrderModel.destinationAddress = self.endAddress
                self.createOrderModel.startCoordinate = self.startCoordinate
                self.createOrderModel.destinationCoordinate = self.destinationCoordinate
                self.createOrderModel.estimatedTime = json[ServerKeys.data]["time"].int ?? 0
                self.createOrderModel.totalDistance = json[ServerKeys.data]["distance"].int ?? 0
                
                let startUnitBusiness:String? =  "\(self.txtStartBusinessName.text ?? "") \(self.startAddressUnitNumberTxtField.text ?? "")".trimWhiteSpace()
          
                let destinationUnitBusiness:String? =  "\(self.txtDestinationBusinessName.text ?? "") \(self.destinationAddressUnitNumberTxtField.text ?? "")".trimWhiteSpace()
        
                self.createOrderModel.startAddressUnitNumber = startUnitBusiness
                self.createOrderModel.destinationAddressUnitNumber = destinationUnitBusiness
               
                self.createOrderModel.immediate = CurrentCustomerOrder.orderScheduleType == .now
                takePicture.createOrderModel = self.createOrderModel
                takePicture.delegate = self
                self.navigationController?.pushViewController(takePicture, animated: true)
            }
        }
    }
    
    private func showDropDown()
    {
        if self.navigationController?.viewControllers.count == 1{
            if let superView = chooseAddressDropDown.anchorView?.plainView
            {
                if (self.view.bounds.maxY - superView.frame.maxY) > 400
                {
                    chooseAddressDropDown.direction = .bottom
                    chooseAddressDropDown.bottomOffset = CGPoint(x: 0, y:(chooseAddressDropDown.anchorView?.plainView.bounds.height)!)
                    chooseAddressDropDown.show()
                }
                else{
                    chooseAddressDropDown.direction = .top
                    chooseAddressDropDown.topOffset = CGPoint(x: 0, y:-(chooseAddressDropDown.anchorView?.plainView.bounds.height)!)
                    chooseAddressDropDown.show()
                }
            }
        }else{
            chooseAddressDropDown.hide()
            addressPlaceKeys.removeAll()
            addressArray.removeAll()
        }
    }
    
    // Get place information
    private func getPlaceInformation(placeID:String)
    {
        self.showProgressBar()
        let placesClient = GMSPlacesClient.shared()
        placesClient.lookUpPlaceID(placeID, callback: { (place, error) -> Void in
            self.hideProgressBar()
            if let error = error {
                // print("lookup place id query error: \(error.localizedDescription)")
                self.showMessage(ValidationMessages.tryAgain, type: .error)
                return
            }
            guard let place = place else {
                //  print("No place details for \(placeID)")
                self.showMessage("No place details this address", type: .error)
                return
            }
            self.openMapWithAddress(selectedPlace: place, selectedAddressType: self.addressType)
            //   print("Place name \(place.name)")
            //   print("Place address \(place.formattedAddress)")
            //   print("Place placeID \(place.placeID)")
            //   print("Place attributions \(place.attributions)")
        })
    }
    
    func convertAddressStringToCoordinates(selectedAddressType :AddressType){
        if addressType == .startAddress
        {
               self.openMapWithAddress(selectedPlace: nil, selectedAddressType: selectedAddressType, currentLatLong: startCoordinate)
        }else{
             self.openMapWithAddress(selectedPlace: nil, selectedAddressType: selectedAddressType, currentLatLong: destinationCoordinate)
        }
    }
    

    //MARK:- IBActions
    @IBAction func getMoving(_ sender: RoundableButton) {
        if startLocation.hasText && endLocation.hasText
        {
            if !isStartAddressSelected
            {
                self.showMessage(ValidationMessages.startLocationError, type: .error)
                return
            }
            if !isEndAddressSelected
            {
                self.showMessage(ValidationMessages.endLocationError, type: .error)
                return
            }
//            if HelperFunction.distanceInMeters(startCoordinate,destination: destinationCoordinate) < 50{
            if startLocation.text == endLocation.text{
                self.showMessage(ValidationMessages.sameLocationError, type: .error,options: [.textNumberOfLines(0)])
                return
            }
            if createOrderModel.orderDate == nil && CurrentCustomerOrder.orderScheduleType == .later{
                self.showMessage(ValidationMessages.dateNotEmpty, type: .error)
                return
            }
            
            getEstimateDistanceAndTime()
        }else{
            if !startLocation.hasText{
                self.view.makeToast(ValidationMessages.emptyStartLocationError, duration: 3.0, position: .bottom)

            }else{
                self.view.makeToast(ValidationMessages.emptyEndLocationError, duration: 3.0, position: .bottom)

            }
        }
    }
    
    @IBAction func confirmLocation(_ sender: UIButton ) {
        if startLocation.text!.trimWhiteSpace().isEmpty &&  endLocation.text!.trimWhiteSpace().isEmpty{
            self.addressType = AddressType(rawValue: sender.tag)!
            openMapWithAddress(selectedAddressType: addressType)
        }else{
             self.addressType = AddressType(rawValue: sender.tag)!
            convertAddressStringToCoordinates(selectedAddressType: addressType)
        }
    }
}

extension CustomerHome : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentOrderList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellName) as! OpenOrderTableViewCell
        cell.currentOrderProtocol = self
        cell.setUpCellData(currentOrderModel: currentOrderList[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewDidLayoutSubviews()
    }
}

extension CustomerHome : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension CustomerHome : CurrentOrderProtocol
{
    func reloadtableViewFromCell() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.getCurrentOrder()
            self.checkImmediateOrder()
        }
    }
    
    func trackDriverClicked(currentOrderModel: CurrentOrderModel) {
        let trackDriver = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.trackDriverViewOrder) as! TrackDriverViewController
        trackDriver.currentOrderModel = currentOrderModel
        self.navigationController?.pushViewController(trackDriver, animated: true)
    }
    
    func viewOrderClicked(currentOrderModel: CurrentOrderModel) {
//        if currentOrderModel.isTimeout{
//            let rescheduleOrder = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.rescheduleOrderController) as! RescheduleOrderController
//            rescheduleOrder.orderModel = currentOrderModel
//            self.navigationController?.pushViewController(rescheduleOrder, animated: true)
        //        }else{
        let trackOrder = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.trackOrder) as! TrackOrderController
        trackOrder.orderId = currentOrderModel.orderId
        
        trackOrder.orderTitle = "Order \(currentOrderModel.orderNumber)"
        self.navigationController?.pushViewController(trackOrder, animated: true)
      //  }
    }
    
    func callOrderClicked(currentOrderModel: CurrentOrderModel) {
        let params :[String:Any] = [
            "orderId": currentOrderModel.orderId
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Main.assignTwilioNumber, jsonToPost: params) { (json) in
            if let json = json
            {
                if let twilioNumber = json["twilioNumber"].string
                {
                    HelperFunction.callToNumber(number: twilioNumber)
                }
            }
        }
    }
}

extension CustomerHome:ConfirmLocationDelegate
{
    func confirmLocation(address: String, coordinate: CLLocationCoordinate2D) {
        if addressType == .startAddress
        {
            startLocation.text = address
            startAddress = address
            startCoordinate = coordinate
            isStartAddressSelected = true
        }else{
            isEndAddressSelected = true
            endAddress = address
            destinationCoordinate = coordinate
            endLocation.text = address
        }
        invalidateTimer()
    }
}

extension CustomerHome : TakePictureViewDelegate{
    func handlebackActionFromTakeAPicktureView() {
        BaseServices.sendGetJson(serverUrl: ServerUrls.Customer.checkImmediateOrder) { (json) in
            if (json != nil){
                let json = JSON(json!)
                if let status = json[ServerKeys.status].int {
                    HelperFunction.saveValueInUserDefaults(key: UserDefaultsKeys.immediateOrderEnable, value: status)
                    
//                    self.segmented.disableItem(at: 0, isEnabled: HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0 ? false : true)
                    // Disable or enable Now Option
                    self.btnNowOutlet.isEnabled = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0 ? false : true
                    
                    if HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0 && CurrentCustomerOrder.orderScheduleType == OrderScheduleTypes.now{
                        
//                        self.segmented.selectItem(at: CurrentCustomerOrder.orderScheduleType.rawValue, withAnimation: false)
                        //check and select now or later option
                        self.selectedNowLaterBtn(orderScheduleType: CurrentCustomerOrder.orderScheduleType.rawValue)
                        
                        let orderType = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable)
                        CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: orderType == 0 ? 1 : 0)!
                    }else if HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0{
                        let orderType = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable)
                        CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: orderType == 0 ? 1 : 0)!
                        let alert = UIAlertController(title: nil, message: json[ServerKeys.message].string ?? "", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
}
//MARK:- APi Method
extension CustomerHome{
 
    func callGetScheduleHoursApi()  {
        BaseServices.sendGetJson(serverUrl: ServerUrls.Customer.getScheduledHour) { (json) in
            if (json != nil){
                let json = JSON(json!)
                if let data = json[ServerKeys.data].dictionary {
                    if let hours = data["hours"]?.string{
                        CurrentCustomerOrder.scheduleHours = Int(hours) ?? 0
                    }
                }
            }
        }
    }
}


extension CustomerHome : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        // print("SlideMenuControllerDelegate: leftWillOpen")
    }
    func leftDidOpen() {
        // print("SlideMenuControllerDelegate: leftDidOpen")
    }
    func leftWillClose() {
        //  print("SlideMenuControllerDelegate: leftWillClose")
    }
    func leftDidClose() {
        //  print("SlideMenuControllerDelegate: leftDidClose")
    }
    func rightWillOpen() {
        //  print("SlideMenuControllerDelegate: rightWillOpen")
    }
    func rightDidOpen() {
        // print("SlideMenuControllerDelegate: rightDidOpen")
    }
    func rightWillClose() {
        //   print("SlideMenuControllerDelegate: rightWillClose")
    }
    func rightDidClose() {
        //  print("SlideMenuControllerDelegate: rightDidClose")
    }
}

