//
//  BiilingAddressController.swift
//  EZER
//
//  Created by TimerackMac1 on 25/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class BillingAddressController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var country: TJTextField!
    @IBOutlet weak var streetAddress: TJTextField!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var stateName: UILabel!
    @IBOutlet weak var postalCode: TJTextField!
    
    //MARK:- Variables
    let stateDropDown = DropDown()
    let stateDataSource : [String] = {
        var statesArray = [String]()
        for state in statesList
        {
            statesArray.append(state.name)
        }
        return statesArray
    }()
    
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        stateView.layer.borderColor = UIColor.gray.cgColor
        stateView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showCityDropDown)))
        stateDropDown.dataSource = stateDataSource
        stateDropDown.anchorView = stateView
        stateDropDown.selectionAction = { [unowned self] (index, item) in
            self.stateName.text = item
        }
        let appearance = DropDown.appearance()
        
        //appearance.cellHeight = 40
        appearance.backgroundColor = UIColor(red:0.38, green:0.48, blue:0.53, alpha:1)
        appearance.selectionBackgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1)
        //appearance.cornerRadius = 4
        //appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        //appearance.shadowOpacity = 0.6
        //appearance.shadowRadius = 5
        //appearance.animationduration = 0.25
        appearance.textColor = .white
        // Do any additional setup after loading the view.
    }
    
    //MARK:- public methods methods
    @objc func showCityDropDown()
    {
        stateDropDown.show()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
