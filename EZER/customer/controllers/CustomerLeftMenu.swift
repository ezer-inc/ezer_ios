//
//  CustomerLeftMenu.swift
//  EZER
//
//  Created by TimerackMac1 on 16/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import ChatSDK
import ChatProvidersSDK
import MessagingSDK
import IQKeyboardManagerSwift

enum CustomerMenu: Int {
    case payment
    case orderHistory
    case settings
    case changePassword
    case faqs
    case support
    case help
    case logout
}
protocol CustLeftMenuProtocol : class {
    func changeViewController(_ menu: CustomerMenu)
}
class CustomerLeftMenu: UIViewController,CustLeftMenuProtocol {
    
    //MARK:- IBOutlets
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var firstNameInitials: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var menuTableView: UITableView!
    
    var mainViewController : CustomerHome!
    var menuArray: [(imgName: String, heading:String)] = [  ("ic_payment", "Payment"),
                                                            ("ic_orderlist", "Order History"),
                                                            ("ic_setting", "Settings"),
                                                            ("ic_changePassword", "Change Password"),
                                                            ("ic_faq", "FAQs"),
                                                            ("chat", "Chat with EZER"),
                                                            ("ic_help", "Help"),
                                                            ("ic_logout", "Logout"),]
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        //set appversion
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        lblVersion.text = String.init(format: "Version: %@", appVersionString)
        if UIScreen.main.bounds.size.width < 350 {
            menuTableView.rowHeight = 45
        }else {
            menuTableView.rowHeight = 50
        }
        menuTableView.separatorStyle = .none
        menuTableView.register(UINib(nibName: "LeftMenuCell", bundle: nil), forCellReuseIdentifier: CellsIdentifiers.leftmenu)
        self.menuTableView.contentInset = UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
        menuTableView.dataSource = self
        menuTableView.delegate = self
        firstNameInitials.layer.cornerRadius = firstNameInitials.frame.width/2
        firstNameInitials.clipsToBounds = true
        // Do any additional setup after loading the view.
        
        if  (HelperFunction.getBoolFromUserDefaults(key: ProfileKeys.isSubAccount)){
            menuArray = [("ic_orderlist", "Order History"),
                         ("ic_setting", "Settings"),
                         ("ic_changePassword", "Change Password"),
                         ("ic_faq", "FAQs"),
                         ("chat", "Chat with EZER"),
                         ("ic_help", "Help"),
                         ("ic_logout", "Logout"),]
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userName.text = HelperFunction.getUserName(loginType:.customer)
        firstNameInitials.text = HelperFunction.getUserInitials(loginType:.customer)
        IQKeyboardManager.shared.enable = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func privacyLinkAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: StoryboardNames.main, bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: IdentifierName.Main.termsAndPrivacy) as! TermsPrivacyController
        self.mainViewController.navigationController?.pushViewController(controller, animated: true)
        self.slideMenuController()?.closeLeft()
    }
    //MARK:- Public methods
    func changeViewController(_ menu: CustomerMenu) {
        
        switch menu
        {
        case .payment:
            let payment = AddNewPayment(nibName: "AddNewPayment", bundle: nil)
            self.mainViewController.navigationController?.pushViewController(payment, animated: false)
        case .orderHistory:
            let orderHistory = OrderHistoryController.init(nibName: "OrderHistoryController", bundle: nil)
            self.mainViewController.navigationController?.pushViewController(orderHistory, animated: false)
        case .settings:
            let settingCon = SaveSettingController.init(nibName: "SaveSettingController", bundle: nil)
            self.mainViewController.navigationController?.pushViewController(settingCon, animated: false)
        case .changePassword:
            let storyBoard = UIStoryboard.init(name: StoryboardNames.main, bundle: nil)
            let changePwd = storyBoard.instantiateViewController(withIdentifier: IdentifierName.Main.changePassword) as! ChangePwdController
            self.mainViewController.navigationController?.pushViewController(changePwd, animated: false)
        case .faqs:
            let storyBoard = UIStoryboard.init(name: StoryboardNames.customer, bundle: nil)
            let faqVC = storyBoard.instantiateViewController(withIdentifier: IdentifierName.Main.FaqVC) as! FaqVC
            self.mainViewController.navigationController?.pushViewController(faqVC, animated: false)
        case .help:
            let helpMenu = HelpViewController(nibName:"HelpViewController",bundle: nil)
            helpMenu.modalPresentationStyle = .overCurrentContext
            helpMenu.modalTransitionStyle = .crossDissolve
            self.mainViewController.present(helpMenu, animated: true, completion: {})
        case .support:
            IQKeyboardManager.shared.enable = false
            self.handleAndOpenChat()
        case .logout:
            self.mainViewController.logoutUser()
            
        }
        self.slideMenuController()?.closeLeft()
    }
    
    
    func handleAndOpenChat()  {
        let chatConfiguration = ChatConfiguration()
        chatConfiguration.isAgentAvailabilityEnabled = false
        chatConfiguration.isPreChatFormEnabled = true
        chatConfiguration.isChatTranscriptPromptEnabled = false
        
        let chatAPIConfiguration = ChatAPIConfiguration()
        chatAPIConfiguration.tags = ["IOS", "mobile_app"]
        
        chatAPIConfiguration.visitorInfo = VisitorInfo(name: HelperFunction.getUserName(loginType:.customer), email: HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.email), phoneNumber: "")
        Chat.instance?.configuration = chatAPIConfiguration
        
        
        // Name for Bot messages
        let messagingConfiguration = MessagingConfiguration()
        messagingConfiguration.name = "Chat Bot"
        
        do {
            // Build view controller
            let chatEngine = try ChatEngine.engine()
            let viewController = try Messaging.instance.buildUI(engines: [chatEngine], configs: [messagingConfiguration, chatConfiguration])
            // Present view controller
            self.mainViewController.navigationController?.pushViewController(viewController, animated: true)
            
            chatEngine.isConversationOngoing { (isConversation) in
                if isConversation{
                    _ = Chat.chatProvider?.observeChatState({ (chatState) in
                        print(chatState)
                        if chatState.chatSessionStatus == .ending{
                            self.hidePresentedAlert {
                                let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                                    self.showProgressBar()
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                                        self.hideProgressBar()
                                        self.mainViewController.navigationController?.popViewController(animated: false)
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                            self.handleAndOpenChat()
                                        }
                                    }
                                })
                                self.showAlertController(message: "Previous session is ended, open Chat again.",action: action)
                               
                            }
                        }
                    })
                }
            }
            
            
        } catch {
            // handle error
        }
        
        
    }
    
    
}
//MARK:- UITableViewDelegate extension
extension CustomerLeftMenu : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var row = indexPath.row
        if  (HelperFunction.getBoolFromUserDefaults(key: ProfileKeys.isSubAccount)){
            row += 1
        }
        if let menu = CustomerMenu(rawValue: row) {
            self.changeViewController(menu)
        }
    }
}

//MARK:- UITableViewDataSource extension
extension CustomerLeftMenu : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellsIdentifiers.leftmenu) as! LeftMenuCell
        let menu = menuArray[indexPath.row]
        cell.menuName.text = menu.heading
        cell.imgLogo.image = UIImage.init(named: menu.imgName)
        return cell
    }
}
