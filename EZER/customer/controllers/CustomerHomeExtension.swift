//
//  CustomerHomeExtension.swift
//  EZER
//
//  Created by TimerackMac1 on 13/01/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import Foundation
import UIKit
extension CustomerHome
{
    func addObserver()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(refreshOrderListStatus(notification:)), name: .orderStatusChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(orderCancelFromDriver(notification:)), name: .orderCancelFromDriver, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ownOrderCancel(notification:)), name: .ownOrderCancel, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ownOrderPlaced(notification:)), name: .ownOrderPlaced, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(resetFieldsValues), name: .resetCustomerHomeData, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(customerOrderTimeout), name: .customerOrderTimeout, object: nil)//
        NotificationCenter.default.addObserver(self, selector: #selector(customerOrderRescheduled), name: .customerOrderRescheduled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshHomeList), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    @objc func refreshHomeList()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.getCurrentOrder()
            self.checkImmediateOrder()
        }
      
    }
    @objc func resetFieldsValues()
    {
        startCoordinate = nil
        destinationCoordinate = nil
        addressType = .startAddress
        isStartAddressSelected = false
        isEndAddressSelected = false
        endLocation.text = ""
        startLocation.text = ""
        startAddressUnitNumberTxtField.text = ""
        destinationAddressUnitNumberTxtField.text = ""
        txtStartBusinessName.text = ""
        txtDestinationBusinessName.text = ""
        lblPickupDateTime.text = ""
        constraintHeightOfPickupDateTime.constant = 0
//        segmented.selectItem(at: 0, withAnimation: false)
        CurrentCustomerOrder.orderScheduleType = .later
        createOrderModel = CreateOrderModel()
         self.checkImmediateOrder()
    }
    
    @objc func refreshOrderListStatus( notification : Notification){
        
        guard let notificationObject = notification.userInfo as? [ String: Any] else{return }
        guard let currentOrderId = notificationObject["orderId"] as? String else{ return}
        guard let status = notificationObject["status"] as? String else{ return}
        let currentStatus = notificationObject["currentStatus"] as? String
       
                    guard let statusType = OrderStatusType(rawValue: status) else{ return}
                    if statusType == .done
                    {
                        currentOrderList = self.currentOrderList.filter { (currentOrderModel) -> Bool in
                            return currentOrderModel.orderId != currentOrderId
                        }
                        reloadTableView()
                    }else{
                        for item in currentOrderList
                        {
                            if item.orderId == currentOrderId
                            {
                                item.driverName = notificationObject["driverName"] as? String ?? ""
                                item.driverId = notificationObject["driverId"] as? String ?? ""
                                item.status = statusType
                                item.CurrentStatus = currentStatus ?? ""
                                reloadTableView()
                                break
                            }
                        }
                    }
    }
    
    @objc func ownOrderCancel( notification : Notification){
        if let orderDictionary = notification.object as? [String : Any]
        {
            let orderId = orderDictionary["orderId"] as! String
            removeOrderFromCustomerData(orderId: orderId)
        }
    }
    @objc func orderCancelFromDriver( notification : Notification)
    {
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            if let currentOrderId = notificationObject["orderId"] as? String
            {
                for item in currentOrderList
                {
                    if item.orderId == currentOrderId
                    {
                        item.driverName = ""
                        item.driverId = ""
                        item.status = .open
                        reloadTableView()
                        break
                    }
                }
            }
        }
    }
    @objc func customerOrderTimeout( notification : Notification)
    {
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            if let currentOrderId = notificationObject["orderId"] as? String
            {
                for item in currentOrderList
                {
                    if item.orderId == currentOrderId
                    {
                        item.isTimeout = true
                        //item.immediate = false
                        item.driverId = ""
                        item.driverName = ""
                        item.status = .open
                        reloadTableView()
                        break
                    }
                }
            }
        }
    }
    @objc func customerOrderRescheduled( notification : Notification)
    {
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            if let currentOrderId = notificationObject["orderId"] as? String
            {
                for item in currentOrderList
                {
                    if item.orderId == currentOrderId
                    {
                        item.isTimeout = false
                        item.immediate = false
                        item.status = .open
                        item.driverId = ""
                        reloadTableView()
                        break
                    }
                }
            }
        }
    }
    @objc func ownOrderPlaced( notification : Notification){
        if let currentOrderModel = notification.object as? CurrentOrderModel
        {
            addNewOrderToCustomerData(currentOrderModel: currentOrderModel)
        }
    }
    
    func removeOrderFromCustomerData(orderId : String)
    {
        currentOrderList = self.currentOrderList.filter { (currentOrderModel) -> Bool in
            return currentOrderModel.orderId != orderId
        }
        reloadTableView()
        
    }
    func addNewOrderToCustomerData(currentOrderModel : CurrentOrderModel)
    {
        let order = self.currentOrderList.filter({$0.orderId == currentOrderModel.orderId})
        if order.count == 0{
            self.currentOrderList.append(currentOrderModel)
        }        
        reloadTableView()
    }
    func reloadTableView()
    {
        self.tableViewOutlet.reloadData()
        self.viewDidLayoutSubviews()
    }
}
