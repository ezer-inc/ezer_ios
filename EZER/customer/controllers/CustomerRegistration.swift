//
//  CustomerRegistration.swift
//  EZER
//
//  Created by TimerackMac1 on 15/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit


protocol ClassHearAboutDelegate: class {
    func getHearAboutEzer(_ value: String?,_ index: Int!)
}

class CustomerRegistration: UIViewController , UITextFieldDelegate , ClassHearAboutDelegate {
    
    //MARK:- IBOutlets
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var email: TJTextField!
    @IBOutlet weak var firstName: TJTextField!
    @IBOutlet weak var confirmPassword: TJTextField!
    @IBOutlet weak var password: TJTextField!
    @IBOutlet weak var phoneNumber: TJPhoneNumberTextField!
    @IBOutlet weak var lastName: TJTextField!
    @IBOutlet weak var aboutInfoView: UIView!
    @IBOutlet weak var hearByLabel: UILabel!
    
    //MARK:- Custom variables
    var hearAboutEzerValue: String!
    var hearIndex:Int = -1
    
    //MARK:- override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        aboutInfoView.layer.cornerRadius = 8
        aboutInfoView.layer.borderWidth = 1
        aboutInfoView.layer.borderColor = UIColor.gray.cgColor
        aboutInfoView.clipsToBounds = true
        
        firstName.delegate = self
        lastName.delegate = self
        email.delegate = self
        phoneNumber.delegate = self
        password.delegate = self
        confirmPassword.delegate = self
        
        phoneNumber.maxDigits = 10
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        for index in 1...6
        {
            if let textField = parentView.viewWithTag(index) as? TJTextField{
                textField.addTarget(self, action: #selector(enterPressed), for: .editingDidEndOnExit)
                textField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(enterPressed))
            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Custom methods
    @objc func enterPressed(textField : TJTextField)
    {
//        if textField.tag == 6
//        {
//            openHearAboutEzerController(textField)
//        }else{
//            parentView.viewWithTag(textField.tag + 1)?.becomeFirstResponder()
//        }
    }
    
    
    func getHearAboutEzer(_ value: String?,_ index: Int!) {
        hearAboutEzerValue = value
        hearByLabel.text = hearAboutEzerValue
        aboutInfoView.layer.borderColor = UIColor.gray.cgColor
        hearIndex = index
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == firstName ||  textField == lastName{
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= IdentifierName.nameLength
        } else if textField == phoneNumber{
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= IdentifierName.phoneNumber
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if  let tjTextField = textField as? TJTextField
        {
            tjTextField.errorEntry = false
        }else
        {
            let tjTextField = textField as! TJPhoneNumberTextField
            tjTextField.errorEntry = false
        }
    }
    
    func validatePassword() -> Bool {
        if(password.text!.length < 6)
        {
            password .errorEntry = true
            self.view.showMessage(ValidationMessages.passwordLenghthError, type: GSMessageType.error)
            return true
        }else if(password.text != confirmPassword.text)
        {
            confirmPassword .errorEntry = true
            self.view.showMessage(ValidationMessages.passwordNotMatch, type: GSMessageType.error)
            return true
        }
        
        return false
    }
    
    func validateAboutText() ->Bool {
        if(hearAboutEzerValue == nil ||  hearAboutEzerValue.isEmpty)
        {
            aboutInfoView.layer.borderColor = UIColor.red.cgColor
            return true
        }
        return false
    }
    
    func checkValidation()->Bool
    {
        var error = false
        
        firstName.text = firstName?.text?.trimWhiteSpace()
        lastName.text = lastName?.text?.trimWhiteSpace()
        email.text = email?.text?.trimWhiteSpace()
        phoneNumber.text = phoneNumber?.text?.trimWhiteSpace()
        password.text = password?.text?.trimWhiteSpace()
        confirmPassword.text = confirmPassword?.text?.trimWhiteSpace()
        
        if(!HelperFunction.validateName(name: firstName.text!))
        {
            firstName.errorEntry = true
            error = true
        }else
        
        if(!HelperFunction.validateName(name: lastName.text!))
        {
            lastName.errorEntry = true
            error = true
        }else
        
        if(!email.hasText)
        {
            email.errorEntry = true
            error = true
        }else
        
        if(!phoneNumber.hasText)
        {
            phoneNumber.errorEntry = true
            error = true
        }else if(!HelperFunction.isValidNumber(testStr: phoneNumber.text!)){
            phoneNumber.errorEntry = true
            error = true
        }
        else if (!HelperFunction.validatePassword(password:password.text!)){
            password.errorEntry = true
            error = true
             self.view.makeToast(ValidationMessages.invalidPassword , duration: 3.0, position: .bottom)
        }
        else if(!password.hasText)
        {
            password.errorEntry = true
            error = true
        }else
        if(!confirmPassword.hasText)
        {
            confirmPassword.errorEntry = true
            error = true
        }
        return error
    }
    
    //MARK:- IBActions
    @IBAction func RegisterUser(_ sender: Any) {
        self.view.endEditing(true)
        if checkValidation() {
            if(!HelperFunction.validateName(name: firstName.text!)){
                 self.view.makeToast(ValidationMessages.invalidFirstName, duration: 3.0, position: .bottom)
            }else if(!HelperFunction.validateName(name: lastName.text!)){
               self.view.makeToast(ValidationMessages.invalidLastName,duration: 3.0, position: .bottom)
            }
            return
        }
        
        if(!HelperFunction.validateEmail(email:email.text!))
        {
            email.errorEntry = true
            self.view.makeToast(ValidationMessages.invalidEmail,duration: 3.0, position: .bottom)
            return
        }
        
        if(validatePassword()) {return}
       
        
        if(validateAboutText()) { return }
        
        //
        //        if(validatePassword()) { return }
        
        let paramneter :[String:Any] = [
            "email":email.text!,
            "password":password.text!,
            "firstName":firstName.text!,
            "lastName":lastName.text!,
            "cellPhone":phoneNumber.text!,
            "profilePic":"",
            "installationId":HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.deviceToken),
            "ezerQA":hearAboutEzerValue
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.register, jsonToPost: paramneter, completion: { (result) in
            if(result != nil)
            {
                self.view.showMessage("\(String(describing: result!["message"]))", type: GSMessageType.success)
                self.loginCustomer()
            }
        })
    }
    
    @IBAction func termofServicesClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name : "Main", bundle : nil)
        
        let controller = storyBoard.instantiateViewController(withIdentifier: IdentifierName.Main.termsAndPrivacy) as! TermsPrivacyController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func openHearAboutEzerController(_ sender: Any) {
        let paramsData : [String: Any] = [:]
        BaseServices.SendPostJson(viewController: self,method: .post ,serverUrl: ServerUrls.Customer.ezerQsnAsn, jsonToPost: paramsData) { (result) in
            if let result = result{
                if(result[ServerKeys.status].int == ServerStatusCode.Success)
                {
                    let hearAboutEzerCon = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.howDidYouHear) as! HowDidYouHear
                    hearAboutEzerCon.informationArray = result[ServerKeys.data].arrayObject
                    hearAboutEzerCon.delegate = self
                    hearAboutEzerCon.indexSelected = self.hearIndex
                    hearAboutEzerCon.userHearChoice = self.hearAboutEzerValue
                    hearAboutEzerCon.module = .customer
                    self.navigationController?.pushViewController(hearAboutEzerCon, animated: true)
                }
            }
        }
    }
    
    //MARK:- private methods
    private func loginCustomer()
    {
        let loginparams :[String:Any] = [
            "email":self.email.text!,
            "password":password.text!,
            "deviceType": "ios",
            "model": UIDevice.modelName,
            "appVersion": HelperFunction.getVersion(),
            "deviceToken": HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.deviceToken)
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.loginUser, jsonToPost: loginparams) { (json) in
            if let json = json
            {
                HelperFunction.saveValueInUserDefaults(key: ProfileKeys.authToken, value: json[ServerKeys.data][ProfileKeys.authToken].string ?? "")
                HelperFunction.saveValueInUserDefaults(key: ProfileKeys.email, value: self.email.text!)
                HelperFunction.saveValueInUserDefaults(key: ProfileKeys.password, value: self.password.text!)
                let model = CustomerLoginModel(json: json[ServerKeys.data])
                
                if model.btCustomerId.isEmpty
                {
                    HelperFunction.saveCustomerInPref(loginModel:model)
                    let payment = AddNewPayment(nibName: "AddNewPayment", bundle: nil)
                    self.navigationController?.pushViewController(payment, animated: false)
                }
                else if model.isEnabled{ // customer is disable by admin
                    HelperFunction.saveCustomerInPref(loginModel:model)
                    HelperFunction.openCustomer(viewController: self, loginModel: model)
                }
                else{
                    let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    self.showAlertController(heading: AppName, message: ValidationMessages.disabledUser, action: action)
                }
            }
        }
    }
}


