
//
//  CustomerCancelOrderPopUP.swift
//  EZER
//
//  Created by Upkesh Thakur on 09/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
class CustomerCancelOrderPopUP: UIViewController {
   
    var message: String!
   
    @IBOutlet weak var parentView: UIView!
    
    @IBOutlet weak var noButton: RoundableButton!
    @IBOutlet weak var yesButton: RoundableButton!
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var reasonText: KMPlaceholderTextView!
    
    var actionConfirmed:((_ action: Bool,_ text:String)->Void)?
    var actionCanceled:((_ action: Bool)->())?
    var placeOrder = false
    override func viewDidLoad() {
        super.viewDidLoad()
        messageLabel.text = message
        // Do any additional setup after loading the view.
        reasonText.layer.borderWidth = 1
        reasonText.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        if placeOrder
        {
            messageLabel.backgroundColor = UIColor(red:0.38, green:0.48, blue:0.53, alpha:1)
            parentView.backgroundColor = UIColor(red:0.38, green:0.48, blue:0.53, alpha:1)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        messageLabel.text = message
    }
    
  
    @IBAction func handleAction(_ sender: RoundableButton) {
    
            switch(sender.tag)
            {
            case 1:
                self.dismiss(animated: false, completion: {
                    self.actionConfirmed?(true, self.reasonText.text ?? "")
                })
            
            //case 2:
            default:
                self.dismiss(animated: false, completion: {
                    self.actionCanceled?(false)
                })
            }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
