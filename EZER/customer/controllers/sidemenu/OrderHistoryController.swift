//
//  OrderHistoryController.swift
//  EZER
//
//  Created by TimerackMac1 on 18/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class OrderHistoryController: UIViewController {
    @IBOutlet weak var orderTableView: UITableView!
    let cellName = "OrderHistoryCell"
    var orderHistoryList :[OrderHistoryModel] = []
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.title = "Order History"
        self.orderTableView.register(UINib(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
        orderTableView.dataSource = self
        orderTableView.delegate = self
        orderTableView.separatorStyle = .none
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        getOrderHistory(count:orderHistoryList.count)
        addObserver()
    }
    private func addObserver()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(ownOrderCancel(notification:)), name: .ownOrderCancel, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleOrderStatusChange(notification:)), name: .orderStatusChange, object: nil)
    }
    @objc func handleOrderStatusChange(notification: Notification)
    {
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            if let currentOrderId = notificationObject["orderId"] as? String
            {
                    guard let status = notificationObject["status"] as? String else{return}
                    guard let statusType = OrderStatusType(rawValue: status) else{ return }
                for item in self.orderHistoryList
                {
                    if item.orderId == currentOrderId
                    {
                        item.status = statusType
                        DispatchQueue.main.async {
                            self.orderTableView.reloadData()
                        }
                        break
                    }
                }
            }
        }
    }
    private func removeObserver()
    {
        NotificationCenter.default.removeObserver(self)
    }
  
    @objc func ownOrderCancel( notification : Notification){
        
        guard let notificationObject = notification.object as? [String : Any] else{return }
        
        guard let currentOrderId = notificationObject["orderId"] as? String else{return }
        
        DispatchQueue.global(qos: .background).async {
            for item in self.orderHistoryList
            {
                if item.orderId == currentOrderId
                {
                    item.status = .cancelled
                    DispatchQueue.main.async {
                        self.orderTableView.reloadData()
                    }
                    break
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent//view popped
        {
            removeObserver()
        }//else new vew pushed
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getOrderHistory(_ showLoader : Bool = true, count:Int=0)
    {
        let params : [String:Any] = [
            "customerId": HelperFunction.getUserId(),
            "offset": count
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.orderList, jsonToPost: params,showLoader : showLoader) { (json) in
            if let json = json
            {
                if let orderArray = json[ServerKeys.data].array{
                    if count == 0{
                      self.orderHistoryList.removeAll()
                    }
                    for order in orderArray
                    {
                        self.orderHistoryList.append(OrderHistoryModel(json : order))
                    }
                    self.orderTableView.reloadData()
                    //self.setUPHeader()
                }
                else{
                    self.showMessage(ValidationMessages.tryAgain, type: .error,options: [.textNumberOfLines(0)])
                }
            }
        }
    }
    
    @IBAction func loadMoreItem(_ sender: UIButton) {
        getOrderHistory(count: orderHistoryList.count)
    }
    
    
}
extension OrderHistoryController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderHistoryList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellName) as! OrderHistoryCell
        cell.setUpCell(historyModel: orderHistoryList[indexPath.row])
        return cell
    }
}
extension OrderHistoryController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: StoryboardNames.customer, bundle: Bundle.main)
        let trackOrder = storyBoard.instantiateViewController(withIdentifier: IdentifierName.Customer.trackOrder) as! TrackOrderController
        trackOrder.orderId = orderHistoryList[indexPath.row].orderId
        trackOrder.openedFromOrderHistory = true
        trackOrder.orderTitle = "Order \(orderHistoryList[indexPath.row].orderNumber)"
//        switch orderHistoryList[indexPath.row].status {
//        case .cancelled, .finished, .timeout, .done:
//            trackOrder.orderSubTitle = "Order Status"
//        default:
//            trackOrder.orderSubTitle = "Order Confirmed"
//        }
        self.navigationController?.pushViewController(trackOrder, animated: true)
    }
}
