//
//  SaveSettingController.swift
//  EZER
//
//  Created by TimerackMac1 on 18/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class SaveSettingController: UIViewController ,UITextFieldDelegate {
    
    //MARK:- IBOutlets
    @IBOutlet weak var firstName: TJTextField!
    @IBOutlet weak var lastName: TJTextField!
    @IBOutlet weak var email: TJTextField!
    @IBOutlet weak var cellPhone: TJPhoneNumberTextField!
    
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Settings"
        // Do any additional setup after loading the view.
        
        firstName.delegate = self
        lastName.delegate = self
        email.delegate = self
        cellPhone.delegate = self
        
        cellPhone.maxDigits = 10        
        getCustomerDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- private methods
    private func getCustomerDetails() {
        let param: [String:Any] = [
            "customerId": HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.Id)
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.getCustomerDetails, jsonToPost: param) { (json) in
            if let json = json{
                let customerLoginModel = CustomerLoginModel(json: json[ServerKeys.data])
                self.firstName.text =  customerLoginModel.firstName
                self.lastName.text = customerLoginModel.lastName
                self.email.text = customerLoginModel.email
                self.cellPhone.text = customerLoginModel.cellPhone
                self.saveCustomerData()
            }
        }
    }
    
    //MARK:- public methods
    func checkValidation()->Bool
    {
        var error = false
        firstName.text = firstName.text
        lastName.text = lastName.text?.trimWhiteSpace()
        email.text = email.text?.trimWhiteSpace()
        cellPhone.text = cellPhone.text?.trimWhiteSpace()
        
        if(!HelperFunction.validateName(name: firstName.text!))
        {
            firstName.errorEntry = true
            error = true
        }else
            
            if(!HelperFunction.validateName(name: lastName.text!))
            {
                lastName.errorEntry = true
                error = true
            }else
                
                if(!email.hasText)
                {
                    email.errorEntry = true
                    error = true
                }else
                    
                    if(!cellPhone.hasText)
                    {
                        cellPhone.errorEntry = true
                        error = true
                        //        }else if(!HelperFunction.isValidNumber(testStr: cellPhone.text!)){
                    } else if cellPhone.text?.length != 14 {
                        
                        let phoneNumber = cellPhone.text?.removeWhitespace()
                        if phoneNumber?.length != 14 {
                            cellPhone.errorEntry = true
                            self.view.makeToast(ValidationMessages.invalidPhoneNumber,duration: 3.0, position: .bottom)
                            error = true
                        }
                    }
        return error
    }
    
    //MARK:- UITextField delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
            }
        
        if textField == lastName || textField == firstName{
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= IdentifierName.nameLength
        }else if textField == cellPhone{
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= IdentifierName.phoneNumber
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if  let tjTextField = textField as? TJTextField
        {
            tjTextField.errorEntry = false
        }else
        {
            let tjTextField = textField as! TJPhoneNumberTextField
            tjTextField.errorEntry = false
        }
    }
    
    //MARK:- IBActions
    fileprivate func saveCustomerData() {
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.cellPhone, value: self.cellPhone.text!)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.lastName, value: self.lastName.text!)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.firstName, value: self.firstName.text!)
        HelperFunction.saveValueInUserDefaults(key: ProfileKeys.email, value: self.email.text!)
    }
    
    @IBAction func saveCustomerDetails(_ sender: Any) {
        self.view.endEditing(true)
        if(checkValidation()) {
            if(!HelperFunction.validateName(name: firstName.text!)){
                self.view.makeToast(ValidationMessages.invalidFirstName, duration: 3.0, position: .bottom)
            }else if(!HelperFunction.validateName(name: lastName.text!)){
                self.view.makeToast(ValidationMessages.invalidLastName,duration: 3.0, position: .bottom)
            }
            return
        }
        
        if(!HelperFunction.validateEmail(email: email.text!))
        {
            email.errorEntry = true;
            self.view.makeToast(ValidationMessages.invalidEmail, duration: 3.0, position: .bottom)
            return
        }
        
        let param: [String:Any] = [
            "customerId": HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.Id),
            "email": email.text!,
            "firstName": firstName.text!,
            "lastName": lastName.text!,
            "cellPhone": cellPhone.text!
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.updateCustomerDetail, jsonToPost: param) { (json) in
            if let json = json{
                self.showMessage("\(json[ServerKeys.message])", type: GSMessageType.success,options: [.textNumberOfLines(0)])
                self.saveCustomerData()
            }
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
