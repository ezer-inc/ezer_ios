//
//  AddcardPaymentViewController.swift
//  EZER
//
//  Created by Akash on 20/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class AddcardPaymentViewController: UIViewController {

    var actionPaypalClicked:((_ action: Bool)->Void)?
    var actionCreditCardClicked:((_ action: Bool)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var closeButtonClicked: UIButton!
    @IBAction func closeButtonClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func paypalButtonClicked(_ sender: Any) {
         self.dismiss(animated: false, completion: {
            self.actionPaypalClicked?(true)
         })
    }
    
    @IBAction func creditCardCreated(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            self.actionCreditCardClicked?(true)
        })
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
