//
//  FaqVC.swift
//  EZER
//
//  Created by Prabhjot Singh on 26/06/19.
//  Copyright © 2019 TimerackMac1. All rights reserved.
//

import UIKit
import WebKit

class FaqVC: UIViewController,WKNavigationDelegate {

    //MARK:- OUTLETS
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK:- VARIABLES
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    }

    //MARK:- CustomizeUI
    func customizeUI()  {
         self.title = "FAQs"
         showWebView(faqUrl: "https://www.getezer.com/faqs-mobile.html")
    }
    
    func showWebView(faqUrl : String) {
        if let url = URL(string: faqUrl){
            let request = URLRequest(url: url)
            webView.navigationDelegate = self
            webView.load(request)
            webView.isHidden = true
            activityIndicator.startAnimating()
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
//        let script="javascript:(function() { " +
//        "document.getElementsByClassName('navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar')[0].style.display='none'; })()"
//        
//        webView.evaluateJavaScript(script) { (result, error) in
//            if error != nil {
//                print(result ?? "")
//            }
//        }
        activityIndicator.stopAnimating()
        webView.isHidden = false
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if webView != self.webView {
            decisionHandler(.allow)
            return
        }
        
        let app = UIApplication.shared
        if let url = navigationAction.request.url {
//             Handle target="_blank"
//            if navigationAction.targetFrame == nil {
//                if app.canOpenURL(url) {
//                    app.open(url)
//                    decisionHandler(.cancel)
//                    return
//                }
//            }
            
            // Handle phone and email links
            if url.scheme == "tel" || url.scheme == "mailto" {
                if app.canOpenURL(url) {
                    app.open(url)
                }
                
                decisionHandler(.cancel)
                return
            }
            
            decisionHandler(.allow)
        }
        
    }
    
}
