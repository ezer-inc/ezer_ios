//
//  AddNewPayment.swift
//  EZER
//
//  Created by TimerackMac1 on 18/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import BraintreeDropIn
import Braintree
class AddNewPayment: UIViewController {

    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var cardInfoViewTopConstraintOutlet: NSLayoutConstraint!
    @IBOutlet weak var addCardInfoHeightConstraintOutlet: NSLayoutConstraint!
    @IBOutlet weak var cardInfoButtonOutlet: RoundableButton!
    @IBOutlet weak var logoutOrSave: RoundableButton!
    @IBOutlet weak var addNewPayment: RoundableButton!
    
    var paymentItemArray : [PaymentItem] = []
    var isCardSaved = false
    let cellName    = "PaymentTableViewCell"
    let addPayment  = "AddNewPaymentTableViewCell"
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.title = "Payment"
        
        // Do any additional setup after loading the view.
        self.tableViewOutlet.register(UINib(nibName: addPayment, bundle: nil), forCellReuseIdentifier: addPayment)
        self.tableViewOutlet.register(UINib(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
        
        if HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.btCustomerId).isEmpty{
            navigationItem.hidesBackButton = true
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationController?.navigationBar.barTintColor = UIColor.customerTheme
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
          //  let nav = self.navigationController?.navigationBar
          //  nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        }else{
            logoutOrSave.isHidden = true
            getPaymentMethods()
        }
    }
    
    //@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var donateButton: UIButton! {
        didSet {
            donateButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -42, bottom: 0, right: 0)
            donateButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: -12, bottom: 0, right: 0)
            donateButton.layer.cornerRadius = donateButton.bounds.midY
            donateButton.layer.masksToBounds = true
        }
    }
    @IBAction func cardInfoButtonClicked(_ sender: Any) {
        
    }
    
    @IBOutlet weak var currencyLabel: UILabel! {
        didSet {
            currencyLabel.layer.cornerRadius = currencyLabel.bounds.midY
            currencyLabel.layer.masksToBounds = true
        }
    }
    
    
    // let toKinizationKey = "sandbox_ybsf7sr5_y3q5wzhskf3vpdz2"
    //let toKinizationKey = "sandbox_9dbg82cq_dcpspy2brwdjr3qn"
   
    var braintreeClient: BTAPIClient?
    
    func startCheckout(toKinizationKey : String) {
        // Example: Initialize BTAPIClient, if you haven't already
//        braintreeClient = BTAPIClient(authorization: "sandbox_9dbg82cq_dcpspy2brwdjr3qn")!
//        braintreeClient = BTAPIClient(authorization: "sandbox_s3spbg96_kh49c4swxcqt52pg")!
        braintreeClient = BTAPIClient(authorization: toKinizationKey)!
        let payPalDriver = BTPayPalDriver(apiClient: braintreeClient!)
        payPalDriver.viewControllerPresentingDelegate = self
        payPalDriver.appSwitchDelegate = self // Optional
        
        let request = BTPayPalRequest()
        request.billingAgreementDescription = "Ezer" //Displayed in customer's PayPal account
        indicatorView.isHidden = false
        payPalDriver.requestBillingAgreement(request) { (tokenizedPayPalAccount, error) -> Void in
            self.indicatorView.isHidden = true
            if let tokenizedPayPalAccount = tokenizedPayPalAccount {
               // print("Got a nonce: \(tokenizedPayPalAccount.nonce)")
                // Send payment method nonce to your server to create a transaction
                //self.isCardSaved = true
                //self.logoutOrSave.setTitle("Done", for: .normal)
                self.savePaymentMethod(paymentNonce: tokenizedPayPalAccount.nonce)
                
            } else if let error = error {
                //print("error", error.localizedDescription)
                // Handle error here...
                self.show(message: error.localizedDescription)
              //  print(error)
            } else {
             //   print("buyer canceled")
                // Buyer canceled payment approval
            }
        }
    }
    
    func show(message: String) {
        DispatchQueue.main.async {
            self.hideProgressBar()
            
            let alertController = UIAlertController(title: message, message: "", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    private func openDropInView(toKinizationKey : String)
    {
        // Test Values
        // Card Number: 4111111111111111
        // Expiration: 08/2018
        //startCheckout()
        let request =  BTDropInRequest()
        request.paypalDisabled = true
        let dropIn = BTDropInController(authorization: toKinizationKey, request: request)
        { [unowned self] (controller, result, error) in
            //print("called for server")
            if let error = error {
                self.show(message: error.localizedDescription)
              //  print(error)
            } else if (result?.isCancelled == true) {
                // self.show(message: "Transaction Cancelled")
            } else if let nonce = result?.paymentMethod?.nonce{
                //self.isCardSaved = true
                //self.logoutOrSave.setTitle("Done", for: .normal)
                self.savePaymentMethod(paymentNonce: nonce)
            }
            controller.dismiss(animated: true, completion: nil)
        }
        guard let dropInCon = dropIn else{
            self.showMessage("In valid Token", type: .error)
            return
        }
        self.present(dropInCon, animated: true, completion: nil)
    }
    
    @IBAction func addNewPayment(_ sender: RoundableButton) {
//        getAuthorisationToken()
    }
    
    
    @IBAction func logoutOrSaveAction(_ sender: RoundableButton) {
        if isCardSaved{
            HelperFunction.openCustomer(viewController: self, loginModel: nil)
        }else{
            logoutUser()
        }
    }
    
 
    func logoutUser()
    {
        
        let param: [String:Any] = [
            "customerId": HelperFunction.getSrtingFromUserDefaults(key:ProfileKeys.Id),
            "deviceToken": HelperFunction.getFileNameFromUrl(stringUrl: ProfileKeys.deviceToken)]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.logout, jsonToPost: param) { (json) in
            if (json != nil){
                SocketManager.sharedInstance.exitFromSocketEvent()
                SocketManager.sharedInstance.closeConnection()
                SocketManager.sharedInstance.isBackground = true
                HelperFunction.clearUserDefault()
                let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
                let mainViewController = storyboard.instantiateInitialViewController()
                let delegate = UIApplication.shared.delegate as! AppDelegate
                delegate.window?.backgroundColor = UIColor.driverTheme
                delegate.window?.rootViewController = mainViewController!
                delegate.window?.makeKeyAndVisible()
                
            }
        }
    }
    
    
   //* Mark Server Methods
    private func getAuthorisationToken (isPaypal : Bool)
   {
    var param: [String:Any]
    if isPaypal {
        param = [
            "customerId":HelperFunction.getUserId(),
            "paymentMethodType" : "PAYPAL"
        ]
    }else{
        param = [
            "customerId":HelperFunction.getUserId(),
            "paymentMethodType" : "CREDITCARD"
        ]
    }
    
    
    BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.createAuthorizationToken, jsonToPost: param) { (json) in
            if let json = json{
                if let token = json["authorizationToken"].string{
                    if isPaypal{
                        self.startCheckout(toKinizationKey:  token)
                    }
                    else{
                        self.openDropInView(toKinizationKey: token)
                    }
                    
                }
            }
        }
    }
    
    private func savePaymentMethod(paymentNonce : String)
    {
        let param: [String:Any] = [
            "customerId": HelperFunction.getUserId(),
            "paymentMethodNonce": paymentNonce
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.savePaymentMethod, jsonToPost: param) { (json) in
            if let json = json{
                if let btCustomerId = json["btCustomerId"].string{
                    self.isCardSaved = true
                    self.logoutOrSave.setTitle("Done", for: .normal)
                    HelperFunction.saveValueInUserDefaults(key: ProfileKeys.btCustomerId, value: btCustomerId)
                    self.getPaymentMethods()
                }
                else{
                    self.show(message: ValidationMessages.errorInBtCusomterId)
                }
            }
        }
    }
    
    private func getPaymentMethods()
    {
        let param: [String:Any] = [
            "customerId": HelperFunction.getUserId(),
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.getPaymentMethods, jsonToPost: param) { (json) in
            if let json = json{
                self.paymentItemArray.removeAll()
                if let dataArray = json[ServerKeys.data].array
                {
                    for payment in dataArray{
                        self.paymentItemArray.append(PaymentItem(json : payment))
                    }
                    self.tableViewOutlet.reloadData()
                }
            }
        }
    }
    
    private func deletePaymentMethod(paymentMethodToken: String)
    {
        let param: [String:Any] = [
            "customerId": HelperFunction.getUserId(),
            "token" : paymentMethodToken
            ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.deletePaymentMethod, jsonToPost: param) { (json) in
            if let _ = json{
                self.getPaymentMethods()
                self.view.makeToast("Card has been removed successfully")
            }
        }
    }
    
    private func updatePaymentMethod(paymentMethodToken: String)
    {
        let param: [String:Any] = [
            "customerId": HelperFunction.getUserId(),
            "token" : paymentMethodToken
        ]
        
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.updatePaymentMethod, jsonToPost: param) { (json) in
            if let _ = json{
                self.getPaymentMethods()
            }
        }
    }
}

extension AddNewPayment: BTAppSwitchDelegate
{
    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
        
    }
    
    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {
        
    }
    
    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
    
    }
}

extension AddNewPayment: BTViewControllerPresentingDelegate
{
    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController){
        dismiss(animated: true, completion: nil)
    }
    
    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
        
        present(viewController, animated: true, completion: nil)
    }
}


extension AddNewPayment : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentItemArray.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == paymentItemArray.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: addPayment) as! AddNewPaymentTableViewCell
            cell.delegate = self
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellName) as! PaymentTableViewCell
            cell.setUpCell(paymentModel: paymentItemArray[indexPath.row])
            cell.delegate = self
            return cell
        }
    }
}

extension AddNewPayment : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row != paymentItemArray.count{
            DispatchQueue.main.async {
            let alertCon = AlertDialogController(nibName: "AlertDialogController",bundle : nil)
            alertCon.actionConfirmed = {
                action in
                self.updatePaymentMethod(paymentMethodToken: self.paymentItemArray[indexPath.row].token)
            }
            alertCon.actionCanceled = { (action)-> () in}
            alertCon.message = ValidationMessages.makeDefaultPayment
            alertCon.modalPresentationStyle = .overCurrentContext
            self.present(alertCon, animated: false, completion: nil)
            }
        }
    }
}

extension AddNewPayment : PaymentTableViewCellDelegate
{
    func deleteButtonClicked(paymentType: PaymentItem) {
        let alertCon = AlertDialogController(nibName: "AlertDialogController",bundle : nil)
        alertCon.actionConfirmed = {
            action in
            self.deletePaymentMethod(paymentMethodToken: paymentType.token)
        }
        alertCon.actionCanceled = { (action)-> () in}
        alertCon.message = ValidationMessages.deletePayment
        alertCon.modalPresentationStyle = .overCurrentContext
        self.present(alertCon, animated: false, completion: nil)
    }
}

extension AddNewPayment : AddcardPaymentTableCellDelegate
{
    func addNewPaymentClicked() {
        
        let alertCon = AddcardPaymentViewController(nibName: "AddcardPaymentViewController",bundle : nil)
        alertCon.actionPaypalClicked = {
            action in
            self.getAuthorisationToken(isPaypal: true)
        }
        alertCon.actionCreditCardClicked = {
            action in
            self.getAuthorisationToken(isPaypal: false)
        }
        alertCon.modalPresentationStyle = .overCurrentContext
        self.present(alertCon, animated: false, completion: nil)
    
    }
}
