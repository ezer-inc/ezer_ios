//
//  HowDidYouHear.swift
//  EZER
//
//  Created by TimerackMac1 on 16/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class HowDidYouHear: UITableViewController {
    
    //MARK:- Custom variables
    weak var delegate: ClassHearAboutDelegate?
    var userHearChoice:String!
    var indexSelected:Int!
    var informationArray:Array<Any>!
    var module:LoginType = .customer
    
    struct CellNames{
        static let labelCell = "labelCell"
        static let textFieldCell = "textFieldCell"
    }
    
    //MARK:- Overrride methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return informationArray.count + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < (informationArray.count+1)-1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellNames.labelCell) as! YouKnowLabel
            
            if module == .customer{
                cell.selectValue.setImage(#imageLiteral(resourceName: "ic_radio_button_off"), for: .normal)
                cell.selectValue.setImage(#imageLiteral(resourceName: "ic_radio_button_on"), for: .selected)
            }else{
                cell.selectValue.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
                cell.selectValue.setImage(#imageLiteral(resourceName: "radio_selected"), for: .selected)
            }
            
            let dictionaryObject = informationArray[indexPath.row] as! Dictionary<String,Any>
            cell.hearBy.text = dictionaryObject["value"] as? String
            if(indexPath.row == indexSelected)
            {
                cell.selectValue.isSelected = true
                userHearChoice = cell.hearBy.text
            }
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: CellNames.textFieldCell) as! YouKnowTextField
            cell.index = indexPath.row
            cell.delegate = self
            
            if module == .customer{
                cell.selectValue.setImage(#imageLiteral(resourceName: "ic_radio_button_off"), for: .normal)
                cell.selectValue.setImage(#imageLiteral(resourceName: "ic_radio_button_on"), for: .selected)
            }else{
                cell.selectValue.setImage(#imageLiteral(resourceName: "radio_unselected"), for: .normal)
                cell.selectValue.setImage(#imageLiteral(resourceName: "radio_selected"), for: .selected)
            }
            
            if(indexPath.row == indexSelected)
            {
                cell.selectValue.isSelected = true
                cell.hearByText.text = userHearChoice
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexSelected = indexPath.row
        if let cell = tableView.cellForRow(at: indexPath) as? YouKnowLabel
        {
            selectValue(cell.selectValue,true, cell.hearBy.text)
        }else
        {
            let cell = tableView.cellForRow(at: indexPath) as! YouKnowTextField
            selectValue(cell.selectValue,true, cell.hearByText.text)
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? YouKnowLabel
        {
            selectValue(cell.selectValue,false, nil)
        }else
        {
            if let cell = tableView.cellForRow(at: indexPath) as? YouKnowTextField{
                selectValue(cell.selectValue,false, nil)
            }
        }
    }
    
    
    //MARK:- public methods
    func selectValue(_ button:UIButton!,_ state:Bool!,_ value:String!) {
        button.isSelected = state
        userHearChoice = value
        if(state)
        {
            if(userHearChoice == nil || userHearChoice.isEmpty)
            {
                button.isSelected = false
                self.showMessage(ValidationMessages.selectHearEzerChoice, type: GSMessageType.error)
            }else
            {
                userHearChoice = userHearChoice.trimWhiteSpace()
                delegate?.getHearAboutEzer(userHearChoice,indexSelected)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    //MARK:- IBAction
    @IBAction func submitUserSelection(_ sender: Any) {
        
        if(userHearChoice == nil || userHearChoice.isEmpty)
        {
            self.showMessage(ValidationMessages.selectHearEzerChoice, type: GSMessageType.error)
        }else
        {
            userHearChoice = userHearChoice.trimWhiteSpace()
            delegate?.getHearAboutEzer(userHearChoice,indexSelected)
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
