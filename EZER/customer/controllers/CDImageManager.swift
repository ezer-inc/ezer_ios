//
//  CDImageManager.swift
//  EZER
//
//  Created by Mac mini on 23/10/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import Foundation
import CoreData

class CDImageManager: CoreDataManager {
    var entityName = "CDImage"
    static let shared = CDImageManager()
    typealias ResultData = (_ result: Result<CDImage, Error>) -> ()
    
    // MARK:- Enity Methods
    func updateUrl(_ id: String, url: String, completionHandler: @escaping ErrorData) {
        self.fetchRecords(predicate: NSPredicate(format: "id = %@", id)) { (result) in
            switch result{
            case .success(let data):
                if let data = data.first as? CDImage {
                    // custom values can be added is needed
                    data.imageUrl = url
                    self.saveContext(completionHandler: { (error) in
                        completionHandler(error)
                    })
                }else{
                    completionHandler(CDError.dataNotFound)
                }
                
            case .failure(let error):
                completionHandler(error)
            }
        }
    }
}
