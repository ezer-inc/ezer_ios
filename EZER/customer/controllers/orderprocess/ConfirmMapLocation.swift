//
//  ConfirmMapLocation.swift
//  EZER
//
//  Created by TimerackMac1 on 17/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

protocol ConfirmLocationDelegate {
    func confirmLocation(address : String, coordinate :CLLocationCoordinate2D)
}

class ConfirmMapLocation: UIViewController {
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var headerLabel: UILabel!
    var locationManager : LocationManager!
    var currentPlace: GMSPlace?
    var zoomLevel: Float = 15.0
    var confirmDelegate : ConfirmLocationDelegate?
    @IBOutlet weak var locationMap: GMSMapView!
    var addressType : AddressType!
    var currentCoordinate : CLLocationCoordinate2D!
    var selectedCountry : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Confirm Pick Up Location"
        if addressType == AddressType.endAddress  {
            self.title = "Confirm Drop Off Location"
            headerLabel.text = "Drop Off Point"
        }
        locationMap.delegate = self
        locationMap.settings.myLocationButton = true
        //mapView.isMyLocationEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let currentPlace = currentPlace{
            
            let gmsMarker     = GMSMarker(position: currentPlace.coordinate)
            gmsMarker.title   = currentPlace.name
            gmsMarker.snippet = currentPlace.formattedAddress
            gmsMarker.map     = locationMap
            
            let camera = GMSCameraPosition.camera(withLatitude: currentPlace.coordinate.latitude,
                                                  longitude: currentPlace.coordinate.longitude,
                                                  zoom: zoomLevel)
            addressLabel.text = currentPlace.formattedAddress?.trimCountry()
            locationMap.camera = camera
            currentCoordinate = currentPlace.coordinate
            if let address = currentPlace.addressComponents{
                for component in address {
                    if component.type == "country" {
                        selectedCountry = component.name
                    }
                }
            }
            
        }else if currentCoordinate != nil{ 
            resetMarkerOnMap(coordinate: currentCoordinate)
        }
        else{
            locationMap.isHidden = true
            locationManager = LocationManager(delegate:self)
            locationManager.startUpdatingLocation()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func confirmLocation_touchUpInside(_ sender: RoundableButton) {
        if addressType == .startAddress{
            checkActiveArea()
        }else{
            if let selectedCountry = selectedCountry{
                //check if user select only US country
                if selectedCountry == "United States" || selectedCountry == "India"{
                    self.confirmDelegate?.confirmLocation(address: self.addressLabel.text!, coordinate: self.currentCoordinate)
                    self.navigationController?.popViewController(animated: true)
                }else{
                    self.showMessage(ValidationMessages.destinationNotAvailable, type: .error,options: [.textNumberOfLines(0)])
                }
            }else{
                self.showMessage(ValidationMessages.destinationNotAvailable, type: .error,options: [.textNumberOfLines(0)])
            }
        }
    }
    
    @IBAction func back_touchUpInside(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        self.showProgressBar()
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            self.hideProgressBar()
            if let address = response?.firstResult() {
                self.selectedCountry = address.country?.trimCountry()
                let lines = address.lines!
                self.addressLabel.text = lines.joined(separator: " ").trimCountry()
                self.currentCoordinate = coordinate
            }else{
                 self.addressLabel.text = ""
                 self.selectedCountry = nil
                 self.currentCoordinate = nil
                //print(error?.localizedDescription)
            }
        }
    }
    
    private func addMrkerOnMap(coordinate : CLLocationCoordinate2D)
    {
        locationMap.clear()
        let gmsMarker = GMSMarker.init(position: coordinate)
        gmsMarker.map = locationMap
    }
    
    private func resetMarkerOnMap(coordinate : CLLocationCoordinate2D){
        let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude,
                                              longitude: coordinate.longitude,
                                              zoom: zoomLevel)
        if locationMap.isHidden {
            locationMap.isHidden = false
            locationMap.camera = camera
        } else {
            locationMap.animate(to: camera)
        }
        addMrkerOnMap(coordinate:coordinate)
        reverseGeocodeCoordinate(coordinate: coordinate)
    }
    
    private func checkActiveArea()
    {
        if currentCoordinate != nil{
            let params :[String:Any] = [
                "lng":currentCoordinate.longitude,
                "lat":currentCoordinate.latitude
            ]
            BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Main.checkActiveArea, jsonToPost: params) { (json) in
                if let json = json
                {
                    let checkAreaModel = CheckActiveAreaModel(json : json)
                    if (checkAreaModel.addressAvailable){
                        self.confirmDelegate?.confirmLocation(address: self.addressLabel.text!, coordinate: self.currentCoordinate)
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        self.showMessage(ValidationMessages.ezerNotActive, type: .error,options: [.textNumberOfLines(0)])
                    }
                }
            }
        }else{
            self.showMessage(ValidationMessages.locationNotAvailable, type: .error,options: [.textNumberOfLines(0)])
        }
    }
}

extension ConfirmMapLocation : LocationManagerDelegate
{
    func locationDenied() {
        
    }
    
    func didChangeinLocation(cordinate: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.camera(withLatitude: cordinate.latitude,
                                              longitude: cordinate.longitude,
                                              zoom: zoomLevel)
        if locationMap.isHidden {
            locationMap.isHidden = false
            locationMap.camera = camera
        } else {
            locationMap.animate(to: camera)
        }
        addMrkerOnMap(coordinate:cordinate)
        reverseGeocodeCoordinate(coordinate: cordinate)
        locationManager.stopUpdatingLocation()
    }
    
    func didErrorinLocation(error: Error) {
        locationManager.stopUpdatingLocation()
    }
    
    func locationNotAvailable() {
        DispatchQueue.main.async {
            if let _ = self.presentedViewController { return }
            self.showLocationAlert()
        }
    }
    
    func locationFaliedToUpdate(status: CLAuthorizationStatus) {
        DispatchQueue.main.async {
            self.showLocationAlert()
        }
    }
    
}
extension ConfirmMapLocation : GMSMapViewDelegate{
//    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
//        if let currentPlace = currentPlace{
//            let camera = GMSCameraPosition.camera(withLatitude: currentPlace.coordinate.latitude,
//                                                  longitude: currentPlace.coordinate.longitude,
//                                                  zoom: zoomLevel)
//            locationMap.camera = camera
//            addMrkerOnMap(coordinate: currentPlace.coordinate)
//        }
//        return true
//    }
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        locationManager = LocationManager(delegate:self)
        locationManager.startUpdatingLocation()
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        addMrkerOnMap(coordinate:coordinate)
        reverseGeocodeCoordinate(coordinate: coordinate)
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        // reverseGeocodeCoordinate(coordinate: position.target)
    }
}
