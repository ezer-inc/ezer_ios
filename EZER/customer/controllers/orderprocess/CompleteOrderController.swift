//
//  CompleteOrderController.swift
//  EZER
//
//  Created by TimerackMac1 on 18/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class CompleteOrderController: UIViewController,UITextFieldDelegate{
    
    
    @IBOutlet weak var txtTip: UITextField!
    @IBOutlet weak var floatRatingView: FloatRatingView!
    
    @IBOutlet weak var btnFirstOption: RoundableButton!
    
    @IBOutlet weak var btnSecondOption: RoundableButton!
    
    @IBOutlet weak var btnThirdOption: RoundableButton!
    
    @IBOutlet weak var btnFourthOption: RoundableButton!
    
    var orderTrackModel :  OrderTrackModel!
    var  tip : Double = 0.00
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Please Review the Driver"
        floatRatingView.delegate = self
        //floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        //floatRatingView.type = .halfRatings
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        let backBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "ic_back"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonClicked))
        backBarButtonItem.imageInsets = UIEdgeInsets(top: 0, left: -10.0, bottom: 0, right: 10.0)
        self.navigationItem.setLeftBarButton(backBarButtonItem, animated: true)
        txtTip.isEnabled = false
    }
    @objc func backButtonClicked(){
        self.showAlertController(message: ValidationMessages.ratingRequired)
    }
    
    @IBAction func btnFirstAction(_ sender: Any) {
  
        txtTip.isEnabled = false
        tip = 5.00
        setValToTextField()
        print(tip)
    }
    
    @IBAction func btnSecondAction(_ sender: Any) {
      
        txtTip.isEnabled = false
        tip = 10.00
        setValToTextField()
    }
    
    @IBAction func btnThirdAction(_ sender: Any) {
     
        txtTip.isEnabled = false
        tip = 15.00
        setValToTextField()
    }
    
    @IBAction func btnFourthAction(_ sender: Any) {
       
        txtTip.isEnabled = false
        tip = 20.00
        setValToTextField()
    }
    
    @IBAction func btnDiffrentAmount(_ sender: Any) {
        txtTip.text = ""
        tip = 0.00
        txtTip.isEnabled = true
        txtTip.becomeFirstResponder()
    }
    
    func setValToTextField() {

        txtTip.text =  String(format: "%.2f", tip)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        guard let text = textField.text, let decimalSeparator = NSLocale.current.decimalSeparator else {
//                   return true
//               }
//
//                var splitText = text.components(separatedBy: decimalSeparator)
//                let totalDecimalSeparators = splitText.count - 1
//                let isEditingEnd = (text.count - 3) < range.lowerBound
//                splitText.removeFirst()
//
//                // Check if we will exceed 2 dp
//                if
//                    splitText.last?.count ?? 0 > 1 && string.count != 0 &&
//                    isEditingEnd
//                {
//                    return false
//                }
//
//                // If there is already a dot we don't want to allow further dots
//                if totalDecimalSeparators > 0 && string == decimalSeparator {
//                    return false
//                }
//
//                // Only allow numbers and decimal separator
//                switch(string) {
//                case "", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", decimalSeparator:
//                    return true
//                default:
//                    return false
//                }
        // CUSTOM SETUP
           let c = NSLocale.current.decimalSeparator ?? "."
           let limitBeforeSeparator = 3
           let limitAfterSeparator = 2
           // ---------
           
           
           var validatorUserInput:Bool = false
           
           let text = (textField.text ?? "") as NSString
           let newText = text.replacingCharacters(in: range, with: string)
           
           
           // Validator
           let pattern = "(?!0[0-9])\\d*(?!\\\(c))^[0-9]{0,\(limitBeforeSeparator)}((\\\(c))[0-9]{0,\(limitAfterSeparator)})?$"
           if let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive) {
               validatorUserInput = regex.numberOfMatches(in: newText, options: .reportProgress, range: NSRange(location: 0, length: (newText as NSString).length)) > 0
           }
            
           
           if validatorUserInput {
               // setting data or something eles before the return
               if let char = string.cString(using: String.Encoding.utf8) {
                   let isBackSpace = strcmp(char, "\\b")
                   if (isBackSpace == -92 && textField.text?.count == 1) {
                       print("Backspace was pressed")
                       print(newText)
                       // do something...
                       
                   } else {
                       print("Number Added")
                       print(newText)
                       // do something...
                       
                   }
               }
         
            return validatorUserInput
           } else {
               return validatorUserInput
           }
        }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let tipText = Double(textField.text!) {
           tip = tipText
           
        }
        setValToTextField()
    }
    
  
    @IBAction func reportIssue(_ sender: RoundableButton) {
        HelperFunction.openMailApp(email: ServerUrls.customerSupportEmail)
    }
    @IBAction func submitRatingAndTip(_ sender: RoundableButton) {
        if self.floatRatingView.rating > 0{
            addTipAndRatingToDriver()
        }
        else{
            self.view.makeToast(ValidationMessages.ratingRequired)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    private func addTipAndRatingToDriver()
    {
        let cancelParams :[String: Any] = [
            "orderId":orderTrackModel.orderId,
            "tipAmount":tip,
            //"customerId": HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.Id),
            "driverId": orderTrackModel.driverId,
            "rating": Int(self.floatRatingView.rating)
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.addUserTipToDriver, jsonToPost: cancelParams) { (json) in
            if let json = json
            {
                let orderFinishDialog = OrderFinishController(nibName: "OrderFinishController", bundle: nil)
                orderFinishDialog.actionConfirmed = {
                    action in
//                    if json["additionalCharges"].boolValue == true{
                        self.navigationController?.popViewController(animated: true)
//                    }else{
//                         self.navigationController?.popToRootViewController(animated: true)
//                    }
                }
                orderFinishDialog.modalPresentationStyle = .overCurrentContext
                self.present(orderFinishDialog, animated: false)
            }
        }
    }
}
extension CompleteOrderController: FloatRatingViewDelegate {
    
    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        //print(String(format: "%.2f", self.floatRatingView.rating))
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        //print(String(format: "%.2f", self.floatRatingView.rating))
    }
    
}
