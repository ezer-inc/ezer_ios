//
//  RescheduleOrderController.swift
//  EZER
//
//  Created by Naveen jangra on 08/05/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit

class RescheduleOrderController: UIViewController {
    
    @IBOutlet weak var promoCodeHeight: NSLayoutConstraint!
    @IBOutlet weak var addressViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var orderNumber: UILabel!
    var orderModel : CurrentOrderModel!
    var orderTrackModel :  OrderTrackModel!
    let cellName = "ImageUploadingCell"
    let addressInfo = "RescheduleOrderCell"
    @IBOutlet weak var addressTableView: UITableView!
    @IBOutlet weak var estimatedPrice: UILabel!
    @IBOutlet weak var orderDetailLabel: UILabel!
    @IBOutlet weak var imagesCollection: UICollectionView!
    @IBOutlet weak var scheduledTime: UILabel!
    @IBOutlet weak var schduledDate: UILabel!
    @IBOutlet weak var parentDatePickerView: UIView!
    @IBOutlet weak var pickerViewBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var dateInfoView: UIView!
    var selectedDate = Date()
    
    @IBOutlet weak var couponCode: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let calendar = Calendar.current
        selectedDate = calendar.date(byAdding: .minute, value: 15, to: selectedDate)!
        self.orderNumber.text = "Order Number #\(orderModel.orderNumber)"
        datePicker.setValue(UIColor.customerTheme, forKey: "textColor")
        self.title = "Reschedule Order"
        var fifteenDaysfromNow: Date {
            return (Calendar.current as NSCalendar).date(byAdding: .day, value: 15, to: Date(), options: [])!
        }
        datePicker.maximumDate = fifteenDaysfromNow
        imagesCollection.register(UINib(nibName:cellName ,bundle: nil), forCellWithReuseIdentifier: cellName)
        imagesCollection.dataSource = self
        // Do any additional setup after loading the view.
        addressTableView.dataSource = self
        addressTableView.register(UINib(nibName: addressInfo, bundle: nil), forCellReuseIdentifier: addressInfo)
        addressTableView.isScrollEnabled = false
        self.addressTableView.rowHeight = UITableView.automaticDimension
        addressTableView.estimatedRowHeight = 44
        self.pickerViewBottomMargin.constant = 216
        self.parentDatePickerView.isHidden = true
        setDate()
        getOrderDetails()
        dateInfoView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(handleDateTimeView)))
        promoCodeHeight.constant = 0
    }
    deinit {
        
    }
    override func viewWillAppear(_ animated: Bool) {
         addressTableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    @objc private func handleDateTimeView()
    {
        datePicker.setDate(Date(), animated: false)
        showHidePickerView(false)
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let obj = object as? UITableView {
            if obj == self.addressTableView && keyPath == "contentSize" {
                if let newSize = change?[NSKeyValueChangeKey.newKey] as? CGSize {
                    //do stuff here
                    addressViewHeight.constant = newSize.height//addressTableView.contentSize.height
                }
            }
        }
    }
    @IBAction func cancelDatePicker(_ sender: UIButton) {
        switch sender.tag {
        case 1: //cancel clicked
            showHidePickerView(true)
        default : // 2
            
            // print(datePicker.date, "Hello " , Date())
            if datePicker.date > Date(){
                showHidePickerView(true)
                selectedDate = datePicker.date
                //let dateTime = HelperFunction.convertToUtc(date :datePicker.date)
                 setDate()
            }
            else{
                self.showMessage(ValidationMessages.dateException, type: .error,options: [.textNumberOfLines(0)])
            }
        }
        
    }
    func showHidePickerView(_ isHide : Bool)
    {
        
        if isHide{
            UIView.animate(withDuration :0.5, delay: 0, usingSpringWithDamping :1, initialSpringVelocity :0.9, options: .curveEaseIn, animations:{
                self.pickerViewBottomMargin.constant = 216
                self.parentDatePickerView.isHidden = true
                self.view.layoutIfNeeded()
            }, completion:  nil)
        }else{
            UIView.animate(withDuration :0.5, delay: 0, usingSpringWithDamping :0.5, initialSpringVelocity :0.9, options: .curveEaseIn, animations:{
                self.pickerViewBottomMargin.constant = 0
                self.parentDatePickerView.isHidden = false
                self.view.layoutIfNeeded()
            }, completion:  nil)
            
        }
    }
    private func setDate()
    {
        let dateModel = HelperFunction.getDateComponent(date: selectedDate)
        var attributedString = NSMutableAttributedString()
        
        let monthAndYear = HelperFunction.createAttributedString("\(dateModel.monthAndYear!)\n", textSize: 14, textColor: UIColor.customerTheme)
        attributedString.append(monthAndYear)
        let dayOfMonth = HelperFunction.createAttributedString(dateModel.dayOfMonth, textSize: 35, textColor: UIColor.customerTheme)
        attributedString.append(dayOfMonth)
        let dayName = HelperFunction.createAttributedString("\n\(dateModel.dayName!)", textSize: 14, textColor: UIColor.customerTheme)
        attributedString.append(dayName)
        schduledDate.attributedText = attributedString
        
        attributedString = NSMutableAttributedString()
        let amOrPm = HelperFunction.createAttributedString("\(dateModel.amOrPM!)\n", textSize: 14, textColor: UIColor.customerTheme)
        attributedString.append(amOrPm)
        let time = HelperFunction.createAttributedString(dateModel.time, textSize: 35, textColor: UIColor.customerTheme)
        attributedString.append(time)
        scheduledTime.attributedText=attributedString
        
    }
    override func viewWillDisappear(_ animated: Bool) {
            self.addressTableView.removeObserver(self, forKeyPath: "contentSize")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func getOrderDetails(showLoader : Bool = true)
    {
        let orderParams :[String:Any] = [
            "orderId":orderModel.orderId,
            ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.orderDetails, jsonToPost: orderParams,showLoader:showLoader ) { (json) in
            if let json = json
            {
                self.orderTrackModel =  OrderTrackModel(json:json[ServerKeys.data])
                self.imagesCollection.reloadData()
                self.setOrderDescription()
                self.setEstimatedPrice()
                self.addressTableView.reloadData()
                self.addressTableView.layoutIfNeeded()
                self.addressTableView.layoutSubviews()
                self.viewDidLayoutSubviews()
            }
        }
    }
    
    private func setEstimatedPrice()
    {
        if orderTrackModel.couponId.isEmpty{
            self.estimatedPrice.text = String(format: "$%.2f - $%.2f",orderTrackModel.estimatedMinPrice!,orderTrackModel.estimatedMaxPrice)
        }else{
            let couponMin = orderTrackModel.estimatedDiscountedMinPrice
            let couponMax = orderTrackModel.estimatedDiscountedMaxPrice
            self.estimatedPrice.text = String(format: "$%.2f - $%.2f", couponMin!,couponMax!)
            self.promoCodeHeight.constant = 50
            self.couponCode.text = orderTrackModel.couponCode
        }
    }
  
    @IBAction func reScheduleOrderAction(_ sender: RoundableButton) {
        if selectedDate < Date()
        {
            self.showMessage(ValidationMessages.dateException, type: .error,options: [.textNumberOfLines(0)])
            return
        }
        updateOrderDate()
    }
    private func setOrderDescription()
    {
        var stringToShow = ""
        for item in orderTrackModel.shippingItems
        {
            stringToShow += item.description
        }
        //stringToShow += "\nNote: \(orderTrackModel.userNotes!)"
        stringToShow += "\n\(orderTrackModel.contentDescription!)"
        for startLocation in orderTrackModel.extraStartLocations
        {
            if startLocation.shippingItems.count > 0
            {
                let shipItem = startLocation.shippingItems[0]
                stringToShow += "\n\(shipItem.description)"
                //if !shipItem.userNotes.isEmpty
                //{
                   // stringToShow += "\nNote: \(shipItem.userNotes!)"
                //}
            }
            stringToShow += "\n\(startLocation.instruction!)"
        }
        //orderDetailLabel.text=stringToShow
        orderDetailLabel.text = String.init(format: "%@\nNote:-%@", stringToShow,orderTrackModel.userNotes)
    }
    private func updateOrderDate()
    {
       
        let dateParams = [ "orderId":orderModel.orderId,
                           "customerId": HelperFunction.getUserId(),
                           "dateTime":HelperFunction.convertToUtc(date :selectedDate)
            ] as [String:Any]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.updateOrderImmediateToScheduled, jsonToPost: dateParams) { (json) in
            if let json = json
            {
                NotificationCenter.default.post(name: .customerOrderRescheduled, object:nil,userInfo: ["orderId":self.orderModel.orderId])
                let action = UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })
                self.showAlertController(message: ValidationMessages.orderRescheduled,action:action)
            }
        }
    }
}
extension RescheduleOrderController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return orderTrackModel?.shippingItems[0].images.count ?? 0
        return orderTrackModel?.allImages.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellName, for: indexPath) as! ImageUploadingCell
        cell.deleteImage.isHidden = true
        cell.setUpImage(stringUrl:orderTrackModel!.allImages[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presentZoomViewController(orderTrackModel!.allImages[indexPath.row])
    }
}
extension RescheduleOrderController :  UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orderTrackModel != nil{
            let count = max(orderTrackModel!.extraStartLocations.count,orderTrackModel!.extraDestinationLocations.count)
            return count+1
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: addressInfo) as! RescheduleOrderCell
            cell.setUpCell(indexPath, trackOrder: orderTrackModel)
        return cell
    }
 
}

extension RescheduleOrderController: zoomImageViewControllerDelegate {
    func presentZoomViewController(_ url: String) {
        let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
        let driverInfoController = storyboard.instantiateViewController(withIdentifier:IdentifierName.Driver.zoomImageViewController) as! ZoomImageViewController
        driverInfoController.urlString = url
        driverInfoController.delegate = self
        self.navigationController?.present(driverInfoController, animated: false, completion: nil)
    }
    
    func dismissPresentViewController() {
        self.navigationController?.dismiss(animated: false, completion: nil)
    }
}
