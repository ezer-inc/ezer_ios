//
//  TakePicture602.swift
//  EZER
//
//  Created by TimerackMac1 on 17/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import AlamofireImage
import SwiftyJSON
import MobileCoreServices
protocol TakePictureViewDelegate:class {
    func handlebackActionFromTakeAPicktureView()
}
class TakePictureView: UIViewController ,UINavigationControllerDelegate{
    
    @IBOutlet weak var assestViewHeight: NSLayoutConstraint!
    @IBOutlet weak var doneButton: RoundableButton!
    @IBOutlet weak var ic_addMore: UIImageView!
    @IBOutlet weak var weightField: TJTextField!
    @IBOutlet weak var descriptionTextView: KMPlaceholderTextView!
    let cellName = "ImageUploadingCell"
    @IBOutlet weak var imagesCollection: UICollectionView!
    @IBOutlet weak var assetView: UIView!
    @IBOutlet weak var takePicture: UIButton!
    
    @IBOutlet weak var progressBar: UIProgressView!
    
    var createOrderModel : CreateOrderModel!
    var imagePicker = UIImagePickerController()
    weak var delegate:TakePictureViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "New Order"
//        takePicture.layer.cornerRadius = takePicture.bounds.height/2
//        takePicture.layer.borderWidth = 1
//        takePicture.layer.borderColor = UIColor.customerTheme.cgColor
        assetView.isHidden = true
        //assetView.isHidden = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        imagesCollection.register(UINib(nibName:cellName ,bundle: nil), forCellWithReuseIdentifier: cellName)
        imagesCollection.dataSource = self
        imagesCollection.delegate = self
        imagePicker.delegate = self
        self.assestViewHeight.constant = 100
        descriptionTextView.layer.borderWidth = 1
        descriptionTextView.layer.borderColor = UIColor.clear.cgColor
        weightField.addTarget(self, action: #selector(edtingChanged(_:)), for: .editingChanged)
        descriptionTextView.delegate = self
        // Do any additional setup after loading the view.
          NotificationCenter.default.addObserver(self, selector: #selector(checkImmediateOrder), name: UIApplication.willEnterForegroundNotification, object: nil)
     
        let backBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "ic_back"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonAction))
        backBarButtonItem.imageInsets = UIEdgeInsets(top: 0, left: -10.0, bottom: 0, right: 10.0)
        self.navigationItem.setLeftBarButton(backBarButtonItem, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkImmediateOrder()
    }
   
    @objc func backButtonAction()  {
        self.navigationController?.popViewController(animated: true)
        delegate?.handlebackActionFromTakeAPicktureView()
    }
    
    @objc func edtingChanged(_ textField: TJTextField) {
        textField.errorEntry = false
    }
    private func removeObserver()
    {
        NotificationCenter.default.removeObserver(self)
    }
    deinit {
        removeObserver()
    }
    
    @objc func checkImmediateOrder() {
        BaseServices.sendGetJson(serverUrl: ServerUrls.Customer.checkImmediateOrder) { (json) in
            if (json != nil){
                let json = JSON(json!)
                if let status = json[ServerKeys.status].int {
                    HelperFunction.saveValueInUserDefaults(key: UserDefaultsKeys.immediateOrderEnable, value: status)
                    if status == 0 && CurrentCustomerOrder.orderScheduleType == OrderScheduleTypes.now {
                        let orderType = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable)
                        CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: orderType == 0 ? 1 : 0)!
                        let alert = UIAlertController(title: nil, message: json[ServerKeys.message].string ?? "", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.navigationController?.popToRootViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func takePictureAction(_ sender: UIButton) {
        if (createOrderModel.shippingItem.imagesArray.count >= IdentifierName.maximumImageUploads ) // check size of image array
        {
            self.showMessage(ValidationMessages.maxImageUploadError, type: .error,options: [.textNumberOfLines(0)])
            return
        }
        let optionAlert = UIAlertController(title: "Select Mode", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            //self.assetView.isHidden=false
            self.checkCameraPermission(completion: { (action) in
                if action {
                    if UIImagePickerController.isSourceTypeAvailable(.camera){
                        
                        self.imagePicker.allowsEditing = false
                        self.imagePicker.sourceType = .camera
                        self.imagePicker.cameraCaptureMode = .photo
                        self.imagePicker.modalPresentationStyle = .fullScreen
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                }
                else{
                    self.showCameraAlert()
                }
            })
            
        }
        optionAlert.addAction(cameraAction)
        let galleryAction = UIAlertAction(title: "Image Gallery", style: .default) { (action) in
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
           // self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.imagePicker.modalPresentationStyle = .popover
            self.imagePicker.popoverPresentationController?.sourceView = sender
            self.present(self.imagePicker, animated: true, completion: nil)
            //self.assetView.isHidden=false
        }
        optionAlert.addAction(galleryAction)
        let pdfAction = UIAlertAction(title: "Select pdf", style: .default) { (action) in
            let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypePDF as String], in: .import)
            documentPicker.delegate = self
            self.present(documentPicker, animated: true, completion: nil)
        }
        optionAlert.addAction(pdfAction)
        
        optionAlert.addAction(UIAlertAction(title: "Close", style: .cancel))
        optionAlert.popoverPresentationController?.sourceView = sender
        optionAlert.popoverPresentationController?.sourceRect = sender.bounds

        self.present(optionAlert, animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    private func checkValidation()-> Bool
    {
        var isValidationFails = false
        
        
        if descriptionTextView.text.trimWhiteSpace().isEmpty
        {
            self.showMessage(ValidationMessages.instructionEmpty, type: .error)
            descriptionTextView.errorEntry = true
            isValidationFails = true
            return isValidationFails
        }
        /* if !weightField.hasText
         {
         weightField.errorEntry = true
         isValidationFails = true
         }*/
        
        
        
        guard let weight = Int(weightField.text!)  else {
            self.showMessage(ValidationMessages.validWeight, type: .error)
            weightField.errorEntry = true
            isValidationFails = true
            return isValidationFails
        }
        
        if weight < 1
        {
            self.showMessage(ValidationMessages.validWeight, type: .error)
            isValidationFails = true
        }
        
        if createOrderModel.shippingItem.imagesArray.count == 0{
            self.showMessage(ValidationMessages.validImageUpload, type: .error)
            isValidationFails = true
        }
        
        
        return isValidationFails
    }
    
    @IBAction func placeOrderAction(_ sender: UIButton) {
        if checkValidation()
        {
            return
        }
        
        if Int(weightField.text!) ?? 0 > HelperFunction.getWeightRange(){
            showAlertController(message:ValidationMessages.weightRangeLimit)
            return
        }
        
        createOrderModel.shippingItem.weight = Int(weightField.text!)
        createOrderModel.contentDescription = self.descriptionTextView.text
        getEstimatedCost()
    }
    @IBAction func cancelAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        imagesCollection.reloadData()
        handleImageUpload()
    }
    private func uploadImageToServer(image : UIImage)
    {
        self.showProgressBar()
        BaseServices.uploadImageToServer(image: image, imageType: ImageType.order.rawValue, role: Roles.client.rawValue) { (json) in
            self.hideProgressBar()
            if let json = json
            {
                if let url = json[ServerKeys.data].string
                {
                    self.createOrderModel.shippingItem.imagesArray.append(url)
                    self.imagesCollection.reloadData()
                    self.handleImageUpload()
                }
            }else{
                self.showMessage(ValidationMessages.tryAgain, type: .error,options: [.textNumberOfLines(0)])
            }
        }
    }
    
    private func uploadFileToServer(data : Data)
    {
        self.showProgressBar()
        BaseServices.uploadFileToServer(image: data, fileType: ImageType.order.rawValue,  role: Roles.client.rawValue) { (json) in
            self.hideProgressBar()
            if let json = json
            {
                if let url = json[ServerKeys.data].string
                {
                    self.createOrderModel.shippingItem.imagesArray.append(url)
                    self.imagesCollection.reloadData()
                    self.handleImageUpload()
                }
            }else{
                self.showMessage(ValidationMessages.tryAgain, type: .error,options: [.textNumberOfLines(0)])
            }
        }
    }
    
    private func handleImageUpload()
    {
        if self.createOrderModel.shippingItem.imagesArray.isEmpty
        {
//            self.ic_addMore.isHidden = true
            self.assetView.isHidden = true
            self.assestViewHeight.constant = 100
        }else{
//            self.ic_addMore.isHidden = false
            self.assetView.isHidden = false
            self.assestViewHeight.constant = 160
            self.progressBar.progress = Float(Float(self.createOrderModel.shippingItem.imagesArray.count)/5.0)
        }
    }
    
    private func getEstimatedCost() // get estimation cost
    {
        var paramsToPost = [String:Any]()
        paramsToPost["startLocation"] = [
            "lat":self.createOrderModel.startCoordinate.latitude,
            "lng":self.createOrderModel.startCoordinate.longitude
        ]
        paramsToPost["truckType"] = self.createOrderModel.truckType
        paramsToPost["totalDistance"] = self.createOrderModel.totalDistance
        paramsToPost["totalTime"] = self.createOrderModel.estimatedTime
        paramsToPost["shippingItem"] = [
            "weight":self.createOrderModel.shippingItem.weight!,
            "length":self.createOrderModel.shippingItem.length!,
            "width":self.createOrderModel.shippingItem.width!,
            "height":self.createOrderModel.shippingItem.height!,
        ]
        paramsToPost["driverID"] = ""
        paramsToPost["destinationLocation"] = [
            "lat":self.createOrderModel.destinationCoordinate.latitude,
            "lng":self.createOrderModel.destinationCoordinate.longitude
        ]
        paramsToPost["extraStartLocations"] = []
        paramsToPost["extraDestinationLocations"] = []
        //print(paramsToPost)
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.getCost, jsonToPost: paramsToPost) { (json) in
            if let json = json
            {
//                let orderPlaceOrSchedule = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.orderPlaceORSchedule) as! PlaceOrderORSchedule
//                self.createOrderModel.estimatedMaxPrice = json[ServerKeys.data]["mxPrice"].double ?? 0.0
//                self.createOrderModel.estimatedMinPrice = json[ServerKeys.data]["miPrice"].double ?? 0.0
//                orderPlaceOrSchedule.createOrderModel = self.createOrderModel
//                self.navigationController?.pushViewController(orderPlaceOrSchedule, animated: true)
                
                let laterScheduleOrder = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.laterScheduleOrder) as! LaterScheduleOrderCon
                self.createOrderModel.estimatedMaxPrice = json[ServerKeys.data]["mxPrice"].double ?? 0.0
                self.createOrderModel.estimatedMinPrice = json[ServerKeys.data]["miPrice"].double ?? 0.0
                
                laterScheduleOrder.createOrderModel = self.createOrderModel
                self.navigationController?.pushViewController(laterScheduleOrder, animated: true)
            }
        }
    }
}

extension TakePictureView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return createOrderModel.shippingItem.imagesArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellName, for: indexPath) as! ImageUploadingCell
        cell.deleteImage.tag = indexPath.row
        cell.delegate = self
        cell.setUpImage(stringUrl: createOrderModel.shippingItem.imagesArray[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presentZoomViewController(createOrderModel.shippingItem.imagesArray[indexPath.row])
    }
}

extension TakePictureView: DeleteImageDelegate
{
    func deleteImageAtIndex(tag: Int) {
        let filename = HelperFunction.getFileNameFromUrl(stringUrl: createOrderModel.shippingItem.imagesArray[tag])
        //print(filename)
        let params :[String:Any] = [
            "role":Roles.client.rawValue,
            "image": filename,
            "imageType":ImageType.order.rawValue
            
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Main.deleteImageFromS3, jsonToPost: params) { (json) in
            if let _ = json
            {
                self.createOrderModel.shippingItem.imagesArray.remove(at: tag)
                self.imagesCollection.reloadData()
                self.handleImageUpload()
            }
        }
    }
}
extension TakePictureView : UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        picker.dismiss(animated: false, completion: nil)
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
        {
            let uploadedImage = image.resizeImage(newWidth: 500)
            uploadImageToServer(image: uploadedImage)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        self.showMessage("Picture selection cancelled", type: .error,options: [.textNumberOfLines(0)])
    }
}

extension TakePictureView: UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        descriptionTextView.errorEntry = false
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars <= IdentifierName.descriptionLength
    }
    
}

//MARK:- UIDocumentPickerDelegate
extension TakePictureView: UIDocumentPickerDelegate{
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
            DispatchQueue.global(qos: .background).async {
                print(url)
                DispatchQueue.global(qos: .background).async {
                    do
                    {
                        let data = try Data.init(contentsOf:url)
                        DispatchQueue.main.async {
                            self.uploadFileToServer(data: data)
                        }
                    }
                    catch {
                        // error
                    }
            }
        }
    }
}

extension TakePictureView: zoomImageViewControllerDelegate {
    func presentZoomViewController(_ url: String) {
        let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
        let driverInfoController = storyboard.instantiateViewController(withIdentifier:IdentifierName.Driver.zoomImageViewController) as! ZoomImageViewController
        driverInfoController.urlString = url
        driverInfoController.delegate = self
        self.navigationController?.present(driverInfoController, animated: false, completion: nil)
    }
    
    func dismissPresentViewController() {
        self.navigationController?.dismiss(animated: false, completion: nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
