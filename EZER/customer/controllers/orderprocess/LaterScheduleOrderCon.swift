//
//  LaterScheduleOrderCon.swift
//  EZER
//
//  Created by TimerackMac1 on 18/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol LaterScheduleOrderConDelegate:class {
    func handlebackActionFromReviewCaontroller()
}
class LaterScheduleOrderCon: UIViewController {
    
   
    //OUTLETS
    
    var customerOrderModel:CustomerOrderModel?
    @IBOutlet weak var dropOffPoint: UILabel!
    @IBOutlet weak var pickupPoint: UILabel!
    @IBOutlet weak var estimatedPrice: UILabel!
    @IBOutlet weak var scheduledTime: UILabel!
    @IBOutlet weak var schduledDate: UILabel!
    @IBOutlet weak var promoCodeField: UITextField!
    @IBOutlet weak var additionalNotes: KMPlaceholderTextView!
    var createOrderModel : CreateOrderModel!
   
    @IBOutlet weak var lblPromoCode: UILabel!
    @IBOutlet weak var viewCouponApplied: UIView!
   
    @IBOutlet weak var orderIDLabel: UILabel!
    @IBOutlet weak var applyCouponButton: RoundableButton!
    @IBOutlet weak var removeCouponButton: UIButton!
    @IBOutlet weak var couponViewHeight: NSLayoutConstraint!
    @IBOutlet weak var couponView: UIView!
    @IBOutlet weak var confirmAndSubmitOrderButton: RoundableButton!
  
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var cancelOrderButton: UIButton!
    
    @IBOutlet weak var dateInfoView: UIView!
    let cellName = "ImageUploadingCell"
    @IBOutlet weak var estimateNoteLabel: UILabel!
    
    @IBOutlet weak var estimatedPriceTitle: UILabel!
    var dateModel : DateModel!
    @IBOutlet weak var parentDatePickerView: UIView!
    @IBOutlet weak var pickerViewBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var constraintHeightOfAdditionNoteVw: NSLayoutConstraint!
    weak var delegate:LaterScheduleOrderConDelegate?
     let alertCon = AlertDialogController(nibName: "AlertDialogController",bundle : nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.setValue(UIColor.customerTheme, forKey: "textColor")
//        imagesArray = self.createOrderModel.shippingItem.imagesArray
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        self.title = "Order Review"
        var fifteenDaysfromNow: Date {
            return (Calendar.current as NSCalendar).date(byAdding: .day, value: 15, to: Date(), options: [])!
        }
        datePicker.maximumDate = fifteenDaysfromNow
//        imagesCollection.register(UINib(nibName:cellName ,bundle: nil), forCellWithReuseIdentifier: cellName)
//        imagesCollection.dataSource = self
        pickupPoint.text = String(format: "%@\n%@",createOrderModel.startAddress,createOrderModel.startAddressUnitNumber ?? "")
        
        dropOffPoint.text = String(format: "%@\n%@",createOrderModel.destinationAddress,createOrderModel.destinationAddressUnitNumber ?? "")
        additionalNotes.text = createOrderModel.userNotes
//        lblInstruction.text = createOrderModel.contentDescription
        setEstimatedPrice()
        setDate()
        additionalNotes.delegate = self
        //set attributed text 
        let attributedString = NSMutableAttributedString()
        let questionEmail = HelperFunction.createAttributedString(ServerUrls.questionEmail, textSize: 13, textColor: UIColor.customerTheme)
        attributedString.append( NSMutableAttributedString(string: ValidationMessages.EstimatedCostNote))
        attributedString.append(questionEmail)
        
        estimateNoteLabel.attributedText = attributedString
        //cehck if
        if createOrderModel.immediate == false{
            dateInfoView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(handleDateTimeView)))
        }
        // Do any additional setup after loading the view.
        self.pickerViewBottomMargin.constant = 216
        self.parentDatePickerView.isHidden = true
        
        let backBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "ic_back"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonAction))
        backBarButtonItem.imageInsets = UIEdgeInsets(top: 0, left: -10.0, bottom: 0, right: 10.0)
        self.navigationItem.setLeftBarButton(backBarButtonItem, animated: true)
        
        cancelOrderButton.layer.borderWidth = 1
        cancelOrderButton.layer.borderColor = CustomerThemeColor.cgColor
        promoCodeField.setPlaceholder()
        addObserver()
        checkImmediateOrder()
        checkNearByDriver()
    }
   
    @objc func backButtonAction()  {
        self.navigationController?.popViewController(animated: true)
        delegate?.handlebackActionFromReviewCaontroller()
    }
    @objc private func handleDateTimeView()
    {
        datePicker.setDate(Date().adding(minutes: CurrentCustomerOrder.scheduleHours*60), animated: false)
        showHidePickerView(false)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    private func addObserver() {
         NotificationCenter.default.addObserver(self, selector:#selector(self.handleOrderStatusChange(notification:)), name: .orderStatusChange, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(checkImmediateOrder), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkNearByDriver), name: Notification.Name.notifyWhenDriverAvailable, object: nil)

    }
    
    @objc func checkImmediateOrder() {
        BaseServices.sendGetJson(serverUrl: ServerUrls.Customer.checkImmediateOrder) { (json) in
            if (json != nil){
                let json = JSON(json!)
                if let status = json[ServerKeys.status].int {
                    HelperFunction.saveValueInUserDefaults(key: UserDefaultsKeys.immediateOrderEnable, value: status)
                    if status == 0 && CurrentCustomerOrder.orderScheduleType == OrderScheduleTypes.now {
                        let orderType = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable)
                        CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: orderType == 0 ? 1 : 0)!
                        let alert = UIAlertController(title: nil, message: json[ServerKeys.message].string ?? "", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.navigationController?.popToRootViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    @objc func handleOrderStatusChange(notification: Notification)
    {
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            if let currentOrderId = notificationObject["orderId"] as? String
            {
                if currentOrderId == self.customerOrderModel?.orderID
                {
                  if let customerOrderModel = self.customerOrderModel
                  {
                    customerOrderModel.driverId = notificationObject["driverId"] as? String ?? ""
                    customerOrderModel.driverName = notificationObject["driverName"] as? String ?? ""
                    }
                    guard let status = notificationObject["status"] as? String else{return}
                    guard let statusType = OrderStatusType(rawValue: status) else{ return }
                    if statusType == .done
                    {
                        openForDriverRating()
                    }
                }
            }
        }
    }
    
    func openForDriverRating()
    {
        let completeOrder = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.completeOrder) as! CompleteOrderController
        let orderTrackModel = OrderTrackModel()
        orderTrackModel.driverId = customerOrderModel!.driverId
        orderTrackModel.orderId = customerOrderModel!.orderID
        orderTrackModel.driverName = customerOrderModel!.driverName
        completeOrder.orderTrackModel = orderTrackModel
        self.navigationController?.pushViewController(completeOrder, animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        alertCon.dismiss(animated: false, completion: nil)
    }
    private func setDate()
    {
        //check if immidate order
        if createOrderModel.immediate{
            createOrderModel.dateTime = HelperFunction.convertToUtc(date :Date())
            createOrderModel.orderDate = Date()
        }
        
        dateModel = HelperFunction.getDateComponent(date: createOrderModel.orderDate)
        var attributedString = NSMutableAttributedString()
        
        let dayOfMonthDate = HelperFunction.createAttributedString("\(dateModel.dayName!) \(dateModel.dayOfMonth!)\n", textSize: 32, textColor: UIColor.customerTheme)
        attributedString.append(dayOfMonthDate)
        
        let monthAndYear = HelperFunction.createAttributedString(dateModel.monthAndYear, textSize: 14, textColor: UIColor.customerTheme)
        attributedString.append(monthAndYear)
//        let dayOfMonth = HelperFunction.createAttributedString(dateModel.dayOfMonth, textSize: 35, textColor: UIColor.customerTheme)
//        attributedString.append(dayOfMonth)
//        let dayName = HelperFunction.createAttributedString("\n\(dateModel.dayName!)", textSize: 14, textColor: UIColor.customerTheme)
//        attributedString.append(dayName)
        schduledDate.attributedText = attributedString
        
        attributedString = NSMutableAttributedString()
        let time = HelperFunction.createAttributedString("\(dateModel.time!)\n", textSize: 32, textColor: UIColor.customerTheme)
        attributedString.append(time)
        let amOrPm = HelperFunction.createAttributedString(dateModel.amOrPM, textSize: 14, textColor: UIColor.customerTheme)
        attributedString.append(amOrPm)
        scheduledTime.attributedText=attributedString
    }
    
    @IBAction func cancelDatePicker(_ sender: UIButton) {
        switch sender.tag {
        case 1: //cancel clicked
            showHidePickerView(true)
        default : // 2
            
            // print(datePicker.date, "Hello " , Date())
            if datePicker.date > Date(){
//                if datePicker.date > Date().adding(minutes: (CurrentCustomerOrder.scheduleHours*60)-1){
                showHidePickerView(true)
                createOrderModel.dateTime = HelperFunction.convertToUtc(date :datePicker.date)
                createOrderModel.orderDate = datePicker.date
                if let _ = customerOrderModel{
                    updateOrderDate()
                }
                else{ setDate() }
            }
            else{
//                let  dateModel = HelperFunction.getDateComponent(date: Date().adding(minutes: (CurrentCustomerOrder.scheduleHours*60)))
//                let dateTime = "\(dateModel.dayOfMonth ?? "") \(dateModel.monthAndYear ?? "") \(dateModel.time ?? "") \(dateModel.amOrPM ?? "")"
                self.showMessage("\(ValidationMessages.scheduleDateError)", type: .error,options: [.textNumberOfLines(0)])
            }
        }
    }
    
    private func updateOrderDate()
    {
        let dateParams = [ "orderId":customerOrderModel!.orderID,
                            "customerId": HelperFunction.getUserId(),
                            "dateTime":createOrderModel.dateTime
        ] as [String:Any]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.updateScheduleDateTime, jsonToPost: dateParams) { (json) in
            if let json = json
            {
                self.setDate()
                self.showMessage(json["message"].string ?? ValidationMessages.orderPlaced, type: .success,options: [.textNumberOfLines(0)])
            }
        }
    }
    
    func showHidePickerView(_ isHide : Bool)
    {
        
        if isHide{
            UIView.animate(withDuration :0.5, delay: 0, usingSpringWithDamping :1, initialSpringVelocity :0.9, options: .curveEaseIn, animations:{
                self.pickerViewBottomMargin.constant = 216
                self.parentDatePickerView.isHidden = true
                self.view.layoutIfNeeded()
            }, completion:  nil)
        }else{
            UIView.animate(withDuration :0.5, delay: 0, usingSpringWithDamping :0.5, initialSpringVelocity :0.9, options: .curveEaseIn, animations:{
                self.pickerViewBottomMargin.constant = 0
                self.parentDatePickerView.isHidden = false
                self.view.layoutIfNeeded()
            }, completion:  nil)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func addNewOrderToCustomerHome()
    {
        //NotificationCenter.default.post(name: .refreshCustomerData, object: nil)
        let currentOrderModel = CurrentOrderModel()
        currentOrderModel.orderId = customerOrderModel!.orderID
        currentOrderModel.orderNumber = customerOrderModel!.orderNumber
        currentOrderModel.status = .open
        currentOrderModel.startAddress = createOrderModel.startAddress
        currentOrderModel.destinationAddress = createOrderModel.destinationAddress
        currentOrderModel.customerId = HelperFunction.getUserId()
        currentOrderModel.immediate = false
        NotificationCenter.default.post(name: .ownOrderPlaced, object : currentOrderModel)
        NotificationCenter.default.post(name: .resetCustomerHomeData, object : nil)
    }
    private func setEstimatedPrice()
    {
        if createOrderModel.isSpecialRateApplied && createOrderModel.customerspecialRate != 0{
            self.estimatedPrice.text = "Price"
            self.estimatedPrice.text = String(format: "$%.2f", createOrderModel.customerspecialRate ?? 0)
        }else{
            self.estimatedPrice.text = "Estimated Price*"
            if createOrderModel.coupon == nil
            {
                self.estimatedPrice.text = String(format: "$%.2f - $%.2f", createOrderModel.estimatedMinPrice!,createOrderModel.estimatedMaxPrice)
            }
            else{
                let couponMin = createOrderModel.coupon.estimatedDiscountedMinPrice
                let couponMax = createOrderModel.coupon.estimatedDiscountedMaxPrice
                self.estimatedPrice.text = String(format: "$%.2f - $%.2f", couponMin!,couponMax!)
            }
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func scheduleOrderAction(_ sender: RoundableButton) {
        if let customerOrderModel = customerOrderModel{
            
            let trackOrder = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.trackOrder) as! TrackOrderController
            trackOrder.orderId = customerOrderModel.orderID
            trackOrder.orderTitle = "Order \(customerOrderModel.orderNumber)"
            self.navigationController?.pushViewController(trackOrder, animated: true)
        }
        else{
            
            //check if schedule order date is less than the Difference 
            
            if createOrderModel.orderDate < Date() &&  createOrderModel.immediate == false
            {
//                let  dateModel = HelperFunction.getDateComponent(date: Date().adding(minutes: (CurrentCustomerOrder.scheduleHours*60)))
//                let dateTime = "\(dateModel.dayOfMonth ?? "") \(dateModel.monthAndYear ?? "") \(dateModel.time ?? "") \(dateModel.amOrPM ?? "")"
                self.showMessage("\(ValidationMessages.scheduleDateError)", type: .error,options: [.textNumberOfLines(0)])
                return
            }
            
            
            createOrderModel.userNotes = additionalNotes.text!
           
            alertCon.actionConfirmed = {
                action in
                self.placeOrder()
            }
            alertCon.actionCanceled = { (action)-> () in}
            alertCon.placeOrder = true
            alertCon.message = createOrderModel.immediate ?  ValidationMessages.placeOrder : String(format:ValidationMessages.placeOrderLater,dateModel.monthWithDate,dateModel.dayName!,dateModel.time ,dateModel.amOrPM!)
            alertCon.modalPresentationStyle = .overCurrentContext
            self.present(alertCon, animated: false, completion: nil)
        }
    }
    @IBAction func cancelOrderAction(_ sender: UIButton) {
        
        if confirmAndSubmitOrderButton.titleLabel?.text == "TRACK ORDER"{
            let alertCon = CustomerCancelOrderPopUP(nibName: "CustomerCancelOrderPopUP",bundle : nil)

            alertCon.actionConfirmed = { (action: Bool,text:String)-> Void in
                self.cancelOrder(reason: text)
            }
            
            alertCon.actionCanceled = { (action)-> () in
                
            }
            alertCon.message = ValidationMessages.cancelOrder
            alertCon.modalPresentationStyle = .overCurrentContext
            self.present(alertCon, animated: false, completion: nil)
        }else{
            alertCon.actionConfirmed = {
                action in
                if let _ = self.customerOrderModel{
                    self.cancelOrder(reason: "")
                }
                else{
                    NotificationCenter.default.post(name: .resetCustomerHomeData, object : nil)
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
            alertCon.actionCanceled = { (action)-> () in}
            alertCon.message = ValidationMessages.cancelOrder
            alertCon.modalPresentationStyle = .overCurrentContext
            self.present(alertCon, animated: false, completion: nil)
        }
        
       
    }
    private func cancelOrder(reason: String)
    {
        let cancelParams = [
            "orderId":customerOrderModel!.orderID,
            "cancelReason": reason
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.cancelOrder, jsonToPost: cancelParams) { (json) in
            if let json = json
            {
                if json[ServerKeys.status].int == 1{
                self.navigationController?.popToRootViewController(animated: true)
                self.emitToSocket()
                }else{
                    self.showAlertController(message: ValidationMessages.OrderCancelRejected)
                }
            }
        }
    }
    private func emitToSocket() {
        NotificationCenter.default.post(name: .ownOrderCancel, object: ["orderId":self.customerOrderModel!.orderID])
    }
    private func placeOrder()
    {

        let orderParams = HelperFunction.createCustomerOrderParams(createOrderModel :createOrderModel)
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.submitOrder, jsonToPost: orderParams, isResponseRequired: true) { (json) in
            if let json = json
            {
                if (json["hasClientCardExpired"].bool ?? false){
                    let action = UIAlertAction.init(title: "Go To Payment", style: .default, handler: { (action) in
                        if let navController = self.navigationController{
                            let payment = AddNewPayment(nibName: "AddNewPayment", bundle: nil)
                            navController.pushViewController(payment, animated: true)
                        }
                    })
                    self.showAlertController( message: json["message"].string ?? "", action: action, showCancel: true)
                    return
                }
                if json[ServerKeys.status].int  == ServerStatusCode.Failure {
                    self.showMessage(json["message"].string ?? "", type: .error)
                    return
                }
                self.couponView.isHidden = true
                self.couponViewHeight.constant = 0
                self.constraintHeightOfAdditionNoteVw.constant = self.additionalNotes.text.isEmpty ? 0 : 115
                self.customerOrderModel = CustomerOrderModel(json: json)
                self.orderIDLabel.text = String.init(format: "Order Id: #%d",(self.customerOrderModel?.orderNumber)!)
                self.confirmAndSubmitOrderButton.setTitle("TRACK ORDER", for: .normal)
                self.title = "Order Status"
                self.additionalNotes.isEditable = false
                self.addNewOrderToCustomerHome()
                //self.notifyToAllDriver()
                self.addBackButton()
                self.showMessage(ValidationMessages.orderPlaced, type: .success)
                self.scrollToBottom()
                self.disableDateTimeUpdate()
            }
        }
    }
    
    func disableDateTimeUpdate()  {
            dateInfoView.gestureRecognizers?.removeAll()
    }
    
    private func scrollToBottom()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.9, execute: {
            let bottomOffset = CGPoint(x: 0, y: self.scrollView.contentSize.height - self.scrollView.bounds.size.height)
            self.scrollView.setContentOffset(bottomOffset, animated: true)
        })
    }
    private func addBackButton()
    {
        let backBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "ic_back"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonClicked))
        backBarButtonItem.imageInsets = UIEdgeInsets(top: 0, left: -10.0, bottom: 0, right: 10.0)
        self.navigationItem.setLeftBarButton(backBarButtonItem, animated: true)
    }
    @objc func backButtonClicked()
    {
         NotificationCenter.default.post(name: .resetCustomerHomeData, object : nil)
        self.navigationController?.popToRootViewController(animated: true)
    }
   /* private func notifyToAllDriver()
    {
        SocketIOManager.sharedInstance.sendNewOrderNotification(fromCustomerId: HelperFunction.getUserId(), orderID: self.customerOrderModel!.orderID)
    }*/
    @IBAction func applyPromoCode(_ sender: RoundableButton) {
        if !promoCodeField.hasText
        {
            self.showMessage(ValidationMessages.promocodeFieldError, type: .error)
            return
        }
        var promoCodeParams = HelperFunction.createCustomerCouponParams(createOrderModel: createOrderModel)
        promoCodeParams["couponCode"] = promoCodeField.text!
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.applycoupon, jsonToPost: promoCodeParams) { (json) in
            if let json = json
            {
                self.showMessage(ValidationMessages.promocodeSuccess, type: .success)
                let coupon = Coupon(json : json[ServerKeys.data])
                self.createOrderModel.coupon = coupon
                self.setEstimatedPrice()
                self.enableCouponView()
                self.couponViewHeight.constant = 35
            }
        }
    }
    
    private func enableCouponView(disable :Bool = false)
    {
        self.applyCouponButton.isEnabled = disable
        self.promoCodeField.isEnabled = disable
        self.removeCouponButton.isHidden = disable
        if promoCodeField.hasText{
            lblPromoCode.text = promoCodeField.text ?? ""
        }
        self.viewCouponApplied.isHidden = disable
        self.applyCouponButton.isHidden = !disable
        self.promoCodeField.isHidden = !disable
    }
    
    @IBAction func removeCouponAction(_ sender: Any) {
        promoCodeField.text = ""
        createOrderModel.coupon = nil
        enableCouponView(disable: true)
        setEstimatedPrice()
        self.couponViewHeight.constant = 85
    }
    
    @IBAction func sendmail(_ sender: UIButton) {
        HelperFunction.openMailApp(email: ServerUrls.customerEmail)
    }
}

extension LaterScheduleOrderCon: UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars <= IdentifierName.descriptionLength
    }
    
}

//MARK:- API Methoda
extension LaterScheduleOrderCon{
    
    @objc private func checkNearByDriver(notifcation: Notification? = nil)
    {
        /* var showLoader = true
         if let _ = notifcation{
         showLoader = false
         }*/
        var params :[String:Any] = HelperFunction.createNearByDriverTerms(createOrderModel: createOrderModel)
        params["customerId"] = HelperFunction.getUserId()
        BaseServices.SendPostJson(viewController: self ,serverUrl: ServerUrls.Customer.checkNearbyDriver, jsonToPost: params,isResponseRequired: true,showLoader:false) { (json) in
            if let json = json
            {
                if json[ServerKeys.status] == 1 && HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 1
                {
                    
//                    self.segmented.disableItem(at: 0, isEnabled: true)
                    
                }else{
                    if CurrentCustomerOrder.orderScheduleType == OrderScheduleTypes.now
                    {
                        self.hidePresentedAlert {
                            
                            let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                                 self.handleLaterOptionWhenNoDriver()
                            })
                            self.showAlertController(message: ValidationMessages.useLaterOption,action: action)
                           
                        }
                    }
                    if HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 1 {

                        CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: 1)!
                    }
                }
              self.handleNowLaterCase()
            }
        }
    }
    
    
    func handleNowLaterCase()  {
        self.createOrderModel.immediate = CurrentCustomerOrder.orderScheduleType == .now
        if CurrentCustomerOrder.orderScheduleType == .later {
            dateInfoView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(handleDateTimeView)))
        }else{
            dateInfoView.gestureRecognizers?.removeAll()
        }
    }
    
    
    func handleLaterOptionWhenNoDriver()  {
            createOrderModel.dateTime  = HelperFunction.convertToUtc(date :Date().adding(minutes: CurrentCustomerOrder.scheduleHours*60))
            createOrderModel.orderDate = Date().adding(minutes: CurrentCustomerOrder.scheduleHours*60)
            setDate() // set the date and time
            handleDateTimeView()
        
    }
    
}


