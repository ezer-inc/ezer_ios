//
//  NowOrderDetailCon.swift
//  EZER
//
//  Created by TimerackMac1 on 18/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import SwiftyJSON
class NowOrderDetailCon: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var dropOffPoint: UILabel!
    @IBOutlet weak var pickupPoint: UILabel!
    @IBOutlet weak var promoCodeField: UITextField!
    
    @IBOutlet weak var lblPromoCode: UILabel!
    @IBOutlet weak var viewCouponApplied: UIView!
    @IBOutlet weak var additionalNotes: KMPlaceholderTextView!
    @IBOutlet weak var descriptionText: KMPlaceholderTextView!
    @IBOutlet weak var imagesCollection: UICollectionView!
    @IBOutlet weak var dateTimeInfoView: UIView!
    @IBOutlet weak var driverAssignLabel: UILabel!
    @IBOutlet weak var estimatePriceTitle: UILabel!
    
    
    
    var customerOrderModel:CustomerOrderModel?
    var imagesArray : [String]!
    var createOrderModel : CreateOrderModel!
    let cellName = "ImageUploadingCell"
    
    @IBOutlet weak var dateTimeView: UIView!
    @IBOutlet weak var dateTimeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var orderIDLabel: UILabel!
    @IBOutlet weak var applyCouponButton: RoundableButton!
    @IBOutlet weak var removeCouponButton: UIButton!
    @IBOutlet weak var estimatedPrice: UILabel!
    @IBOutlet weak var couponViewHeight: NSLayoutConstraint!
    @IBOutlet weak var couponView: UIView!
    @IBOutlet weak var confirmAndSubmitOrderButton: RoundableButton!
    @IBOutlet weak var cancelOrderButton: UIButton!
    
    @IBOutlet weak var currentdateLabel: UILabel!
    @IBOutlet weak var awayTimeLabel: UILabel!
    
    @IBOutlet weak var estimateNoteLabel: UILabel!
    
    let alertCon = AlertDialogController(nibName: "AlertDialogController",bundle : nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        imagesArray = self.createOrderModel.shippingItem.imagesArray
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        self.title = "Order Status"
        imagesCollection.register(UINib(nibName:cellName ,bundle: nil), forCellWithReuseIdentifier: cellName)
        imagesCollection.dataSource = self
        pickupPoint.text = String(format: "%@\n%@",createOrderModel.startAddress,createOrderModel.startAddressUnitNumber ?? "")
        
        dropOffPoint.text = String(format: "%@\n%@",createOrderModel.destinationAddress,createOrderModel.destinationAddressUnitNumber ?? "")
        
        additionalNotes.text = createOrderModel.userNotes
        descriptionText.text = createOrderModel.contentDescription
        setEstimatedPrice()
        dateTimeViewHeight.constant = 0
        dateTimeView.isHidden = true
        additionalNotes.delegate = self
        
        //set attributed text
        let attributedString = NSMutableAttributedString()
        let questionEmail = HelperFunction.createAttributedString(ServerUrls.questionEmail, textSize: 13, textColor: UIColor.customerTheme)
        attributedString.append( NSMutableAttributedString(string: ValidationMessages.EstimatedCostNote))
        attributedString.append(questionEmail)
        
        estimateNoteLabel.attributedText = attributedString
        
        // Do any additional setup after loading the view.
        addObserver()
        checkImmediateOrder()
    }
    private func setEstimatedPrice()
    {
        if createOrderModel.isSpecialRateApplied && createOrderModel.customerspecialRate != 0{
            estimatePriceTitle.text = "Price"
            self.estimatedPrice.text = String(format: "$%.2f",createOrderModel.customerspecialRate ?? 0)
        }else{
            estimatePriceTitle.text = "Estimated Price*"
            if createOrderModel.coupon == nil
            {
                self.estimatedPrice.text = String(format: "$%.2f - $%.2f", createOrderModel.estimatedMinPrice!,createOrderModel.estimatedMaxPrice)
            }
            else{
                let couponMin = createOrderModel.coupon.estimatedDiscountedMinPrice
                let couponMax = createOrderModel.coupon.estimatedDiscountedMaxPrice
                self.estimatedPrice.text = String(format: "$%.2f - $%.2f", couponMin!,couponMax!)
            }
        }
      
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    private func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleOrderStatusChange(notification:)), name: .orderStatusChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.orderCancelFromDriver(notification:)), name: .orderCancelFromDriver, object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(checkImmediateOrder), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func checkImmediateOrder() {
            BaseServices.sendGetJson(serverUrl: ServerUrls.Customer.checkImmediateOrder) { (json) in
            if (json != nil){
                let json = JSON(json!)
                if let status = json[ServerKeys.status].int {
                    HelperFunction.saveValueInUserDefaults(key: UserDefaultsKeys.immediateOrderEnable, value: status)
                    if status == 0 && CurrentCustomerOrder.orderScheduleType == OrderScheduleTypes.now {
                        let orderType = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable)
                        CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: orderType == 0 ? 1 : 0)!
                        let alert = UIAlertController(title: nil, message: json[ServerKeys.message].string ?? "", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            NotificationCenter.default.post(name: .resetCustomerHomeData, object : nil)
                            self.navigationController?.popToRootViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
            }
        }
    }
    
    @IBAction func trackOrderAction(_ sender: UIButton) {
        if let customerOrderModel = customerOrderModel{
        let trackOrder = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.trackOrder) as! TrackOrderController
            trackOrder.orderId = customerOrderModel.orderID
            trackOrder.orderTitle = "Order \(customerOrderModel.orderNumber)"
        self.navigationController?.pushViewController(trackOrder, animated: true)
        }
        else{
            createOrderModel.userNotes = additionalNotes.text!
            alertCon.actionConfirmed = {
                action in
                self.placeOrder()
            }
            alertCon.actionCanceled = { (action)-> () in}
            alertCon.message = ValidationMessages.placeOrder
            alertCon.placeOrder = true
            alertCon.modalPresentationStyle = .overCurrentContext
            self.present(alertCon, animated: false, completion: nil)
        }
    }
    private func placeOrder()
    {
        
        let orderParams = HelperFunction.createCustomerOrderParams(createOrderModel :createOrderModel)
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.submitOrder, jsonToPost: orderParams,isResponseRequired: true) { (json) in
            if let json = json
            {
                if (json["hasClientCardExpired"].bool ?? false){
                    let action = UIAlertAction.init(title: "Go To Payment", style: .default, handler: { (action) in
                        if let navController = self.navigationController{
                            let payment = AddNewPayment(nibName: "AddNewPayment", bundle: nil)
                            navController.pushViewController(payment, animated: true)
                        }
                    })
                    self.showAlertController( message: json["message"].string ?? "", action: action, showCancel: true)
                    return
                }
                if json[ServerKeys.status].int  == ServerStatusCode.Failure {
                    self.showMessage(json["message"].string ?? "", type: .error)
                    return
                }
                self.couponView.isHidden = true
                self.couponViewHeight.constant = 0
                self.customerOrderModel = CustomerOrderModel(json: json)
                self.orderIDLabel.text = String.init(format: "Order Id: #%d",(self.customerOrderModel?.orderNumber)!)
                self.confirmAndSubmitOrderButton.setTitle("TRACK ORDER", for: .normal)
                self.additionalNotes.isEditable = false
                self.dateTimeView.isHidden = false
                self.dateTimeViewHeight.constant = 115
                //self.getNearestDriver()
                self.addBackButton()
                self.addNewCustomerOrderToHome()
                self.showMessage(ValidationMessages.orderPlaced, type: .success,options: [.textNumberOfLines(0)])
                self.scrollToBottom()
            }
        }
    }
    
    private func scrollToBottom()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.9, execute: {
            let bottomOffset = CGPoint(x: 0, y: self.scrollView.contentSize.height - self.scrollView.bounds.size.height)
            self.scrollView.setContentOffset(bottomOffset, animated: true)
        })
    }
   private func addBackButton()
   {
        let backBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonClicked))
        backBarButtonItem.imageInsets = UIEdgeInsets(top: 0, left: -10.0, bottom: 0, right: 10.0)
        self.navigationItem.setLeftBarButton(backBarButtonItem, animated: true)
    }
    
    @objc func backButtonClicked()
    {
            NotificationCenter.default.post(name: .resetCustomerHomeData, object : nil)
            self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func addNewCustomerOrderToHome()
    {
        let currentOrderModel = CurrentOrderModel()
        currentOrderModel.orderId = customerOrderModel!.orderID
        currentOrderModel.orderNumber = customerOrderModel!.orderNumber
        currentOrderModel.status = .open
        currentOrderModel.startAddress = createOrderModel.startAddress
        currentOrderModel.destinationAddress = createOrderModel.destinationAddress
        currentOrderModel.customerId = HelperFunction.getUserId()
        currentOrderModel.immediate = true
        NotificationCenter.default.post(name: .ownOrderPlaced, object : currentOrderModel)
        NotificationCenter.default.post(name: .resetCustomerHomeData, object : nil)
    }
@objc func handleOrderStatusChange(notification: Notification)
{
    if let notificationObject = notification.userInfo as? [ String: Any]
    {
        if let currentOrderId = notificationObject["orderId"] as? String
        {
            if currentOrderId == self.customerOrderModel?.orderID
            {
                // update time here
                dateTimeInfoView.isHidden = false
                setDate()
                
                if let sourceLocation = notificationObject["sourceLocation"] as? [String:Any]
                {
                    awayTimeLabel.text = ""
                    let orderlat = sourceLocation["lat"] as! Double
                    let orderlong = sourceLocation["lng"] as! Double
                    getEstimateDistanceAndTime(lattitude: orderlat,longitude: orderlong)
                }
                if let customerOrderModel = self.customerOrderModel
                {
                    customerOrderModel.driverId = notificationObject["driverId"] as? String ?? ""
                    customerOrderModel.driverName = notificationObject["driverName"] as? String ?? ""
                }
                guard let status = notificationObject["status"] as? String else{return}
                guard let statusType = OrderStatusType(rawValue: status) else{ return }
                if statusType == .done
                {
                    openForDriverRating()
                }
            }
        }
    }
}
    func openForDriverRating()
    {
        let completeOrder = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.completeOrder) as! CompleteOrderController
        let orderTrackModel = OrderTrackModel()
        orderTrackModel.driverId = customerOrderModel!.driverId
        orderTrackModel.orderId = customerOrderModel!.orderID
        orderTrackModel.driverName = customerOrderModel!.driverName
        completeOrder.orderTrackModel = orderTrackModel
        self.navigationController?.pushViewController(completeOrder, animated: true)
    }
    @objc func orderCancelFromDriver(notification: Notification)
    {
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            if let currentOrderId = notificationObject["orderId"] as? String
            {
                if currentOrderId == self.customerOrderModel?.orderID
                {
                    dateTimeInfoView.isHidden = true
                }
            }
        }
    }
    private func setDate()
    {
        let attributedString = NSMutableAttributedString()
        let dateModel = HelperFunction.getDateComponent(date:Date())
        let monthAndYear = HelperFunction.createAttributedString("\(dateModel.monthAndYear!)\n", textSize: 14, textColor: UIColor.customerTheme)
        attributedString.append(monthAndYear)
        let dayOfMonth = HelperFunction.createAttributedString(dateModel.dayOfMonth, textSize: 35, textColor: UIColor.customerTheme)
        attributedString.append(dayOfMonth)
        let dayName = HelperFunction.createAttributedString("\n\(dateModel.dayName!)", textSize: 14, textColor: UIColor.customerTheme)
        attributedString.append(dayName)
        currentdateLabel.attributedText = attributedString
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        alertCon.dismiss(animated: false, completion: nil)
    }
    
    private func removeObserver()
    {
        NotificationCenter.default.removeObserver(self)
    }
   
    deinit {
        removeObserver()
    }
    @IBAction func cancelOrderAction(_ sender: UIButton) {
        if confirmAndSubmitOrderButton.titleLabel?.text == "TRACK ORDER"{
            let alertCon = CustomerCancelOrderPopUP(nibName: "CustomerCancelOrderPopUP",bundle : nil)

            alertCon.actionConfirmed = { (action: Bool,text:String)-> Void in
                self.cancelOrder(reason: text)
            }
            
            alertCon.actionCanceled = { (action)-> () in
                
            }
            alertCon.message = ValidationMessages.cancelOrder
            alertCon.modalPresentationStyle = .overCurrentContext
            self.present(alertCon, animated: false, completion: nil)
        }else{
            alertCon.actionConfirmed = {
                action in
                if let _ = self.customerOrderModel{
                    self.cancelOrder(reason: "")
                }
                else{
                    NotificationCenter.default.post(name: .resetCustomerHomeData, object : nil)
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
            alertCon.actionCanceled = { (action)-> () in}
            alertCon.message = ValidationMessages.cancelOrder
            alertCon.modalPresentationStyle = .overCurrentContext
            self.present(alertCon, animated: false, completion: nil)
        }
    }
    private func cancelOrder(reason:String)
    {
        let cancelParams = [
            "orderId":customerOrderModel!.orderID,
            "cancelReason": reason
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.cancelOrder, jsonToPost: cancelParams) { (json) in
            if let json = json
            {
                if json[ServerKeys.status].int == 1{
                    self.cancelOrderOnHomeScreen()
                    self.navigationController?.popToRootViewController(animated: true)
                }else{
                    self.showAlertController(message: ValidationMessages.OrderCancelRejected)
                }
            }
        }
    }
   /* private func emitToSocket()
    {
        if let customerOrderModel = self.customerOrderModel , !self.customerOrderModel!.driverId.isEmpty{
            SocketIOManager.sharedInstance.sendCancelOrderToDriver(toDriverId: customerOrderModel.driverId, fromCustomerId: HelperFunction.getUserId(), orderId: customerOrderModel.orderID)
        }
    }*/
    private func cancelOrderOnHomeScreen()
    {
        NotificationCenter.default.post(name: .ownOrderCancel, object: ["orderId":self.customerOrderModel!.orderID])
    }
    
    @IBAction func applyPromoCode(_ sender: RoundableButton) {
        if !promoCodeField.hasText
        {
            self.showMessage(ValidationMessages.promocodeFieldError, type: .error,options: [.textNumberOfLines(0)])
            return
        }
        var promoCodeParams = HelperFunction.createCustomerCouponParams(createOrderModel: createOrderModel)
        promoCodeParams["couponCode"] = promoCodeField.text!
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.applycoupon, jsonToPost: promoCodeParams) { (json) in
            if let json = json
            {
                self.showMessage(ValidationMessages.promocodeSuccess, type: .success,options: [.textNumberOfLines(0)])
                let coupon = Coupon(json : json[ServerKeys.data])
                self.createOrderModel.coupon = coupon
                self.setEstimatedPrice()
                self.enableCouponView()
            }
        }
    }
    
    private func enableCouponView(disable :Bool = false)
    {
        self.applyCouponButton.isEnabled = disable
        self.promoCodeField.isEnabled = disable
        self.removeCouponButton.isHidden = disable
        if promoCodeField.hasText{
            lblPromoCode.text = promoCodeField.text ?? ""
        }
        self.viewCouponApplied.isHidden = disable
        self.applyCouponButton.isHidden = !disable
        self.promoCodeField.isHidden = !disable
    }
    @IBAction func sendmail(_ sender: UIButton) {
        HelperFunction.openMailApp(email: ServerUrls.customerEmail)
    }
    
    @IBAction func removeCouponAction(_ sender: Any) {
        promoCodeField.text = ""
        createOrderModel.coupon = nil
        setEstimatedPrice()
        enableCouponView(disable: true)
    }
   /* private func getNearestDriver()
    {
        let orderParams : [String: Any] = ["srcCoordinates":[
                createOrderModel.startCoordinate.longitude,
                createOrderModel.startCoordinate.latitude
            ],
            "vehicleType":createOrderModel.truckType,
            "orderId":customerOrderModel!.orderID
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.notifyNearestDriver, jsonToPost: orderParams,isResponseRequired : true) { (json) in
            if let json = json
            {
                if json[ServerKeys.status].int == 1 {
                let driverId = json["driverId"].string!
                let driverName = json["driverName"].string!
                self.customerOrderModel?.driverId = driverId
                self.customerOrderModel?.driverName = driverName
                var paramsToSend : [String:Any] = [:]
                paramsToSend["isImmediate"] = true
                paramsToSend["orderId"] = self.customerOrderModel!.orderID
                paramsToSend["sourceLocation"] = ["lat":self.createOrderModel.startCoordinate.latitude,
                                                  "lng":self.createOrderModel.startCoordinate.longitude]
                SocketIOManager.sharedInstance.sendNotificationToNearestDriver(toDriverId: driverId, fromCustomerId: HelperFunction.getUserId(), driverData: paramsToSend)
                }
            }
        }
    }*/
    private func getEstimateDistanceAndTime(lattitude: Double,longitude: Double)
    {
        
            let params :[String:Any] = [
                "startlat":self.createOrderModel.startCoordinate.latitude,
                "endlat":lattitude,
                "startlong":self.createOrderModel.startCoordinate.longitude,
                "endlong":longitude
            ]
            BaseServices.SendPostJson(viewController: self ,serverUrl: ServerUrls.Customer.getTotalDistanceAndTime, jsonToPost: params, isResponseRequired : true,showLoader: false) { (json) in
                if let json = json
                {
                    let data = json[ServerKeys.data]
                    if let timeInHours = data["timeInHours"].string
                    {
                        self.awayTimeLabel.text = String(format: "%@\nAway",timeInHours)
                    }
                }
                else{
                     self.awayTimeLabel.text = "Driver will reach soon"
                }
            }
        }
}
extension NowOrderDetailCon: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellName, for: indexPath) as! ImageUploadingCell
        cell.deleteImage.isHidden = true
        cell.setUpImage(stringUrl :imagesArray[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presentZoomViewController(imagesArray[indexPath.row])
    }
}


extension NowOrderDetailCon: UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars <= IdentifierName.descriptionLength
    }
    
}

extension NowOrderDetailCon: zoomImageViewControllerDelegate {
    func presentZoomViewController(_ url: String) {
        let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
        let driverInfoController = storyboard.instantiateViewController(withIdentifier:IdentifierName.Driver.zoomImageViewController) as! ZoomImageViewController
        driverInfoController.urlString = url
        driverInfoController.delegate = self
        self.navigationController?.present(driverInfoController, animated: false, completion: nil)
    }
    
    func dismissPresentViewController() {
        self.navigationController?.dismiss(animated: false, completion: nil)
    }
}
