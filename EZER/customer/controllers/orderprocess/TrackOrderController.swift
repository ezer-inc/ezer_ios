//
//  TrackOrderController.swift
//  EZER
//
//  Created by TimerackMac1 on 18/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import SwiftyJSON

class TrackOrderController: UIViewController {
    
    public var orderId : String!
    
    @IBOutlet weak var orderStatusView: CardView!
    @IBOutlet weak var driverView: CardView!
    @IBOutlet weak var stckViewOrder: UIStackView!
    @IBOutlet weak var costView: CardView!
    @IBOutlet weak var chrgesView: CardView!
    @IBOutlet weak var lblTimeConst: NSLayoutConstraint!
    
    @IBOutlet weak var declineButton: RoundableButton!
    @IBOutlet weak var tableViewLoc: UITableView!
    @IBOutlet weak var tableViewLocHeightConstraintOutlet: NSLayoutConstraint!
    @IBOutlet weak var costTableView: UITableView!
    @IBOutlet weak var costTableViewHeightConstraintOutlet: NSLayoutConstraint!
    @IBOutlet weak var itemDescritionTableView: UITableView!
    @IBOutlet weak var itemDescritionTableViewHeightConstraintOutlet: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTip: UILabel!
    @IBOutlet weak var lblEarnings: UILabel!
    @IBOutlet weak var lblPickDate: UILabel!
    @IBOutlet weak var lblPickDay: UILabel!
    @IBOutlet weak var lblPickTime: UILabel!
    @IBOutlet weak var lblPickTimeAm: UILabel!
    
    @IBOutlet weak var lblEarningTitle: UILabel!
    //////////////////////////// //////
    
    @IBOutlet weak var trackOrderBar: OrderStatus!
    var orderTrackModel :  OrderTrackModel?
    
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var callButtonImg: UIImageView!
    var openedFromOrderHistory = false
    var orderSubTitle = ""
    var orderTitle = "Order"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callButton.isHidden = true
        callButtonImg.isHidden = true
        chrgesView.isHidden = true
        costView.isHidden = true
        orderStatusView.isHidden = true
        
        updateUI()
        getOrderDetails()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        let backBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "ic_back"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonClicked))
        backBarButtonItem.imageInsets = UIEdgeInsets(top: 0, left: -10.0, bottom: 0, right: 10.0)
        self.navigationItem.setLeftBarButton(backBarButtonItem, animated: true)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserver()
        if !self.isMovingToParent{
            getOrderDetails()
        }
        UserDefaults.standard.set(nil, forKey: UserDefaultkeys.isNotification)
    }
    func updateUI(){
        self.title = orderTitle
        lblOrderStatus.text = orderSubTitle
        guard let orderModel = orderTrackModel else {
            return
        }
        if orderModel.driverId.isEmpty{
            lblDriverName.text = "No Driver is assigned yet."
        }else {
            lblDriverName.text = orderModel.driverName
        }
        lblTotal.text = String(format:"$%.2f",orderModel.totalCost)
        lblTip.text = String(format:"$%d.00",orderModel.tip)
        lblSubTotal.text = String(format:"$%.2f",orderModel.cost)
        if let pickUpDTModel = orderModel.pickupDateModel {
            lblPickDate.text = pickUpDTModel.monthAndYear
            lblPickDay.text = "\(pickUpDTModel.dayName ?? "") \(pickUpDTModel.dayOfMonth ?? "")"
            lblPickTime.text = pickUpDTModel.time
            lblPickTimeAm.text = pickUpDTModel.amOrPM
        }
        
        if orderModel.isSpecialRateApplied && orderModel.customerspecialRate != 0{
            self.lblEarningTitle.text = "Price"
            self.lblEarnings.text = String(format: "$%.2f",orderModel.customerspecialRate)
        }else{
            self.lblEarningTitle.text = "Estimated Price*"
            if orderModel.estimatedMaxPrice != 0.0 {
                lblEarnings.text  = String(format: "$%.2f - $%.2f", orderModel.estimatedMinPrice, orderModel.estimatedMaxPrice)
            }else {
                let couponMin = orderModel.estimatedDiscountedMinPrice
                let couponMax = orderModel.estimatedDiscountedMaxPrice
                lblEarnings.text  = String(format: "$%.2f - $%.2f", couponMin ?? 0.0, couponMax ?? 0.0)
            }
          
       }
        
     
        tableViewLoc.reloadData()
        itemDescritionTableView.reloadData()
        costTableView.reloadData()
    }
    
    override func viewWillLayoutSubviews() {
        self.view.layoutIfNeeded()
        super.updateViewConstraints()
        self.tableViewLocHeightConstraintOutlet?.constant = self.tableViewLoc.contentSize.height
        self.itemDescritionTableViewHeightConstraintOutlet?.constant = self.itemDescritionTableView.contentSize.height
        self.costTableViewHeightConstraintOutlet?.constant = self.costTableView.contentSize.height
    }
    
    @objc func backButtonClicked() {
        if openedFromOrderHistory {
            self.navigationController?.popViewController(animated: true)
        }else {
            NotificationCenter.default.post(name: .resetCustomerHomeData, object : nil)
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    private func addObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(handleOrderStatusChange(notification:)), name: .orderStatusChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(orderCancelFromDriver(notification:)), name: .orderCancelFromDriver, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshOrderDetail(notification:)), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func refreshOrderDetail(notification:Notification) {
        getOrderDetails()
    }
    @objc func handleOrderStatusChange(notification: Notification) {
        if let notificationObject = notification.userInfo as? [ String: Any] {
            if let currentOrderId = notificationObject["orderId"] as? String {
                if currentOrderId == orderId {
                    guard let status = notificationObject["status"] as? String else{return}
                    let currentStatus = notificationObject["currentStatus"] as? String
                    guard let statusType = OrderStatusType(rawValue: status) else{ return }
                    if statusType == .done {
                        getOrderDetails(showLoader: false)
                    }else {
                        if let orderTrackModel = orderTrackModel{
                            orderTrackModel.driverId = notificationObject["driverId"] as? String ?? ""
                            orderTrackModel.driverName =  notificationObject["driverName"] as? String ?? ""
                            orderTrackModel.status = statusType
                            orderSubTitle = self.setOrderStatus(status: self.orderTrackModel!.status, currentStatus: currentStatus ?? "")
                            updateUI()
                        }else{
                            getOrderDetails(showLoader: false)
                        }
                        
                    }
                }
            }
        }
    }
    
    @objc func orderCancelFromDriver(notification: Notification)
    {
        if let notificationObject = notification.userInfo as? [ String: Any]
        {
            if let currentOrderId = notificationObject["orderId"] as? String
            {
                if currentOrderId == orderId
                {
                    //showAlertController(message: ValidationMessages.orderCancelByDriver)
                    if let orderTrackModel = orderTrackModel{
                        orderTrackModel.driverId = ""
                        orderTrackModel.driverName = ""
                        self.orderTrackModel!.status = .open
                        orderSubTitle = self.setOrderStatus(status: self.orderTrackModel!.status, currentStatus: "")
                        updateUI()
                    }
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    func getOrderDetails(showLoader : Bool = true) {
        let orderParams :[String:Any] = [
            "orderId":orderId,
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.orderDetails, jsonToPost: orderParams,showLoader:showLoader ) { (json) in
            if let json = json  {
                print(json)
                self.orderTrackModel =  OrderTrackModel(json:json[ServerKeys.data])
                
                self.orderTitle = "Order \(self.orderTrackModel!.orderNumber!)"
                //check if addtional charges array is not null
                
                if  (self.orderTrackModel?.additionalCharges) != nil {
                    self.chrgesView.isHidden =  self.orderTrackModel?.additionalCharges.count != 0 ? false : true
                }
                
                self.orderSubTitle = self.setOrderStatus(status: self.orderTrackModel!.status, currentStatus: "")
                self.updateUI()
                if self.orderTrackModel!.driverRatingRequired && self.orderTrackModel?.status == .done {
                    self.openForDriverRating()
                }
            }
        }
    }
    
    private func ownCancelOrder() {/*
         if self.orderTrackModel!.driverId.isEmpty{
         SocketIOManager.sharedInstance.sendScheduledOrderCanceled(fromCustomerId: HelperFunction.getUserId(), orderId: orderTrackModel!.orderId)
         }
         else{
         SocketIOManager.sharedInstance.sendCancelOrderToDriver(toDriverId: orderTrackModel!.driverId, fromCustomerId: HelperFunction.getUserId(), orderId: orderTrackModel!.orderId)
         }*/
        NotificationCenter.default.post(name: .ownOrderCancel, object: ["orderId":orderId])
    }
    
    @objc func openForDriverRating(){
        let completeOrder = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.completeOrder) as! CompleteOrderController
        completeOrder.orderTrackModel = orderTrackModel
        self.navigationController?.pushViewController(completeOrder, animated: true)
    }
    
    @IBAction func callAction(_ sender: UIButton) {
        let params :[String:Any] = [
            "orderId": orderId
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Main.assignTwilioNumber, jsonToPost: params) { (json) in
            if let json = json{
                if let twilioNumber = json["twilioNumber"].string{
                    HelperFunction.callToNumber(number: twilioNumber.trimWhiteSpace())
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func cancelOrderAction(_ sender: UIButton) {
//        let alertCon = AlertDialogController(nibName: "AlertDialogController",bundle : nil)
//        alertCon.actionConfirmed = { (action : Bool)-> Void in
//            self.cancelOrder()
//        }
//        alertCon.actionCanceled = { (action)-> () in
//
//        }
//        alertCon.message = ValidationMessages.cancelOrder
//        alertCon.modalPresentationStyle = .overCurrentContext
//        self.present(alertCon, animated: false, completion: nil)
        
        
        let alertCon = CustomerCancelOrderPopUP(nibName: "CustomerCancelOrderPopUP",bundle : nil)

        alertCon.actionConfirmed = { (action: Bool,text:String)-> Void in
            self.cancelOrder(reason: text)
        }
        
        alertCon.actionCanceled = { (action)-> () in
            
        }
        alertCon.message = ValidationMessages.cancelOrder
        alertCon.modalPresentationStyle = .overCurrentContext
        self.present(alertCon, animated: false, completion: nil)
        
    }
    private func cancelOrder(reason:String){
        let cancelParams :[String:Any] = [
            "orderId": orderId,
            "cancelReason": reason
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Customer.cancelOrder, jsonToPost: cancelParams) { (json) in
            if let _ = json{
                
                self.ownCancelOrder()
                //self.navigationController?.popViewController(animated: true)
                self.backButtonClicked()
            }
        }
    }
    
    
    private func setOrderStatus(status: OrderStatusType, currentStatus:String) -> String{
        callButton.isHidden = false
        callButtonImg.isHidden = false
        orderStatusView.isHidden = false
        var orderStatus = ""
        var statusViewIndex = 0
        switch status {
        case .open:
            statusViewIndex = 0
            orderStatus = "Open"
            orderStatusView.isHidden = true
            callButton.isHidden = true
            callButtonImg.isHidden = true
        case .driverReview:
            callButton.isEnabled = true
            statusViewIndex = 1
            //            trackOrderBar.firstLabel.text = "Assigned"
            orderStatus = "Assigned"
        case .startLoading:
            //            trackOrderBar.secondLabel.text = "Driver On Route"
            orderStatus = "Loading"
            statusViewIndex = 2
        case .accepted, .assigned:
            statusViewIndex = 1
            //            trackOrderBar.firstLabel.text = "Driver Away"
            orderStatus = "Accepted"
        case .onTheWay, .onRouteToOrigin:
            //            trackOrderBar.secondLabel.text = "Driver On Route"
            orderStatus = "On Route to Pick Up Location"
            statusViewIndex = 2
        case .atSource, .arrievedToOrigin:
            //            trackOrderBar.secondLabel.text = "Driver Arrived"
            orderStatus = "Arrived at the Pick Up Location"
            statusViewIndex = 2
        case .loaded, .loading:
            //            trackOrderBar.thirdLabel.text = "Driver On Route"
            orderStatus = "Loaded"
            statusViewIndex = 3
        case .inTransit, .onRouteDestination:
            //            trackOrderBar.thirdLabel.text = "Driver On Route"
            orderStatus = "On Route to drop off"
            statusViewIndex = 3
        case .multi_pickup_transit:
            //            trackOrderBar.thirdLabel.text = "Driver On Route"
            orderStatus = currentStatus
            
            guard let multiStatus = OrderStatusType(rawValue: orderStatus) else{
                return orderStatus
            }
            switch multiStatus
            {
            case .onTheWay, .onRouteToOrigin:
                orderStatus = "On Route to Pick Up Location"
                statusViewIndex = 2
            case .atSource, .arrievedToOrigin:
                orderStatus = "Arrived at the Pick Up Location"
                statusViewIndex = 2
            case .startLoading:
                orderStatus = "Loading"
                statusViewIndex = 2
            case .loaded, .loading :
                orderStatus = "Loaded"
                statusViewIndex = 3
                
            default:
               break
            
            }
            statusViewIndex = 3
        case .multi_drop_transit:
            //            trackOrderBar.fourthLabel.text = "Driver On Route"
            orderStatus = currentStatus
            guard let multiStatus = OrderStatusType(rawValue:orderStatus) else{
                return orderStatus
            }
            switch multiStatus
            {
            case .inTransit, .onRouteDestination:
                orderStatus = "On Route to drop off"
                statusViewIndex = 3
            case .atDestination, .arrivedAtDestination:
                orderStatus = "Arrived at the drop off"
                statusViewIndex = 4
            case .beforeUnload:
                orderStatus = "Unloading"
                statusViewIndex = 4
        
            case .unloaded, .unLoaded:
                orderStatus = "Unloaded"
                statusViewIndex = 4
            default:
                break
            }
            statusViewIndex = 4
        case .atDestination, .arrivedAtDestination:
            //            trackOrderBar.fourthLabel.text = "Driver At Destination"
            orderStatus = "Arrived at the drop off"
            statusViewIndex = 4
        case .beforeUnload:
            //            trackOrderBar.secondLabel.text = "Driver On Route"
            orderStatus = "Unloading"
            statusViewIndex = 4
        case .unloaded, .unLoaded:
            //            trackOrderBar.fourthLabel.text = "Delivery Completed"
            orderStatus = "Unloaded"
            statusViewIndex = 4
        case .finished:
            orderStatusView.isHidden = true
            costView.isHidden = false
            orderStatus = "Completed"
            statusViewIndex = -1
            statusViewIndex = 4
            declineButton.isEnabled = false
            callButton.isHidden = true
            callButtonImg.isHidden = true
            
            lblEarnings.font = UIFont(name:"Roboto-Medium",size:16)
            lblPickDay.font = UIFont(name:"Roboto-Medium",size:16)
            lblPickTime.font = UIFont(name:"Roboto-Medium",size:16)
            lblTimeConst.constant = 0
            
        case .done:
            orderStatusView.isHidden = true
            costView.isHidden = false
            orderStatus = "Completed"
            statusViewIndex = -1
            statusViewIndex = 4
            declineButton.isEnabled = false
            callButton.isHidden = true
            callButtonImg.isHidden = true
            
            lblEarnings.font = UIFont(name:"Roboto-Medium",size:16)
            lblPickDay.font = UIFont(name:"Roboto-Medium",size:16)
            lblPickTime.font = UIFont(name:"Roboto-Medium",size:16)
            lblTimeConst.constant = 0
        case .cancelled:
            statusViewIndex = -1
            orderStatusView.isHidden = true
            costView.isHidden = false
            orderStatus = "Cancelled"
            declineButton.isEnabled = false
            callButton.isHidden = true
            callButtonImg.isHidden = true
        case .timeout:
            orderStatusView.isHidden = true
            orderStatus = "Timed Out"
        case .pause:
            orderStatus = "Pause"
            orderStatusView.isHidden = true
            callButton.isHidden = true
            callButtonImg.isHidden = true
        default:
            orderStatusView.isHidden = true
            callButton.isHidden = true
            callButtonImg.isHidden = true
        }
        
        declineButton.isEnabled = (statusViewIndex > 2 || statusViewIndex < 0) ? false : true
        trackOrderBar.orderStatus = statusViewIndex
        return orderStatus
    }
}
extension TrackOrderController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (orderTrackModel == nil){
            return 0
        }
        if tableView == tableViewLoc {
            let count = max(orderTrackModel!.extraStartLocations.count, orderTrackModel!.extraDestinationLocations.count)
            return count + 1// ONe is default row must have Source Or Destination
        }else if tableView == itemDescritionTableView {
            let count = orderTrackModel!.extraStartLocations.count
            return count + 1
        }else {
            return orderTrackModel?.additionalCharges.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewLoc {
            let  cell = tableView.dequeueReusableCell(withIdentifier:  "CustomerAddressInfoCell") as! CustomerAddressInfoCell
            cell.setUpCell(indexPath, trackOrder: orderTrackModel!)
            return cell
        }else if tableView == itemDescritionTableView {
            let  cell = tableView.dequeueReusableCell(withIdentifier:  "itemDescriptionDetailTableViewCell") as! itemDescriptionDetailTableViewCell
            cell.setUpCell(indexPath, driverJob: orderTrackModel!)
            cell.delegate = self
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdditionalCostDetailTableViewCell") as! AdditionalCostDetailTableViewCell
            cell.customizeCell(data: orderTrackModel!.additionalCharges[indexPath.row], index: indexPath.row)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
}
extension TrackOrderController: itemDescriptionDetailTableViewCellDelegate{
    func PresentZoomViewController(_ url: String) {
        let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
        let driverInfoController = storyboard.instantiateViewController(withIdentifier:IdentifierName.Driver.zoomImageViewController) as! ZoomImageViewController
        driverInfoController.urlString = url
        driverInfoController.delegate = self
        self.navigationController?.present(driverInfoController, animated: false, completion: nil)
    }
}

extension TrackOrderController:zoomImageViewControllerDelegate{
    func dismissPresentViewController() {
        self.navigationController?.dismiss(animated: false, completion: {})
    }
}
