//
//  NowPlaceORSchedule.swift
//  EZER
//
//  Created by TimerackMac1 on 17/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import SwiftyJSON


class PlaceOrderORSchedule: UIViewController {
    @IBOutlet weak var segmented: YSSegmentedControl!
    @IBOutlet weak var imagesCollection: UICollectionView!
    @IBOutlet weak var descriptionText: KMPlaceholderTextView!
    @IBOutlet weak var endAddress: UILabel!
    @IBOutlet weak var startAddress: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var pickerViewBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var parentDatePickerView: UIView!
    @IBOutlet weak var actionButton: RoundableButton!
    var createOrderModel : CreateOrderModel!
    let cellName = "ImageUploadingCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.setValue(UIColor.customerTheme, forKey: "textColor")
        
        var fifteenDaysfromNow: Date {
            return (Calendar.current as NSCalendar).date(byAdding: .day, value: 15, to: Date(), options: [])!
        }
        
        datePicker.maximumDate = fifteenDaysfromNow
        self.title = "New Order"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)

        segmented.titles = ["Now", "Later"]
        //segmented.appearance.se = UIColor.blue
        segmented.appearance?.unselectedTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.white]
        segmented.appearance?.selectedTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.white]
        if CurrentCustomerOrder.orderScheduleType == OrderScheduleTypes.later
        {
            segmented.selectItem(at: 1, withAnimation: false)
            self.handleSegment()
        }
        startAddress.text = String(format: "%@\n%@",createOrderModel.startAddress,createOrderModel.startAddressUnitNumber ?? "")
        endAddress.text = String(format: "%@\n%@",createOrderModel.destinationAddress,createOrderModel.destinationAddressUnitNumber ?? "" )
        descriptionText.text = createOrderModel.contentDescription
        self.pickerViewBottomMargin.constant = 216
        imagesCollection.register(UINib(nibName:cellName ,bundle: nil), forCellWithReuseIdentifier: cellName)
        imagesCollection.dataSource = self
        segmented.action = { control, index in
            CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: index)!
            self.handleSegment()
        }
        descriptionText.layer.borderWidth = 1
        descriptionText.layer.borderColor = UIColor.customerTheme.cgColor
        self.parentDatePickerView.isHidden = true
         addObserver()
        self.handleSegment()
        fireEventOnGetSocketForRefreshOrderType()
         checkImmediateOrder()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.imagesCollection.flashScrollIndicators()
    }
   
    
    private func handleSegment()
    {
        
        if CurrentCustomerOrder.orderScheduleType == OrderScheduleTypes.now
        {
            actionButton.setTitle("REVIEW ORDER", for: UIControl.State.normal)
            createOrderModel.immediate = true
             self.pickerViewBottomMargin.constant = 216
             segmented.selectItem(at: 0, withAnimation: true)
        }
        else{
            actionButton.setTitle("SELECT DATE & TIME", for: UIControl.State.normal)
            createOrderModel.immediate = false
             segmented.selectItem(at: 1, withAnimation: true)
        }
        createOrderModel.dateTime = ""
        createOrderModel.coupon = nil
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         NotificationCenter.default.removeObserver(self)
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func backToHome(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func placeOrderAction(_ sender: UIButton) {
        
        if self.createOrderModel.shippingItem.imagesArray.isEmpty
        {
            self.showMessage(ValidationMessages.uploadImageError, type: .error,options: [.textNumberOfLines(0)])
            return
        }
        if CurrentCustomerOrder.orderScheduleType == OrderScheduleTypes.now
        {
            
            let laterScheduleOrder = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.laterScheduleOrder) as! LaterScheduleOrderCon
            createOrderModel.dateTime = HelperFunction.convertToUtc(date :Date())
            createOrderModel.orderDate = Date()
            laterScheduleOrder.createOrderModel = createOrderModel
            laterScheduleOrder.delegate = self
            self.navigationController?.pushViewController(laterScheduleOrder, animated: true)
            
//            let nowOrderDetail = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.nowOrderDetail) as! NowOrderDetailCon
//            nowOrderDetail.createOrderModel = createOrderModel
//            self.navigationController?.pushViewController(nowOrderDetail, animated: true)
        }
        else{
            showHidePickerView(false)
        }
    }
    
    
    @IBAction func cancelDatePicker(_ sender: UIButton) {
        switch sender.tag {
        case 1: //cancel clicked
            showHidePickerView(true)
        default : // 2
            
           // print(datePicker.date, "Hello " , Date())
            if datePicker.date > Date(){
                showHidePickerView(true)
            let laterScheduleOrder = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Customer.laterScheduleOrder) as! LaterScheduleOrderCon
                createOrderModel.dateTime = HelperFunction.convertToUtc(date :datePicker.date)
                createOrderModel.orderDate = datePicker.date
               laterScheduleOrder.createOrderModel = createOrderModel
                laterScheduleOrder.delegate = self
                 self.navigationController?.pushViewController(laterScheduleOrder, animated: true)
            }
            else{
                self.showMessage(ValidationMessages.dateException, type: .error,options: [.textNumberOfLines(0)])
            }
        }
    }
    
    func showHidePickerView(_ isHide : Bool)
    {
        
        if isHide{
            UIView.animate(withDuration :0.5, delay: 0, usingSpringWithDamping :1, initialSpringVelocity :0.9, options: .curveEaseIn, animations:{
                self.pickerViewBottomMargin.constant = 216
                self.parentDatePickerView.isHidden = true
                self.view.layoutIfNeeded()
            }, completion:  nil)
        }else{
            UIView.animate(withDuration :0.5, delay: 0, usingSpringWithDamping :0.5, initialSpringVelocity :0.9, options: .curveEaseIn, animations:{
                self.pickerViewBottomMargin.constant = 0
                if self.createOrderModel.orderDate != nil
                {
                    self.datePicker.date = self.createOrderModel.orderDate
                }
                self.parentDatePickerView.isHidden = false
                self.view.layoutIfNeeded()
            }, completion:  nil)
           
        }
    }
   
    @objc private func checkNearByDriver(notifcation: Notification? = nil)
    {
       /* var showLoader = true
        if let _ = notifcation{
             showLoader = false
        }*/
        var params :[String:Any] = HelperFunction.createNearByDriverTerms(createOrderModel: createOrderModel)
        params["customerId"] = HelperFunction.getUserId()
        BaseServices.SendPostJson(viewController: self ,serverUrl: ServerUrls.Customer.checkNearbyDriver, jsonToPost: params,isResponseRequired: true,showLoader:false) { (json) in
            if let json = json
            {
                if json[ServerKeys.status] == 1 && HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 1
                {
                    
                    self.segmented.disableItem(at: 0, isEnabled: true)
                    
                }else{
                    if CurrentCustomerOrder.orderScheduleType == OrderScheduleTypes.now
                    {
                        self.hidePresentedAlert {
                            self.showAlertController(message: ValidationMessages.useLaterOption)
                        }
                    }
                    if HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 1 {
                        self.segmented.selectItem(at: 1, withAnimation: true)
                        self.segmented.disableItem(at: 0, isEnabled: false)
                        CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: 1)!
                    }
                }
                self.handleSegment()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkNearByDriver()
       
    }
    private func addObserver()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(checkNearByDriver), name: Notification.Name.notifyWhenDriverAvailable, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callObserverApis), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func callObserverApis(){
        checkNearByDriver()
        checkImmediateOrder()
    }
   
    func checkImmediateOrder() {
        BaseServices.sendGetJson(serverUrl: ServerUrls.Customer.checkImmediateOrder) { (json) in
            if (json != nil){
                let json = JSON(json!)
                if let status = json[ServerKeys.status].int {
                    HelperFunction.saveValueInUserDefaults(key: UserDefaultsKeys.immediateOrderEnable, value: status)
                    self.segmented.disableItem(at: 0, isEnabled: HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0 ? false : true)
                    if status == 0 && CurrentCustomerOrder.orderScheduleType == OrderScheduleTypes.now {
                        let orderType = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable)
                        CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: orderType == 0 ? 1 : 0)!
                        let alert = UIAlertController(title: nil, message: json[ServerKeys.message].string ?? "", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.navigationController?.popToRootViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }else if status == 0{
                        let orderType = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable)
                        CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: orderType == 0 ? 1 : 0)!
                    }
                }
                self.handleSegment()
            }
        }
    }

    
    func  fireEventOnGetSocketForRefreshOrderType() {
        SocketManager.sharedInstance.getSocketForRefreshOrderType { (result) in
            if result != nil{
                let json = JSON(result!)
                if json[ServerKeys.immediateOrderEnable].int ?? 1 == 0 && CurrentCustomerOrder.orderScheduleType == OrderScheduleTypes.now  {
                    
                    if let controller = self.navigationController?.viewControllers.last
                    {
                          if controller is TakePictureView || controller is CustomerHome || controller is PlaceOrderORSchedule || controller is NowOrderDetailCon || controller is LaterScheduleOrderCon
                            {
                                let alert = UIAlertController(title: nil, message: json[ServerKeys.message].string ?? "", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                    self.segmented.disableItem(at: 0, isEnabled: HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0 ? false : true)
                                    let orderType = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable)
                                    CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: orderType == 0 ? 1 : 0)!
                                    if HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0{
                                        self.segmented.selectItem(at: CurrentCustomerOrder.orderScheduleType.rawValue, withAnimation: false)
                                    }
                                    self.navigationController?.popToRootViewController(animated: true)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                    }
                        
                
                }else{
                    self.segmented.disableItem(at: 0, isEnabled: HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0 ? false : true)
                   
                    if HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0{
                        self.segmented.selectItem(at: CurrentCustomerOrder.orderScheduleType.rawValue, withAnimation: false)
                        let orderType = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable)
                        CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: orderType == 0 ? 1 : 0)!
                    }
                }
                 self.handleSegment()
            }
        }
    }    
}

extension PlaceOrderORSchedule: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.createOrderModel.shippingItem.imagesArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellName, for: indexPath) as! ImageUploadingCell
        //cell.deleteImage.isHidden = true
        cell.deleteImage.tag = indexPath.row
        cell.delegate = self
        cell.setUpImage(stringUrl :self.createOrderModel.shippingItem.imagesArray[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presentZoomViewController(self.createOrderModel.shippingItem.imagesArray[indexPath.row])
    }
}
extension PlaceOrderORSchedule: DeleteImageDelegate
{
    func deleteImageAtIndex(tag: Int) {
        let filename = HelperFunction.getFileNameFromUrl(stringUrl: self.createOrderModel.shippingItem.imagesArray[tag])
        let params :[String:Any] = [
            "role":Roles.client.rawValue,
            "image": filename,
            "imageType":ImageType.order.rawValue
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Main.deleteImageFromS3, jsonToPost: params) { (json) in
            if let _ = json
            {
                self.createOrderModel.shippingItem.imagesArray.remove(at: tag)
                self.imagesCollection.reloadData()
            }
        }
    }
}
extension PlaceOrderORSchedule: LaterScheduleOrderConDelegate{
    func handlebackActionFromReviewCaontroller() {
        BaseServices.sendGetJson(serverUrl: ServerUrls.Customer.checkImmediateOrder) { (json) in
            if (json != nil){
                let json = JSON(json!)
                if let status = json[ServerKeys.status].int {
                    HelperFunction.saveValueInUserDefaults(key: UserDefaultsKeys.immediateOrderEnable, value: status)
                    self.segmented.disableItem(at: 0, isEnabled: HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable) == 0 ? false : true)
                    if status == 0 && CurrentCustomerOrder.orderScheduleType == OrderScheduleTypes.now {
                        let orderType = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable)
                        CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: orderType == 0 ? 1 : 0)!
                        let alert = UIAlertController(title: nil, message: json[ServerKeys.message].string ?? "", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.navigationController?.popToRootViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }else if status == 0{
                        let orderType = HelperFunction.getIntFromUserDefaults(key: UserDefaultsKeys.immediateOrderEnable)
                        CurrentCustomerOrder.orderScheduleType = OrderScheduleTypes(rawValue: orderType == 0 ? 1 : 0)!
                        let alert = UIAlertController(title: nil, message: json[ServerKeys.message].string ?? "", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                self.handleSegment()
            }
        }
    }
}


extension PlaceOrderORSchedule: zoomImageViewControllerDelegate {
    func presentZoomViewController(_ url: String) {
        let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
        let driverInfoController = storyboard.instantiateViewController(withIdentifier:IdentifierName.Driver.zoomImageViewController) as! ZoomImageViewController
        driverInfoController.urlString = url
        driverInfoController.delegate = self
        self.navigationController?.present(driverInfoController, animated: false, completion: nil)
    }
    
    func dismissPresentViewController() {
        self.navigationController?.dismiss(animated: false, completion: nil)
    }
}
