//
//  OrderFinishController.swift
//  EZER
//
//  Created by TimerackMac1 on 23/01/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit

class OrderFinishController: UIViewController {
    var actionConfirmed:((_ action: Bool)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func okAction(_ sender: RoundableButton) {
        self.dismiss(animated: false)
        actionConfirmed?(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
