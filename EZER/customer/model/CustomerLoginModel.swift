//
//  CustomerLoginModel.swift
//  EZER
//
//  Created by TimerackMac1 on 29/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
import SwiftyJSON
open class CustomerLoginModel
{
    let isCardExpectedToExpire : Bool
    let isDefaultCardExpired : Bool
    let _id:String
    let firstName:String
    let lastName:String
    let cellPhone:String
    let installationId:String
    let userId:String
    let totalJobCount:Int
    let canceledJobCount:Int
    let ratedJobCount:Int
    let totalJobRating:Int
    let averageJobRating:Float
    let isEnabled:Bool
    let updatedAt:Int
    let profilePic:String
    let btCustomerId:String
    let isBusinessUser : Bool
    let email:String
    var isSubAccount: Bool
    var weightRange:Int
    
    init(json : JSON)
    {
        isCardExpectedToExpire = json["isCardExpectedToExpire"].bool ?? false
        isDefaultCardExpired = json["isDefaultCardExpired"].bool ?? false
        _id = json["_id"].string ?? ""
        firstName = json["firstName"].string ?? ""
        lastName = json["lastName"].string ?? ""
        cellPhone = json["cellPhone"].string ?? ""
        installationId = json["installationId"].string ?? ""
        userId = json["userId"].string ?? ""
        totalJobCount = json["totalJobCount"].int ?? 0
        canceledJobCount = json["canceledJobCount"].int ?? 0
        ratedJobCount = json["ratedJobCount"].int ?? 0
        totalJobRating = json["totalJobRating"].int ?? 0
        averageJobRating = json["averageJobRating"].float ?? 0.0
        isEnabled = json["isEnabled"].bool ?? false
        isBusinessUser = json["isBusinessUser"].bool ?? false
        updatedAt = json["updatedAt"].int ?? 0
        profilePic = json["profilePic"].string ?? ""
        btCustomerId = json["btCustomerId"].string ?? ""
        email = json["email"].string ?? ""
        isSubAccount = json["isSubAccount"].bool ?? false
        weightRange = json["weightRange"].int ?? 0
    }

}
