//
//  OrderTrackModel.swift
//  EZER
//
//  Created by TimerackMac1 on 12/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
import SwiftyJSON
class OrderTrackModel {
    var pickupDateModel: DateModel?
    var driverName :String!
    var tip :Int!
    var orderId :String!
    var orderNumber:Int!
    var orderDate : String!
    var startAddress : String!
    var pickupDate: String!
    var driverRatingRequired: Bool!
    var pickupTime : String!
    var destinationAddress :String!
    var cost:Double!
    var status :OrderStatusType!
    var ratingOfDriver: Double!
    var extraStartLocations : [ExtraStartLocation]!
    var extraDestinationLocations : [ExtraDestinationLocation]!
    var totalCost: Double!
    var shippingItems: [ShippingItems]!
    var driverId : String!
    var contentDescription:String!
    var userNotes:String!
    var startLocation : LocationModel!
    var destinationLocation : LocationModel!
    var estimatedMaxPrice : Double!
    var estimatedMinPrice : Double!
    var allImages = [String]()
    var estimatedDiscountedMaxPrice : Double!
    var estimatedDiscountedMinPrice: Double!
    var couponCode : String = ""
    var couponId : String = ""
    var additionalChargesLabel : String = ""
    var additionalChargesAmount:Double!
    var additionalCharges:[AdditionalCharges]!
    var coupon : Coupon!
    var destinationAddressUnitNumber : String!
    var startAddressUnitNumber :String!
    var customerspecialRate: Double!
    var isSpecialRateApplied: Bool!
    
    
    init(json :  JSON)
    {
        print(json["additionalChargesAmount"].double ?? 0.0)
        driverName = json["driverName"].string ?? ""
        tip = json["tip"].int ?? 0
        orderId = json["orderId"].string ?? ""
        orderNumber = json["orderNumber"].int ?? 0
        userNotes = json["userNotes"].string ?? ""
        if userNotes.isEmpty{
            userNotes = "No Additional Notes."
        }
        let orderDateTime = HelperFunction.getCurrentFullDate(date: json["orderDateTime"].string ?? "")
        orderDate =  HelperFunction.getCurrentDate(date: orderDateTime)
//        orderDate = json["orderDate"].string ?? ""
        startAddress = json["startAddress"].string ?? ""
        startAddressUnitNumber = json["startAddressUnitNumber"].string ?? ""
        contentDescription = json["contentDescription"].string ?? ""
        driverId = json["driverId"].string ?? ""
        driverRatingRequired = json["driverRatingRequired"].bool ?? false
        customerspecialRate = json["customerspecialRate"].double ?? 0
        isSpecialRateApplied = json["isSpecialRateApplied"].bool ?? false
        let localDate = HelperFunction.getCurrentFullDate(date: json["pickupDateTime"].string ?? "")
        
        pickupDate = HelperFunction.getCurrentDate(date: localDate)
        pickupTime = HelperFunction.getTime(date: localDate)
        pickupDateModel = HelperFunction.getDateComponent(dateString : json["pickupDateTime"].string ?? "")
        
//        pickupDate = json["pickupDate"].string ?? ""
//        pickupTime = json["pickupTime"].string ?? ""
        destinationAddress = json["destinationAddress"].string ?? ""
        destinationAddressUnitNumber = json["destinationAddressUnitNumber"].string ?? ""
        cost = json["cost"].double ?? 0.0
        status  = OrderStatusType(rawValue: json["status"].string ?? "TIMEOUT") ?? OrderStatusType.timeout
        ratingOfDriver = json["ratingOfDriver"].double ?? 0.0
        extraStartLocations = [ExtraStartLocation]()
        if let extraStartLocationes = json["extraStartLocations"].array
        {
            for item in extraStartLocationes
            {
                let model = ExtraStartLocation(json : item)
                if !model.shippingItems.isEmpty
                {
                   let shipItem = model.shippingItems[0]
                    allImages.append(contentsOf: shipItem.images)
                }
                extraStartLocations.append(model)
            }
        }
        extraDestinationLocations = [ExtraDestinationLocation]()
        if let extraDestinations = json["extraDestinationLocations"].array
        {
            for item in extraDestinations
            {
                let model = ExtraDestinationLocation(json : item)
                extraDestinationLocations.append(model)
            }
        }
        totalCost  = json["totalCost"].double ?? 0
        shippingItems = [ShippingItems]()
        if let shippingArray = json["shippingItems"].array
        {
            for item in shippingArray{
               let item = ShippingItems(json :item)
                allImages.append(contentsOf: item.images)
                shippingItems.append(item)
            }
        }
        startLocation = LocationModel(json : json["startLocation"])
        destinationLocation = LocationModel(json : json["destinationLocation"])
        estimatedMaxPrice = json["estimatedMaxPrice"].double ?? 0
        estimatedMinPrice = json["estimatedMinPrice"].double ?? 0
        
        estimatedDiscountedMaxPrice = json["estimatedDiscountedMaxPrice"].double ?? 0
        estimatedDiscountedMinPrice = json["estimatedDiscountedMinPrice"].double ?? 0
        additionalChargesAmount = json["additionalChargesAmount"].double ?? 0.0
        print(additionalChargesAmount)
        couponCode = json["couponCode"].string ?? ""
        couponId = json["couponId"].string ?? ""
        additionalChargesLabel = json["additionalChargesLabel"].string ?? ""
        additionalCharges = [AdditionalCharges]()
        if let additionalCharge = json["additionalCharges"].array
        {
            for item in additionalCharge
            {
                let model = AdditionalCharges(json : item)
                additionalCharges.append(model)
            }
        }
    }
    init()
    {
    
    }
}

class AdditionalCharges
{
    var amount : Double!
    var dateTime  :String!
    var reason : String!
    init(json :  JSON)
    {
        amount = json["amount"].double ?? 0
        let localDate = HelperFunction.getCurrentFullDate(date: json["dateTime"].string ?? "")
        let tempDate = HelperFunction.getCurrentDateWithoutDayName(date: localDate)
        let tempTime = HelperFunction.getTime(date: localDate)
        dateTime = String.init(format: "%@ %@", tempDate,tempTime)
        reason = json["reason"].string ?? ""
    }
}






class ShippingItems : NSObject {
    var unitType :String?
    var quantity: Int?
    var width : Int!
    var name : String!
    var weight : Int!
    var images :[String]!
    var height : Int!
    var length : Int!
    var po : String!
    
    init(json : JSON) {
        unitType = json["unitName"].string
        quantity = json["quantity"].int
        width = json["width"].int ?? 0
        name = json["name"].string ?? ""
        po = json["po"].string ?? ""
        weight = json["weight"].int ?? 0
        images = [String]()
        if let imageArray = json["images"].array{
            for item in imageArray {
                images.append(item.string ?? "")
            }
        }
        height = json["height"].int ?? 0
        length = json["length"].int ?? 0
    }
    override var description : String {
        if length == 1 && width == 1 &&  height == 1{
           return String(format:  "Weight: \"%d\"lb",weight)
        }
        return String(format: "H: \"%d\", W: \"%d\", L: \"%d\", Weight: \"%d\"lb",height,width,length,weight )
        
    }
}

class ExtraDestinationLocation
{
    var address : String!
    var destinationAddressUnitNumber  :String!
    var location : LocationModel!
    var podRequired = false
    init(json :  JSON) {
        address = json["address"].string ?? ""
        destinationAddressUnitNumber = json["addressUnitNumber"].string ?? ""
        location = LocationModel(json : json["location"])
        podRequired = json["podRequired"].bool ?? false
    }
}

class ExtraStartLocation
{

    var address : String!
    var startAddressUnitNumber:String!
    var location : LocationModel!
    var shippingItems = [ShippingItems]()
    var instruction : String!
    var userNotes :String!
    init(json :  JSON)
    {
        address = json["address"].string ?? ""
        if let shippingArray = json["shippingItems"].array
        {
            for item in shippingArray{
                shippingItems.append(ShippingItems(json :item))
            }
        }
        location = LocationModel(json : json["location"])
        startAddressUnitNumber = json["addressUnitNumber"].string ?? ""
        instruction = json["instruction"].string ?? ""
        userNotes = json["userNotes"].string ?? ""
        if userNotes.isEmpty{
            userNotes = "No Additional Notes."
        }
    }
}
