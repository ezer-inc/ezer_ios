//
//  OrderHistoryModel.swift
//  EZER
//
//  Created by TimerackMac1 on 18/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
import SwiftyJSON
class OrderHistoryModel
{
    let orderId: String
    let orderNumber: Int
    var status: OrderStatusType
    let startAddress: String
    let destinationAddress: String
    let date: String
    let dateSort: String
    let time: String
    init(json : JSON)
    {
        //let "status": "OPEN",
        orderId = json["orderId"].string ?? ""
        orderNumber = json["orderNumber"].int ?? 0
        status = OrderStatusType(rawValue: json["status"].string ?? "") ?? OrderStatusType.finished
        startAddress = json["startAddress"].string ?? ""
        destinationAddress = json["destinationAddress"].string ?? ""
        dateSort = json["dateSort"].string ?? ""
        
        
        let localDate = HelperFunction.getCurrentFullDate(date: json["dateTime"].string ?? "")

        date = HelperFunction.getCurrentHistoryDate(date: localDate)
        time = HelperFunction.getTime(date: localDate)
//        date = json["date"].string ?? ""
//        time = json["time"].string ?? ""
    }
   
}
