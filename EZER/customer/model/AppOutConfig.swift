//
//  AppOutConfig.swift
//  EZER
//
//  Created by TimerackMac1 on 21/03/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import Foundation
import SwiftyJSON
class AppOutConfig
{
    
    var messageForForceUpdate:String
    var messageForUpdateAvailable:String
    var iosCurrentAppVersion:Float
    var iosMinAppVersion:Float
    //"name":"appOutConfigObject",
    //"androidCurrentAppVersion":"1.0",
    //"androidMinAppVersion":"0.1",
    //"linkAndroid":"https:\/\/play.google.com\/store\/apps\/details?id=com.ezerapp&hl=en",
    //var _id:String,
    var linkiOS:String
    init(json:JSON)
    {
        messageForForceUpdate = json["messageForForceUpdate"].string ?? ""
        messageForUpdateAvailable = json["messageForUpdateAvailable"].string ?? ""
        iosCurrentAppVersion = Float(json["iosCurrentAppVersion"].string ?? "") ?? 0.0
        iosMinAppVersion = Float(json["iosMinAppVersion"].string ?? "") ?? 0.0
        linkiOS = json["linkiOS"].string ?? ""
    }
}
