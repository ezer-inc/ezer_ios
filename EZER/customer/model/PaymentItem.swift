//
//  PaymentItem.swift
//  EZER
//
//  Created by Akash on 03/01/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit
import SwiftyJSON

class PaymentItem: NSObject {

    var defaultValue : Bool!
    var expired : Bool!
    var expirationYear : String!
    var expirationMonth : String!
    var token : String!
    var cardType : String!
    var paymentMethod : String!
    var btCustomerId : String!
    var last4Digit  : String!
    var imageUrl : String!
    var email : String!
    
    init(json :  JSON)
    {
        defaultValue = json["_default"].bool ?? false
        expired = json["expired"].bool ?? false
        expirationYear = json["expirationYear"].string ?? ""
        expirationMonth = json["expirationMonth"].string ?? ""
        token = json["token"].string ?? ""
        cardType = json["cardType"].string ?? ""
        paymentMethod = json["paymentMethod"].string ?? ""
        btCustomerId = json["btCustomerId"].string ?? ""
        last4Digit = json["last4"].string ?? ""
        imageUrl = json["imageUrl"].string ?? ""
        email = json["email"].string ?? ""
    }
}
