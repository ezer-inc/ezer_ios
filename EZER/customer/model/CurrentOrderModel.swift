//
//  CustomerOpenOrder.swift
//  EZER
//
//  Created by TimerackMac1 on 05/01/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//
import SwiftyJSON
import Foundation
class CurrentOrderModel
{
    var orderId: String
    var orderNumber: Int
    var status :OrderStatusType!
    //let status: "OPEN"
    var startAddress : String
    var destinationAddress: String
    var customerId : String
    var driverName : String
    var driverId : String
    var immediate : Bool
    var isTimeout = false
    var startAddressUnitNumber :  String
    var multiWorkOrderStatus : String
    var CurrentStatus : String
    
    init(json: JSON)
    {
        orderId = json["orderId"].string ?? ""
        orderNumber = json["orderNumber"].int ?? 0
        status  = OrderStatusType(rawValue: json["status"].string ?? "TIMEOUT") ?? .timeout
        startAddress = json["startAddress"].string ?? ""
        startAddressUnitNumber = json["startAddressUnitNumber"].string ?? ""
        destinationAddress = json["destinationAddress"].string ?? ""
        customerId = json["customerId"].string ?? ""
        driverName = json["driverName"].string ?? ""
        driverId = json["driverId"].string ?? ""
        immediate = json["immediate"].bool ?? true
        isTimeout = json["isTimeout"].bool ?? false
        multiWorkOrderStatus  = json["MultiWoStatus"].string ?? ""
        CurrentStatus  = json["currentStatus"].string ?? ""
        
    }
    init()
    {
         orderId = ""
         orderNumber = 0
         status = .open
         startAddress = ""
         destinationAddress = ""
         customerId = ""
         driverName = ""
         driverId = ""
         immediate = false
        startAddressUnitNumber = ""
        multiWorkOrderStatus = ""
        CurrentStatus = ""
    }
}
