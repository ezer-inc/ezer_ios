//
//  CustomerOrderModel.swift
//  EZER
//
//  Created by TimerackMac1 on 09/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
import SwiftyJSON
class CustomerOrderModel
{
    let orderNumber :Int
    let orderID:String
    var driverId = ""
    var driverName = ""
    init(json : JSON)
    {
        self.orderID = json["orderId"].string ?? ""
        self.orderNumber =  json["orderNumber"].int ?? 0
    }
}
