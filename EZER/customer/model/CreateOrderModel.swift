//
//  CreateOrderModel.swift
//  EZER
//
//  Created by TimerackMac1 on 06/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftyJSON
class CreateOrderModel
{
    var name = "Default Item"
    var startAddress :String!
    var destinationAddress: String!
    var startCoordinate: CLLocationCoordinate2D!
    var destinationCoordinate :CLLocationCoordinate2D!
    var immediate : Bool = true
    var truckType = TruckTypes.small.rawValue
    var contentDescription :String!
    var totalDistance : Int!
    var estimatedMinPrice : Double!
    var estimatedMaxPrice : Double!
    var estimatedTime : Int!
    var customerId : String!
    var shippingItem : ShippingItem = ShippingItem()
    var isMultiOrder = false
    var isTestOrder = false
    var extraStartLocations = [String]()
    var extraDestinationLocations = [String]()
    var userNotes: String!
    var coupon : Coupon!
    var dateTime : String! = "" // send it to server when immediate is false // scheduled date and time
    var orderId = ""
    var orderDate : Date!
    var startAddressUnitNumber : String? = ""
    var destinationAddressUnitNumber : String? =  ""
    var plateformName : String? = ""
    var model : String? = ""
    var osVersion : String? = ""
    var manufacturer : String? = ""
    var network : String? = ""
    var appVersion : String? = ""
    var appVersionCode : String? = ""
    var customerspecialRate:Double?
    var isSpecialRateApplied = false
    
}

class ShippingItem{
    var name: String = "Default Item"
    var weight: Int!
    let length: Int! = 1
    let width: Int! = 1
    let height: Int! = 1
    var imagesArray : [String]! = [String]()
}
class Coupon{
    var couponId :String!
    var estimatedDiscountedMaxPrice: Double!
    var estimatedDiscountedMinPrice: Double!
    init()
    {
        
    }
    init( json :  JSON)
    {
        couponId = json["couponId"].string ?? ""
        estimatedDiscountedMinPrice = json["estimatedDiscountedMinPrice"].double ?? 0.0
        estimatedDiscountedMaxPrice = json["estimatedDiscountedMaxPrice"].double ?? 0.0
    }
}
