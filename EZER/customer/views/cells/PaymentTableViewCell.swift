//
//  PaymentTableViewCell.swift
//  EZER
//
//  Created by Akash on 03/01/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit

protocol PaymentTableViewCellDelegate {
    func deleteButtonClicked(paymentType : PaymentItem)
}

class PaymentTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewOutlet: UIImageView!
    @IBOutlet weak var cardTypeLblOutlet: UILabel!
    @IBOutlet weak var defaultTickOutlet: UIImageView!
    @IBOutlet weak var deleteMethodButtonOutlet: UIButton!
    @IBOutlet weak var cardnumberLblOutlet: UILabel!
    
    var delegate : PaymentTableViewCellDelegate!
    
    var paymentItem : PaymentItem!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool){
        super.setSelected(false, animated: animated)
        // Configure the view for the selected state
        
    }
    
    func setUpCell(paymentModel : PaymentItem){
        paymentItem = paymentModel
        if let url = URL(string:paymentModel.imageUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? "")
        {
            imageViewOutlet.af_setImage(withURL: url)
        }
        
        if paymentModel.defaultValue{
            defaultTickOutlet.isHidden = false
            deleteMethodButtonOutlet.isHidden = true
        }else{
            defaultTickOutlet.isHidden = true
            deleteMethodButtonOutlet.isHidden = false
        }
        
        if paymentModel.paymentMethod == "Paypal" {
            cardTypeLblOutlet.text = "Paypal"
            cardnumberLblOutlet.text = paymentModel.email
        }else{
            cardTypeLblOutlet.text = paymentModel.cardType
            cardnumberLblOutlet.text = "********\(paymentModel.last4Digit!)"
        }
        
    }
    
    @IBAction func deleteMethodButtonAction(_ sender: Any){
        delegate.deleteButtonClicked(paymentType: paymentItem)
    }
    
}
