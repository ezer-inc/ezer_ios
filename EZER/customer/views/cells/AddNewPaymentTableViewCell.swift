//
//  AddNewPaymentTableViewCell.swift
//  EZER
//
//  Created by Akash on 03/01/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit

protocol AddcardPaymentTableCellDelegate:class{
    func addNewPaymentClicked()
}

class AddNewPaymentTableViewCell: UITableViewCell {

   weak var delegate : AddcardPaymentTableCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func addNewPaymentClicked(_ sender: Any){
        delegate.addNewPaymentClicked()
    }
    
}
