//
//  AddressInfoCell.swift
//  EZER
//
//  Created by TimerackMac1 on 21/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class CustomerAddressInfoCell: UITableViewCell {
    
    @IBOutlet weak var startAddress: UILabel!
    @IBOutlet weak var destinationAddress: UILabel!
    @IBOutlet weak var addressIndex: UILabel!
    @IBOutlet weak var heartIconImageOutlet: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setUpCell(_ indexPath: IndexPath, trackOrder : OrderTrackModel) {
        let count = max(trackOrder.extraStartLocations.count, trackOrder.extraDestinationLocations.count)
        heartIconImageOutlet.isHidden = (count == 0) ? true : false
        addressIndex.isHidden = (count == 0) ? true : false
        addressIndex.text = "\(indexPath.row + 1)"
        
        if indexPath.row == 0{
            startAddress.text = String(format: "%@\n%@",trackOrder.startAddress,trackOrder.startAddressUnitNumber)
            destinationAddress.text = String(format: "%@\n%@",trackOrder.destinationAddress,trackOrder.destinationAddressUnitNumber)
        }
        else{
            let row = indexPath.row - 1
            if trackOrder.extraStartLocations.indices.contains(row)
            {
                startAddress.text = String(format: "%@\n%@",trackOrder.extraStartLocations[row].address,trackOrder.extraStartLocations[row].startAddressUnitNumber)
            }
            else{
                startAddress.text = ""
            }
            if trackOrder.extraDestinationLocations.indices.contains(row)
            {
                destinationAddress.text = String(format: "%@\n%@",trackOrder.extraDestinationLocations[row].address, trackOrder.extraDestinationLocations[row].destinationAddressUnitNumber)
            }
            else{
                destinationAddress.text = ""
            }
        }
    }
}
