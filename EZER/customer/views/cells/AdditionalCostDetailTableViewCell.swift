//
//  AdditionalCostDetailTableViewCell.swift
//  EZER
//
//  Created by Virender on 02/03/19.
//  Copyright © 2019 TimerackMac1. All rights reserved.
//

import UIKit

class AdditionalCostDetailTableViewCell: UITableViewCell {
//OUTLETS
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblReason: UILabel!
    @IBOutlet weak var lblHeading: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func customizeCell(data:AdditionalCharges,index:Int)  {
        lblHeading.text = "  #\(index+1)  Additional Charges"
        lblDate.text = data.dateTime
        lblAmount.text = String.init(format: "$%.0f", data.amount)
        lblReason.text = data.reason
    }
}
