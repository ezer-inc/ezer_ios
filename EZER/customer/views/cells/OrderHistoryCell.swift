//
//  OrderHistory.swift
//  EZER
//
//  Created by TimerackMac1 on 18/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class OrderHistoryCell: UITableViewCell {
    @IBOutlet weak var orderId: UILabel!
    @IBOutlet weak var orderDate: UILabel!
    @IBOutlet weak var orderTime: UILabel!
    @IBOutlet weak var startAddress: UILabel!
    @IBOutlet weak var endAddress: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblStatus.layer.cornerRadius = 5
        lblStatus.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUpCell(historyModel : OrderHistoryModel)
    {
        orderId.text = "Order #\(historyModel.orderNumber)"
        orderDate.text = historyModel.date
        orderTime.text = historyModel.time
        startAddress.text = historyModel.startAddress
        endAddress.text = historyModel.destinationAddress
        lblStatus.text =  historyModel.status.rawValue
        if historyModel.status == .finished{
            lblStatus.backgroundColor = .green
        }else{
            lblStatus.backgroundColor = .red
        }
       
        
    }
//    private func getHeader(_ status : OrderStatusType) -> String
//    {
//        switch status {
//        case .cancelled, .timeout:
//            orderStatus.isHidden = true
//            return " - \"Order Cancelled\""
//        case .finished, .done:
//            orderStatus.isHidden = true
//            return " - \"Delivered\""
//        default:
//            orderStatus.isHidden = false
//            return " - \"Order Pending\""
//        }
//    }
}
