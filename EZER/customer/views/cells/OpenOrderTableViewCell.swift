//
//  OpenOrderTableViewCell.swift
//  EZER
//
//  Created by Akash on 04/01/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit

class OpenOrderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewOrderButton: RoundableButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var trackOrderBar: OrderStatus!
    @IBOutlet weak var callDriverButton: UIButton!
    @IBOutlet weak var orderStatusLabel: UILabel!
    
    @IBOutlet weak var notifyDriverLabel: UILabel!
    @IBOutlet weak var immediateView: UIView!
    @IBOutlet weak var showAddressTextViewOutlet: UILabel!
    @IBOutlet weak var orderNumberOutlet: UILabel!
    var currentOrderProtocol : CurrentOrderProtocol?
    var currentOrderModel : CurrentOrderModel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func  setUpCellData(currentOrderModel :CurrentOrderModel) {
        self.currentOrderModel = currentOrderModel
        self.orderNumberOutlet.text = "Order \(currentOrderModel.orderNumber)"
        // self.showAddressTextViewOutlet.text = currentOrderModel.startAddress
        self.showAddressTextViewOutlet.text = String(format: "%@\n%@",currentOrderModel.startAddress ,currentOrderModel.startAddressUnitNumber).trimWhiteSpace()
        //check if driver id is empty
        if currentOrderModel.driverId.isEmpty
        {
            checkOrderStatus(currentOrderModel: currentOrderModel)
        }else{
            //check if order status is open
            if currentOrderModel.status == OrderStatusType.open{
                checkOrderStatus(currentOrderModel: currentOrderModel)
            }else{
                callDriverButton.isHidden = false
                immediateView.isHidden = true
                notifyDriverLabel.isHidden = true
            }
        }
        orderStatusLabel.text = setOrderStatus(status: currentOrderModel.status, multiWorkOrderStatus: currentOrderModel.CurrentStatus)
    }
    
    func checkOrderStatus(currentOrderModel:CurrentOrderModel)  {
        callDriverButton.isHidden = true
        if currentOrderModel.immediate
        {
            /*immediateView.isHidden = false
             indicator.startAnimating()
             notifyDriverLabel.isHidden = true*/
            if currentOrderModel.isTimeout
            {
                notifyDriverLabel.text = ValidationMessages.rescheduleLater
                notifyDriverLabel.isHidden = false
                immediateView.isHidden = true
                indicator.stopAnimating()
            }else{
                immediateView.isHidden = false
                indicator.startAnimating()
                notifyDriverLabel.isHidden = true
            }
        }
        else{
            notifyDriverLabel.text = ValidationMessages.notifyOrderText
            notifyDriverLabel.isHidden = false
            immediateView.isHidden = true
        }
    }
    
    private func setOrderStatus(status: OrderStatusType, multiWorkOrderStatus: String) -> String{
        var orderStatus = ""
        var statusViewIndex = 0
        switch status {
        case .open:
            statusViewIndex = 0
            orderStatus = "Open"
        case .driverReview:
            statusViewIndex = 1
            //            trackOrderBar.firstLabel.text = "Assigned"
            orderStatus = "Assigned"
        case .accepted, .assigned:
            statusViewIndex = 1
            //            trackOrderBar.firstLabel.text = "Driver Away"
            orderStatus = "Accepted"
        case .startLoading:
            //            trackOrderBar.secondLabel.text = "Driver On Route"
            orderStatus = "Loading"
            statusViewIndex = 2
        case .onTheWay, .onRouteToOrigin:
            //            trackOrderBar.secondLabel.text = "Driver On Route"
            orderStatus = "On Route to Pick Up Location"
            statusViewIndex = 2
        case .atSource, .arrievedToOrigin:
            //            trackOrderBar.secondLabel.text = "Driver Arrived"
            orderStatus = "Arrived at the Pick Up Location"
            statusViewIndex = 2
        case .loaded, .loading:
            //            trackOrderBar.thirdLabel.text = "Driver On Route"
            orderStatus = "Loaded"
            statusViewIndex = 3
        case .inTransit, .onRouteDestination:
            //            trackOrderBar.thirdLabel.text = "Driver On Route"
            orderStatus = "On Route to drop off"
            statusViewIndex = 3
        case .multi_pickup_transit:
            //            trackOrderBar.thirdLabel.text = "Driver On Route"
            orderStatus = multiWorkOrderStatus
            
            guard let multiStatus = OrderStatusType(rawValue: orderStatus) else{
                return orderStatus
            }
            switch multiStatus
            {
            case .onTheWay, .onRouteToOrigin:
                orderStatus = "On Route to Pick Up Location"
                statusViewIndex = 2
            case .atSource, .arrievedToOrigin:
                orderStatus = "Arrived at the Pick Up Location"
                statusViewIndex = 2
            case .startLoading:
                orderStatus = "Loading"
                statusViewIndex = 2
            case .loaded, .loading :
                orderStatus = "Loaded"
                statusViewIndex = 3
                
            default:
               break
            
            }
            statusViewIndex = 3
        case .multi_drop_transit:
            //            trackOrderBar.fourthLabel.text = "Driver On Route"
            orderStatus = multiWorkOrderStatus
            guard let multiStatus = OrderStatusType(rawValue:orderStatus) else{
                return orderStatus
            }
            switch multiStatus
            {
            case .inTransit, .onRouteDestination:
                orderStatus = "On Route to drop off"
                statusViewIndex = 3
            case .atDestination, .arrivedAtDestination:
                orderStatus = "Arrived at the drop off"
                statusViewIndex = 4
            case .beforeUnload:
                orderStatus = "Unloading"
                statusViewIndex = 4
        
            case .unloaded, .unLoaded:
                orderStatus = "Unloaded"
                statusViewIndex = 4
            default:
                break
            }
            statusViewIndex = 4
        case .atDestination, .arrivedAtDestination:
            //            trackOrderBar.fourthLabel.text = "Driver At Destination"
            orderStatus = "Arrived at the drop off"
            statusViewIndex = 4
        case .beforeUnload:
            //            trackOrderBar.secondLabel.text = "Driver On Route"
            orderStatus = "Unloading"
            statusViewIndex = 4
        case .unloaded, .unLoaded:
            //            trackOrderBar.fourthLabel.text = "Delivery Completed"
            orderStatus = "Unloaded"
            statusViewIndex = 4
        case .finished:
            orderStatus = "Completed"
            statusViewIndex = -1
            statusViewIndex = 4
            
        case .done:
            orderStatus = "Completed"
            statusViewIndex = -1
            statusViewIndex = 4
        case .cancelled:
            statusViewIndex = -1
            orderStatus = "Cancelled"
            
        case .timeout:
            orderStatus = "Timed Out"
        case .pause:
            orderStatus = "Pause"
        default:
            break
        }
        trackOrderBar.orderStatus = statusViewIndex
        return orderStatus
    }
    
    @IBAction func trackDriverClicked(_ sender: Any) {
        currentOrderProtocol?.trackDriverClicked(currentOrderModel: currentOrderModel)
    }
    
    @IBAction func viewOrderClicked(_ sender: Any) {
        currentOrderProtocol?.viewOrderClicked(currentOrderModel: currentOrderModel)
    }
    @IBAction func callOrderClicked(_ sender: Any) {
        currentOrderProtocol?.callOrderClicked(currentOrderModel: currentOrderModel)
    }
    
    
}
protocol CurrentOrderProtocol:class
{
    func trackDriverClicked(currentOrderModel : CurrentOrderModel)
    func viewOrderClicked(currentOrderModel : CurrentOrderModel)
    func callOrderClicked(currentOrderModel : CurrentOrderModel)
}
