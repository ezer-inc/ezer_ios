//
//  AddressInfoCell.swift
//  EZER
//
//  Created by TimerackMac1 on 21/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class RescheduleOrderCell: UITableViewCell {

    @IBOutlet weak var backgroundColorView: UIView!
    @IBOutlet weak var headerheight: NSLayoutConstraint!
    @IBOutlet weak var indexView: UIView!
    @IBOutlet weak var addressIndex: UILabel!
    @IBOutlet weak var destinationAddress: UILabel!
    @IBOutlet weak var startAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColorView.backgroundColor = UIColor.clear
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUpCell(_ indexPath: IndexPath, trackOrder : OrderTrackModel)
    {
        addressIndex.text = "\(indexPath.row + 1)"
        let count = max(trackOrder.extraStartLocations.count,trackOrder.extraDestinationLocations.count)
        indexView.isHidden = (count == 0) ? true : false
        if indexPath.row == 0{
            startAddress.text = trackOrder.startAddress
            destinationAddress.text = trackOrder.destinationAddress
            headerheight.constant = 30
        }
        else{
            headerheight.constant = 0
            let row = indexPath.row - 1
            if trackOrder.extraStartLocations.indices.contains(row)
            {
                startAddress.text = trackOrder.extraStartLocations[row].address
            }
            else{
                startAddress.text = ""
            }
            if trackOrder.extraDestinationLocations.indices.contains(row)
            {
                destinationAddress.text = trackOrder.extraDestinationLocations[row].address
            }
            else{
                destinationAddress.text = ""
            }
        }
    }
}
