//
//  YouKnowLabel.swift
//  EZER
//
//  Created by TimerackMac1 on 16/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class YouKnowTextField: UITableViewCell {

    @IBOutlet weak var hearByText: UITextField!
    @IBOutlet weak var selectValue: UIButton!
    var delegate:HowDidYouHear!
     var index:Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       hearByText.addDoneOnKeyboardWithTarget(self, action: #selector(self.selectRowValue(_:)), shouldShowPlaceholder: true)
       //hearByText.addTarget(self, action: #selector(editingDidEnd(_:)), for: .editingDidEnd)
        
    }
    @objc func editingDidEnd(_ textField: TJTextField) {
       selectRowValue(textField)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func selectRowValue(_ sender : UITextField!) {
        selectValue.isSelected = true
        hearByText.resignFirstResponder()
        if(delegate?.indexSelected != -1)
        {
            delegate?.tableView(delegate.tableView, didDeselectRowAt: IndexPath.init(row: delegate.indexSelected, section: 0))
        }
        delegate?.tableView(delegate.tableView, didSelectRowAt: IndexPath.init(row: index, section: 0))
    }
    


}
