//
//  ImageUploadingCell.swift
//  EZER
//
//  Created by TimerackMac1 on 05/12/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class ImageUploadingCell: UICollectionViewCell {

    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var deleteImage: UIButton!
    @IBOutlet weak var uploadedImage: UIImageView!
    weak var delegate : DeleteImageDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func deleteImage_Action(_ sender: Any) {
        delegate.deleteImageAtIndex(tag: deleteImage.tag)
    }
    func setUpImage(stringUrl:String) {
        indicator.startAnimating()
        uploadedImage.image = nil
        if let url = URL(string: stringUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? "")
        {
            if url.pathExtension == "pdf"{
                uploadedImage.image = #imageLiteral(resourceName: "ic_pdf_icon")
                self.indicator.stopAnimating()
            }else{
                uploadedImage.sd_setImage(with: url)
                uploadedImage.af_setImage(withURL: url, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), completion: { (response) in
                    self.indicator.stopAnimating()
                })
            }
           //age(withURL: url)
        }else{
            indicator.stopAnimating()
        }
    }
}
protocol DeleteImageDelegate:class
{
    func deleteImageAtIndex(tag : Int)
}
