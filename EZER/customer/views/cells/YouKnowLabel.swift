//
//  YouKnowLabel.swift
//  EZER
//
//  Created by TimerackMac1 on 16/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class YouKnowLabel: UITableViewCell {

    @IBOutlet weak var selectValue: UIButton!
    @IBOutlet weak var hearBy: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
