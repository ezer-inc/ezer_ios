//
//  StartViewController.swift
//  EZER
//
//  Created by TimerackMac1 on 15/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import CoreLocation
class StartViewController: UIViewController {
    
    //MARK: - Enum
    enum ActiveAreaFor{
        case customerLogin
        case customerRegister
        case driverLogin
        case driverRegister
    }
    
    //MARK: - IBOutlets
    @IBOutlet weak var aboutInfoView: UIView!
    @IBOutlet weak var lblVersion: UILabel!
    
    //MARK: - Custom varialbles
    var locationManager : LocationManager!
    var checkingActiveAreaFor : ActiveAreaFor = ActiveAreaFor.customerLogin
    
    //MARK:- override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.tintColor = UIColor.white
        locationManager = LocationManager(delegate: self)
        
        checkAppVersion(showLoader: true)
        //set appversion
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        lblVersion.text = String.init(format: "Version: %@", appVersionString)
//        Crashlytics.sharedInstance().crash()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBActions
    @IBAction func handleButtonClick(_ sender: RoundableButton) {
        switch sender.tag{
        case 1: //customer login
            //self.showProgressBar(header: AppName, footer: ValidationMessages.gettingLocation)
            checkingActiveAreaFor = ActiveAreaFor.customerLogin
            //locationManager.startUpdatingLocation()
            
        case 2: // customer registration
            checkingActiveAreaFor = ActiveAreaFor.customerRegister
            //locationManager.startUpdatingLocation()
            
        case 3://driver login
            checkingActiveAreaFor = ActiveAreaFor.driverLogin
            /*let loginCon = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Main.loginViewCon) as! LoginViewController
             loginCon.loginType = .driver
             self.navigationController?.pushViewController(loginCon, animated: true)*/
            break
        case 4://driver registration
            checkingActiveAreaFor = ActiveAreaFor.driverRegister
            /*let driverLoginSplash = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.driverLoginSplash) as! DriverLoginSplash
             self.navigationController?.pushViewController(driverLoginSplash, animated: true)*/
        default:
            break
        }
        locationManager.startUpdatingLocation()
    }
    
    @IBAction func quickPriceEstimationAction(_ sender: Any) {
        let loginCon = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Main.estimatedPriceViewController) as! EstimatedPriceViewController
        self.navigationController?.pushViewController(loginCon, animated: true)
    }
    
    
    //MARK:- Private Methods
    private func checkActiveArea(cordinate: CLLocationCoordinate2D)
    {
        
        let params :[String:Any] = [
            "lng":cordinate.longitude,
            "lat":cordinate.latitude
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Main.checkActiveArea, jsonToPost: params, embedAuthTocken: false) { (json) in
            if let json = json
            {
                let checkAreaModel = CheckActiveAreaModel(json : json)
                if (checkAreaModel.addressAvailable){
                    if self.checkingActiveAreaFor == .customerLogin // check in login type
                    {
                        let loginCon = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Main.loginViewCon) as! LoginViewController
                        self.navigationController?.pushViewController(loginCon, animated: true)
                    }else
                    {
                        let customerStoryBoard = UIStoryboard.init(name: StoryboardNames.customer, bundle: nil)
                        let registerCon = customerStoryBoard.instantiateViewController(withIdentifier: IdentifierName.Customer.customerRegistration) as! CustomerRegistration
                        self.navigationController?.pushViewController(registerCon, animated: true)
                    }
                }else{
                    let ezerNotActive = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Main.ezerNotActive) as! EzerNotActiveController
                    self.navigationController?.pushViewController(ezerNotActive, animated: true)
                }
            }
        }
    }
}

//MARK:- LocationManagerDelegate extension
extension StartViewController : LocationManagerDelegate
{
    func locationDenied() {
        
    }
    
    func didChangeinLocation(cordinate: CLLocationCoordinate2D){
        DispatchQueue.main.async {
            if self.checkingActiveAreaFor == .driverLogin
            {
                let loginCon = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Main.loginViewCon) as! LoginViewController
                loginCon.loginType = .driver
                self.navigationController?.pushViewController(loginCon, animated: true)
            }else if self.checkingActiveAreaFor == .driverRegister{
                /* let driverLoginSplash = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Driver.driverLoginSplash) as! DriverLoginSplash
                 self.navigationController?.pushViewController(driverLoginSplash, animated: true) */
                let driverStoryBoard = UIStoryboard.init(name: StoryboardNames.driver, bundle: nil)
                let driverRegistrationCon = driverStoryBoard.instantiateViewController(withIdentifier: IdentifierName.Driver.driverRegister) as! DriverRegistration
                self.navigationController?.pushViewController(driverRegistrationCon, animated: true)
            }else{ // in case of driver we did not need to check active area
                self.checkActiveArea(cordinate:cordinate)
            }
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    func didErrorinLocation(error: Error){
        DispatchQueue.main.async {
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    func locationNotAvailable(){
        DispatchQueue.main.async {
            if let _ = self.presentedViewController { return }
            self.showLocationAlert()
        }
    }
    
    func locationFaliedToUpdate(status:CLAuthorizationStatus)
    {
        DispatchQueue.main.async {
            self.showLocationAlert()
        }
    }
    
}
