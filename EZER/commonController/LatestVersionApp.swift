//
//  LatestVersionApp.swift
//  EZER
//
//  Created by TimerackMac1 on 21/03/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit

class LatestVersionApp: UIViewController {
   
    //MARK:- Custom Variables
    var appOutConfig : AppOutConfig?
   
    //MARK:- override methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBActions
    @IBAction func openItunes(_ sender: RoundableButton) {
            HelperFunction.openStringUrl(appOutConfig!.linkiOS)
    }
}
