//
//  DecideViewController.swift
//  
//
//  Created by TimerackMac1 on 06/01/18.
//

import UIKit
import Reachability
class DecideViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var retryButton: RoundableButton!
    
    //MARK:- override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        do{
            if try Reachability().connection == .unavailable {
                if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType) {
                    HelperFunction.openDriver(driverLoginModel: DriverLoginModel(), viewController: self, isOffline: true)
                    return
                }
                
                showAlertController( message: ValidationMessages.noInternet)
                retryButton.isHidden = false
                return
            }
        }catch{
            print("could not start reachability notifier")
            showAlertController( message: ValidationMessages.noInternet)
            retryButton.isHidden = false
            return
        }
        loginUser()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBActions
    @IBAction func retryDialog(_ sender: RoundableButton) {
        
        loginUser()
    }
    
    //MARK: - Private Methods
    private func openStartPage() {
        HelperFunction.clearUserDefault()
        let storyboard = UIStoryboard(name: StoryboardNames.main, bundle: nil)
        let mainViewController = storyboard.instantiateInitialViewController()
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.backgroundColor = UIColor.driverTheme
        delegate.window?.rootViewController = mainViewController!
        delegate.window?.makeKeyAndVisible()
    }
    
    private func openDriverInformationController(driverLoginModel : DriverLoginModel){
        let storyboard = UIStoryboard(name: StoryboardNames.driver, bundle: nil)
        let driverInfoController = storyboard.instantiateViewController(withIdentifier:IdentifierName.Driver.driverInformation) as! DriverInformationController
        driverInfoController.driverLoginModel = driverLoginModel
        
        self.navigationController?.pushViewController(driverInfoController, animated: true)
    }
    
    //MARK: - Public Methods
    func loginUser() {
        
        var url = ServerUrls.Customer.loginUser
        if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType){
            url = ServerUrls.Driver.login
        }
        let params :[String:Any] = [
            "email":HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.email),
            "password":HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.password),
            "deviceType": "ios",
            "model": UIDevice.modelName,
            "appVersion": HelperFunction.getVersion(),
            "deviceToken": HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.deviceToken)
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: url, jsonToPost: params ,isResponseRequired: true, embedAuthTocken: false) { (json) in
            if let json = json
            {
                if json[ServerKeys.status].int  == ServerStatusCode.Failure
                {
                    self.openStartPage()
                    return
                }
                HelperFunction.saveValueInUserDefaults(key: ProfileKeys.authToken, value: json[ServerKeys.data][ProfileKeys.authToken].string ?? "")
                
              //  SocketManager.sharedInstance.startSocket()
                if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType) // check in login type
                {
                    
                    let model = DriverLoginModel(json: json[ServerKeys.data])
                    HelperFunction.saveDriverInPref(loginModel: model)
                    if model.personalInfoVerified && model.vehicleInfoVerified && model.backgroundCheck == "ACCEPT"{
                        HelperFunction.openDriver(driverLoginModel: model, viewController: self)
                    }else{
                        self.openDriverInformationController(driverLoginModel: model)
                    }
                }
                else
                {
                    let model = CustomerLoginModel(json: json[ServerKeys.data])
                    if model.btCustomerId.isEmpty
                    {
                        HelperFunction.saveCustomerInPref(loginModel:model)
                        let payment = AddNewPayment(nibName: "AddNewPayment", bundle: nil)
                        self.navigationController?.pushViewController(payment, animated: false)
                    }
                    else if model.isEnabled{ // customer is disable by admin
                        HelperFunction.saveCustomerInPref(loginModel:model)
                        HelperFunction.openCustomer(viewController: self, isCalledFromCache: true, loginModel: model)
                    }
                    else{
                        let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                            self.openStartPage()
                        })
                        self.showAlertController(heading: AppName, message: ValidationMessages.disabledUser, action: action)
                    }
                }
                
            }
            else{
                self.retryButton.isHidden = false
            }
        }
        
    }
}
