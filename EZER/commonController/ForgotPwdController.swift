//
//  ForgotPwdController.swift
//  EZER
//
//  Created by TimerackMac1 on 15/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class ForgotPwdController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var resetPwdOutlet: RoundableButton!
    @IBOutlet weak var emailField: TJTextField!
    
    //MARK:- Custom variables
    var loginType = LoginType.customer
    var currentColor = UIColor.customerTheme
    
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Forgot Password?"
        if loginType == .driver
        {
            currentColor = UIColor.driverTheme
        }
        emailField.delegate = self
        self.navigationController?.navigationBar.barTintColor = currentColor
        backgroundView.backgroundColor = currentColor
        resetPwdOutlet.backgroundColor = currentColor
        emailField.errorEntry = false
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBActions
    @IBAction func resetPwdAction(_ sender: RoundableButton) {
        if !HelperFunction.validateEmail(email: emailField.text!){
            emailField.errorEntry = true
            return
        }
        let params :[String:Any] = [
            "email":emailField.text!
        ]
        
        var url = ServerUrls.Customer.forgotPassword
        if(loginType == .driver)
        {
            url = ServerUrls.Driver.forgotPassword
        }
        BaseServices.SendPostJson(viewController: self, serverUrl: url, jsonToPost: params, embedAuthTocken: false) { (json) in
            if let json = json
            {
                let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })
                self.showAlertController(heading: AppName, message: json[ServerKeys.message].string!, action: action)
            }
        }
    }
}

//MARK:- UITextField delegates
extension ForgotPwdController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        emailField.errorEntry = false
    }
}
