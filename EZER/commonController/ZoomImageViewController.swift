//
//  ZoomImageViewController.swift
//  EZER
//
//  Created by Virender on 18/09/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//soc

import UIKit
import WebKit

protocol zoomImageViewControllerDelegate:class{
    func dismissPresentViewController()
}
class ZoomImageViewController: UIViewController{

    //MARK:- IBOutlets
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK:- Custom variables
    weak var delegate:zoomImageViewControllerDelegate?
    var urlString = ""
    var image: Data?
    var mineType = "image/jpeg"
    
    //MARK:- override methods
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //self.zoomImageView.image = itemImage
        //self.zoomImageView.contentMode = .left
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.barTintColor = UIColor.driverTheme
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        webView.navigationDelegate = self
        
        if let image = image {
            webView.load(image, mimeType: mineType, characterEncodingName: "", baseURL: URL(fileURLWithPath: ""))
        }else {if let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) ?? "") {
            let requestObj = URLRequest(url: url)
            activityIndicator.startAnimating()
            webView.load(requestObj)
        }
        }
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBActions
    @IBAction func btnCrossAction(_ sender: Any) {
         self.navigationController?.navigationBar.isHidden = false
        delegate?.dismissPresentViewController()
    }
    
}

//MARK:- WKNavigationDelegate extension
extension ZoomImageViewController:WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
}
