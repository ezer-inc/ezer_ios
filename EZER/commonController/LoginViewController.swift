//
//  LoginViewController.swift
//  EZER
//
//  Created by TimerackMac1 on 15/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoginViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var out_LoginButton: RoundableButton!
    @IBOutlet weak var passwordField: TJTextField!
    @IBOutlet weak var emailField: TJTextField!
    
    //MARK:- Custom variables
    var loginType = LoginType.customer
    
    //MARK:- Override methods    
    override func viewDidLoad() {
        super.viewDidLoad()
        if loginType == .driver
        {
            out_LoginButton.backgroundColor = UIColor.driverTheme
        }
        passwordField.addTarget(self, action: #selector(edtingChanged(_:)), for: .editingChanged)
        emailField.addTarget(self, action: #selector(edtingChanged(_:)), for: .editingChanged)
        emailField.addTarget(self, action: #selector(enterPressed), for: .editingDidEndOnExit)
        passwordField.addTarget(self, action: #selector(enterPressed), for: .editingDidEndOnExit)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        // Do any additional setup after loading the view.
       // var img:UIImage = UIImage(imageLiteralResourceName: "name")
//        if loginType == .customer{
//            emailField.text = "k1@tr.com"
//            passwordField.text = "123456"
//        }else{
//            emailField.text = "k3@tr.com"
//            passwordField.text = "123456"
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Custom methods
    @objc func enterPressed(_ textField : UITextField){
        //do something with typed text if needed
        if textField == emailField{
            passwordField.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
    }
    
    @objc func edtingChanged(_ textField: TJTextField) {
        textField.errorEntry = false
    }
    
    func checkValidation()-> Bool
    {
        var error = false
        if !HelperFunction.validateEmail(email:emailField.text!)
        {
            emailField.errorEntry = true
            error = true
        }
        if passwordField.text!.trimWhiteSpace().isEmpty
        {
            passwordField.errorEntry = true
            error = true
        }
        return error
    }
    
    //MARK:- Private Methods
    private func openDriverInformationController(driverLoginModel : DriverLoginModel){
        let storyboard = UIStoryboard(name: StoryboardNames.driver, bundle: nil)
        let driverInfoController = storyboard.instantiateViewController(withIdentifier:IdentifierName.Driver.driverInformation) as! DriverInformationController
        driverInfoController.driverLoginModel = driverLoginModel
        
        self.navigationController?.pushViewController(driverInfoController, animated: true)
    }
    
    //MARK:- IBActions
    @IBAction func loginUser(_ sender: UIButton) {
        
        if checkValidation() { return }
        var url = ServerUrls.Customer.loginUser
        if loginType == .driver
        {
            url = ServerUrls.Driver.login
        }
        
        let params :[String:Any] = [
            "email":emailField.text!,
            "password":passwordField.text!,
            "deviceType": "ios",
            "model": UIDevice.modelName,
            "appVersion": HelperFunction.getVersion(),
            "deviceToken": HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.deviceToken)
        ]
        BaseServices.SendPostJson(viewController: self, serverUrl: url, jsonToPost: params, embedAuthTocken: false) { (json) in
            if let json = json
            {
                print(json)
                HelperFunction.saveValueInUserDefaults(key: ProfileKeys.authToken, value: json[ServerKeys.data][ProfileKeys.authToken].string ?? "")
                HelperFunction.saveValueInUserDefaults(key: ProfileKeys.email, value: self.emailField.text!)
                HelperFunction.saveValueInUserDefaults(key: ProfileKeys.password, value: self.passwordField.text!)
                SocketManager.sharedInstance.startSocket()
                if self.loginType == .driver {
                    let model = DriverLoginModel(json: json[ServerKeys.data])
                    HelperFunction.saveDriverInPref(loginModel: model)
                    if model.personalInfoVerified && model.vehicleInfoVerified && model.backgroundCheck == "ACCEPT"{
                        HelperFunction.openDriver(driverLoginModel: model, viewController: self)
                    }
                    else{
                        self.openDriverInformationController(driverLoginModel: model)
                    }
                }
                else {
                    let model = CustomerLoginModel(json: json[ServerKeys.data])
                    if model.btCustomerId.isEmpty
                    {
                        HelperFunction.saveCustomerInPref(loginModel:model)
                        let payment = AddNewPayment(nibName: "AddNewPayment", bundle: nil)
                        self.navigationController?.pushViewController(payment, animated: false)
                    }
                    else if model.isEnabled{ // customer is disable by admin
                        HelperFunction.saveCustomerInPref(loginModel:model)
                        HelperFunction.openCustomer(viewController: self, loginModel: model)
                    }
                    else{
                        let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                            self.navigationController?.popViewController(animated: true)
                        })
                        self.showAlertController(heading: AppName, message: ValidationMessages.disabledUser, action: action)
                    }
                }
            }
        }
    }
    
    @IBAction func forgotPasswordAction(_ sender: UIButton) {
        let forgotPassword = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Main.forgotPassword) as! ForgotPwdController
        forgotPassword.loginType = loginType
        self.navigationController?.pushViewController(forgotPassword, animated: true)
    }
    
    @IBAction func openTermsAndPrivacy(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: IdentifierName.Main.termsAndPrivacy) as! TermsPrivacyController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

