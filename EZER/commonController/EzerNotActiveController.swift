//
//  NotActiveAreaController.swift
//  EZER
//
//  Created by TimerackMac1 on 29/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class EzerNotActiveController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var emailField: TJTextField!
//    @IBOutlet weak var verticalSpace: NSLayoutConstraint!
//    @IBOutlet weak var emailMe: UIButton!
//    @IBOutlet weak var pushNotifyMe: UIButton!
    @IBOutlet weak var zipCode: TJTextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK:- override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
        zipCode.delegate = self
        zipCode.addTarget(self, action: #selector(edtingChanged(_:)), for: .editingChanged)
        emailField.addTarget(self, action: #selector(edtingChanged(_:)), for: .editingChanged)
        emailField.addTarget(self, action: #selector(enterPressed), for: .editingDidEndOnExit)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction
//    @IBAction func  emailMeAction(_ sender: UIButton) {
//        sender.isSelected = !sender.isSelected
//        emailField.isHidden = !sender.isSelected
//        verticalSpace.constant = (emailField.isHidden) ? 25 : 55
//    }
//    @IBAction func sendmail(_ sender: UIButton) {
//        HelperFunction.openMailApp(email: ServerUrls.customerEmail)
//    }
    
    @IBAction func pushNotifyAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func submitRecord(_ sender: RoundableButton) {
        var deviceToken = HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.deviceToken)
        if deviceToken.isEmpty{
            deviceToken = "noDeviceToken"
        }
        if checkValidation() {
            let params :[String:Any] = [
                "zipCode":zipCode.text!,
                "allowEmail": true,
                "email": emailField.text!,
                "allowPush": false,
                "installationId": deviceToken,
                "deviceType":"ios"
            ]
            
            BaseServices.SendPostJson(viewController: self, serverUrl: ServerUrls.Main.insertInterestedUser, jsonToPost: params, embedAuthTocken: false){ (json) in
                if let _ = json
                {
                    let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    self.showAlertController(heading: AppName, message: ValidationMessages.thanksUser, action: action)
                    
                }
            }
            
        }
    }
    
    //MARK:- Custom methods
    @objc func enterPressed(_ textField : UITextField){
        //do something with typed text if needed
        textField.resignFirstResponder()
    }
    
    @objc func edtingChanged(_ textField: TJTextField) {
        textField.errorEntry = false
    }
    
    //MARK:- private methods
    private func checkValidation()->Bool
    {
        var isValid = true
        if !HelperFunction.isValidZip(testStr: zipCode.text!)
        {
            zipCode.errorEntry = true
            isValid = false
        }
//        if emailMe.isSelected
//        {
            if !HelperFunction.validateEmail(email:emailField.text!)
            {
                emailField.errorEntry = true
                isValid = false
            }
//        }
        return isValid
    }
}
extension EzerNotActiveController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == zipCode{
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= IdentifierName.zipCode
        }
        return true
    }
}
