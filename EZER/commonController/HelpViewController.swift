//
//  HelpViewController.swift
//  EZER
//
//  Created by TimerackMac1 on 30/03/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var helpMessageOutlet: UILabel!
    @IBOutlet weak var emailButtonOutlet: RoundableButton!
  
    //MARK:- Custom variables
    var supportEmailId=ServerUrls.driverSupportEmail
    
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if LoginType.customer.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType)
        {
            supportEmailId = ServerUrls.customerEmail
            emailButtonOutlet.backgroundColor = UIColor.customerTheme
            helpMessageOutlet.text = "Please write us on\n\(ServerUrls.customerEmail)"
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBActions
    @IBAction func openEmail(_ sender: RoundableButton) {
        self.dismiss(animated: true, completion: nil)
        HelperFunction.openMailApp(email: supportEmailId)
    }
    
    @IBAction func dismissViewController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
