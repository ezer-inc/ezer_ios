//
//  TermsPrivacyController.swift
//  EZER
//
//  Created by TimerackMac1 on 15/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit
import WebKit

class TermsPrivacyController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var termsWebView: WKWebView!
    
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Terms And Privacy"
        termsWebView.navigationDelegate = self
        let url = URL(string: ServerUrls.termsAndPrivacyUrl)!
        indicator.startAnimating()
        termsWebView.load(URLRequest(url: url ))
        // Do any additional setup after loading the view.
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension TermsPrivacyController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        indicator.stopAnimating()
    }
}
