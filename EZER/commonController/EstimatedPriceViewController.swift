//
//  EstimatedPriceViewController.swift
//  EZER
//
//  Created by Virender on 24/11/18.
//  Copyright © 2018 TimerackMac1. All rights reserved.
//

import UIKit

class EstimatedPriceViewController: UIViewController {
    
    //MARK:- IBOutlets
  
    @IBOutlet weak var emailField: TJTextField!
    
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Price Estimation"
        emailField.addTarget(self, action: #selector(edtingChanged(_:)), for: .editingChanged)
        emailField.addTarget(self, action: #selector(enterPressed), for: .editingDidEndOnExit)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 0.5, blue: 0.25, alpha: 1)
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Custom methods
    @objc func enterPressed(_ textField : UITextField){
        textField.resignFirstResponder()
    }
    
    @objc func edtingChanged(_ textField: TJTextField) {
        textField.errorEntry = false
        if HelperFunction.validateEmail(email:emailField.text!)
        {
          
        }else{
          
        }
    }
    
    
}
