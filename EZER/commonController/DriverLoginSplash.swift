//
//  DriverLoginSplash.swift
//  EZER
//
//  Created by TimerackMac1 on 22/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class DriverLoginSplash: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageB: UIImageView!
    @IBOutlet weak var imageA: UIImageView!
    @IBOutlet weak var filledPageControl: FilledPageControl!
    
    //MARK:- Custom variables
    let textArray = ["Have a truck\nDrive with EZER",
                     "Be your own boss on your own schedule",
                     "Make extra cash while leading a helping hand"]
    var timer :Timer!
    
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        //imageArray = [imageA,imageB]
        scrollView.delegate = self
        scrollView.contentSize.width = UIScreen.main.bounds.width*3
        let width = UIScreen.main.bounds.width
        scrollView.isPagingEnabled = true
        self.imageA.frame.origin.x = UIScreen.main.bounds.width
        self.imageB.frame.origin.x = 2*UIScreen.main.bounds.width
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
        for index in  0...2
        {
            let label = UILabel.init(frame: CGRect(x:CGFloat(index) * width,y:UIScreen.main.bounds.height - 230,width:width,height:60))
            scrollView.addSubview(label)
            label.text = textArray[index]
            label.textAlignment = .center
            label.textColor = .white
            label.font = UIFont(name: FontNames.regular, size: 15)
            label.numberOfLines = 0
            //label.ius
        }
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.navigationBar.barTintColor = UIColor.clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(scrollPage), userInfo: nil, repeats: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if timer.isValid
        {
            timer.invalidate()
        }
    }
    
    //MARK:-Custom methods
    @objc private func scrollPage()
    {
        var page = scrollView.contentOffset.x / scrollView.bounds.width
        var frame = CGRect.init(x: scrollView.frame.minX, y: scrollView.frame.minY, width: scrollView.frame.size.width, height: scrollView.frame.size.height)
        
        if page == 2
        {
            page = 0
        }else
        {
            page += 1
        }
        
        frame.origin.x = frame.size.width * page
        scrollView.setContentOffset(CGPoint.init(x: frame.minX, y: scrollView.frame.minY), animated: true)
        //scrollViewDidScroll(scrollView)
    }
    
    //MARK:-IBActions
    @IBAction func loginAction(_ sender: RoundableButton) {
        let driverStoryBoard = UIStoryboard.init(name: StoryboardNames.driver, bundle: nil)
        let driverRegistrationCon = driverStoryBoard.instantiateViewController(withIdentifier: IdentifierName.Driver.driverRegister) as! DriverRegistration
        self.navigationController?.pushViewController(driverRegistrationCon, animated: true)
    }
    
}

//MARK:- UIScrollViewDelegate extensions
extension DriverLoginSplash : UIScrollViewDelegate
{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x / scrollView.bounds.width
        filledPageControl.progress = page
        if scrollView.contentOffset.x > UIScreen.main.bounds.width{
            self.imageA.frame.origin.x = 0
        }else{
            self.imageA.frame.origin.x = UIScreen.main.bounds.width - scrollView.contentOffset.x
        }
        self.imageB.frame.origin.x = CGFloat(2)*UIScreen.main.bounds.width - scrollView.contentOffset.x
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
    }
}
