//
//  ChangePwdController.swift
//  EZER
//
//  Created by TimerackMac1 on 25/11/17.
//  Copyright © 2017 TimerackMac1. All rights reserved.
//

import UIKit

class ChangePwdController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var changePwdButton: RoundableButton!
    @IBOutlet weak var confirmPassword: TJTextField!
    @IBOutlet weak var newPassword: TJTextField!
    @IBOutlet weak var oldPassword: TJTextField!
    
    //MARK:- Override  methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Change Password"
        if LoginType.driver.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType)
        {
            changePwdButton.backgroundColor = UIColor.driverTheme
        }
        confirmPassword.addTarget(self, action: #selector(edtingChanged(_:)), for: .editingChanged)
        confirmPassword.addTarget(self, action: #selector(enterPressed), for: .editingDidEndOnExit)
        newPassword.addTarget(self, action: #selector(edtingChanged(_:)), for: .editingChanged)
        newPassword.addTarget(self, action: #selector(enterPressed), for: .editingDidEndOnExit)
        oldPassword.addTarget(self, action: #selector(edtingChanged(_:)), for: .editingChanged)
        oldPassword.addTarget(self, action: #selector(enterPressed), for: .editingDidEndOnExit)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- Custom methods
    @objc func enterPressed(_ textField : UITextField){
        //do something with typed text if needed
        if textField == oldPassword{
            newPassword.becomeFirstResponder()
        }
        else if textField == newPassword{
            confirmPassword.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        
    }
    
    @objc func edtingChanged(_ textField: TJTextField) {
        textField.errorEntry = false
    }
    
    //MARK:- Private methods
    private func checkValidation()-> Bool
    {
        var isValidationFails : Bool = false
        if oldPassword.text!.trimWhiteSpace().isEmpty
        {
            isValidationFails = true
            oldPassword.errorEntry = true
            return isValidationFails
        }
        if (!HelperFunction.validatePassword(password:newPassword.text!)){
            newPassword.errorEntry = true
            isValidationFails = true
             self.view.makeToast(ValidationMessages.invalidPassword , duration: 3.0, position: .bottom)
        }
        if newPassword.text!.trimWhiteSpace().isEmpty
        {
            isValidationFails = true
            newPassword.errorEntry = true
            return isValidationFails
        }
        if newPassword.text!.trimWhiteSpace().length < 6
        {
            isValidationFails = true
            self.showMessage(ValidationMessages.passwordLenghthError, type: .error,options: [.textNumberOfLines(0)])
            return isValidationFails
        }
        if newPassword.text! != confirmPassword.text!
        {
            isValidationFails = true
            self.showMessage(ValidationMessages.passwordNotMatch, type: .error,options: [.textNumberOfLines(0)])
            return isValidationFails
        }
        return isValidationFails
    }
    
    //MARK:- IBAction
    @IBAction func changePassWordAction()
    {
        if checkValidation(){ return }
        let userId = HelperFunction.getUserId()
        var stringUrl = ""
        var params :[String: Any] = ["password":newPassword.text!,
                                     "oldPassword":oldPassword.text!]
        if LoginType.customer.rawValue == HelperFunction.getSrtingFromUserDefaults(key: ProfileKeys.profileType)
        {
            stringUrl = ServerUrls.Customer.changePassword
            params["customerId"] = userId
        }
        else {
            stringUrl = ServerUrls.Driver.changePassword
            params["driverId"] = userId
        }
        //
        BaseServices.SendPostJson(viewController: self, serverUrl: stringUrl, jsonToPost: params) { (json) in
            if let json = json
            {
                HelperFunction.saveValueInUserDefaults(key: ProfileKeys.password, value: self.newPassword.text!.trimWhiteSpace())
                let action = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })
                self.showAlertController(heading: AppName, message: json[ServerKeys.message].string!, action: action)
            }
        }
    }
    
}
