//
//  CustomerOrderAPITests.swift
//  EZERTests
//
//  Created by Mac mini on 17/01/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import XCTest
@testable import EZER

class CustomerOrderAPITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

   // Asynchronous test: success fast, failure slow
    
    func testCustomerOrderDetails() {
        // given
        let url = ServerUrls.Customer.getCurrentOrderDetail
        let params :[String:Any] = ["customerId": "5aa2368b4e95d9282c559deb"]
        let successApi = expectation(description: "Status code: 200")
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
    func testCustomerImmediateOrder() {
        // given
        let url = ServerUrls.Customer.checkImmediateOrder
        let params :[String:Any] = [:]
        let successApi = expectation(description: "Status code: 200")
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
    func testCustomerScheduledHour() {
        // given
        let url = ServerUrls.Customer.getScheduledHour
        let params :[String:Any] = [:]
        let successApi = expectation(description: "Status code: 200")
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
    func testCustomerOrderGetCost() {
        // given
        let url = ServerUrls.Customer.getCost
        let params :[String:Any] = ["totalTime": 166, "extraStartLocations": [], "startLocation": ["lat": 30.697826, "lng": 76.708074], "destinationLocation": ["lng": 76.70760177075863, "lat": 30.693914424955263], "extraDestinationLocations": [], "totalDistance": 1026, "shippingItem": ["length": 1, "width": 1, "height": 1, "weight": 5], "driverID": "", "truckType": "SMALL"]
        let successApi = expectation(description: "Status code: 200")
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    func testCheckNearbyDriver() {
        // given
        let url = ServerUrls.Customer.checkNearbyDriver
        let params :[String:Any] = ["customerId": "5aa2368b4e95d9282c559deb", "truckType": "SMALL", "weight": 5, "startLocation": ["type": "Point", "coordinates": [76.708074, 30.697826]]]
        let successApi = expectation(description: "Status code: 200")
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
    func testCustomerOrderDetail() {
        // given
        let url = ServerUrls.Customer.orderDetails
        let params :[String:Any] = ["orderId": "5de0b4b04e95d97c89483553"]
        let successApi = expectation(description: "Status code: 200")
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
    func testCustomerSubmitOrder() {
        // given
        let url = ServerUrls.Customer.submitOrder
        let params :[String:Any] = ["isTestOrder": false, "appVersion": "2.21", "startLocation": ["type": "Point", "coordinates": [76.708074, 30.697826]], "shippingItems": ["height": 1, "width": 1, "images": Optional(["https://ezer-api.s3.us-west-1.amazonaws.com/client/order/5e2bede49ba21.jpeg"]), "length": 1, "weight": 5, "name": "Default Item"], "userNotes": "", "destinationAddress": "NH5, Sector 77, Sector 71, Sahibzada Ajit Singh Nagar, Punjab 140308, India", "appVersionCode": "1", "customerId": "5aa2368b4e95d9282c559deb", "platformName": "ios", "osVersion": "13.3", "estimatedTime": 166, "model": "Simulator iPhone 8", "extraStartLocations": [], "network": "WiFi", "truckType": "SMALL", "estimatedMaxPrice": 48.0, "estimatedMinPrice": 38.0, "startAddress": "3517, Sector 70, Sahibzada Ajit Singh Nagar, Punjab 160071, India", "destinationAddressUnitNumber": "test test", "manufacturer": "Apple", "dateTime": "2020-01-25T09:27:43.449Z", "extraDestinationLocations": [], "destinationLocation": ["coordinates": [76.70760177075863, 30.693914424955263], "type": "Point"], "isMultiOrder": false, "contentDescription": "Test", "coupon": [], "immediate": false, "totalDistance": 1026, "startAddressUnitNumber": "test  test"]
        let successApi = expectation(description: "Status code: 200")
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
}
