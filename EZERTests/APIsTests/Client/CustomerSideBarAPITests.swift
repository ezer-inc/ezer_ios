//
//  CustomerSideBarAPITests.swift
//  EZERTests
//
//  Created by Mac mini on 18/01/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import XCTest
@testable import EZER

class CustomerSideBarAPITests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // Asynchronous test: success fast, failure slow
    func testCustomerGetPaymentMethods() {
        // given
        let url = ServerUrls.Customer.getPaymentMethods
        let params :[String:Any] = ["customerId": "5aa2368b4e95d9282c559deb"]
        let successApi = expectation(description: "Status code: 200")
        let successParsing = expectation(description: "Data parsing successed")
        
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                    if let dataArray = json[ServerKeys.data].array {
                        for data in dataArray{
                            let model = PaymentItem(json : data)
                            if !model.expirationMonth.isEmpty{
                                successParsing.fulfill()
                                return
                            }else{
                                XCTFail("Error: Api Parsing failed")
                            }
                        }
                    }else {
                        XCTFail("Error: Api Parsing failed")
                    }
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi, successParsing], timeout: 20)
    }
    
    func testUpdatePaymentMethod() {
        // given
        let url = ServerUrls.Customer.updatePaymentMethod
        let params :[String:Any] = ["token": "fbkx7pb", "customerId": "5aa2368b4e95d9282c559deb"]
        let successApi = expectation(description: "Status code: 200")
        
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
    func testCustomerGetOrdersList() {
        // given
        let url = ServerUrls.Customer.orderList
        let params :[String:Any] = ["customerId": "5aa2368b4e95d9282c559deb", "offset": 0]
        let successApi = expectation(description: "Status code: 200")
        let successParsing = expectation(description: "Data parsing successed")
        
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                    if let dataArray = json[ServerKeys.data].array {
                        for data in dataArray{
                            let model = OrderHistoryModel(json : data)
                            if !model.orderId.isEmpty{
                                successParsing.fulfill()
                                return
                            }else{
                                XCTFail("Error: Api Parsing failed")
                            }
                        }
                    }else {
                        XCTFail("Error: Api Parsing failed")
                    }
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi, successParsing], timeout: 20)
    }
    
    func testGetCustomerDetail() {
        // given
        let url = ServerUrls.Customer.getCustomerDetails
        let params :[String:Any] = ["customerId": "5aa2368b4e95d9282c559deb"]
        let successApi = expectation(description: "Status code: 200")
        let successParsing = expectation(description: "Data parsing successed")
        
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                    let model = CustomerLoginModel(json: json[ServerKeys.data])
                    if !model._id.isEmpty{
                        successParsing.fulfill()
                    }else {
                        XCTFail("Error: Api Parsing failed")
                    }
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi, successParsing], timeout: 20)
    }
    
    func testUpdateCustomerProfile() {
        // given
        let url = ServerUrls.Customer.updateCustomerDetail
        let params :[String:Any] = ["customerId": "5aa2368b4e95d9282c559deb", "cellPhone": "(981) 234-5789", "firstName": "QA", "email": "k1@tr.com", "lastName": "test"]
        let successApi = expectation(description: "Status code: 200")
        
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
}
