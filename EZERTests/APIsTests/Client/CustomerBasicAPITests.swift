//
//  CustomerBasicAPITests.swift
//  EZERTests
//
//  Created by Mac mini on 16/01/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import XCTest
@testable import EZER

class CustomerBasicAPITests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // Asynchronous test: success fast, failure slow
    
    func testCustomerForgetPassword() {
        // given
        let url = ServerUrls.Customer.forgotPassword
        let params :[String:Any] = ["email": "k1@tr.com"]
        let successApi = expectation(description: "Status code: 200")
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
    func testCustomerLogin() {
        // given
        let url = ServerUrls.Customer.loginUser
        let params :[String:Any] = ["deviceType": "ios", "email": "k1@tr.com", "password": "123456", "deviceToken": ""]
        let successApi = expectation(description: "Status code: 200")
        
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
    func testCustomerLogout() {
        // given
        let url = ServerUrls.Customer.logout
        let params :[String:Any] = ["customerId": "5aa2368b4e95d9282c559deb", "deviceToken": ""]
        let successApi = expectation(description: "Status code: 200")
        
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
}
