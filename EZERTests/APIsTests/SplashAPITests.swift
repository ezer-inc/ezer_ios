//
//  SplashAPITests.swift
//  EZER
//
//  Created by Mac mini on 03/01/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import XCTest
@testable import EZER
class SplashAPITests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // Asynchronous test: success fast, failure slow
    
    func testValidCallTocheckAppVersion() {
        // given
        let url = ServerUrls.Main.getAppOutConfigs
        let params :[String:Any] = [:]
        let successApi = expectation(description: "Status code: 200")
        let successParsing = expectation(description: "Data parsing successed")
        
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                    let appConfig = AppOutConfig(json: json[ServerKeys.data])
                    if appConfig.iosMinAppVersion != 0{
                        successParsing.fulfill()
                    }else {
                        XCTFail("Error: Api Parsing failed")
                    }
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi, successParsing], timeout: 20)
    }
    
    func testValidCallToCheckActiveArea() {
        // given
        let url = ServerUrls.Main.checkActiveArea
        let params :[String:Any] = [
            "lng": 76.708074,
            "lat": 30.697826
        ]
        let successParsing = expectation(description: "Data parsing successed")
        
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                let checkAreaModel = CheckActiveAreaModel(json : json)
                if checkAreaModel.addressAvailable{
                    successParsing.fulfill()
                }else{
                    XCTFail("Error: Api Message - \(checkAreaModel.message)")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successParsing], timeout: 20)
    }
}
