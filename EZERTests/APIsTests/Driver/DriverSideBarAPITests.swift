//
//  DriverSideBarAPITests.swift
//  EZERTests
//
//  Created by Mac mini on 30/01/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import XCTest
@testable import EZER

class DriverSideBarAPITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

   func testDriverJobHistory() {
       // given
       let url = ServerUrls.Driver.getJobHistory
       let params :[String:Any] = ["offset": 0, "driverId": "5aa2387f4e95d9285d1070c2"]
       let successApi = expectation(description: "Status code: 200")
       // when
       BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
           switch result{
           // then
           case.success(let json):
               guard let json = json else {
                   XCTFail("Error: Api data nil")
                   return
               }
               if json[ServerKeys.errorCode].int == 200{
                   successApi.fulfill()
               }else {
                   XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
               }
               
           case .failure(let error):
               XCTFail("Error: \(error.localizedDescription)")
           }
       }
       // wait for timeout
       wait(for: [successApi], timeout: 20)
   }
    
   func testDriverDetail() {
         // given
         let url = ServerUrls.Driver.getDriverDetails
         let params :[String:Any] = ["driverId": "5aa2387f4e95d9285d1070c2"]
         let successApi = expectation(description: "Status code: 200")
         // when
         BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
             switch result{
             // then
             case.success(let json):
                 guard let json = json else {
                     XCTFail("Error: Api data nil")
                     return
                 }
                 if json[ServerKeys.errorCode].int == 200{
                     successApi.fulfill()
                 }else {
                     XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                 }
                 
             case .failure(let error):
                 XCTFail("Error: \(error.localizedDescription)")
             }
         }
         // wait for timeout
         wait(for: [successApi], timeout: 20)
     }
    
     func testUpdateDriverProfile() {
               // given
               let url = ServerUrls.Driver.getDriverDetails
               let params :[String:Any] = ["driverId": "5aa2387f4e95d9285d1070c2", "email": "k3@tr.com", "lastName": "uubsbs", "phoneNumber": "(234) 234-2342", "isTextNotification": true, "profilePic": "", "firstName": "Alexa"]
               let successApi = expectation(description: "Status code: 200")
               // when
               BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
                   switch result{
                   // then
                   case.success(let json):
                       guard let json = json else {
                           XCTFail("Error: Api data nil")
                           return
                       }
                       if json[ServerKeys.errorCode].int == 200{
                           successApi.fulfill()
                       }else {
                           XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                       }
                       
                   case .failure(let error):
                       XCTFail("Error: \(error.localizedDescription)")
                   }
               }
               // wait for timeout
               wait(for: [successApi], timeout: 20)
           }
    
           func testChangePassword() {
                     // given
                     let url = ServerUrls.Driver.changePassword
                     let params :[String:Any] = ["password": "123456", "oldPassword": "123456", "driverId": "5aa2387f4e95d9285d1070c2"]
                     let successApi = expectation(description: "Status code: 200")
                     // when
                     BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
                         switch result{
                         // then
                         case.success(let json):
                             guard let json = json else {
                                 XCTFail("Error: Api data nil")
                                 return
                             }
                             if json[ServerKeys.errorCode].int == 200{
                                 successApi.fulfill()
                             }else {
                                 XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                             }
                             
                         case .failure(let error):
                             XCTFail("Error: \(error.localizedDescription)")
                         }
                     }
                     // wait for timeout
                     wait(for: [successApi], timeout: 20)
                 }
                 
}
