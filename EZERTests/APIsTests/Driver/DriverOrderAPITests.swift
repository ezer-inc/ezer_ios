//
//  DriverOrderAPITests.swift
//  EZERTests
//
//  Created by Mac mini on 30/01/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import XCTest
@testable import EZER

class DriverOrderAPITests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testDriverAccountDetail() {
        // given
        let url = ServerUrls.Driver.getDriverAccountDetail
        let params :[String:Any] = ["driverId": "5aa2387f4e95d9285d1070c2"]
        let successApi = expectation(description: "Status code: 200")
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
    func testCurrentOrderForDriver() {
        // given
        let url = ServerUrls.Driver.getCurrentOrderForDriver
        let params :[String:Any] = ["driverId": "5aa2387f4e95d9285d1070c2"]
        let successApi = expectation(description: "Status code: 200")
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
    func testScheduledJob() {
        // given
        let url = ServerUrls.Driver.getScheduledJob
        let params :[String:Any] = ["driverId": "5aa2387f4e95d9285d1070c2"]
        let successApi = expectation(description: "Status code: 200")
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
    func testDriverOrderStart() {
        // given
        let url = ServerUrls.Driver.driverOrderStart
        let params :[String:Any] = ["dropLocation": ["coordinates": [76.707601770759, 30.693914424955], "type": "Point"], "customerId": "5aa2368b4e95d9282c559deb", "driverId": "5aa2387f4e95d9285d1070c2", "orderId": "5e33f9234e95d961ac5384b3", "pickupLocation": ["type": "Point", "coordinates": [76.708074, 30.697826]]]
        let successApi = expectation(description: "Status code: 200")
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
    
}
