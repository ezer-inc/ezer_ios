//
//  DriverBasicAPITests.swift
//  EZERTests
//
//  Created by Mac mini on 28/01/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import XCTest
@testable import EZER

class DriverBasicAPITests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // Asynchronous test: success fast, failure slow
    
    func testDriverForgetPassword() {
        // given
        let url = ServerUrls.Driver.forgotPassword
        let params :[String:Any] = ["email": "k3@tr.com"]
        let successApi = expectation(description: "Status code: 200")
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
    func testDriverLogin() {
        // given
        let url = ServerUrls.Driver.login
        let params :[String:Any] = ["deviceType": "ios", "email": "k3@tr.com", "password": "123456", "deviceToken": ""]
        let successApi = expectation(description: "Status code: 200")
        
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
    func testDriverLogout() {
        // given
        let url = ServerUrls.Driver.logout
        let params :[String:Any] = ["driverId": "5aa2387f4e95d9285d1070c2", "deviceToken": ""]
        let successApi = expectation(description: "Status code: 200")
        
        // when
        BaseServices.testAPI(serverUrl: url, jsonToPost: params) { (result) in
            switch result{
            // then
            case.success(let json):
                guard let json = json else {
                    XCTFail("Error: Api data nil")
                    return
                }
                if json[ServerKeys.errorCode].int == 200{
                    successApi.fulfill()
                }else {
                    XCTFail("Error: Api Message - \(json[ServerKeys.message].string ?? "")")
                }
                
            case .failure(let error):
                XCTFail("Error: \(error.localizedDescription)")
            }
        }
        // wait for timeout
        wait(for: [successApi], timeout: 20)
    }
    
}
