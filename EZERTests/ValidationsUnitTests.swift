//
//  ValidationsUnitTests.swift
//  EZERTests
//
//  Created by Mac mini on 04/02/20.
//  Copyright © 2020 TimerackMac1. All rights reserved.
//

import XCTest
@testable import EZER

class ValidationsUnitTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testEmailValidations() {
        // 1. given
        let validEmail = "test@test.com"
        let invalidEmail = "TestWrong"
        
        // 2. when
        let validTest = HelperFunction.validateEmail(email: validEmail)
        let invalidTest = HelperFunction.validateEmail(email: invalidEmail)
        
        // 3. then
        //Positive Test Case
        XCTAssert(validTest, "Email validation computed from expected is wrong")
        
        //Negative Test Case
        XCTAssert(!invalidTest, "Email validation computed from expected is wrong")
    }
    
    func testNameValidations() {
        // 1. given
        let validName = "test right"
        let invalidName = "Test 1Wrong"
        
        // 2. when
        let validTest = HelperFunction.validateName(name: validName)
        let invalidTest = HelperFunction.validateName(name: invalidName)
        
        // 3. then
        //Positive Test Case
        XCTAssert(validTest, "Name validation computed from expected is wrong")
        
        //Negative Test Case
        XCTAssert(!invalidTest, "Name validation computed from expected is wrong")
    }
    
    func testZipCodeValidations() {
        // 1. given
        let validZip = "12345"
        let invalidZip = "TestWrong"
        let invalidZip1 = "Wrng1"
        
        // 2. when
        let validTest = HelperFunction.isValidZip(testStr: validZip)
        let invalidTest = HelperFunction.isValidZip(testStr: invalidZip)
        let invalidTest1 = HelperFunction.isValidZip(testStr: invalidZip1)
        
        // 3. then
        //Positive Test Case
        XCTAssert(validTest, "Zip Code validation computed from expected is wrong")
        
        //Negative Test Case
        XCTAssert(!invalidTest, "Zip Code validation computed from expected is wrong")
        XCTAssert(!invalidTest1, "Zip Code validation computed from expected is wrong")
    }
    
    func testPhoneNumberValidations() {
        // 1. given
        let validNumber = "(987) 654-3210"
        let invalidNumber = "TestWrong"
        let invalidNumber1 = "987654=2#1"
        
        // 2. when
        let validTest = HelperFunction.isValidNumber(testStr: validNumber)
        let invalidTest = HelperFunction.isValidNumber(testStr: invalidNumber)
        let invalidTest1 = HelperFunction.isValidNumber(testStr: invalidNumber1)
        
        // 3. then
        //Positive Test Case
        XCTAssert(validTest, "Phone Number validation computed from expected is wrong")
        
        //Negative Test Case
        XCTAssert(!invalidTest, "Phone Number validation computed from expected is wrong")
        XCTAssert(!invalidTest1, "Phone Number validation computed from expected is wrong")
    }
}
